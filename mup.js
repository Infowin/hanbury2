module.exports = {
  servers: {
    one: {
      host: "ec2-52-193-175-88.ap-northeast-1.compute.amazonaws.com",
      username: "ubuntu",
      pem: "/Users/michaelchen/Dropbox/_code/lb0505.pem"
      // pem:
      // password:
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'hanbury2',
    path: '/Users/michaelchen/Dropbox/_code/hanbury2',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://ec2-52-193-175-88.ap-northeast-1.compute.amazonaws.com:80',
      MONGO_URL: 'mongodb://localhost/meteor'
    },
    dockerImage: "abernix/meteord:base",

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};