Package.describe({
  summary: "PDFKit, the PDF generation library",
  version: "1.0.5",
  git: "https://github.com/pascoual/meteor-pdfkit"
});

Npm.depends({
 'pdfkit-cjk': "0.0.4",
  'fibers': "1.0.15",

});

Package.on_use(function (api) {
  api.versionsFrom("METEOR@0.9.0");
  api.use(['underscore'], 'server');
  api.export('PDFDocument');
  // api.export('PDFMerge');
  api.add_files(['pdfkitWrapper.js'], 'server');
});
