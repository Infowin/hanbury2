import { Email } from 'meteor/email'
import { check } from 'meteor/check'
import { isMoment } from 'moment';

var world = function() {
    var now = new Date();

    // console.log('Check Send Mail Box! ' + now);

    var e = Emails.find({sent_status_id: "2", sent_time:{$lte: now}}).fetch();
    if(e.length){
        var from = "notice@hanburyifa.com";
        for(var i=0; i<e.length; i++){
        	var entry = e[i];

            // if(!entry.receiver || typeof entry.receiver == "undefined"){
            //     continue;
            // }

            // console.log("entry");
            // console.log(entry);

        	var id = entry._id;
          	delete entry._id;

            var to = entry.receiver;
            var cc = entry.cc;
            var bcc = entry.bcc;
            var subject = entry.subject;
            var html = entry.context;

            // console.log(to);
            // console.log(subject);

            var attachments = entry.attachments;
            // this.unblock();

            var obj = {};
            obj = {
              to: to,
              cc: cc,
              bcc: bcc,
              from: from,
              subject: subject,
              html: html,
              attachments: attachments
            };

            Email.send(obj);
            entry.sent_status_id = "1";
            Emails.update(id, {$set: entry });
        }


        // console.log("排程寄信：");
        // console.log(e);
    }
}

var insBirDay = function() {
    console.log('My Birth Day!');
    function replaceDynamicText(mail, context) {
        var name_cht = "";

        if (context.indexOf("[name_cht]") != -1) {
            if (Clients.find({ email: mail }).count()) {
                name_cht = Clients.findOne({ email: mail }).name_cht;
                // console.log("replaceDynamicText: "+ mail + " name: "+name_cht);
            }
        }
        // return context;
        return context.replace(/\[name_cht]/g, name_cht);
    }

    var nowMonth = moment().month();

    var arr = Clients.find({ periodmail7: "1" }).fetch(); // 客戶有點生日賀卡Ｙ
    var mailTitle = MailTitle.findOne({ _id: 'mG3Tvenbe6HrSZttp'});
    var html_header = '<img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/rY66GsLbHnTm9Tvrx-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Header.png" />';
    var html_content = '<div style="padding:10px;">' + mailTitle.context + '</div>';
    var html_footer = '<BR><img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/yKNhmTQoRepiYtRSj-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Footer.png" />';
    var html = "";
    // html = html_header + replaceDynamicText(item.email, html_content) + html_footer;

    arr.forEach(function (item) {
        var obj ={}
        if (moment(item.birthday).month() == nowMonth + 1) {
            obj={
                "uid": "M" + funcStrPad(getNextSequence('mailbox'), 7),
                "subject": mailTitle.view_title,
                "receiver": item.email,
                "sent_status_id": "2",
                "cc": "",
                "bcc": "aaron.chen@hanburyifa.com",
                "context": html_header + replaceDynamicText(item.email, html_content) + html_footer,
                "sent_time": moment().format('YYYY') + '-' + moment(item.birthday).subtract({ second: 1 }).format('MM-DD HH:mm'),
                // sender_id: currentUserId,
                // sender_name: Users.find({_id: currentUserId}).engName,
                // attachments
            }
            Emails.insert(obj);
        }
    })
}

var cron = new Meteor.Cron({
    events: {
        "* * * * *": world,
        "0 0 25 * *": insBirDay
    }
});
