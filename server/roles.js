// https://github.com/alanning/meteor-roles
// http://stackoverflow.com/questions/27585020/meteorjs-useraccountscore-and-meteor-roles
/*

1. 管理者 	admin
2. 佣金管理者 commission
3. 總監 		director
4. 行政主管 	supervisor
5. 一般行政 	administrative
//6. agentleader 這個用勾的
7. agent 這個用勾的
8. guest
    { id:"1", role:"guest",           value:"訪客"},
    { id:"10", role:"agent",          value:"Agent"},
    { id:"20", role:"agentleader",    value:"Agent Leader"},
    { id:"30", role:"commission",     value:"一般行政"},
    { id:"40", role:"director1",      value:"行政主管"},
    { id:"41", role:"director2",      value:"財務主管"},
    { id:"42", role:"director4",      value:"財務行政主管"},
    { id:"50", role:"supervisor",     value:"總監"},
    { id:"90", role:"administrative", value:"佣金管理者"},
    { id:"95", role:"designer",       value:"系統開發者"},
    { id:"99", role:"admin",          value:"管理者"}
 */

Meteor.startup(function() {
	// console.log("Roles here: ");
  // if(Meteor.users.find().count() < 1) {
  if(0){
    var users = [{
      username: 'michael',
      name: "陳胤辰",
      email: "yinchen618@gmail.com",
      roles: ['agent', 'agentleader', 'commission', 'director', 'supervisor', 'administrative', 'admin']
    }, {
      username: 'ivan',
      name: "Ivan",
      email: "test@gmail.com",
      roles: ['agent']
    }];
    _.each(users, function(userData) {
    	console.log("Create user: " + userData.name);
      var userid = Accounts.createUser({
        username: userData.username,
        password: "111",
        email: userData.email,
        profile: {
          name: userData.name
        }
      });
      Meteor.users.update({
        _id: userid
      }, {
        $set: {
          'emails.0.verified': true // 不用驗證mail
        }
      });
      Roles.addUsersToRoles(userid, userData.roles);
    })
  }
});
// server
Meteor.methods({
  createUsers: function(email, password, roles, name) {
  	// console.log("createUsers: "+email);
  	// console.log(roles);
    if(!roles || roles.length < 1){
      roles = ['guest'];
    }
    var users = [{
      name: name,
      email: email,
      roles: [roles]
    }];
    _.each(users, function(user) {
      var id;
      id = Accounts.createUser({
        email: user.email,
        password: password,
        profile: {
          name: user.name
        }
      });
      if(user.roles.length > 0) {
        Roles.addUsersToRoles(id, user.roles);
      }
    });
  },
  /**
   * update a user's permissions
   *
   * @param {Object} targetUserId Id of user to update
   * @param {Array} roles User's new permissions
   * @param {String} group Company to update permissions for
   */
  updateRoles: function(targetUserId, roles, group) {
    var loggedInUser = Meteor.user()
    if(!loggedInUser ||
      !Roles.userIsInRole(loggedInUser, ['admin'], group)) {
      throw new Meteor.Error(403, "Access denied")
    }
    Roles.setUserRoles(targetUserId, roles, group)
  },
  deleteUser: function(id) { ///Some Delete Method (ignore if dont needed)
    return Meteor.users.remove(id);
  },
});
