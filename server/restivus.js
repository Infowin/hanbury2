// import {PDFDocument} from "meteor/my-meteor-pdfkit";

// Global API configuration
Api = new Restivus({
  // auth: {
  //   token: 'services.resume.loginTokens.hashedToken', //'auth.apiKey',
  //   user: function () {
  //     console.log(this.request.headers);
  //     return {
  //       userId: this.request.headers['x-auth-token'],
  //       token: this.request.headers['x-user-id']
  //     };
  //   }
  // },
  useDefaultAuth: true,
  prettyJson: true
});


// Generates: POST on /api/users and GET, DELETE /api/users/:id for
// Meteor.users collection
Api.addCollection(Meteor.users, {
  excludedEndpoints: ['getAll', 'put'],
  routeOptions: {
    authRequired: true
  },
  endpoints: {
    post: {
      authRequired: false
    },
    delete: {
      roleRequired: 'admin'
    }
  }
});

Api.getSortParams = function (arr1, arr2) {
  for (var i in arr1) {
    if (!arr1[i]) {
      delete arr1[i];
    } else if (i == "sortField") {
      arr2[arr1.sortField] = (arr1["sortOrder"] == "asc") ? 1 : -1;

      delete arr1[i];
      delete arr1["sortOrder"];
    }
  }
}
Api.getPageParams = function (arr1, arr2) {
  if (!!arr1.pageIndex && !!arr1.pageSize) {
    var index = arr1.pageIndex - 1 || 0;

    arr2.skip = arr1.pageSize * index;
    arr2.limit = Number(arr1.pageSize) || 5;

    delete arr1.pageIndex;
    delete arr1.pageSize;
  }
}
Api.mydate = function (str) {
  var dd = new Date(str);
  return dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate() + " " + dd.getHours() + ':' + dd.getMinutes() + ':' + dd.getSeconds();
}
Api.nowdate = function () {
  var dd = new Date();
  return dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate() + " " + dd.getHours() + ':' + dd.getMinutes() + ':' + dd.getSeconds();
}
Api.strChtEngMix = function (doc, str, x, y, fontsize) {
  doc.fontSize(fontsize);
  var top_shift = Math.ceil(fontsize / 10);
  var now_x = x;
  var now_y = y;
  for (var i = 0; i < str.length; i++) {
    var nowchar = str.charAt(i);

    if (nowchar.Blength() == 1) { // 如果是英文或數字
      if (nowchar == "\n") {
        now_y = doc.y + fontsize;
        now_x = x;
      } else {
        doc.font('Helvetica').text(nowchar, now_x, now_y + top_shift);
        now_x = doc.x;
      }
    } else { // 如果是中文
      doc.font('msjh').text(nowchar, now_x, now_y - 4);

      if (doc.x - now_x != fontsize) {
        now_x = doc.x + fontsize;
      } else {
        now_x = doc.x;
      }
    }
  }
  // now_x = doc.x;
  // doc.y = now_y;
}

Api.addRoute('xeditclient', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Clients update PUT");
      console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params.pk;
      delete params._id;

      var obj = {};
      obj[params.name] = params.value;

      if (params.name == "agent_id") {
        obj.agent_text = Agents.findOne({
          _id: params.value
        }).name;
        console.log(obj);
      }

      if (Clients.update(id, {
          $set: obj
        })) {
        params._id = id;
        return "1";
        // return {status: 'success', data: {message: 'Clients updated'}};
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});
Api.addRoute('xeditportfolioarr', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Portfolios update PUT");
      // console.log(this.bodyParams);
      /*
      {                      name: 'wdba_holdernum',
  I20160914-03:45:59.013(8)?   value: 'hhh',
  I20160914-03:45:59.013(8)?   pk: 'MFkG6fzvgivfL4jju',
  I20160914-03:45:59.014(8)?   field: 'arrWithdrawBankAccount',
  I20160914-03:45:59.014(8)?   order_id: '1' }
       */
      // return "1";

      var params = this.bodyParams;
      var id = params.pk;
      delete params._id;

      var obj = Portfolios.findOne({
        _id: id
      });

      console.log("xeditportfolioarr");
      console.log(params);
      // console.log(obj[params.field]);
      for (var i in obj[params.field]) {
        if (obj[params.field][i].order_id == Number(params.order_id)) {
          obj[params.field][i][params.name] = params.value;
          break;
        }
      }
      // console.log(obj[params.field]);

      if (Portfolios.update(id, {
          $set: obj
        })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Clients updated'}};
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest file DELETE");
      // console.log(this.queryParams);

      var params = this.queryParams;
      var id = params.pk;
      var obj = Portfolios.findOne({
        _id: id
      });

      // console.log(obj[params.field]);
      obj[params.field] = obj[params.field].filter(function (el) {
        // console.log("el.order_id");
        // console.log(el.order_id);
        // console.log("params.order_id");
        // console.log(params.order_id);
        return el.order_id != Number(params.order_id);
      });

      for (var i in obj[params.field]) {
        obj[params.field][i].order_id = Number(i) + 1;
      }
      // console.log(obj[params.field]);

      if (Portfolios.update(id, {
          $set: obj
        })) {
        return {
          status: 'success',
          data: params
        };
      }

      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});
Api.addRoute('xeditportfolioarr2', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Portfolios update PUT");
      // console.log(this.bodyParams);
      /*
I20161216-16:20:26.030(8)? xeditportfolioarr2
I20161216-16:20:26.031(8)? { name: 'amount_dispensed',
I20161216-16:20:26.031(8)?   value: '678',
I20161216-16:20:26.032(8)?   pk: 'E3NkZw968FJkcwxs6',
I20161216-16:20:26.032(8)?   field: 'arrGiveInterestRecord',
I20161216-16:20:26.032(8)?   order_id: '1',
I20161216-16:20:26.033(8)?   order_id2: '1' }
       */
      // return "1";

      var params = this.bodyParams;
      var id = params.pk;
      delete params._id;

      var obj = Portfolios.findOne({
        _id: id
      });

      // console.log("xeditportfolioarr2");
      // console.log(params);
      // console.log(obj[params.field]);

      var isModify = 0;
      for (var i in obj[params.field]) {
        if (obj[params.field][i].order_id == Number(params.order_id) && obj[params.field][i].order_id2 == Number(params.order_id2)) {

          obj[params.field][i][params.name] = params.value;
          isModify = 1;
          break;
        }
      }

      if (isModify == 0) {
        var tobj = {};
        tobj.order_id = Number(params.order_id);
        tobj.order_id2 = Number(params.order_id2);
        tobj[params.name] = params.value;

        if (!obj[params.field]) {
          obj[params.field] = []
        }

        obj[params.field].push(tobj);
      }
      // console.log(obj[params.field]);

      if (Portfolios.update(id, {
          $set: obj
        })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Clients updated'}};
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest file DELETE");
      // console.log(this.queryParams);

      var params = this.queryParams;
      var id = params.pk;
      var obj = Portfolios.findOne({
        _id: id
      });

      // console.log(obj[params.field]);
      obj[params.field] = obj[params.field].filter(function (el) {
        // console.log("el.order_id");
        // console.log(el.order_id);
        // console.log("params.order_id");
        // console.log(params.order_id);
        return el.order_id != Number(params.order_id);
      });

      for (var i in obj[params.field]) {
        obj[params.field][i].order_id = Number(i) + 1;
      }
      // console.log(obj[params.field]);

      if (Portfolios.update(id, {
          $set: obj
        })) {
        return {
          status: 'success',
          data: params
        };
      }

      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});
Api.addRoute('xeditportfolio', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Portfolios update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params.pk;
      delete params._id;

      var obj = {};
      obj[params.name] = params.value;

      if (Portfolios.update(id, {
          $set: obj
        })) {
        params._id = id;
        // return ;
        return {
          status: 'success',
          data: params
        };
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});

Api.addRoute('xeditaccountyear', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Portfolios update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params.pk;
      delete params._id;

      var obj = {};
      obj[params.name] = params.value;

      if (Accountyear.update(id, {
          $set: obj
        })) {
        params._id = id;
        // return ;
        return {
          status: 'success',
          data: params
        };
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});
Api.addRoute('xedituser', {
  authRequired: false
}, {
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      // console.log("Rest Portfolios update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      console.log(params);
      var id = params.pk;
      delete params._id;

      var profile = Meteor.users.findOne(id).profile;

      profile[params.name] = params.value;
      // var key = "profile."+params.name;
      // console.log(profile);
      // console.log(key);

      /*    {
              "username": params.username,
              // "emails[0].address": params.email,
              "profile.name": params.name,
              "profile.name2": params.name2,
              "profile.team": params.team,
              "profile.cms1": params.cms1,
              "profile.cms2": params.cms2,
              "profile.cms3": params.cms3,
              "profile.roles": params.roles,
          }*/

      if (!!profile.aan) {
        if (!Agents.findOne({
            "user_id": id
          })) {
          console.log("insert ann")
          Agents.insert({
            user_id: id,
            // agent_id: Agents.find().count() + 1,
            agent_id: Meteor.users.findOne(id).order_id,
            name: profile.engname
          });
        }
      } else {
        if (!!Agents.findOne({
            "user_id": id
          })) {
          console.log("remove ann")
          Agents.remove({
            "user_id": id
          });
        }
      }

      //Meteor.users.update(id, {$set: {"profile.someNewField": newData}});
      var r = Meteor.users.update(id, {
        $set: {
          "profile": profile
        }
      });
      // var r = Meteor.users.update({"_id": id}, {$set: {key: params.value}});
      // console.log(r);

      if (r) {
        params._id = id;
        return "1";
        // return {status: 'success', data: {message: 'Clients updated'}};
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});
Api.addRoute('birthday_yyyymmdd', {
  authRequired: false
}, {
  get: function () {
    Clients.find().forEach(function (u) {
      if (!!u.birthday) {
        let arr = u.birthday.split("/");
        let yyyy = arr[0];
        let mm = arr[1];
        let dd = arr[2];
        Clients.update(u, {
          $set: {
            "birthday_yyyy": Number(yyyy),
            "birthday_mm": Number(mm),
            "birthday_dd": Number(dd)
          }
        });
        console.log("client " + u.name_cht + " birthday: " + u.birthday + " to: " + yyyy + " " + mm + " " + dd);
      }
    });
  },
});

Api.addRoute('uploads', {
  authRequired: false
}, {
  get: function () {
    // console.log("Rest uploads GET");
    var queryVar, sortVar = {},
      pageVar = {};

    queryVar = this.queryParams || {};
    Api.getSortParams(queryVar, sortVar);
    Api.getPageParams(queryVar, pageVar);
    console.log("queryVar: ");
    console.log(queryVar);
    // console.log("sortVar: ");
    // console.log(sortVar);
    // console.log("pageVar: ");
    // console.log(pageVar);

    if (!!queryVar.findData) {
      var searchValue = queryVar.findData;
      delete queryVar.findData;

      // console.log(searchValue);
      queryVar = {
        $and: [{
            $or: [
              // {name_cht: {$regex : ".*"+searchValue+".*", $options: '-i'}},
              {
                receiver_name: {
                  $regex: searchValue,
                  $options: 'i'
                }
              },
              {
                name: {
                  $regex: searchValue,
                  $options: 'i'
                }
              },
            ]
          },
          queryVar,
        ]
      };
    } else {
      delete queryVar.findData;
    }


    var p = Uploads.find(queryVar, {
      sort: sortVar,
      skip: pageVar.skip,
      limit: pageVar.limit
    }).fetch();
    for (var i = 0; i < p.length; i++) {
      p[i].myItemIndex = i;
      if (!p[i].createdByName) {
        p[i].createdByName = p[i].insertedByName;
      }
    }
    return {
      data: p,
      itemsCount: Uploads.find(queryVar).count()
    }
  },
  post: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest uploads insert POST");
      // console.log(this.bodyParams);
      console.log("Uploads post:");

      var params = this.bodyParams;

      params.createdByName = params.insertedByName;
      // console.log(params);

      var id = Uploads.insert(params);
      if (id) {
        return {
          insert: 'success',
          data: Uploads.findOne(id)
        };
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
  put: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest file update PUT");
      // console.log(this.bodyParams);
      var params = this.bodyParams;
      var id = params._id;
      delete params._id;
      // console.log(params);
      if (Uploads.update(id, {
          $set: params
        })) {
        params._id = id;
        return params;
        // return {status: 'success', data: {message: 'Uploads updated'}};
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
  delete: {
    // roleRequired: ['author', 'admin'],
    action: function () {
      console.log("Rest file DELETE");
      // console.log(this.bodyParams);

      // Uploads.findOne({_id: this.bodyParams._id}).file.getFileRecord().remove();

      if (Uploads.remove(this.bodyParams._id)) {
        return {
          status: 'success',
          data: {
            message: 'removed'
          }
        };
      }
      return {
        statusCode: 404,
        body: {
          status: 'fail',
          message: 'Article not found'
        }
      };
    }
  },
});

Api.chgSortObj = function (arr) {
  for (var i in arr) {
    if (arr[i] == "asc") arr[i] = 1;
    else if (arr[i] == "desc") arr[i] = -1;
  }
  return arr;
}

Api.dateTW = function (doc, x, y, fontsize, datetime) {
  var top_padding = Math.floor(fontsize / 10) * 2;

  doc.fontSize(fontsize).font('BiauKai').text("中華民國", x, y, {
      lineBreak: false,
      continued: "yes"
    })
    .font('Times-Roman').text("  " + datetime.yyy + "  ", doc.x, doc.y + top_padding, {
      lineBreak: false,
      continued: "yes"
    }).font('BiauKai').text("年", doc.x, doc.y - top_padding, {
      lineBreak: false,
      continued: "yes"
    })
    .font('Times-Roman').text("  " + datetime.mm + "  ", doc.x, doc.y + top_padding, {
      lineBreak: false,
      continued: "yes"
    }).font('BiauKai').text("月", doc.x, doc.y - top_padding, {
      lineBreak: false,
      continued: "yes"
    })
    .font('Times-Roman').text("  " + datetime.dd + "  ", doc.x, doc.y + top_padding, {
      lineBreak: false,
      continued: "yes"
    }).font('BiauKai').text("日", doc.x, doc.y - top_padding);
}

/*Api.strChtEngMix = function(doc, str, x, y, fontsize){
  doc.fontSize(fontsize);
  var top_shift = Math.ceil(fontsize/10);
  var now_x = x;
  var now_y = y;
  for(var i=0; i< str.length; i++){
    var nowchar = str.charAt(i);

    if(nowchar.Blength() == 1){// 如果是英文或數字
      if(nowchar == "\n"){
        now_y = doc.y+fontsize;
        now_x = x;
      }
      else{
        doc.font('Times-Roman').text(nowchar, now_x, now_y+top_shift);
        now_x = doc.x;
      }
    }
    else{ // 如果是中文
      doc.font('BiauKai').text(nowchar, now_x, now_y);

      if(doc.x-now_x != fontsize){
        now_x = doc.x+fontsize;
      }
      else{
        now_x = doc.x;
      }
    }
  }
  // now_x = doc.x;
  // doc.y = now_y;
}*/

Api.returnPDF = function (doc) {
  if (typeof doc == "undefined") {
    doc = new PDFDocument({
      size: "A4",
      margin: 1
    });
    doc.registerFont('msjh', msjhTTF);

    doc.font('msjh');
    doc.fontSize(30).text("無資料", 100, 100);
  }

  return {
    headers: {
      'Content-Type': 'application/pdf'
    },
    body: doc.outputSync()
  };
}
Api.returnHTML = function (content) {
  if (!content) {
    return {
      headers: {
        'Content-Type': 'text/html; charset=utf-8'
      },
      body: "無資料"
    };
  } else {
    return {
      headers: {
        'Content-Type': 'text/html; charset=utf-8'
      },
      body: content
    };
  }
}
Api.pdfLayout = function (doc) {
  var x3 = 150,
    y3 = 705;
  // var fontsize = doc.fontSize();

  doc.image(pngB1, 22, 35, {
    width: 548
  }); //, height: 200
  doc.image(pngH1, 80, 208, {
    width: 450
  }); //, height: 200
  doc.image(pngH2, 40, 680, {
    width: 100
  }); //, height: 200

  doc.fontSize(7);
  doc.font('Helvetica-Bold');
  doc.text("Hanbury IFA Corporation Limited", x3, y3);
  doc.font('Helvetica');
  doc.text("Hong Kong: Room 1502, 15/F, Wing On House, 71 Des Voeux Road, Centra", x3, y3 + 10, {
    width: 180,
    align: 'left'
  });

  doc.text("Taipei: 5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taiwan", x3 + 190, y3 + 10, {
    width: 200,
    align: 'left'
  });
  doc.text("Tel: +886 2 2758 2239", x3 + 190, y3 + 20, {
    width: 180,
    align: 'left'
  });
  doc.text("Email: ", x3 + 190, y3 + 30, {
    width: 160,
    align: 'left'
  });

  doc.fontSize(7);
  var width = doc.widthOfString('www.hanburyifa.com');
  var height = doc.currentLineHeight();
  doc.fillColor('blue').underline(20, 0, width, height, {
    color: 'blue'
  }).text('www.hanburyifa.com', x3, y3 + 30, {
    link: 'http://www.hanburyifa.com',
    underline: true
  });

  var width = doc.widthOfString('info@hanburyifa.com');
  var height = doc.currentLineHeight();
  doc.fillColor('blue').underline(20, 0, width, height, {
    color: 'blue'
  }).text('info@hanburyifa.com', x3 + 190 + 22, y3 + 30, {
    link: 'mailto:info@hanburyifa.com',
    underline: true
  });

  doc.fontSize(7);

  // doc.font('Helvetica-Bold');
  doc.font('Helvetica');
  doc.fillColor('black');
  doc.text("This message, along with any attachments, may be confidential or legally privileged. It is intended only for the named person(s), who is/are the only authorized recipient(s). If this message has reached you in error, please delete it without review and notify the sender immediately.", 55, 770, {
    width: 490,
    align: 'left'
  });

  // doc.fontSize(fontsize);

}

/*Api.addRoute('pdfafterserviceQQQ', {authRequired: false}, {
  get: function () {

    var queryVar, sortVar = {}, pageVar = {};
    queryVar = this.queryParams || {};

    var pf = queryVar;

    var doc = new PDFDocument({size: 'A4', margin: 1});
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');

    doc.image(as1_1, 0, 0, {width: a4pageWidth});
    doc.addPage();
    doc.image(as1_2, 0, 0, {width: a4pageWidth});

    doc.text(pf.account_num, 150, 97, { width: 380, align: 'left'} );
    // doc.font('BiauKai');
    doc.text(pf.name_eng, 150, 146, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng || "(empty)", 150, 170, { width: 480, align: 'left'} );

    doc.addPage();
    doc.image(as1_3, 0, 0, {width: a4pageWidth});

    doc.text(pf.account_num, 150, 97, { width: 380, align: 'left'} );
    // doc.font('BiauKai');
    doc.text(pf.name_eng, 150, 146, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng || "(empty)", 150, 170, { width: 480, align: 'left'} );

    doc.addPage();
    doc.image(as1_4, 0, 0, {width: a4pageWidth});


    return Api.returnPDF(doc);
  }
});
*/
Api.strChtEngMix = function (doc, str, x, y, fontsize) {
  doc.fontSize(fontsize);
  var top_shift = Math.ceil(fontsize / 3.5);
  var now_x = x;
  var now_y = y;
  for (var i = 0; i < str.length; i++) {
    var nowchar = str.charAt(i);

    if (nowchar.Blength() == 1) { // 如果是英文或數字
      if (nowchar == "\n") {
        now_y = doc.y + fontsize;
        now_x = x;
      } else {
        doc.font('Courier').text(nowchar, now_x, now_y + top_shift);
        now_x = doc.x;
      }
    } else { // 如果是中文
      doc.font('msjh').text(nowchar, now_x, now_y);

      if (doc.x - now_x != fontsize) {
        now_x = doc.x + fontsize;
      } else {
        now_x = doc.x;
      }
    }
  }
}
Api.addRoute('pdfafterservice1', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.image(wd_1, 0, 0, {
      width: a4pageWidth
    });
    doc.font('Courier');
    // doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
    // doc.font('BiauKai');
    // doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
    // doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );
    doc.fontSize(12);
    doc.text(pf.name_eng, 190, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 400, {
      width: 340,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 190, 400, 12);
    // doc.font('Courier');
    // doc.text(decodeURI(pf.addr1_eng), 190, 400, { width: 380, align: 'left'} );
    doc.text("v", 218, 480, {
      width: 380,
      align: 'left'
    });
    Api.strChtEngMix(doc, decodeURI(pf.insertedBy), 190, 565, 12);
    // doc.text(pf.insertedBy, 190, 565, { width: 380, align: 'left'} );
    doc.text(pf.account_num, 190, 597, {
      width: 380,
      align: 'left'
    });

    if (pf.email.length > 20) {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left'
      });
    } else {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left',
        characterSpacing: 11.2
      });
    };

    doc.text(pf.contactnum, 190, 657, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 190, 717, {
      width: 380,
      align: 'left',
      characterSpacing: 11.2
    });

    doc.addPage();
    doc.image(wd_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.withdrawlamount, 420, 225, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 190, 297, { width: 380, align: 'left'} );
    /*doc.text("v", 250, 297, { width: 380, align: 'left'} );
    doc.text("v", 325, 297, { width: 380, align: 'left'} );
    doc.text("v", 402, 297, { width: 380, align: 'left'} );
    doc.text("v", 480, 297, { width: 380, align: 'left'} );*/
    doc.text(pf.with_reason, 194, 637, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_3, 0, 0, {
      width: a4pageWidth
    });
    /*doc.text("v", 186, 83, { width: 380, align: 'left'} );*/
    // doc.text("v", 186, 128, { width: 380, align: 'left'} );
    doc.text(pf.wdba_bankname, 186, 192, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_bankaddr, 186, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 186, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_holdernum, 186, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_swift_bic, 186, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.add_imfo, 70, 530, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_8, 0, 0, {
      width: a4pageWidth
    });

    /*
        doc.addPage();
        doc.image(as1_3, 0, 0, {width: a4pageWidth});

        doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
        // doc.font('BiauKai');
        doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
        doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );



        doc.addPage();
        doc.image(as1_4, 0, 0, {width: a4pageWidth});

    */
    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice2', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.image(wd_1, 0, 0, {
      width: a4pageWidth
    });
    doc.font('Courier');
    // doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
    // doc.font('BiauKai');
    // doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
    // doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );
    doc.fontSize(12);
    doc.text(pf.name_eng, 190, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 400, {
      width: 340,
      align: 'left'
    });
    doc.font('Courier');
    // doc.text(decodeURI(pf.addr1_eng), 190, 400, { width: 380, align: 'left'} );
    doc.text("v", 218, 485, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.insertedBy, 190, 565, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, decodeURI(pf.insertedBy), 190, 565, 12);
    doc.text(pf.account_num, 190, 597, {
      width: 380,
      align: 'left'
    });

    if (pf.email.length > 20) {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left'
      });
    } else {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left',
        characterSpacing: 11.2
      });
    };

    doc.text(pf.contactnum, 190, 657, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 190, 717, {
      width: 380,
      align: 'left',
      characterSpacing: 11.2
    });

    doc.addPage();
    doc.image(wd_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.withdrawlamount, 420, 225, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 190, 297, { width: 380, align: 'left'} );
    // doc.text(pf.with_reason, 194, 637, { width: 380, align: 'left'} );
    doc.text("As the policy reach its maturity,", 194, 637, {
      width: 380,
      align: 'left'
    });
    doc.text("I wish to withdraw all my money for my retirement fund.", 194, 647, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_3, 0, 0, {
      width: a4pageWidth
    });
    /*doc.text("v", 186, 83, { width: 380, align: 'left'} );*/
    // doc.text("v", 186, 128, { width: 380, align: 'left'} );
    doc.text(pf.wdba_bankname, 186, 192, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_bankaddr, 186, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 186, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_holdernum, 186, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_swift_bic, 186, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.add_imfo, 70, 530, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_8, 0, 0, {
      width: a4pageWidth
    });

    /*
        doc.addPage();
        doc.image(as1_3, 0, 0, {width: a4pageWidth});

        doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
        // doc.font('BiauKai');
        doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
        doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );



        doc.addPage();
        doc.image(as1_4, 0, 0, {width: a4pageWidth});

    */
    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice02', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.image(wd_1, 0, 0, {
      width: a4pageWidth
    });
    doc.font('Courier');
    // doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
    // doc.font('BiauKai');
    // doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
    // doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );
    doc.fontSize(12);
    doc.text(pf.name_eng, 190, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 400, {
      width: 340,
      align: 'left'
    });
    doc.font('Courier');
    // doc.text(decodeURI(pf.addr1_eng), 190, 400, { width: 380, align: 'left'} );
    doc.text("v", 218, 485, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.insertedBy, 190, 565, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, decodeURI(pf.insertedBy), 190, 565, 12);
    doc.text(pf.account_num, 190, 597, {
      width: 380,
      align: 'left'
    });

    if (pf.email.length > 20) {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left'
      });
    } else {
      doc.text(pf.email, 190, 627, {
        width: 380,
        align: 'left',
        characterSpacing: 11.2
      });
    };

    doc.text(pf.contactnum, 190, 657, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 190, 717, {
      width: 380,
      align: 'left',
      characterSpacing: 11.2
    });

    doc.addPage();
    doc.image(wd_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.withdrawlamount, 420, 225, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 190, 297, { width: 380, align: 'left'} );
    /*doc.text("v", 250, 297, { width: 380, align: 'left'} );
    doc.text("v", 325, 297, { width: 380, align: 'left'} );
    doc.text("v", 402, 297, { width: 380, align: 'left'} );
    doc.text("v", 480, 297, { width: 380, align: 'left'} );*/
    doc.text(pf.with_reason, 194, 637, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_3, 0, 0, {
      width: a4pageWidth
    });
    /*doc.text("v", 186, 83, { width: 380, align: 'left'} );*/
    // doc.text("v", 186, 128, { width: 380, align: 'left'} );
    doc.text(pf.wdba_bankname, 186, 192, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_bankaddr, 186, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 186, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_holdernum, 186, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.wdba_swift_bic, 186, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.add_imfo, 70, 530, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(wd_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(wd_8, 0, 0, {
      width: a4pageWidth
    });

    /*
        doc.addPage();
        doc.image(as1_3, 0, 0, {width: a4pageWidth});

        doc.text("Q1234321aaa", 150, 97, { width: 380, align: 'left'} );
        // doc.font('BiauKai');
        doc.text("YU, MEI HUA", 150, 146, { width: 380, align: 'left'} );
        doc.text("5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taipei City 110, Taiwan", 150, 170, { width: 480, align: 'left'} );



        doc.addPage();
        doc.image(as1_4, 0, 0, {width: a4pageWidth});

    */
    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice3', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice4', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice5', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice6', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    // doc.text(pf.addr1_eng, 223, 560, { width: 320, align: 'left'} );
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });
    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice7', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice8', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(10);
    doc.image(BIL_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("Hong Kong & Shanghai Banking Corporation Limited", 220, 198, {
      width: 380,
      align: 'left'
    });
    doc.text("1 Queen's Road Central, Hong Kong", 220, 218, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 220, 265, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_name, 220, 282, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 220, 310, {
      width: 380,
      align: 'left'
    });
    // doc.text("54" + " " + "12" + " " + "34", 220, 330, { width: 380, align: 'left', characterSpacing: 9} );
    doc.text("HSBCHKHHHKH", 220, 348, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 220, 370, { width: 380, align: 'left'} );
    doc.text("v", 162, 493, {
      width: 380,
      align: 'left',
    });
    doc.text(pf.fpi_now_same_payment, 220, 493, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 220, 518, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num2, 47, 575, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });
    doc.text(pf.addr1_eng, 220, 697, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BIL_3, 0, 0, {
      width: a4pageWidth
    });
    /*doc.text("1", 220, 198, { width: 380, align: 'left'} );
    doc.text("2", 220, 216, { width: 380, align: 'left'} );
    doc.text("3", 220, 261, { width: 380, align: 'left'} );
    doc.text("4", 220, 278, { width: 380, align: 'left'} );
    doc.text("5", 220, 308, { width: 380, align: 'left'} );
    doc.text("54" + " " + "12" + " " + "34", 220, 325, { width: 380, align: 'left', characterSpacing: 5.6} );
    doc.text("6", 220, 346, { width: 380, align: 'left'} );
    doc.text("7", 220, 366, { width: 380, align: 'left'} );
    doc.text("vvvv", 65, 477, { width: 380, align: 'left', characterSpacing: 42} );
    doc.text("8", 260, 477, { width: 380, align: 'left'} );
    doc.text("9", 260, 500, { width: 380, align: 'left'} );
    doc.text("v", 220, 542, { width: 380, align: 'left'} );
    doc.text("v", 498, 542, { width: 380, align: 'left'} );
    doc.text("v", 220, 565, { width: 380, align: 'left'} );
    doc.text("v", 498, 565, { width: 380, align: 'left'} );
    doc.text("12345678901234", 47, 607, { width: 380, align: 'left', characterSpacing: 7.8} );*/

    doc.addPage();
    doc.image(BIL_4, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice9', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(Reinstatement, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 140, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("USD 100,000", 165, 343, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 110, 500, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 525, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 693, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice10', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(Reinstatement, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 140, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("USD 100,000", 165, 343, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 110, 500, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 525, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 693, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice11', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(Reinstatement, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 140, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("USD 100,000", 165, 343, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 110, 500, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 525, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 693, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BIL_1, 0, 0, {
      width: a4pageWidth
    });

    doc.fontSize(10);
    doc.addPage();
    doc.image(BIL_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("Hong Kong & Shanghai Banking Corporation Limited", 220, 198, {
      width: 380,
      align: 'left'
    });
    doc.text("1 Queen's Road Central, Hong Kong", 220, 218, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 220, 265, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_name, 220, 282, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 220, 310, {
      width: 380,
      align: 'left'
    });
    // doc.text("54" + " " + "12" + " " + "34", 220, 330, { width: 380, align: 'left', characterSpacing: 9} );
    doc.text("HSBCHKHHHKH", 220, 348, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 220, 370, { width: 380, align: 'left'} );
    doc.text("v", 162, 493, {
      width: 380,
      align: 'left',
    });
    doc.text(pf.fpi_now_same_payment, 220, 493, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 220, 518, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num2, 47, 575, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });
    doc.text(pf.addr1_eng, 220, 697, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BIL_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_4, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice12', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(Reinstatement, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 140, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("USD 100,000", 165, 343, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 110, 500, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 525, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 693, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_1, 0, 0, {
      width: a4pageWidth
    });

    doc.fontSize(10);
    doc.addPage();
    doc.image(BIL_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("Hong Kong & Shanghai Banking Corporation Limited", 220, 198, {
      width: 380,
      align: 'left'
    });
    doc.text("1 Queen's Road Central, Hong Kong", 220, 218, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 220, 265, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_name, 220, 282, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 220, 310, {
      width: 380,
      align: 'left'
    });
    // doc.text("54" + " " + "12" + " " + "34", 220, 330, { width: 380, align: 'left', characterSpacing: 9} );
    doc.text("HSBCHKHHHKH", 220, 348, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 220, 370, { width: 380, align: 'left'} );
    doc.text("v", 162, 493, {
      width: 380,
      align: 'left',
    });
    doc.text(pf.fpi_now_same_payment, 220, 493, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 220, 518, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num2, 47, 575, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });
    doc.text(pf.addr1_eng, 220, 697, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BIL_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_4, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice13', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(Reinstatement, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 140, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("USD 100,000", 165, 343, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 110, 500, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 525, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 693, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_1, 0, 0, {
      width: a4pageWidth
    });

    doc.fontSize(10);
    doc.addPage();
    doc.image(BIL_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("Hong Kong & Shanghai Banking Corporation Limited", 220, 198, {
      width: 380,
      align: 'left'
    });
    doc.text("1 Queen's Road Central, Hong Kong", 220, 218, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 220, 265, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_name, 220, 282, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 220, 310, {
      width: 380,
      align: 'left'
    });
    // doc.text("54" + " " + "12" + " " + "34", 220, 330, { width: 380, align: 'left', characterSpacing: 9} );
    doc.text("HSBCHKHHHKH", 220, 348, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 220, 370, { width: 380, align: 'left'} );
    doc.text("v", 162, 493, {
      width: 380,
      align: 'left',
    });
    doc.text(pf.fpi_now_same_payment, 220, 493, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 220, 518, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num2, 47, 575, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });
    doc.text(pf.addr1_eng, 220, 697, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BIL_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BIL_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice14', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(paidUp, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 185, 323, {
      width: 380,
      align: 'left'
    });
    // doc.text("12/1999", 185, 343, { width: 380, align: 'left'} );
    doc.text("personal reason", 170, 368, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 130, 442, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 360, 455, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 565, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.font('Courier');
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice15', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(paidUp, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 185, 323, {
      width: 380,
      align: 'left'
    });
    // doc.text("12/1999", 185, 343, { width: 380, align: 'left'} );
    doc.text(pf.custom_1, 170, 368, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 130, 442, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 360, 455, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 565, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice16', {
  authRequired: false
}, {
  get: function () {
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(premium, 0, 0, {
      width: a4pageWidth
    });
    doc.font('Helvetica');
    doc.fontSize(12);
    doc.text(pf.account_num, 180, 285, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 130, 490, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 360, 505, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 600, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.font('Courier');
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice17', {
  authRequired: false
}, {
  get: function () {
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(premium, 0, 0, {
      width: a4pageWidth
    });
    doc.font('Helvetica');
    doc.fontSize(12);
    doc.text(pf.account_num, 180, 285, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 130, 490, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 360, 505, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 160, 600, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice18', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice19', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice20', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.font('Courier');
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice21', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice22', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.font('Courier');
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice23', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(blank, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(13);
    doc.text(pf.account_num, 355, 298, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 440, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 290, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 715, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 735, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice24', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(8);
    doc.image(ChangeAdd, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 105, 259, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 188, 259, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.addr1_eng, 95, 340, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 95, 340, 12);
    // doc.text(pf.addr1_eng, 95, 410, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 95, 640, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice25', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(ChangeNum, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 190, 250, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 190, 285, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 190, 355, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice26', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(20);
    doc.image(CFPI, 0, 0, {
      width: a4pageWidth
    });
    // doc.text(pf.last_name, 155, 420, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, pf.last_name, 155, 415, 20);
    Api.strChtEngMix(doc, pf.first_name, 370, 415, 20);
    doc.font('Courier');
    // Api.strChtEngMix(doc, decodeURI(pf.last_name), 155, 420, 12);
    // doc.text(pf.first_name, 370, 420, { width: 380, align: 'left'} );
    doc.text(pf.email, 155, 455, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 155, 495, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.birthday, 155, 530, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.identify, 370, 530, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice27', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(GT_Madison, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 215, 213, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.insertedBy_CE, 215, 247, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, pf.insertedBy_CE, 215, 245, 12);
    doc.font('Courier');
    doc.text(pf.account_num, 215, 265, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficiary_bank_name, 215, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 215, 377, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficiary_account_number, 215, 394, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.swift_code, 215, 413, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficiary_bank_address, 215, 449, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 215, 538, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 215, 575, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice28', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');
    doc.fontSize(10);
    doc.image(ChangeEmail, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng || "(empty)", 107, 293, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 298, 293, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email || "(empty)", 185, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 175, 688, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng || "(empty)", 175, 707, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice29', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');
    doc.fontSize(10);
    doc.image(CancelOMA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 300, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 220, 342, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.account_num, 220, 387, { width: 380, align: 'left'} );
    doc.text(pf.account_num, 220, 365, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(CancelOMA_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice30', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.fontSize(10);
    doc.image(FPI_Lossor_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_Lossor_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 150, 100, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 145, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng || "add", 150, 170, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 150, 170, 12);

    doc.addPage();
    doc.font('Courier');
    doc.image(FPI_Lossor_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 150, 100, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 150, 145, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng || "add", 150, 170, {
      width: 420,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 150, 167, 12);
    doc.text(pf.name_eng, 150, 205, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng || "add", 150, 228, {
      width: 420,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 150, 225, 12);

    doc.addPage();
    doc.image(FPI_Lossor_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_Broker, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 190, 250, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 190, 285, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice31', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.fontSize(18);
    doc.image(FPI_OMA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 280, 300, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.insertedBy, 280, 341, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, decodeURI(pf.insertedBy), 280, 341, 12);
    doc.text(pf.account_num, 280, 382, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_OMA_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_OMA_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_OMA_4, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice32', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_ReName, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice33', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.fontSize(12);
    doc.image(Sign_0, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 190, 255, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 190, 285, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 355, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 393, {
      width: 380,
      align: 'left'
    });
    doc.text("Person Reason", 220, 435, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(Sign_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 180, 605, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 180, 640, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 180, 665, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(Sign_2, 0, 0, {
      width: a4pageWidth
    });
    // doc.text(pf.last_name, 190, 390, { width: 380, align: 'left'} );
    // doc.text(pf.first_name, 190, 410, { width: 380, align: 'left'} );
    Api.strChtEngMix(doc, pf.last_name, 190, 385, 12);
    Api.strChtEngMix(doc, pf.first_name, 190, 405, 12);
    doc.font('Courier');
    doc.text(pf.addr1_eng, 190, 447, {
      width: 180,
      align: 'left'
    });

    doc.addPage();
    doc.image(Sign_3, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice34', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');
    doc.fontSize(12);
    doc.image(ASS_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.policy_firstperson, 225, 422, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 225, 440, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 225, 460, { width: 380, align: 'left'} );
    doc.fontSize(10);
    doc.text(pf.addr1_eng, 225, 460, {
      width: 140,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 225, 515, {
      width: 380,
      align: 'left'
    });
    // doc.text("5", 225, 537, { width: 380, align: 'left'} );

    doc.text(pf.policy_secondperson, 400, 422, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 400, 440, { width: 380, align: 'left'} );
    doc.fontSize(10);
    doc.text(pf.addr1_eng, 400, 460, {
      width: 140,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.account_num, 400, 515, {
      width: 380,
      align: 'left'
    });
    // doc.text("0", 400, 537, { width: 380, align: 'left'} );

    doc.text(pf.custom_1, 225, 615, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 225, 633, { width: 380, align: 'left'} );
    doc.text(pf.custom_3, 225, 653, {
      width: 380,
      align: 'left'
    });
    // doc.text("4", 225, 708, { width: 380, align: 'left'} );
    doc.text("v", 237, 730, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 282, 730, { width: 380, align: 'left'} );
    // doc.text("5", 225, 753, { width: 380, align: 'left'} );

    doc.text(pf.custom_2, 400, 615, {
      width: 380,
      align: 'left'
    });
    // doc.text("7", 400, 633, { width: 380, align: 'left'} );
    doc.text(pf.custom_4, 400, 653, {
      width: 380,
      align: 'left'
    });
    // doc.text("9", 400, 708, { width: 380, align: 'left'} );
    // doc.text("v", 412, 730, { width: 380, align: 'left'} );
    // doc.text("v", 458, 730, { width: 380, align: 'left'} );
    // doc.text("0", 400, 753, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(ASS_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.custom_5, 225, 160, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_6, 265, 185, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_7, 300, 208, {
      width: 380,
      align: 'left'
    });
    // doc.text("4", 265, 230, { width: 380, align: 'left'} );
    // doc.text("5", 300, 252, { width: 380, align: 'left'} );
    // doc.text("6", 265, 274, { width: 380, align: 'left'} );
    // doc.text("7", 300, 296, { width: 380, align: 'left'} );

    // doc.text("1", 400, 160, { width: 380, align: 'left'} );
    // doc.text("2", 440, 185, { width: 380, align: 'left'} );
    // doc.text("3", 475, 208, { width: 380, align: 'left'} );
    // doc.text("4", 440, 230, { width: 380, align: 'left'} );
    // doc.text("5", 475, 252, { width: 380, align: 'left'} );
    // doc.text("6", 440, 274, { width: 380, align: 'left'} );
    // doc.text("7", 475, 296, { width: 380, align: 'left'} );

    doc.text(pf.custom_6, 265, 390, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_7, 300, 413, {
      width: 380,
      align: 'left'
    });
    // doc.text("4", 265, 435, { width: 380, align: 'left'} );
    // doc.text("5", 300, 457, { width: 380, align: 'left'} );
    // doc.text("6", 265, 479, { width: 380, align: 'left'} );
    // doc.text("7", 300, 501, { width: 380, align: 'left'} );

    // doc.text("2", 440, 390, { width: 380, align: 'left'} );
    // doc.text("3", 475, 413, { width: 380, align: 'left'} );
    // doc.text("4", 440, 435, { width: 380, align: 'left'} );
    // doc.text("5", 475, 457, { width: 380, align: 'left'} );
    // doc.text("6", 440, 479, { width: 380, align: 'left'} );
    // doc.text("7", 475, 501, { width: 380, align: 'left'} );

    doc.text(pf.custom_8, 225, 560, {
      width: 380,
      align: 'left'
    });
    // doc.text("asd", 400, 560, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(ASS_3, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 320, 108, { width: 380, align: 'left'} );
    // doc.text("2", 430, 108, { width: 380, align: 'left'} );
    // doc.text("3", 500, 108, { width: 380, align: 'left'} );
    doc.text(pf.policy_firstperson, 220, 193, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_secondperson, 400, 193, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 400, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 135, 468, {
      width: 380,
      align: 'left'
    });
    doc.text("5F., No.400, Sec. 1, Keelung Rd., Xinyi Dist., Taipei City 110, Taiwan (R.O.C.)", 135, 490, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(ASS_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.custom_9, 220, 147, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_10, 220, 203, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 238, 230, { width: 380, align: 'left'} );
    // doc.text("v", 281, 230, { width: 380, align: 'left'} );
    // doc.text("123", 420, 230, { width: 380, align: 'left'} );

    doc.text("v", 220, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_11, 425, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_12, 485, 358, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_13, 390, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_14, 390, 405, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_15, 390, 460, {
      width: 380,
      align: 'left'
    });

    // doc.text("v", 220, 488, { width: 380, align: 'left'} );
    // doc.text("1", 390, 488, { width: 380, align: 'left'} );
    // doc.text("2", 390, 528, { width: 380, align: 'left'} );
    // doc.text("12" + "   " + "12", 430, 550, { width: 380, align: 'left', characterSpacing: 7.3} );
    // doc.text("3", 428, 573, { width: 380, align: 'left'} );
    // doc.text("4", 490, 573, { width: 380, align: 'left'} );
    // doc.text("12" + " " + "12" + " " + "2012", 384, 596, { width: 380, align: 'left', characterSpacing: 7.2} );
    // doc.text("v", 220, 616, { width: 380, align: 'left'} );
    // doc.text("5", 260, 616, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(ASS_5, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 238, 160, { width: 380, align: 'left'} );
    // doc.text("v", 280, 160, { width: 380, align: 'left'} );
    // doc.text("v", 390, 178, { width: 380, align: 'left'} );
    // doc.text("v", 491, 178, { width: 380, align: 'left'} );

    // doc.text("v", 238, 252, { width: 380, align: 'left'} );
    // doc.text("v", 280, 252, { width: 380, align: 'left'} );

    // doc.text("v", 390, 268, { width: 380, align: 'left'} );
    // doc.text("v", 491, 268, { width: 380, align: 'left'} );
    // doc.text("v", 390, 290, { width: 380, align: 'left'} );
    // doc.text("v", 491, 290, { width: 380, align: 'left'} );

    // doc.text("v", 238, 325, { width: 380, align: 'left'} );
    // doc.text("v", 280, 325, { width: 380, align: 'left'} );

    // doc.text("v", 390, 344, { width: 380, align: 'left'} );
    // doc.text("v", 491, 344, { width: 380, align: 'left'} );
    // doc.text("v", 390, 368, { width: 380, align: 'left'} );

    // doc.text("123", 225, 395, { width: 380, align: 'left'} );
    // doc.text("456", 225, 451, { width: 380, align: 'left'} );

    // doc.text("v", 235, 549, { width: 380, align: 'left'} );
    // doc.text("v", 445, 549, { width: 380, align: 'left'} );
    // doc.text("v", 495, 549, { width: 380, align: 'left'} );

    // doc.text("v", 235, 574, { width: 380, align: 'left'} );
    // doc.text("v", 445, 574, { width: 380, align: 'left'} );
    // doc.text("v", 495, 574, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(ASS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.fontSize(15);
    doc.image(FPI_CMEA, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("12" + " " + "12" + " " + "2012", 80, 75, { width: 380, align: 'left', characterSpacing: 7.3} );
    doc.text(pf.account_num, 190, 233, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_firstperson + "/" + pf.policy_secondperson, 190, 270, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 190, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 265, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 265, 450, {
      width: 380,
      align: 'left'
    });
    // doc.text("789", 95, 485, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(FPI_Self, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text(pf.custom_1, 165, 114, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 165, 127, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(15);
    doc.text(pf.custom_2, 150, 220, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 150, 244, { width: 380, align: 'left'} );
    // doc.text("3", 150, 268, { width: 380, align: 'left'} );
    // doc.text("4", 150, 292, { width: 380, align: 'left'} );
    doc.text(pf.custom_3, 310, 220, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 310, 244, { width: 380, align: 'left'} );
    // doc.text("7", 310, 268, { width: 380, align: 'left'} );
    // doc.text("8", 310, 292, { width: 380, align: 'left'} );

    doc.text(pf.custom_2, 150, 390, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 150, 414, { width: 380, align: 'left'} );
    // doc.text("3", 150, 438, { width: 380, align: 'left'} );
    // doc.text("4", 150, 462, { width: 380, align: 'left'} );
    doc.text(pf.custom_2, 310, 390, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 310, 414, { width: 380, align: 'left'} );
    // doc.text("7", 310, 438, { width: 380, align: 'left'} );
    // doc.text("8", 310, 462, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 190, 590, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice35', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice36', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice37', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice38', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice39', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice40', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice41', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(ChangeInv, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.payperiod_fpi, 235, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 185, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 230, 240, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 120, 670, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice42', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice43', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice44', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice45', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice46', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice47', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice48', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_Adj, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.custom_1, 490, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 95, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 190, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.payperiod_fpi, 250, 380, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 80, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 140, 397, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 155, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 155, 742, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice49', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice50', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice51', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice52', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    // Api.strChtEngMix(doc, decodeURI(pf.addr1_eng), 223, 555, 12);

    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice53', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice54', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    // doc.text("IBAN", 340, 340, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    // doc.text("v", 50, 462, { width: 380, align: 'left'} );
    // doc.text("v", 50, 483, { width: 380, align: 'left'} );
    // doc.text("v", 50, 502, { width: 380, align: 'left'} );
    // doc.text("v", 50, 521, { width: 380, align: 'left'} );
    // doc.text("v", 140, 565, { width: 380, align: 'left'} );
    // doc.text("v", 218, 565, { width: 380, align: 'left'} );
    // doc.text("v", 295, 565, { width: 380, align: 'left'} );
    // doc.text("v", 372, 565, { width: 380, align: 'left'} );
    // doc.text(pf.addr1_eng, 150, 710, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice55', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(CancelBSO, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HSBC", 55, 150, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 55, 200, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 55, 290, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 55, 338, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 430, 390, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 55, 625, 12);
    doc.fontSize(12);
    doc.font('Courier');
    doc.text(pf.account_num2, 54, 718, {
      width: 380,
      align: 'left',
      characterSpacing: 11
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice56', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_CPM, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(9);
    doc.text(pf.name_eng, 270, 292, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.paymethod_fpi, 380, 310, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 150, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 347, 329, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 403, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 100, 730, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice57', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_TPB, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng + " " + pf.account_num, 210, 273, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 230, 293, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 300, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 258, 347, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 100, 620, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 710, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice58', {
  authRequired: false
}, {
  get: function () {
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    doc.font('Courier');
    doc.image(FPI_TPC, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng + " " + pf.account_num, 200, 273, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 260, 293, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 300, 328, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 258, 347, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 100, 615, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 670, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice59', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(9);
    doc.image(TOPUP_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.insertedBy, 220, 274, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 302, {
      width: 380,
      align: 'left'
    });
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 220, 370, {
      width: 380,
      align: 'left'
    });
    doc.text("9800042", 220, 398, {
      width: 380,
      align: 'left'
    });
    // doc.text("5", 220, 448, { width: 380, align: 'left'} );
    // doc.text("6", 220, 476, { width: 380, align: 'left'} );
    doc.text("investments@grandtag.com", 220, 504, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_3, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 240, 163, { width: 380, align: 'left'} );
    // doc.text("v", 282, 163, { width: 380, align: 'left'} );
    // doc.text("v", 325, 163, { width: 380, align: 'left'} );
    // doc.text("v", 368, 163, { width: 380, align: 'left'} );
    // doc.text("v", 415, 163, { width: 380, align: 'left'} );
    // doc.text("v", 458, 163, { width: 380, align: 'left'} );
    // doc.text("v", 500, 163, { width: 380, align: 'left'} );
    // doc.text("v", 543, 163, { width: 380, align: 'left'} );
    // doc.text("1", 270, 188, { width: 380, align: 'left'} );
    // doc.text("2", 440, 188, { width: 380, align: 'left'} );
    doc.text(pf.last_name, 220, 225, {
      width: 380,
      align: 'left'
    });
    // doc.text("4", 400, 225, { width: 380, align: 'left'} );
    doc.text(pf.first_name, 220, 260, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 400, 260, { width: 380, align: 'left'} );
    doc.text(pf.apply_passport, 220, 305, {
      width: 380,
      align: 'left'
    });
    // doc.text("8", 400, 305, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 220, 340, {
      width: 180,
      align: 'left'
    });
    // doc.text("9qwerqwerqwe rqwerqwreqwe rqwerqwer", 400, 340, { width: 180, align: 'left'} );
    // doc.text("1", 250, 410, { width: 380, align: 'left'} );
    // doc.text("2", 360, 410, { width: 380, align: 'left'} );
    // doc.text("3", 425, 410, { width: 380, align: 'left'} );
    // doc.text("4", 535, 410, { width: 380, align: 'left'} );
    // doc.text("qweqweqweqwe qwqwerqwerqw erqwereqwer", 220, 455, { width: 180, align: 'left'} );
    // doc.text("9qwerqwerqwe rqwerqwreqwe rqwerqwer", 400, 455, { width: 180, align: 'left'} );
    doc.text(pf.apply_telephone, 220, 550, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 400, 550, { width: 380, align: 'left'} );
    // doc.text("3", 220, 575, { width: 380, align: 'left'} );
    // doc.text("4", 400, 575, { width: 380, align: 'left'} );
    doc.text(pf.apply_cellphone, 220, 600, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 400, 600, { width: 380, align: 'left'} );
    doc.text(pf.email, 220, 625, {
      width: 380,
      align: 'left'
    });
    // doc.text("8", 400, 625, { width: 380, align: 'left'} );
    doc.text(pf.custom_1, 220, 665, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 400, 665, { width: 380, align: 'left'} );
    // doc.text("3", 220, 700, { width: 380, align: 'left'} );
    // doc.text("4", 400, 700, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_4, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 240, 196, { width: 380, align: 'left'} );
    // doc.text("v", 282, 196, { width: 380, align: 'left'} );
    // doc.text("v", 325, 196, { width: 380, align: 'left'} );
    // doc.text("v", 368, 196, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 220, 252, {
      width: 380,
      align: 'left'
    });
    // doc.text("24" + " " + "12" + " " + "1234" , 220, 277, { width: 380, align: 'left', characterSpacing: 5.4} );
    // doc.text("v", 220, 460, { width: 380, align: 'left'} );
    // doc.text("v", 220, 522, { width: 380, align: 'left'} );
    // doc.text("v", 220, 584, { width: 380, align: 'left'} );
    // doc.text("v", 220, 638, { width: 380, align: 'left'} );
    // doc.text("v", 220, 678, { width: 380, align: 'left'} );
    // doc.text("v", 220, 740, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_5, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 240, 208, { width: 380, align: 'left'} );
    // doc.text("v", 282, 208, { width: 380, align: 'left'} );
    // doc.text("v", 325, 208, { width: 380, align: 'left'} );
    // doc.text("v", 368, 208, { width: 380, align: 'left'} );
    // doc.text("1", 220, 235, { width: 380, align: 'left'} );
    // doc.text("v", 220, 318, { width: 380, align: 'left'} );
    // doc.text("v", 220, 353, { width: 380, align: 'left'} );
    // doc.text("v", 220, 393, { width: 380, align: 'left'} );
    // doc.text("24" + " " + "12" + " " + "34" , 220, 532, { width: 380, align: 'left', characterSpacing: 6.2} );
    // doc.text("1", 220, 560, { width: 380, align: 'left'} );
    // doc.text("2", 220, 590, { width: 380, align: 'left'} );
    // doc.text("3", 220, 620, { width: 380, align: 'left'} );
    // doc.text("4", 220, 650, { width: 380, align: 'left'} );
    // doc.text("5", 220, 680, { width: 380, align: 'left'} );
    // doc.text("6", 220, 710, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_6, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 173, { width: 380, align: 'left'} );
    // doc.text("2", 220, 197, { width: 380, align: 'left'} );
    // doc.text("3", 220, 221, { width: 380, align: 'left'} );
    // doc.text("4", 220, 245, { width: 380, align: 'left'} );
    // doc.text("1", 75, 563, { width: 380, align: 'left'} );
    // doc.text("2", 170, 563, { width: 380, align: 'left'} );
    // doc.text("3", 505, 563, { width: 380, align: 'left'} );
    // doc.text("1", 75, 585, { width: 380, align: 'left'} );
    // doc.text("2", 170, 585, { width: 380, align: 'left'} );
    // doc.text("3", 505, 585, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_7, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 60, 265, { width: 380, align: 'left'} );
    // doc.text("2", 130, 265, { width: 380, align: 'left'} );
    // doc.text("3", 210, 265, { width: 380, align: 'left'} );
    // doc.text("4", 300, 265, { width: 380, align: 'left'} );
    // doc.text("1", 60, 288, { width: 380, align: 'left'} );
    // doc.text("2", 130, 288, { width: 380, align: 'left'} );
    // doc.text("3", 210, 288, { width: 380, align: 'left'} );
    // doc.text("4", 300, 288, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_8, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 185, 184, { width: 380, align: 'left'} );
    // doc.text("1", 384, 184, { width: 380, align: 'left'} );
    // doc.text("2", 456, 184, { width: 380, align: 'left'} );
    // doc.text("3", 340, 208, { width: 380, align: 'left'} );
    // doc.text("4", 340, 230, { width: 380, align: 'left'} );
    // doc.text("5", 340, 285, { width: 380, align: 'left'} );
    // doc.text("v", 185, 332, { width: 380, align: 'left'} );
    // doc.text("1", 384, 332, { width: 380, align: 'left'} );
    // doc.text("2", 456, 332, { width: 380, align: 'left'} );
    // doc.text("3", 340, 356, { width: 380, align: 'left'} );
    // doc.text("4", 340, 378, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "1234", 338, 403, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("v", 185, 450, { width: 380, align: 'left'} );
    // doc.text("1", 340, 450, { width: 380, align: 'left'} );
    // doc.text("2", 340, 495, { width: 380, align: 'left'} );
    // doc.text("16", 368, 518, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("12", 457, 518, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("1", 384, 540, { width: 380, align: 'left'} );
    // doc.text("2", 456, 540, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "1234", 338, 564, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("v", 185, 612, { width: 380, align: 'left'} );
    // doc.text("1", 340, 612, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "1234", 338, 668, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("1", 384, 690, { width: 380, align: 'left'} );
    // doc.text("2", 456, 690, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_9, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 186, 136, { width: 380, align: 'left'} );
    // doc.text("1", 340, 136, { width: 380, align: 'left'} );
    // doc.text("2", 340, 161, { width: 380, align: 'left'} );
    // doc.text("3", 340, 215, { width: 380, align: 'left'} );
    // doc.text("4", 384, 241, { width: 380, align: 'left'} );
    // doc.text("5", 456, 241, { width: 380, align: 'left'} );
    // doc.text("v", 186, 288, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "1234", 340, 288, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("1", 340, 313, { width: 380, align: 'left'} );
    // doc.text("2", 340, 336, { width: 380, align: 'left'} );
    // doc.text("3", 340, 359, { width: 380, align: 'left'} );
    // doc.text("4", 340, 413, { width: 380, align: 'left'} );
    // doc.text("5", 340, 436, { width: 380, align: 'left'} );
    // doc.text("6", 340, 469, { width: 380, align: 'left'} );
    // doc.text("7", 340, 491, { width: 380, align: 'left'} );
    // doc.text("v", 186, 535, { width: 380, align: 'left'} );
    // doc.text("asd", 213, 537, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HONG KONG", 350, 448, {
      width: 380,
      align: 'left'
    });
    doc.text("HONG KONG", 350, 484, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 623, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_14, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 460, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 460, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 483, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 483, { width: 380, align: 'left'} );
    // doc.text("v", 220, 528, { width: 380, align: 'left'} );
    // doc.text("v", 507, 528, { width: 380, align: 'left'} );
    // doc.text("v", 220, 550, { width: 380, align: 'left'} );
    // doc.text("v", 507, 550, { width: 380, align: 'left'} );
    // doc.text("1", 225, 573, { width: 380, align: 'left'} );
    // doc.text("2", 330, 573, { width: 380, align: 'left'} );
    // doc.text("3", 483, 573, { width: 380, align: 'left'} );
    // doc.text("v", 265, 595, { width: 380, align: 'left'} );
    // doc.text("v", 360, 595, { width: 380, align: 'left'} );
    // doc.text("v", 460, 595, { width: 380, align: 'left'} );
    // doc.text("v", 543, 595, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 660, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_15, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 448, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 448, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 470, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 470, { width: 380, align: 'left'} );
    // doc.text("v", 232, 514, { width: 380, align: 'left'} );
    // doc.text("v", 485, 514, { width: 380, align: 'left'} );
    // doc.text("v", 232, 542, { width: 380, align: 'left'} );
    // doc.text("v", 485, 542, { width: 380, align: 'left'} );
    // doc.text("1", 225, 577, { width: 380, align: 'left'} );
    // doc.text("2", 330, 577, { width: 380, align: 'left'} );
    // doc.text("3", 483, 577, { width: 380, align: 'left'} );
    // doc.text("v", 265, 596, { width: 380, align: 'left'} );
    // doc.text("v", 360, 596, { width: 380, align: 'left'} );
    // doc.text("v", 460, 596, { width: 380, align: 'left'} );
    // doc.text("v", 543, 596, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 660, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_16, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 494, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 494, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 517, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 517, { width: 380, align: 'left'} );
    // doc.text("v", 220, 575, { width: 380, align: 'left'} );
    // doc.text("v", 507, 575, { width: 380, align: 'left'} );
    // doc.text("v", 220, 595, { width: 380, align: 'left'} );
    // doc.text("v", 507, 595, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 649, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_17, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 490, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 490, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 512, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 512, { width: 380, align: 'left'} );
    // doc.text("v", 232, 565, { width: 380, align: 'left'} );
    // doc.text("v", 488, 565, { width: 380, align: 'left'} );
    // doc.text("v", 232, 595, { width: 380, align: 'left'} );
    // doc.text("v", 488, 595, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 653, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_18, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("v", 220, 500, { width: 380, align: 'left'} );
    // doc.text("v", 507, 500, { width: 380, align: 'left'} );
    // doc.text("v", 220, 520, { width: 380, align: 'left'} );
    // doc.text("v", 507, 520, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 552, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 552, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 575, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 575, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 627, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_19, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 195, { width: 380, align: 'left'} );
    // doc.text("2", 220, 213, { width: 380, align: 'left'} );
    // doc.text("3", 220, 258, { width: 380, align: 'left'} );
    // doc.text("4", 220, 275, { width: 380, align: 'left'} );
    // doc.text("5", 220, 305, { width: 380, align: 'left'} );
    // doc.text("54" + " " + "12" + " " + "34", 220, 322, { width: 380, align: 'left', characterSpacing: 5.3} );
    // doc.text("6", 220, 341, { width: 380, align: 'left'} );
    // doc.text("7", 220, 361, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 538, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("8", 260, 538, { width: 380, align: 'left'} );
    // doc.text("vvvv", 69, 561, { width: 380, align: 'left', characterSpacing: 43} );
    // doc.text("9", 260, 561, { width: 380, align: 'left'} );
    // doc.text("1234567890123", 47, 612, { width: 380, align: 'left', characterSpacing: 9.2} );

    doc.addPage();
    doc.image(TOPUP_20, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 48, 365, { width: 380, align: 'left'} );
    // doc.text("123456789012345", 312, 365, { width: 380, align: 'left', characterSpacing: 7.6} );
    // doc.text("2", 48, 410, { width: 380, align: 'left'} );
    // doc.text("3", 312, 410, { width: 380, align: 'left'} );
    // doc.text("v", 108, 430, { width: 380, align: 'left'} );
    // doc.text("v", 176, 430, { width: 380, align: 'left'} );
    // doc.text("4", 48, 455, { width: 380, align: 'left'} );
    // doc.text("5", 312, 452, { width: 380, align: 'left'} );
    // doc.text("12345678", 48, 487, { width: 380, align: 'left', characterSpacing: 10} );
    // doc.text("6", 48, 550, { width: 380, align: 'left'} );
    // doc.text("123456789", 312, 550, { width: 380, align: 'left', characterSpacing: 8} );
    // doc.text("54" + " " + "12" + " " + "3456", 312, 581, { width: 380, align: 'left', characterSpacing: 5.3} );

    doc.addPage();
    doc.image(TOPUP_21, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 220, 242, { width: 380, align: 'left'} );
    // doc.text("v", 330, 242, { width: 380, align: 'left'} );
    doc.text(pf.creditcard_bank, 220, 278, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 220, 303, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 218, 328, {
      width: 380,
      align: 'left',
      characterSpacing: 9.8
    });
    doc.text(pf.creditcard_term_month, 218, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.creditcard_term_year, 288, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.custom_2, 220, 378, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 398, {
      width: 380,
      align: 'left'
    });
    // doc.text("vvvv", 238, 419, { width: 380, align: 'left', characterSpacing: 48} );
    doc.text(pf.account_num, 220, 447, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 220, 480, { width: 380, align: 'left'} );
    // doc.text("7", 320, 480, { width: 380, align: 'left'} );
    // doc.text("8", 480, 480, { width: 380, align: 'left'} );
    // doc.text("v", 220, 505, { width: 380, align: 'left'} );
    // doc.text("v", 315, 505, { width: 380, align: 'left'} );
    // doc.text("v", 400, 505, { width: 380, align: 'left'} );
    // doc.text("v", 490, 505, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 575, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 695, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_22, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 534, 382, { width: 380, align: 'left'} );
    // doc.text("v", 534, 412, { width: 380, align: 'left'} );
    // doc.text("v", 534, 442, { width: 380, align: 'left'} );
    // doc.text("v", 534, 472, { width: 380, align: 'left'} );
    // doc.text("v", 534, 502, { width: 380, align: 'left'} );
    // doc.text("v", 534, 532, { width: 380, align: 'left'} );
    // doc.text("v", 534, 562, { width: 380, align: 'left'} );
    // doc.text("v", 534, 592, { width: 380, align: 'left'} );
    // doc.text("v", 534, 622, { width: 380, align: 'left'} );
    // doc.text("v", 534, 652, { width: 380, align: 'left'} );
    // doc.text("v", 534, 682, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(TOPUP_23, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 630, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 620, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 500, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_5, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 660, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_6, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(7);
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 215, 205, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text("M189", 250, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 510, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 190, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_8, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 660, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(GT_FNAC, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 260, 430, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 260, 595, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 260, 640, {
      width: 380,
      align: 'left'
    });


    doc.fontSize(12);
    doc.addPage();
    doc.image(gt_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 255, 425, {
      width: 380,
      align: 'left'
    });
    // doc.text("21/12/16", 255, 465, { width: 380, align: 'left'} );
    // doc.text("name", 255, 545, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 190, 115, {
      width: 380,
      align: 'left'
    });
    // doc.text("male", 190, 180, { width: 380, align: 'left'} );
    doc.text(pf.birthday, 375, 180, {
      width: 380,
      align: 'left'
    });
    // doc.text("taiwan", 190, 215, { width: 380, align: 'left'} );
    // doc.text("job", 375, 215, { width: 380, align: 'left'} );
    // doc.text("2", 190, 330, { width: 380, align: 'left'} );
    doc.text(pf.contactnum, 375, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 365, {
      width: 380,
      align: 'left'
    });
    // doc.text("name", 190, 430, { width: 380, align: 'left'} );
    // doc.text("male", 190, 495, { width: 380, align: 'left'} );
    // doc.text("10/10", 375, 495, { width: 380, align: 'left'} );
    // doc.text("taiwan", 190, 530, { width: 380, align: 'left'} );
    // doc.text("job", 375, 530, { width: 380, align: 'left'} );
    // doc.text("2", 190, 645, { width: 380, align: 'left'} );
    // doc.text("212-1232", 375, 645, { width: 380, align: 'left'} );
    // doc.text("address", 190, 680, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_4, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("USD", 420, 85, { width: 380, align: 'left'} );
    // doc.text("123", 280, 145, { width: 380, align: 'left'} );
    // doc.text("234", 280, 195, { width: 380, align: 'left'} );
    // doc.text("345", 280, 235, { width: 380, align: 'left'} );
    // doc.text("456", 280, 280, { width: 380, align: 'left'} );
    // doc.text("567", 280, 315, { width: 380, align: 'left'} );
    // doc.text("678", 415, 350, { width: 380, align: 'left'} );
    // doc.text("789", 415, 385, { width: 380, align: 'left'} );
    // doc.text("11", 415, 420, { width: 380, align: 'left'} );
    // doc.text("22", 415, 455, { width: 380, align: 'left'} );
    // doc.text("33", 280, 515, { width: 380, align: 'left'} );
    // doc.text("44", 280, 550, { width: 380, align: 'left'} );
    // doc.text("55", 280, 585, { width: 380, align: 'left'} );
    // doc.text("66", 280, 620, { width: 380, align: 'left'} );
    // doc.text("77", 280, 655, { width: 380, align: 'left'} );
    // doc.text("88", 415, 690, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_5, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 280, 135, { width: 380, align: 'left'} );
    // doc.text("2", 280, 175, { width: 380, align: 'left'} );
    // doc.text("3", 420, 210, { width: 380, align: 'left'} );
    // doc.text("4", 420, 250, { width: 380, align: 'left'} );
    // doc.text("5", 280, 315, { width: 380, align: 'left'} );
    // doc.text("6", 280, 350, { width: 380, align: 'left'} );
    // doc.text("7", 280, 385, { width: 380, align: 'left'} );
    // doc.text("8", 280, 420, { width: 380, align: 'left'} );
    // doc.text("9", 280, 455, { width: 380, align: 'left'} );
    // doc.text("10", 420, 490, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_8, 0, 0, {
      width: a4pageWidth
    });
    // doc.fontSize(14);
    // doc.text("1", 95, 123, { width: 380, align: 'left'} );
    // doc.text("2", 230, 123, { width: 380, align: 'left'} );
    // doc.text("3", 95, 139, { width: 380, align: 'left'} );
    // doc.text("4", 230, 139, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 230, 420, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_10, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 445, 127, { width: 380, align: 'left'} );
    // doc.text("2", 445, 145, { width: 380, align: 'left'} );
    // doc.text("3", 445, 163, { width: 380, align: 'left'} );
    // doc.text("4", 445, 181, { width: 380, align: 'left'} );
    // doc.text("5", 445, 199, { width: 380, align: 'left'} );
    // doc.text("6", 475, 235, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_11, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 200, 460, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_12, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 200, 160, { width: 380, align: 'left'} );
    // doc.text("2", 200, 190, { width: 380, align: 'left'} );
    // doc.text("3", 400, 225, { width: 380, align: 'left'} );
    // doc.text("4", 170, 285, { width: 380, align: 'left'} );
    // doc.text("5", 170, 315, { width: 380, align: 'left'} );
    // doc.text("6", 170, 350, { width: 380, align: 'left'} );
    // doc.text("7", 170, 375, { width: 380, align: 'left'} );
    // doc.text("8", 170, 393, { width: 380, align: 'left'} );
    // doc.text("9", 170, 510, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(gt_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 170, 240, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_14, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 590, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_16, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 210, 310, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 150, 190, {
      width: 380,
      align: 'left'
    });
    // doc.text(pf.name_cht, 370, 190, { width: 380, align: 'left'} );
    // Api.strChtEngMix(doc, pf.name_cht, 370, 190, 12);
    Api.strChtEngMix(doc, pf.last_name, 370, 190, 12);
    doc.font('Courier');
    doc.text(pf.birthday, 150, 225, {
      width: 380,
      align: 'left'
    });
    // doc.text("Taiwn", 370, 225, { width: 380, align: 'left'} );
    // doc.text("v", 475, 217, { width: 380, align: 'left'} );
    // doc.text("v", 475, 226, { width: 380, align: 'left'} );
    doc.fontSize(8.5);
    doc.text(pf.contactnum, 185, 255, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 313, 255, { width: 380, align: 'left'} );
    // doc.text("7", 455, 255, { width: 380, align: 'left'} );
    doc.fontSize(8.5);
    doc.text(pf.email, 150, 280, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    // doc.text("v", 365, 269, { width: 380, align: 'left'} );
    // doc.text("v", 458, 269, { width: 380, align: 'left'} );
    // doc.text("v", 365, 280, { width: 380, align: 'left'} );
    // doc.text("v", 458, 280, { width: 380, align: 'left'} );
    doc.text(pf.addr1_eng, 150, 300, {
      width: 380,
      align: 'left'
    });
    // doc.text("9", 370, 300, { width: 380, align: 'left'} );
    // doc.text("v", 148, 358, { width: 380, align: 'left'} );
    // doc.text("v", 282, 360, { width: 380, align: 'left'} );
    // doc.text("v", 416, 360, { width: 380, align: 'left'} );

    // doc.text("v", 148, 381, { width: 380, align: 'left'} );
    // doc.text("v", 350, 381, { width: 380, align: 'left'} );
    // doc.text("v", 148, 400, { width: 380, align: 'left'} );
    // doc.text("222",273 , 400, { width: 380, align: 'left'} );
    // doc.text("v", 377, 400, { width: 380, align: 'left'} );
    // doc.text("111", 430, 400, { width: 380, align: 'left'} );

    // doc.text("v", 148, 419, { width: 380, align: 'left'} );
    // doc.text("v", 282, 419, { width: 380, align: 'left'} );
    // doc.text("v", 416, 419, { width: 380, align: 'left'} );

    // doc.text("v", 148, 440, { width: 380, align: 'left'} );
    // doc.text("v", 282, 436, { width: 380, align: 'left'} );
    // doc.text("v", 416, 436, { width: 380, align: 'left'} );

    // doc.text("v", 148, 463, { width: 380, align: 'left'} );
    // doc.text("v", 282, 463, { width: 380, align: 'left'} );
    // doc.text("v", 416, 463, { width: 380, align: 'left'} );
    // doc.text("333", 465, 463, { width: 380, align: 'left'} );

    // doc.text("1", 150, 485, { width: 380, align: 'left'} );
    // doc.text("2", 370, 485, { width: 380, align: 'left'} );
    // doc.text("3", 295, 537, { width: 380, align: 'left'} );
    // doc.text("4", 295, 552, { width: 380, align: 'left'} );
    // doc.text("5", 73, 655, { width: 380, align: 'left'} );
    // doc.text("6", 206, 645, { width: 380, align: 'left'} );
    // doc.text("7", 206, 672, { width: 380, align: 'left'} );
    // doc.text("8", 252, 645, { width: 380, align: 'left'} );
    // doc.text("9", 252, 672, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(FNA_2, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("100,000", 210, 118, { width: 380, align: 'left'} );
    // doc.text("100,000", 210, 160, { width: 380, align: 'left'} );
    // doc.text("100,000", 210, 202, { width: 380, align: 'left'} );
    // doc.text("100,000", 210, 244, { width: 380, align: 'left'} );
    // doc.text("100,000", 210, 294, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 115, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 142, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 169, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 196, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 223, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 250, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 294, { width: 380, align: 'left'} );
    // doc.text("200,000", 465, 326, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 383, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 410, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 437, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 464, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 498, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 542, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 569, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 596, { width: 380, align: 'left'} );
    // doc.text("300,000", 210, 630, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 387, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 441, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 507, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 572, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 633, { width: 380, align: 'left'} );
    // doc.text("400,000", 465, 668, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(FNA_3, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("100,000", 225, 85, { width: 380, align: 'left'} );
    // doc.text("200,000", 225, 107, { width: 380, align: 'left'} );
    // doc.text("300,000", 225, 130, { width: 380, align: 'left'} );
    // doc.text("1", 280, 153, { width: 380, align: 'left'} );
    // doc.text("2", 60, 208, { width: 380, align: 'left'} );
    // doc.text("3", 225, 208, { width: 380, align: 'left'} );
    // doc.text("4", 60, 238, { width: 380, align: 'left'} );
    // doc.text("5", 225, 238, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 90, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_4, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 80, 164, { width: 380, align: 'left'} );
    // doc.text("v", 80, 202, { width: 380, align: 'left'} );
    // doc.text("v", 80, 232, { width: 380, align: 'left'} );
    // doc.text("v", 290, 164, { width: 380, align: 'left'} );
    // doc.text("v", 290, 202, { width: 380, align: 'left'} );
    // doc.text("v", 290, 232, { width: 380, align: 'left'} );
    // doc.text("123", 405, 242, { width: 380, align: 'left'} );
    // doc.text("v", 80, 310, { width: 380, align: 'left'} );
    // doc.text("v", 80, 330, { width: 380, align: 'left'} );
    // doc.text("v", 80, 350, { width: 380, align: 'left'} );
    // doc.text("v", 80, 380, { width: 380, align: 'left'} );
    // doc.text("v", 80, 410, { width: 380, align: 'left'} );
    // doc.text("123", 240, 410, { width: 380, align: 'left'} );

    // doc.text("v", 80, 477, { width: 380, align: 'left'} );
    // doc.text("v", 217, 477, { width: 380, align: 'left'} );
    // doc.text("v", 357, 477, { width: 380, align: 'left'} );
    // doc.text("v", 80, 490, { width: 380, align: 'left'} );
    // doc.text("v", 217, 490, { width: 380, align: 'left'} );
    // doc.text("v", 357, 490, { width: 380, align: 'left'} );
    // doc.text("v", 95, 589, { width: 380, align: 'left'} );

    // doc.text("123", 138, 611, { width: 380, align: 'left'} );
    // doc.text("v", 296, 589, { width: 380, align: 'left'} );
    // doc.text("v", 311, 602, { width: 380, align: 'left'} );
    // doc.text("v", 311, 615, { width: 380, align: 'left'} );
    // doc.text("v", 311, 628, { width: 380, align: 'left'} );
    // doc.text("v", 311, 640, { width: 380, align: 'left'} );
    // doc.text("v", 311, 653, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(FNA_5, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("v", 136, 113, { width: 380, align: 'left'} );
    // doc.text("v", 258, 113, { width: 380, align: 'left'} );
    // doc.text("v", 382, 113, { width: 380, align: 'left'} );

    // doc.text("v", 136, 133, { width: 380, align: 'left'} );
    // doc.text("v", 258, 133, { width: 380, align: 'left'} );
    // doc.text("v", 382, 133, { width: 380, align: 'left'} );

    // doc.text("v", 136, 153, { width: 380, align: 'left'} );
    // doc.text("123", 277, 156, { width: 380, align: 'left'} );
    // doc.text("456,678", 177, 178, { width: 380, align: 'left'} );

    // doc.text("v", 96, 493, { width: 380, align: 'left'} );
    // doc.text("v", 225, 493, { width: 380, align: 'left'} );
    // doc.text("v", 347, 493, { width: 380, align: 'left'} );
    // doc.text("v", 96, 507, { width: 380, align: 'left'} );
    // doc.text("v", 225, 507, { width: 380, align: 'left'} );
    // doc.text("v", 347, 507, { width: 380, align: 'left'} );

    // doc.text("v", 96, 575, { width: 380, align: 'left'} );
    // doc.text("v", 225, 575, { width: 380, align: 'left'} );
    // doc.text("v", 347, 575, { width: 380, align: 'left'} );
    // doc.text("v", 96, 589, { width: 380, align: 'left'} );
    // doc.text("v", 225, 589, { width: 380, align: 'left'} );
    // doc.text("v", 347, 589, { width: 380, align: 'left'} );

    // doc.text("v", 96, 641, { width: 380, align: 'left'} );
    // doc.text("v", 225, 641, { width: 380, align: 'left'} );
    // doc.text("v", 347, 641, { width: 380, align: 'left'} );
    // doc.text("v", 96, 651, { width: 380, align: 'left'} );
    // doc.text("v", 225, 651, { width: 380, align: 'left'} );
    // doc.text("123", 330, 663, { width: 380, align: 'left'} );

    doc.addPage();
    doc.image(FNA_6, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 106, 153, { width: 380, align: 'left'} );
    // doc.text("2", 206, 153, { width: 380, align: 'left'} );
    // doc.text("3", 306, 153, { width: 380, align: 'left'} );
    // doc.text("4", 426, 153, { width: 380, align: 'left'} );

    // doc.text("1", 106, 173, { width: 380, align: 'left'} );
    // doc.text("2", 206, 173, { width: 380, align: 'left'} );
    // doc.text("3", 306, 173, { width: 380, align: 'left'} );
    // doc.text("4", 426, 173, { width: 380, align: 'left'} );
    doc.text(pf.name_eng, 100, 635, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 110, 420, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice60', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(9);
    doc.image(TOPUP_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 274, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 302, {
      width: 380,
      align: 'left'
    });
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 220, 370, {
      width: 380,
      align: 'left'
    });
    doc.text("9800042", 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text("investments@grandtag.com", 220, 504, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.last_name, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.first_name, 220, 260, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_passport, 220, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 340, {
      width: 180,
      align: 'left'
    });
    doc.text(pf.apply_telephone, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_cellphone, 220, 600, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 625, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 665, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.fpi_now_same_payment, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.text("1", 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text("2", 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HONG KONG", 350, 448, {
      width: 380,
      align: 'left'
    });
    doc.text("HONG KONG", 350, 484, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 623, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_14, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_16, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_18, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_19, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_20, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_21, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 220, 278, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 220, 303, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 218, 328, {
      width: 380,
      align: 'left',
      characterSpacing: 9.8
    });
    doc.text(pf.creditcard_term_month, 218, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.creditcard_term_year, 288, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.custom_2, 220, 378, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 447, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 575, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 695, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_22, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_23, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_12, 0, 0, {
      width: a4pageWidth
    });

    // doc.addPage();
    // doc.image(IFS_P_1, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 630, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_2, 0, 0, {width: a4pageWidth});

    // doc.addPage();
    // doc.image(IFS_P_3, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 620, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_4, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 500, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_5, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 660, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_6, 0, 0, {width: a4pageWidth});
    // doc.fontSize(7);
    // doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 215, 205, { width: 380, align: 'left'} );
    // doc.fontSize(9);
    // doc.text("M189", 250, 220, { width: 380, align: 'left'} );
    // doc.text(pf.name_eng, 100, 510, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_7, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 190, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_8, 0, 0, {width: a4pageWidth});
    // doc.text(pf.name_eng, 100, 660, { width: 380, align: 'left'} );

    // doc.addPage();
    // doc.image(IFS_P_9, 0, 0, {width: a4pageWidth});

    // doc.addPage();
    // doc.image(IFS_P_10, 0, 0, {width: a4pageWidth});

    // doc.addPage();
    // doc.image(IFS_P_11, 0, 0, {width: a4pageWidth});

    // doc.addPage();
    // doc.image(IFS_P_12, 0, 0, {width: a4pageWidth});

    doc.addPage();
    doc.image(GT_FNAC, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 260, 430, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 260, 595, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 260, 640, {
      width: 380,
      align: 'left'
    });


    doc.fontSize(12);
    doc.addPage();
    doc.image(gt_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 255, 425, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 190, 115, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.birthday, 375, 180, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 375, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 365, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_8, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 230, 420, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_11, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 200, 460, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 170, 240, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_14, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 590, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_16, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 210, 310, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 150, 190, {
      width: 380,
      align: 'left'
    });
    Api.strChtEngMix(doc, pf.last_name, 370, 190, 12);
    doc.font('Courier');
    doc.text(pf.birthday, 150, 225, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.contactnum, 185, 255, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.email, 150, 280, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.addr1_eng, 150, 300, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 90, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_6, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 635, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 110, 420, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice61', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(VOYA_CPI, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 140, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 380, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.trust_name, 200, 255, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 120, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 380, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 370, 540, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 100, 575, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 100, 680, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice62', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(VOYA_TO_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 140, 475, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 480, 475, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.trust_grantor, 140, 505, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 140, 580, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 480, 580, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 605, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 180, 635, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(VOYA_TO_2, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(10);
    doc.text(pf.trust_name, 80, 80, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.trust_trustee, 100, 100, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 140, 130, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(VOYA_TO_3, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice63', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(VOYA_BDV_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(VOYA_BDV_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(VOYA_BDV_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 140, 278, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 470, 278, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8);
    doc.text(pf.trust_name, 128, 300, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(11);
    doc.text(pf.beneficial1_name, 60, 410, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial1_percent, 480, 410, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial2_name, 60, 457, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial2_percent, 480, 457, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial3_name, 60, 502, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial3_percent, 480, 502, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial4_name, 60, 547, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial4_percent, 480, 547, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);

    doc.addPage();
    doc.image(VOYA_BDV_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.trust_name, 100, 345, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.trust_trustee, 100, 372, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.trust_ssnitin, 330, 372, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.trust_lawyer, 100, 398, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(VOYA_BDV_5, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice64', {
  authRequired: false
}, {
  get: function () {
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(12);
    doc.image(FPI_TT, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 300, 415, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 300, 445, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 330, 470, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 20), 510, 470, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 350, 490, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice65', {
  authRequired: false
}, {
  get: function () {
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Helvetica-Bold');
    doc.fontSize(12);
    doc.image(VOYA_WF, 0, 0, {
      width: a4pageWidth
    });
    // doc.text(pf.fpi_now_same_payment, 240, 470, { width: 380, align: 'left'} );
    doc.text(pf.fpi_now_same_payment, 240, 480, {
      width: 380,
      align: 'left'
    });
    doc.text((Number(pf.fpi_now_same_payment) + 35), 450, 480, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 330, 515, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 330, 545, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice66', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(14);
    doc.image(NEW_Third_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.policy_firstperson, 220, 327, {
      width: 380,
      align: 'left'
    });
    // doc.text("2", 220, 350, { width: 380, align: 'left'} );
    doc.text(pf.email, 220, 396, {
      width: 380,
      align: 'left'
    });
    // doc.text("4", 220, 419, { width: 380, align: 'left'} );
    doc.text(pf.insertedBy, 220, 442, {
      width: 380,
      align: 'left'
    });
    // doc.text("6", 220, 465, { width: 380, align: 'left'} );
    doc.text(pf.account_num, 220, 488, {
      width: 380,
      align: 'left'
    });

    doc.text(pf.beneficial1_name2, 220, 630, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial1_name2, 220, 653, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial1_percent, 523, 653, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial1_relationship, 220, 676, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.beneficial1_relationship, 220, 699, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 722, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 220, 744, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 767, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.custom_4, 220, 790, {
      width: 320,
      align: 'left'
    });
    doc.fontSize(14);

    doc.addPage();
    doc.image(NEW_Third_2, 0, 0, {
      width: a4pageWidth
    });
    // doc.text("1", 220, 108, { width: 380, align: 'left'} );
    // doc.text("2", 220, 131, { width: 380, align: 'left'} );
    // doc.text("3", 523, 131, { width: 380, align: 'left'} );
    // doc.text("4", 220, 154, { width: 380, align: 'left'} );
    // doc.text("5", 220, 177, { width: 380, align: 'left'} );
    // doc.text("6", 220, 200, { width: 380, align: 'left'} );
    // doc.text("7", 220, 223, { width: 380, align: 'left'} );
    // doc.text("8", 220, 246, { width: 380, align: 'left'} );
    // doc.text("9", 220, 269, { width: 380, align: 'left'} );

    // doc.text("1", 220, 340, { width: 380, align: 'left'} );
    // doc.text("2", 220, 363, { width: 380, align: 'left'} );
    // doc.text("3", 523, 363, { width: 380, align: 'left'} );
    // doc.text("4", 220, 385, { width: 380, align: 'left'} );
    // doc.text("5", 220, 408, { width: 380, align: 'left'} );
    // doc.text("6", 220, 431, { width: 380, align: 'left'} );
    // doc.text("7", 220, 453, { width: 380, align: 'left'} );
    // doc.text("8", 220, 476, { width: 380, align: 'left'} );
    // doc.text("9", 220, 499, { width: 380, align: 'left'} );

    doc.text("Hanbury IFA Corporation Limited", 220, 670, {
      width: 380,
      align: 'left'
    });
    doc.text("+886 2 2758 2239", 220, 693, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 220, 716, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text("5F., No. 400, Sec.1, Keelung Road., Xinyi District., Taipei City 11051, Taiwan", 220, 738, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(14);

    doc.addPage();
    doc.image(NEW_Third_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(NEW_Third_4, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice67', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.fontSize(14);
    doc.image(FPI_CSA, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num, 270, 260, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 270, 300, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice18a', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');
    doc.fontSize(15);
    doc.image(FPI_CMEA, 0, 0, {
      width: a4pageWidth
    });
    doc.text("12" + " " + "12" + " " + "2012", 80, 75, {
      width: 380,
      align: 'left',
      characterSpacing: 7.3
    });
    doc.text("123", 190, 233, {
      width: 380,
      align: 'left'
    });
    doc.text("456", 190, 270, {
      width: 380,
      align: 'left'
    });
    doc.text("789", 190, 307, {
      width: 380,
      align: 'left'
    });
    doc.text("123", 265, 415, {
      width: 380,
      align: 'left'
    });
    doc.text("456", 265, 450, {
      width: 380,
      align: 'left'
    });
    doc.text("789", 95, 485, {
      width: 380,
      align: 'left'
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice19a', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    doc.font('Courier');
    doc.image(FPI_Self, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("asd", 165, 114, {
      width: 380,
      align: 'left'
    });
    doc.text("zxc", 165, 127, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(15);
    doc.text("1", 150, 220, {
      width: 380,
      align: 'left'
    });
    doc.text("2", 150, 244, {
      width: 380,
      align: 'left'
    });
    doc.text("3", 150, 268, {
      width: 380,
      align: 'left'
    });
    doc.text("4", 150, 292, {
      width: 380,
      align: 'left'
    });
    doc.text("5", 310, 220, {
      width: 380,
      align: 'left'
    });
    doc.text("6", 310, 244, {
      width: 380,
      align: 'left'
    });
    doc.text("7", 310, 268, {
      width: 380,
      align: 'left'
    });
    doc.text("8", 310, 292, {
      width: 380,
      align: 'left'
    });

    doc.text("1", 150, 390, {
      width: 380,
      align: 'left'
    });
    doc.text("2", 150, 414, {
      width: 380,
      align: 'left'
    });
    doc.text("3", 150, 438, {
      width: 380,
      align: 'left'
    });
    doc.text("4", 150, 462, {
      width: 380,
      align: 'left'
    });
    doc.text("5", 310, 390, {
      width: 380,
      align: 'left'
    });
    doc.text("6", 310, 414, {
      width: 380,
      align: 'left'
    });
    doc.text("7", 310, 438, {
      width: 380,
      align: 'left'
    });
    doc.text("8", 310, 462, {
      width: 380,
      align: 'left'
    });


    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice81', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_RPBWS_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_8, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice82', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_RPBWS_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_8, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice83', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_RPBWS_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_8, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice84', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_CCCA_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_CCCA_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("v", 365, 373, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 225, 223, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 225, 248, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 225, 323, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 225, 347, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 220, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_name, 220, 503, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_addr, 220, 547, {
      width: 380,
      align: 'left',
      lineGap: 5
    });
    doc.text(pf.contactnum, 220, 605, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 630, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_CCCA_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_CCCA_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_CCCA_5, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice85', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_AOTPAP_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 225, 333, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 225, 402, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 225, 450, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 225, 495, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 225, 640, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 225, 660, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 225, 683, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_4, 225, 706, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_5, 225, 728, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_6, 225, 750, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_7, 225, 773, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_8, 225, 795, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_9, 523, 662, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_AOTPAP_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("Hanbury IFA Corporation Limited", 220, 670, {
      width: 380,
      align: 'left'
    });
    doc.text("+886 2 2758 2239", 220, 693, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 220, 716, {
      width: 380,
      align: 'left'
    });
    doc.text("5F., No. 400, Sec.1, Keelung Road., Xinyi District., Taipei City 11051, Taiwan", 220, 738, {
      width: 380,
      align: 'left',
      lineGap: 8
    });

    doc.addPage();
    doc.image(FPI_AOTPAP_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_AOTPAP_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_AOTPAP_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_AOTPAP_6, 0, 0, {
      width: a4pageWidth
    });


    return Api.returnPDF(doc);
  }
});
Api.addRoute('pdfafterservice86', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_RPBWS_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.policy_firstperson, 220, 310, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 220, 355, {
      width: 320,
      align: 'left',
      lineGap: 10
    });
    doc.fontSize(12);
    doc.text(pf.insertedBy, 220, 438, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 220, 465, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 488, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 512, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 220, 555, {
      width: 380,
      align: 'left'
    });
    doc.text("Personal Use", 220, 660, {
      width: 380,
      align: 'left'
    });


    doc.addPage();
    doc.image(FPI_RPBWS_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("V", 265, 295, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_RPBWS_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text("V", 222, 148, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_firstperson, 225, 283, {
      width: 380,
      align: 'left'
    });


    doc.addPage();
    doc.image(FPI_RPBWS_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_8, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});
Api.addRoute('pdfafterservice87', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_RPBWS_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.policy_firstperson, 220, 310, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.addr1_eng, 220, 355, {
      width: 320,
      align: 'left',
      lineGap: 10
    });
    doc.fontSize(12);
    doc.text(pf.insertedBy, 220, 438, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 220, 465, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 488, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 512, {
      width: 380,
      align: 'left'
    });
    doc.text("info@hanburyifa.com", 220, 555, {
      width: 380,
      align: 'left'
    });
    doc.text("Personal Use", 220, 660, {
      width: 380,
      align: 'left'
    });


    doc.addPage();
    doc.image(FPI_RPBWS_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text("V", 265, 295, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FPI_RPBWS_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text("V", 222, 148, {
      width: 380,
      align: 'left'
    });
    doc.text("Wells Fargo Bank", 225, 222, {
      width: 380,
      align: 'left'
    });
    doc.text("420 Montgomery, San Francisco, CA 94104 U.S.A", 225, 245, {
      width: 380,
      align: 'left'
    });
    doc.text("Security Life of Denver", 225, 283, {
      width: 380,
      align: 'left'
    });
    doc.text("413-2697-228", 225, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("WFBIUS6S", 225, 355, {
      width: 380,
      align: 'left'
    });
    doc.text("12100248", 225, 440, {
      width: 380,
      align: 'left'
    });
    doc.text("Policy Number: " + pf.custom_1, 50, 700, {
      width: 380,
      align: 'left'
    });
    doc.text("Policy Holder: " + pf.custom_2, 50, 713, {
      width: 380,
      align: 'left'
    });
    doc.text("Amount: " + pf.custom_3, 50, 726, {
      width: 380,
      align: 'left'
    });


    doc.addPage();
    doc.image(FPI_RPBWS_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_RPBWS_8, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice88', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(update_54_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 220, 368, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 417, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.email, 220, 710, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);

    doc.addPage();
    doc.image(update_54_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(update_54_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(update_54_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(update_54_5, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice89', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(signature_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(signature_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(signature_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(signature_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(signature_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(signature_6, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice90', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(policy_92_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.account_num + "/" + pf.account_num2, 220, 360, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 380, {
      width: 380,
      align: 'left'
    });
    doc.text("V", 175, 425, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 588, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(policy_92_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(policy_92_13, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice68', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_68_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice69', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_68_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice70', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_70_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 150, 190, {
      width: 380,
      align: 'left'
    });
    Api.strChtEngMix(doc, pf.last_name, 370, 190, 12);
    doc.font('Courier');
    doc.text(pf.birthday, 150, 225, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.contactnum, 185, 255, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.email, 150, 280, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.addr1_eng, 150, 300, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 90, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_6, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 635, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 110, 420, {
      width: 380,
      align: 'left'
    });

    doc.fontSize(12);
    doc.addPage();
    doc.image(gt_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 255, 425, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 190, 115, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.birthday, 375, 180, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 375, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 365, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_8, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 230, 420, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_11, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 200, 460, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 170, 240, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_14, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 590, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_16, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 210, 310, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(GT_FNAC, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 260, 430, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 260, 595, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 260, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 630, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 620, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 500, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_5, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 660, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_6, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(7);
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 215, 205, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text("M189", 250, 220, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 100, 510, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 190, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_8, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 660, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_12, 0, 0, {
      width: a4pageWidth
    });

    doc.fontSize(9);
    doc.image(TOPUP_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 274, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 302, {
      width: 380,
      align: 'left'
    });
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 220, 370, {
      width: 380,
      align: 'left'
    });
    doc.text("9800042", 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text("investments@grandtag.com", 220, 504, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.last_name, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.first_name, 220, 260, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_passport, 220, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 340, {
      width: 180,
      align: 'left'
    });
    doc.text(pf.apply_telephone, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_cellphone, 220, 600, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 625, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 665, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.fpi_now_same_payment, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.text("1", 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text("2", 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HONG KONG", 350, 448, {
      width: 380,
      align: 'left'
    });
    doc.text("HONG KONG", 350, 484, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 623, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_14, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_16, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_18, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_19, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_20, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_21, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 220, 278, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 220, 303, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 218, 328, {
      width: 380,
      align: 'left',
      characterSpacing: 9.8
    });
    doc.text(pf.creditcard_term_month, 218, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.creditcard_term_year, 288, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.custom_2, 220, 378, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 447, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 575, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 695, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_22, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_23, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfafterservice71', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);
    var queryVar, sortVar = {},
      pageVar = {};
    queryVar = this.queryParams || {};
    var pf = queryVar;

    doc.font('Courier');
    doc.image(FPI_70_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FPI_70_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(BSO, 0, 0, {
      width: a4pageWidth
    });
    doc.fontSize(12);
    doc.text("HSBC", 170, 165, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBC Main Building 1 Queen's Road Central, HK", 95, 183, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.hsbc_account_num, 50, 268, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 50, 308, {
      width: 380,
      align: 'left'
    });
    doc.text("HSBCHKHHHKH", 340, 308, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 50, 340, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment, 140, 425, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(9);
    doc.text(pf.fpi_now_same_payment_words, 340, 425, {
      width: 200,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.check, 50, 464, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 150, 710, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.account_num2, 50, 805, {
      width: 380,
      align: 'left',
      characterSpacing: 14
    });

    doc.addPage();
    doc.image(NEW_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 223, 307, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 223, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 223, 353, {
      width: 380,
      align: 'left',
      characterSpacing: 8.5
    });
    doc.text(pf.creditcard_term_month, 223, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.creditcard_term_year, 300, 377, {
      width: 380,
      align: 'left',
      characterSpacing: 11.5
    });
    doc.text(pf.fpi_now_same_payment, 223, 400, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.fpi_now_same_payment_words, 223, 420, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.policy_currency_type, 223, 440, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 223, 463, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 223, 533, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 223, 560, {
      width: 320,
      align: 'left'
    });
    doc.font('Courier');
    doc.text(pf.contactnum, 223, 635, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 223, 658, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(NEW_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 150, 190, {
      width: 380,
      align: 'left'
    });
    Api.strChtEngMix(doc, pf.last_name, 370, 190, 12);
    doc.font('Courier');
    doc.text(pf.birthday, 150, 225, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.contactnum, 185, 255, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(8.5);
    doc.text(pf.email, 150, 280, {
      width: 380,
      align: 'left'
    });
    doc.fontSize(12);
    doc.text(pf.addr1_eng, 150, 300, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 90, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(FNA_6, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 100, 635, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(FNA_7, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 110, 420, {
      width: 380,
      align: 'left'
    });

    doc.fontSize(12);
    doc.addPage();
    doc.image(gt_1, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 255, 425, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.image(gt_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 190, 115, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.birthday, 375, 180, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 375, 330, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 190, 365, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_8, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 230, 420, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_11, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 200, 460, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 170, 240, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_14, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 590, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(gt_16, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 210, 310, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(gt_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(GT_FNAC, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 260, 430, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.insertedBy, 260, 595, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 260, 640, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(IFS_P_U_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_2, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_3, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_4, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(IFS_P_U_12, 0, 0, {
      width: a4pageWidth
    });

    doc.fontSize(9);
    doc.image(TOPUP_1, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_2, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.name_eng, 220, 274, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 302, {
      width: 380,
      align: 'left'
    });
    doc.text("Grandtag Financial Consultancy & Insurance Brokers Limited", 220, 370, {
      width: 380,
      align: 'left'
    });
    doc.text("9800042", 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text("investments@grandtag.com", 220, 504, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_3, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.last_name, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.first_name, 220, 260, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_passport, 220, 305, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 340, {
      width: 180,
      align: 'left'
    });
    doc.text(pf.apply_telephone, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.apply_cellphone, 220, 600, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 625, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_1, 220, 665, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_4, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.fpi_now_same_payment, 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_2, 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.text("1", 220, 225, {
      width: 380,
      align: 'left'
    });
    doc.text("2", 220, 252, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_5, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_6, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_7, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_8, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_9, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_10, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_11, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_12, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_13, 0, 0, {
      width: a4pageWidth
    });
    doc.text("HONG KONG", 350, 448, {
      width: 380,
      align: 'left'
    });
    doc.text("HONG KONG", 350, 484, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 623, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_14, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_15, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_16, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_17, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_18, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_19, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_20, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_21, 0, 0, {
      width: a4pageWidth
    });
    doc.text(pf.creditcard_bank, 220, 278, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_bankname, 220, 303, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.creditcard_num, 218, 328, {
      width: 380,
      align: 'left',
      characterSpacing: 9.8
    });
    doc.text(pf.creditcard_term_month, 218, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.creditcard_term_year, 288, 352, {
      width: 380,
      align: 'left',
      characterSpacing: 12
    });
    doc.text(pf.custom_2, 220, 378, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.custom_3, 220, 398, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.account_num, 220, 447, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.name_eng, 220, 550, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.addr1_eng, 220, 575, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.contactnum, 220, 650, {
      width: 380,
      align: 'left'
    });
    doc.text(pf.email, 220, 695, {
      width: 380,
      align: 'left'
    });

    doc.addPage();
    doc.image(TOPUP_22, 0, 0, {
      width: a4pageWidth
    });

    doc.addPage();
    doc.image(TOPUP_23, 0, 0, {
      width: a4pageWidth
    });

    return Api.returnPDF(doc);
  }
});

Api.addRoute('pdfportfolio', {
  authRequired: false
}, {
  get: function () {
    // var doc;
    var doc = new PDFDocument({
      size: 'A4',
      margin: 1
    });
    doc.registerFont('BiauKai', BiauKaiTTF);
    doc.registerFont('msjh', msjhTTF);
    // doc.registerFont('myarial', arialTTF);

    Api.pdfLayout(doc); // 列印的樣版

    // 看要填什麼 在這邊弄
    var queryVar = this.queryParams || {};
    // console.log(queryVar);

    if (queryVar.type == "1") {
      //'check-invest-generate-pdf': [ 'YqNsGWepMqT2PsLMj', '9WS8cqNQ4WjWuoQfb', 'WmFQeGrF9QFcJLBqd' ] }

      var id = "";
      var arrPid = [];
      if (Array.isArray(queryVar.pid)) {
        id = queryVar.pid[0];
        arrPid = queryVar.pid;
      } else {
        id = queryVar.pid;
        arrPid.push(id);
      }

      var data = Portfolios.findOne(id);

      var name = data.name_cht;
      doc.fontSize(20);
      doc.font('msjh');
      doc.text("投資人：" + name, 56, 150, {
        width: 280,
        align: 'left'
      });

      doc.fontSize(12);
      doc.text("表單日期：", 356, 160, {
        width: 280,
        align: 'left'
      });

      var date = currentdate2();
      if (!!queryVar.printdate) {
        date = queryVar.printdate;
      }

      doc.font('Helvetica');
      doc.text(date, 416, 160 + 4, {
        width: 280,
        align: 'left'
      });

      var doc_y = 200;
      for (var i in arrPid) {
        if (i % 2 == 0 && i != 0) {
          doc.addPage({
            size: 'A4',
            margin: 1
          });
          Api.pdfLayout(doc);
          if (i == 0)
            doc_y = 200; // 第一個有名字 比較下面
          else
            doc_y = 150; // 其他就正常了
        }
        var obj = Portfolios.findOne(arrPid[i]);
        // console.log(queryVar);

        var review_id = Number(obj.review_id);

        doc.font('Helvetica');
        doc.fontSize(16);
        // doc.font('Times-Roman');
        var ii = Number(i) + 1;
        doc.text("(" + ii + ") " + obj.uid, 56, doc_y, {
          width: 280,
          align: 'left'
        });
        doc_y += 25;

        doc.font('msjh');
        doc.text(obj.product1, 56, doc_y, {
          width: 280,
          align: 'left'
        });
        doc.font('Helvetica');
        doc.text(obj.product2, 56 + 200, doc_y + 5, {
          width: 280,
          align: 'left'
        });


        var arrText1 = [];
        var arrText2 = [];
        var arrField2 = [];
        // doc.moveTo(0, 20).lineTo(100, 160);
        // doc.moveTo(56, doc_y+2).lineTo(300, doc_y+2).stroke();

        if (obj.product1_id == "1" || obj.product1_id == "2") { // 房產 安養院
          arrText1 = ["帳戶號碼", "帳戶生效日", "投資金額", "最近一次配息", "累積已配息金額", "本益比", "預計下次配息", "預計下次配息金額"];
          arrText2 = ["account_num", "start_date", "prod_money", "最近一次配息", "累積已配息金額", "本益比", "預計下次配息", "預計下次配息金額"];

        } else if (obj.product1_id == "3") { // 保險
          // 看起來不用?
          // arrText1 = ["帳戶號碼", "帳戶生效日", "帳戶到期日", "年期", "繳款狀況", "基金配置", "供款總額", "提款總額", "帳戶淨值", "本金淨值比"];
          // arrText2 = ["account_num", "start_date", "prod_money", "最近一次配息", "累積已配息金額", "本益比", "預計下次配息", "預計下次配息金額"];
        } else if (obj.product1_id == "5") { // 投資
          arrText1 = ["帳戶號碼", "帳戶生效日", "帳戶到期日", "年期", "繳款狀況", "基金配置", "供款總額", "提款總額", "帳戶淨值", "本金淨值比"];
          arrText2 = ["account_num", "start_date", "帳戶到期日", "fpi_period_year", "fpi_pay_status", "基金配置", "供款總額", "提款總額", "帳戶淨值", "本金淨值比"];
        }

        for (var j in arrText1) {
          doc_y += 20;
          doc.moveTo(56, doc_y + 2).lineTo(450, doc_y + 2).stroke(); // 字上面的
          doc.font('msjh');
          doc.text(arrText1[j], 56, doc_y, {
            width: 280,
            align: 'left'
          });

          doc.font('Helvetica');
          var str = ""
          if (!!obj[arrText2[j]] && typeof str != "undefined") {
            str = obj[arrText2[j]];
          }

          // doc.text(str, 56+200, doc_y+5, { width: 280, align: 'left'} );

          // Api.strChtEngMix = function(doc, str, x, y, fontsize){
          Api.strChtEngMix(doc, str, 56 + 200, doc_y + 5, 16);
        }
        doc_y += 40;
      }
    }
    return Api.returnPDF(doc);
  }
});

Api.addRoute('addreng', {
  authRequired: false
}, {
  get: function () {

    var myArray = Clients.find().fetch();
    for (var i in myArray) {

      var data = myArray[i];
      var addr = data.addr1_eng;
      var id = data._id;

      console.log("////// 第 " + i + " 筆 ////////");
      console.log(addr);
      var addr_eng = "";
      var arrAddr1 = Addr1.find().fetch();

      for (var j in arrAddr1) {
        if (arrAddr1[j].cht == addr.substr(0, arrAddr1[j].cht.length)) { // ○○市○○區
          addr = addr.substr(arrAddr1[j].cht.length);
          addr_eng = arrAddr1[j].eng;

          // console.log(addr);
          // console.log(addr_eng);

          var arrAddr2 = Addr2.find().fetch();
          for (var k in arrAddr2) {
            if (arrAddr2[k].cht == addr.substr(0, arrAddr2[k].cht.length)) { // ○○里 或 ○○村
              addr = addr.substr(arrAddr2[k].cht.length);
              addr_eng = arrAddr2[k].eng + ", " + addr_eng;

              // console.log(addr);
              // console.log(addr_eng);

              var neb = addr.indexOf("鄰");
              if (neb == 1 || neb == 2) {
                neb_num = addr.substring(0, neb);

                addr = addr.substr(neb + 1);
                addr_eng = "Neighborhood " + neb_num + ", " + addr_eng;

                // console.log(addr);
                // console.log(addr_eng);
              }
              break;
            }
          }
          var arrAddr3 = Addr3.find().fetch();
          for (var l in arrAddr3) {
            if (arrAddr3[l].cht == addr.substr(0, arrAddr3[l].cht.length)) { // 路
              addr = addr.substr(arrAddr3[l].cht.length);
              // addr_eng = arrAddr3[l].eng + ", " + addr_eng;
              var tmp_eng = arrAddr3[l].eng;

              if (addr.indexOf("東") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " E.";
              } else if (addr.indexOf("西") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " W.";
              } else if (addr.indexOf("南") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " S.";
              } else if (addr.indexOf("北") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " N.";
              }

              if (addr.indexOf("路") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " Rd.";
              } else if (addr.indexOf("街") == 0) {
                addr = addr.substr(1);
                tmp_eng = tmp_eng + " St.";
              }
              addr_eng = tmp_eng + ", " + addr_eng;

              // console.log(addr);
              // console.log(addr_eng);
              break;
            }
          }

          var tmp_eng = "";
          if (addr.indexOf("一") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 1st";
          } else if (addr.indexOf("二") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 2nd";
          } else if (addr.indexOf("三") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 3rd";
          } else if (addr.indexOf("四") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 4th";
          } else if (addr.indexOf("五") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 5th";
          } else if (addr.indexOf("六") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 6th";
          } else if (addr.indexOf("七") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 7th";
          } else if (addr.indexOf("八") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 8th";
          } else if (addr.indexOf("九") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 9th";
          } else if (addr.indexOf("十") == 0) {
            addr = addr.substr(1);
            tmp_eng = " 10th";
          }

          if (addr.indexOf("路") == 0) {
            addr = addr.substr(1);
            tmp_eng = "Rd." + tmp_eng;
          } else if (addr.indexOf("街") == 0) {
            addr = addr.substr(1);
            tmp_eng = "St." + tmp_eng;
          } else if (addr.indexOf("段") == 0) {
            addr = addr.substr(1);
            tmp_eng = "Sec." + tmp_eng;
          }
          if (tmp_eng)
            addr_eng = tmp_eng + ", " + addr_eng;

          // 這邊後，就剩下巷/弄/號/樓/之/室了

          var tmp_eng = "";
          if (addr.indexOf("段") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("段")); // 數字
            addr = addr.substr(addr.indexOf("段") + 1);
            tmp_eng = "Sec. " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }
          if (addr.indexOf("巷") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("巷")); // 數字
            addr = addr.substr(addr.indexOf("巷") + 1);
            tmp_eng = "Ln. " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }
          if (addr.indexOf("弄") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("弄")); // 數字
            addr = addr.substr(addr.indexOf("弄") + 1);
            tmp_eng = "Aly. " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }
          if (addr.indexOf("衖") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("衖")); // 數字
            addr = addr.substr(addr.indexOf("衖") + 1);
            tmp_eng = "Sub-Alley " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }


          var tmp_eng = "";
          if (addr.indexOf("巷") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("巷")); // 數字
            addr = addr.substr(addr.indexOf("巷") + 1);
            tmp_eng = "Ln. " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }

          var isDash = 0;
          if (addr.indexOf("之") != -1) {
            addr = addr.replace("之", "-");
            isDash = 1;
          }
          if (addr.indexOf("號") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("號")); // 數字
            addr = addr.substr(addr.indexOf("號") + 1);
            tmp_eng = "No." + tmp_eng;

            if (isDash && addr.indexOf("樓") == -1) {
              tmp_eng = tmp_eng + addr;
            }

            addr_eng = tmp_eng + ", " + addr_eng;
          }
          if (addr.indexOf("樓") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("樓")); // 數字
            addr = addr.substr(addr.indexOf("樓") + 1);
            tmp_eng = tmp_eng + "F.";


            if (isDash && addr.indexOf("室") == -1) {
              tmp_eng = tmp_eng + addr;
            }

            addr_eng = tmp_eng + ", " + addr_eng;
          }
          if (addr.indexOf("室") != -1) {
            tmp_eng = addr.substr(0, addr.indexOf("室")); // 數字
            addr = addr.substr(addr.indexOf("室") + 1);
            tmp_eng = "Rm. " + tmp_eng;

            addr_eng = tmp_eng + ", " + addr_eng;
          }

          break;
        }
      } // 這邊出來

      console.log(addr);
      console.log(addr_eng);
      if (addr_eng) {
        Clients.update({
          _id: id
        }, {
          $set: {
            addr1_eng: addr_eng
          }
        });
      }

    }
    console.log("addr2 ok");
    return "";
  }
});