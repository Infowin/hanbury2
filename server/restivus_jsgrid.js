Api.addRoute('emaillist', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        delete queryVar.page; // 第二頁就會有傳入

        /*    "total_count": 7408,
             "incomplete_results": false,
             "items": [*/

        if (!!queryVar.q) {
            var searchValue = queryVar.q;
            delete queryVar.q;
            queryVar = {
                $and: [{
                    $or: [{
                        name_cht: {
                            $regex: ".*" + searchValue + ".*",
                            $options: 'i'
                        }
                    },
                    {
                        name_eng: {
                            $regex: ".*" + searchValue + ".*",
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: ".*" + searchValue + ".*",
                            $options: 'i'
                        }
                    },
                    ]
                },
                    queryVar,
                ]
            };

        } else {
            delete queryVar.q;
        }

        return {
            total_count: Clients.find(queryVar).count(),
            incomplete_results: false,
            // incomplete_results: true,
            items: Clients.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit,
                fields: {
                    name_cht: 1,
                    name_eng: 1,
                    email: 1
                }
            }).fetch(),
        }
    }
});
Api.addRoute('contactlist', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        delete queryVar.page; // 第二頁就會有傳入

        /*    "total_count": 7408,
             "incomplete_results": false,
             "items": [*/

        if (!!queryVar.q) {
            var searchValue = queryVar.q;
            delete queryVar.q;
            queryVar = {
                $and: [{
                    $or: [
                        // {name_cht: {$regex : ".*"+searchValue+".*"}},
                        // {name_eng: {$regex : ".*"+searchValue+".*"}},
                        {
                            name: {
                                $regex: ".*" + searchValue + ".*",
                                $options: 'i'
                            }
                        },
                    ]
                },
                    queryVar,
                ]
            };

        } else {
            delete queryVar.q;
        }

        return {
            total_count: Contact1.find(queryVar).count(),
            incomplete_results: false,
            // incomplete_results: true,
            items: Contact1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit,
                // fields: {name_cht: 1, name_eng: 1, email: 1}
            }).fetch(),
        }
    }
});

Api.addRoute('email', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);


        if (!!queryVar.searchtext) {
            var searchValue = queryVar.searchtext;
            delete queryVar.searchtext;
            queryVar = {
                $and: [{
                    context: {
                        $regex: ".*" + searchValue + ".*"
                    }
                },
                    queryVar,
                ]
            };

        }

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log(Emails.find(queryVar).count());
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        /*
              delete queryVar.page;
    
              if(!!queryVar.q){
                var searchValue = queryVar.q;
                delete queryVar.q;
                queryVar = {
                  $and: [
                    {$or: [
                      {name_cht: {$regex : ".*"+searchValue+".*"}},
                      {name_eng: {$regex : ".*"+searchValue+".*"}},
                      {email: {$regex : ".*"+searchValue+".*"}},
                    ]},
                    queryVar,
                  ]
                };
    
              }
              else{
                delete queryVar.q;
              }
            */
        return {
            data: Emails.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Emails.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Email insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "V" + funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Emails.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Email update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Emails.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Email DELETE");
            // console.log(this.bodyParams);
            if (Emails.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('afterservice', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest afterservice GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        let sortTmp = sortVar;

        // insertedAtYY = (new Date(queryVar.insertedAt)).getFullYear();
        // sortVar = {insertedAtYY: 1, insertedAt: -1};
        // sortVar = {
        //     isMark: -1
        // };
        // if (queryVar.pf6_status == "1") {
        //   console.log("pf6_status");
        //   console.log(queryVar);
        //   queryVar = {
        //     $and: [{ "status": { "$ne": "1" }},
        //       queryVar,
        //     ]
        //   };
        // }

        if (!!sortTmp.insertedAt) {
            sortVar = Object.assign(sortVar, sortTmp);
        }

        if (!!sortTmp.updatedAt1) {
            sortVar = Object.assign(sortVar, sortTmp);
        }

        if (!!queryVar.pf6_status) {
            delete queryVar.pf6_status;

            queryVar = {
                $and: [{
                    "status": {
                        "$ne": "2"
                    }
                },
                    queryVar,
                ]
            };
        } else {
            delete queryVar.pf6_status;
        }

        if (!!queryVar.findData) {
            var searchValue = queryVar.findData;
            delete queryVar.findData;

            // console.log(searchValue);
            queryVar = {
                $and: [{
                    $or: [
                        // {name_cht: {$regex : ".*"+searchValue+".*", $options: '-i'}},
                        {
                            name_eng: {
                                $regex: searchValue,
                                $options: 'i'
                            }
                        },
                        {
                            'portfolio.name_cht': {
                                $regex: searchValue,
                                $options: 'i'
                            }
                        },
                    ]
                },
                    queryVar,
                ]
            };
        } else {
            delete queryVar.findData;
        }

        // console.log("after sortVar: ")
        // console.log(sortVar);

        var arr_after = Afterservice.find(queryVar, {
            sort: sortVar,
            // skip: pageVar.skip,
            // limit: pageVar.limit
        }).fetch()

        // arr_after = arr_after.sort(function (a, b) {

        //     var time_a;
        //     var time_b;

        //     if (a.updatedAt1 == undefined) {
        //         time_a = new Date();
        //     } else {
        //         time_a = new Date(a.updatedAt1);
        //     }
        //     if (b.updatedAt1 == undefined) {
        //         time_b = new Date();
        //     } else {
        //         time_b = new Date(b.updatedAt1);
        //     }
        //     // a = a.split('/').join('');
        //     // b = b.split('/').join('');
        //     return time_a > time_b ? 1 : time_a < time_b ? -1 : 0;
        // });

        if (!!queryVar.p_agent_id) {
            queryVar["portfolio.agent_id"] = queryVar.p_agent_id;
            delete queryVar.p_agent_id;

        }

        var slice_length = pageVar.skip + pageVar.limit;
        var ret_arr = arr_after.slice(pageVar.skip, slice_length);

        return {
            data: ret_arr,
            // data: Afterservice.find(queryVar, {
            //     sort: sortVar,
            //     skip: pageVar.skip,
            //     limit: pageVar.limit
            // }).fetch(),
            // data: arr_after,
            // itemsCount: Afterservice.find(queryVar).count()
            itemsCount: arr_after.length
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Afterservice insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "S" + funcStrPad(getNextSequence('afterservice'), 6);
            // params.uid = "A"+funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Afterservice.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Afterservice update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            var isMark = params.isMark;
            delete params.isMark;

            // checkbox 要改 null 才能work
            // console.log('res', isMark)
            if (isMark == 'true') {
                params.isMark = 'true';
            } else {
                params.isMark = null;
            }
            params.insertedAt = new Date(params.insertedAt || null);

            if (Afterservice.update(id, {
                $set: params
            })) {
                params._id = id;
                // console.log('in param', params)
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Afterservice DELETE");
            // console.log(this.bodyParams);
            if (Afterservice.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('reorg_afterservice', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        //console.log("Start. building agent_id in AS");
        Afterservice.find().forEach(function (u) {
            if (!!u.portfolio_id) {
                var p = Portfolios.findOne(u.portfolio_id);
                var agent_id = p.agent_id;
                //console.log(agent_id);
                Afterservice.update(u, {
                    $set: {
                        agent_id: agent_id
                    }
                });
                // console.log("client "+ u.name_cht +" birthday: "+ u.birthday + " to: "+ yyyy + " "+ mm + " "+ dd );
            }
        });
        //console.log("Done. building agent_id in AS");

        return {
            data: "success",
            // itemsCount: Contact1.find(queryVar).count()
        }
    },
});
Api.addRoute('reorg_portfolio', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        /*
    
            "product1_id" : "siqmGfLF6BKXZd67g",
            "product1_text" : "保險",
            "product2_id" : "dcbgshQPP9mcZsxvZ",
            "product2_text" : "指數型萬能壽險",
            "product3_id" : "rz2jqP5MZQFT5NC5k",
            "product3_text" : "Voya 指數型 - 全球選擇",
            "product4_id" : "DSTRj6Y62wkWFuveo",
            "product4_text" : "Voya Indexed UL - Global Choice (3Mark)",
            */
        //console.log("Start. building product in Porofolios");
        Portfolios.find({
            product1_id: "siqmGfLF6BKXZd67g"
        }).forEach(function (u) {

            if (!!u) {
                // var p = Portfolios.findOne(u.portfolio_id);
                // console.log(agent_id);
                var a = Agents.findOne({
                    name: u.agent_text
                });
                var p = Product4.findOne({
                    name_eng: u.product4_text
                });
                //console.log(u);
                //console.log(a);
                // if(typeof a == "undefined"){
                //   a = Agents.findOne({name: "Aaron"});
                // }
                Portfolios.update(u, {
                    $set: {
                        product1_id: p.product1_id,
                        product2_id: p.product2_id,
                        product3_id: p.product3_id,
                        product4_id: p.product4_id,
                        // agent_id: a._id,
                    }
                });
                // console.log("client "+ u.name_cht +" birthday: "+ u.birthday + " to: "+ yyyy + " "+ mm + " "+ dd );
            }
        });
        //console.log("Done. building agent_id in AS");

        return {
            data: "success",
            // itemsCount: Contact1.find(queryVar).count()
        }
    },
});

Api.addRoute('contact1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Contact1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Contact1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact1 insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            // params.uid = "A"+funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Contact1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact1 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Contact1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact1 DELETE");
            // console.log(this.bodyParams);
            if (Contact1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('contact2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Contact2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Contact2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact2 insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            // params.uid = "A"+funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Contact2.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact2 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Contact2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Contact2 DELETE");
            // console.log(this.bodyParams);
            if (Contact2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('nopdfform', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Nopdfform.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Nopdfform.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Nopdfform.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Nopdfform.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Nopdfform.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('hrform_management', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Hrform_management.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Hrform_management.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Hrform_management.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Hrform_management.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Hrform_management.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('blank_form', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Blank_form.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Blank_form.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Blank_form.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Blank_form.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Blank_form.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('portfoliostatus', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: PortfolioStatus.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: PortfolioStatus.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (PortfolioStatus.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (PortfolioStatus.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (PortfolioStatus.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('fin_sales', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Fin_sales.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Fin_sales.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Fin_sales.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // params.targets1 = Number(params.targets1) || 0;
            // params.targets2 = 0;
            //console.log(params);
            if (Fin_sales.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Fin_sales.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('fin_sales1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Fin_sales1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Fin_sales1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Fin_sales1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Fin_sales1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Fin_sales1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('fin_sales_year', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Fin_sales_year.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Fin_sales_year.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Fin_sales_year.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Fin_sales_year.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Fin_sales_year.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('sales_level', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Sales_level.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Sales_level.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Nopdfform insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Sales_level.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Sales_level.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Sales_level.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('news', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: News.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: News.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest News insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "A" + funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (News.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest News update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (News.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest News DELETE");
            // console.log(this.bodyParams);
            if (News.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('calendar', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Calendar.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Calendar.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Calendar insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "V" + funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Calendar.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Calendar update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Calendar.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Calendar DELETE");
            // console.log(this.bodyParams);
            if (Calendar.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('interview', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Interviews.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Interviews.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Interviews insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "V" + funcStrPad(getNextSequence('interview'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Interviews.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Interviews update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Interviews.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Interviews DELETE");
            // console.log(this.bodyParams);
            if (Interviews.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

//data.template_id == "2"
Api.addRoute('payreminder', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Portfolios GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);


        let pay_status_no = queryVar.pay_status_no;
        delete queryVar.pay_status_no;


        queryVar.template_id == "2";
        // 現在的p 有所有的voya
        var p = Portfolios.find(queryVar, {
            sort: sortVar,
            skip: pageVar.skip,
            limit: pageVar.limit
        }).fetch();
        var d = new Date();
        var next_mm = d.getMonth() + 2 //下個月份
        var next_yy = d.getFullYear() //今年
        var arr_new_p = []; // 判斷這個月或下個月的單 是不是沒有繳費的, 有的話 就把這一筆 放到一個陣列
        for (var i = 0; i < p.length; i++) {
            var arr_pay = p[i].arrPaymentRecord;
            if (!!arr_pay) { //有設定 規劃繳費日期 規劃繳費金額
                for (var j = 0; j < arr_pay.length; j++) {
                    //有規劃繳費日期&&金額 && 沒有實際繳費日期、金額 (未繳費)
                    if (!!arr_pay[j].pmr_payment1date && !!arr_pay[j].pmr_payment1money) {
                        if (!arr_pay[j].pmr_payment2money || commaToNumber(arr_pay[j].pmr_payment2money) < commaToNumber(arr_pay[j].pmr_payment1money) && commaToNumber(arr_pay[j].pmr_payment2money) != "0") {
                            // if (!arr_pay[j].pmr_payment2date || !arr_pay[j].pmr_payment2money || commaToNumber(arr_pay[j].pmr_payment2money) < commaToNumber(arr_pay[j].pmr_payment1money)) {
                            var pay_dd = Number((arr_pay[j].pmr_payment1date).split("/")[2]) //規劃繳費月份
                            var pay_mm = Number((arr_pay[j].pmr_payment1date).split("/")[1]) //規劃繳費月份
                            var pay_yy = Number((arr_pay[j].pmr_payment1date).split("/")[0]) //規劃繳費年份
                            if (pay_yy < next_yy) { //規劃年份小於今年(目前不做)
                                // if (pay_mm == next_mm - 1) {//規劃繳費月份=這個月
                                // var obj = arr_pay[j];
                                // obj.p_id = p[i]._id;
                                // obj.pay_yy = pay_yy;
                                // obj.pay_mm = pay_mm;
                                // obj.pay_dd = pay_dd;
                                // obj.p_account_num = p[i].account_num;
                                // obj.p_name_cht = p[i].name_cht;
                                // obj.p_name_eng = p[i].name_eng;
                                // obj.p_agent_id = p[i].agent_id;
                                // obj.p_agent_text = p[i].agent_text;
                                // obj.pmr_ps = p[i].pmr_ps;
                                // arr_new_p.push(obj);
                                // break;
                            } else if (pay_yy == next_yy && pay_mm >= next_mm - 1 && pay_mm <= next_mm) { //規劃年份等於今年&&規劃月份大於等於這個月&&小於等於下個月
                                var obj = arr_pay[j];
                                obj.p_id = p[i]._id;
                                obj.pay_yy = pay_yy;
                                obj.pay_mm = pay_mm;
                                obj.pay_dd = pay_dd;
                                obj.p_account_num = p[i].account_num;
                                obj.p_name_cht = p[i].name_cht;
                                obj.p_name_eng = p[i].name_eng;
                                obj.p_agent_id = p[i].agent_id;
                                obj.p_agent_text = p[i].agent_text;
                                // obj.pmr_ps = p[i].pmr_ps;
                                arr_new_p.push(obj);
                                // break;
                            }
                        }
                    }
                }
            }
        }

        // for (var i = 0; i < p.length; i++) {
        //   var arr_pay = p[i].arrPaymentRecord;
        //   if (!!arr_pay) {//有設定 規劃繳費日期 規劃繳費金額
        //     for (var j = 0; j < arr_pay.length; j++) {
        //       //有規劃繳費日期 規劃繳費金額 && 沒有實際繳費日期 實際繳費金額 (未繳費)
        //       if (!!arr_pay[j].pmr_payment1date && !!arr_pay[j].pmr_payment1money && !arr_pay[j].pmr_payment2date && !arr_pay[j].pmr_payment2money && arr_pay[j].pmr_payment2money != arr_pay[j].pmr_payment1money) {
        //         var pay_mm = Number((arr_pay[j].pmr_payment1date).split("/")[1])//規劃繳費月份
        //         var pay_yy = Number((arr_pay[j].pmr_payment1date).split("/")[0])//規劃繳費年份

        //         if (pay_mm == next_mm) {//規劃繳費月份=下個月
        //           var obj = arr_pay[j];
        //           obj.p_id = p[i]._id;
        //           obj.p_account_num = p[i].account_num;
        //           obj.p_name_cht = p[i].name_cht;
        //           obj.p_name_eng = p[i].name_eng;
        //           obj.p_agent_id = p[i].agent_id;
        //           obj.p_agent_text = p[i].agent_text;
        //           // obj.pmr_ps = p[i].pmr_ps;
        //           arr_new_p.push(obj);
        //           break;
        //         }
        //       }
        //     }
        //   }
        // }

        // if (d.getMonth() == 11) { // now = Dec.
        //     for (var i = 0; i < p.length; i++) {
        //         var arr_pay = p[i].arrPaymentRecord;
        //         if (!!arr_pay) { //有設定 規劃繳費日期 規劃繳費金額
        //             for (var j = 0; j < arr_pay.length; j++) {
        //                 //有規劃繳費日期 規劃繳費金額 && 沒有實際繳費日期 實際繳費金額 (未繳費)
        //                 if (!!arr_pay[j].pmr_payment1date && !!arr_pay[j].pmr_payment1money) {
        //                     if (!arr_pay[j].pmr_payment2date || !arr_pay[j].pmr_payment2money || commaToNumber(arr_pay[j].pmr_payment2money) < commaToNumber(arr_pay[j].pmr_payment1money)) {
        //                         var pay_mm = Number((arr_pay[j].pmr_payment1date).split("/")[1]) //規劃繳費月份
        //                         var pay_yy = Number((arr_pay[j].pmr_payment1date).split("/")[0]) //規劃繳費年份

        //                         // if (next_mm == 13) {}
        //                         if (d.getFullYear() + 1 == pay_yy && pay_mm == "1") { //規劃繳費月份=12月
        //                             var obj = arr_pay[j];
        //                             obj.p_id = p[i]._id;
        //                             obj.pay_yy = pay_yy;
        //                             obj.pay_mm = pay_mm;
        //                             obj.p_account_num = p[i].account_num;
        //                             obj.p_name_cht = p[i].name_cht;
        //                             obj.p_name_eng = p[i].name_eng;
        //                             obj.p_agent_id = p[i].agent_id;
        //                             obj.p_agent_text = p[i].agent_text;
        //                             // obj.pmr_ps = p[i].pmr_ps;
        //                             arr_new_p.push(obj);
        //                             // break;
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        // arr_new_p = arr_new_p.sort(function(a, b) {
        //     var keyA = new Date(a.pay_mm),
        //         keyB = new Date(b.pay_mm);
        //     // Compare the 2 dates
        //     if (keyA < keyB) return -1;
        //     if (keyA > keyB) return 1;
        //     return 0;
        // });

        // arr_new_p = arr_new_p.sort(function(a, b) {
        //     var keyA = new Date(a.pay_yy),
        //         keyB = new Date(b.pay_yy);
        //     // Compare the 2 dates
        //     if (keyA < keyB) return -1;
        //     if (keyA > keyB) return 1;
        //     return 0;
        // });

        arr_new_p = arr_new_p.sort(function (a, b) {
            var a = a.pmr_payment1date;
            var b = b.pmr_payment1date;
            a = a.split('/').join('');
            b = b.split('/').join('');
            return a > b ? 1 : a < b ? -1 : 0;

        });

        return {
            // data: Portfolios.find(queryVar, {
            //     sort: sortVar,
            //     skip: pageVar.skip,
            //     limit: pageVar.limit
            //   }).fetch(),
            data: arr_new_p,
            itemsCount: arr_new_p.length
        }
    },
    // post: {
    //   // roleRequired: ['author', 'admin'],
    //   action: function () {
    //     // console.log("Rest Portfolios insert POST");
    //     // console.log(this.bodyParams);

    //     var params = this.bodyParams;

    //     var c = Clients.findOne({_id: params.client_id});
    //     var p4 = Product4.findOne({_id: params.product4_id});
    //     var uid = "P"+funcStrPad(getNextSequence('portfolio_p'), 7);

    //     params.uid = uid;
    //     params.client_id = c._id;
    //     params.client_uid = c.uid;
    //     params.review_id = "0";
    //     params.review_text = "草稿";
    //     params.product1_id = p4.product1_id;
    //     params.product1_text = p4.product1_text;
    //     params.product2_id = p4.product2_id;
    //     params.product2_text = p4.product2_text;
    //     params.product3_id = p4.product3_id;
    //     params.product3_text = p4.product3_text;
    //     params.product4_id = p4._id;
    //     params.product4_text = p4.name_cht;
    //     params.name_cht = c.name_cht;
    //     params.name_eng = c.name_eng;
    //     params.contactnum = c.cellnum || c.addr1_phone;
    //     params.email = c.email;
    //     params.provider_id  = p4.provider_id;
    //     params.provider_chttext  = p4.provider_chttext;
    //     params.provider_engtext  = p4.provider_engtext;
    //     params.template_id  = p4.template_id;
    //     params.agent_id = c.agent_id || "";
    //     params.agent_text = c.agent_text || "";

    //     params.ps = "";
    //     params.review0_total_se = 0;
    //     params.review1_total_se = 0;
    //     params.review2_total_se = 0;
    //     params.review3_total_se = 0;
    //     params.review4_total_se = 0;
    //     params.review5_total_se = 0;
    //     params.review6_total_se = 0;
    //     params.review7_total_se = 0;
    //     params.review8_total_se = 0;
    //     params.review0_start_time = "";
    //     params.review1_start_time = new Date();
    //     params.review2_start_time = "";
    //     params.review3_start_time = "";
    //     params.review4_start_time = "";
    //     params.review5_start_time = "";
    //     params.review6_start_time = "";
    //     params.review7_start_time = "";
    //     params.review8_start_time = "";
    //     params.review0_end_time = "";
    //     params.review1_end_time = "";
    //     params.review2_end_time = "";
    //     params.review3_end_time = "";
    //     params.review4_end_time = "";
    //     params.review5_end_time = "";
    //     params.review6_end_time = "";
    //     params.review7_end_time = "";
    //     params.review8_end_time = "";
    //     params.review0_by = "";
    //     params.review1_by = "";
    //     params.review2_by = "";
    //     params.review3_by = "";
    //     params.review4_by = "";
    //     params.review5_by = "";
    //     params.review6_by = "";
    //     params.review7_by = "";
    //     params.review8_by = "";
    //     params.review0_name = "";
    //     params.review1_name = "";
    //     params.review2_name = "";
    //     params.review3_name = "";
    //     params.review4_name = "";
    //     params.review5_name = "";
    //     params.review6_name = "";
    //     params.review7_name = "";
    //     params.review8_name = "";
    //     params.review_count = 0;

    //     params.insertedAt = new Date();
    //     // console.log(params);

    //     var id = Portfolios.insert(params);

    //     var obj = {
    //       parent_id: id,
    //       from_review_id: "0",
    //       from_review_text: "草稿",
    //       to_review_id: "0",
    //       to_review_text: "草稿",
    //       interval: 0,
    //       review_count: 0,
    //       insertedBy: "",
    //       insertedName: "",
    //       insertedAt: new Date(),
    //       ps: "",
    //       p_uid: uid,
    //       p_name_cht: params.name_cht,
    //       // p_product: data.product1+"-"+data.product2,
    //     }
    //     Review2.insert(obj);

    //       // params.fullname = params.countryname+" - "+params.value;
    //     if (id) {
    //       params._id = id;
    //       return {insert: 'success', data: params};
    //     }
    //     return {
    //       statusCode: 404,
    //       body: {status: 'fail', message: 'Article not found'}
    //     };
    //   }
    // },
    // put: {
    //   // roleRequired: ['author', 'admin'],
    //   action: function () {
    //     console.log("Rest Portfolios update PUT");
    //     // console.log(this.bodyParams);
    //     var params = this.bodyParams;
    //     var id = params._id;
    //     delete params._id;
    //     console.log(params);


    //     if (Portfolios.update(id, {$set: params })) {
    //       params._id = id;
    //       return params;
    //       // return {status: 'success', data: {message: 'Teams updated'}};
    //     }
    //     return {
    //       statusCode: 404,
    //       body: {status: 'fail', message: 'Article not found'}
    //     };
    //   }
    // },
    // delete: {
    //   // roleRequired: ['author', 'admin'],
    //   action: function () {
    //     // console.log("Rest Portfolios DELETE");
    //     // console.log(this.bodyParams);
    //     if (Portfolios.remove(this.bodyParams._id)) {
    //       return {status: 'success', data: {message: 'Teams removed'}};
    //     }
    //     return {
    //       statusCode: 404,
    //       body: {status: 'fail', message: 'Article not found'}
    //     };
    //   }
    // },
});
Api.addRoute('portfolio', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Portfolios GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);


        //console.log(queryVar);
        // if(this.queryParams.template_id == "0"){
        //   queryVar.template_id = "0";
        // }
        let pay_status_no = queryVar.pay_status_no;
        delete queryVar.pay_status_no;

        if (!!queryVar.initial) {
            var searchInitial = queryVar.initial;
            delete queryVar.initial;

            queryVar = {
                $and: [{
                    name_eng: {
                        $regex: "^" + searchInitial,
                        $options: 'i'
                    }
                },
                    queryVar,
                ]
            };
            // console.log(queryVar);
            // console.log(searchInitial);
        } else {
            delete queryVar.initial;
        }

        if (!!queryVar.follownewcase) {
            // queryVar = {
            //   $and: [{
            //       name_eng: {
            //         $regex: "^" + searchInitial,
            //         $options: 'i'
            //       }
            //     },
            //     queryVar,
            //   ]
            // };
        }
        delete queryVar.follownewcase;

        if (!!queryVar.findData) {
            var searchValue = queryVar.findData;
            delete queryVar.findData;

            // console.log(searchValue);
            // console.log(queryVar);
            queryVar = {
                $and: [{
                    $or: [{
                        client_uid: {
                            $regex: ".*" + searchValue + ".*",
                            $options: '-i'
                        }
                    },
                    // {name_cht: {$regex : ".*"+searchValue+".*", $options: '-i'}},
                    {
                        name_cht: {
                            $regex: searchValue,
                            $options: 'i'
                        }
                    },
                    {
                        name_eng: {
                            $regex: searchValue,
                            $options: 'i'
                        }
                    },
                    {
                        contactnum: {
                            $regex: ".*" + searchValue + ".*",
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchValue,
                            $options: 'i'
                        }
                    },
                    {
                        account_num: {
                            $regex: ".*" + searchValue + ".*",
                            $options: 'i'
                        }
                    },
                    ]
                },
                    queryVar,
                ]
            };
        } else {
            delete queryVar.findData;
        }

        // if (pay_status_no == "1") {

        //   let d = new Date();
        //   let nd_month = d.getMonth() + 1;//現在月份
        //   let nd_date = d.getDate();//現在日期
        //   let nd_next_month = d.getMonth() + 2;//下個月月份
        //   let This_year = d.getFullYear();//今年

        //   queryVar = {
        //     $and: [
        //       {$and: [
        //         {$and: [
        //           {effective_date: {$exists: true}},//存在有效日期
        //           {pay_status: {$ne: "1"}},//已繳費以外
        //         ]},
        //         {$or: [
        //           {$and: [
        //             {effective_date_yyyy: {$eq: This_year}},//日期等於今年
        //             {effective_date_mm: {$eq: nd_next_month}},//日期等於下個月
        //           ]},
        //           {$and: [
        //             {effective_date_yyyy: {$eq: This_year}},//日期等於今年
        //             {effective_date_mm: {$eq: nd_month}},//日期等於這個月
        //             {effective_date_dd: {$gte: nd_date}},//日期大於等於今天
        //           ]}
        //         ]}
        //       ]},
        //       queryVar,
        //     ]
        //   };
        // }
        // if(isEmpty(sortVar))
        //   sortVar = {uid: 1};
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        var p = Portfolios.find(queryVar, {
            sort: sortVar,
            skip: pageVar.skip,
            limit: pageVar.limit
        }).fetch();

        for (var i = 0; i < p.length; i++) {
            p[i].myItemIndex = i;
        }

        var cnt = Portfolios.find(queryVar).count();

        // 無產品客戶
        if (queryVar.nowphase == '4') {
            // 所有有產品客戶之編號
            var pAll = Portfolios.find({ nowphase: { $ne: '3' } }).fetch();
            var res_arr = [];

            pAll.forEach(function (item, i) {
                var existing = res_arr.filter(function (v, i) {
                    return v == item.client_uid;
                });
                if (existing.length) {
                } else {
                    res_arr.push(item.client_uid)
                }
            });

            // 找無產品客戶
            p = Clients.find({ uid: { $nin: res_arr } }, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch();

            for (var i = 0; i < p.length; i++) {
                var obj = {};
                obj = {
                    client_uid: p[i].uid,
                    name_cht: p[i].name_cht,
                    contactnum: p[i].cellnum,
                    email: p[i].email
                }
                p[i] = obj;
            }
            cnt = Clients.find({ uid: { $nin: res_arr } }).count();
        }

        // console.log(p);
        return {
            // data: Portfolios.find(queryVar, {
            //     sort: sortVar,
            //     skip: pageVar.skip,
            //     limit: pageVar.limit
            //   }).fetch(),
            data: p,
            itemsCount: cnt
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Portfolios insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;

            var c = Clients.findOne({
                _id: params.client_id
            });
            var p4 = Product4.findOne({
                _id: params.product4_id
            });
            var uid = "P" + funcStrPad(getNextSequence('portfolio_p'), 7);

            params.uid = uid;
            params.client_id = c._id;
            params.client_uid = c.uid;
            params.review_id = "0";
            params.review_text = "草稿";
            params.product1_id = p4.product1_id;
            params.product1_text = p4.product1_text;
            params.product2_id = p4.product2_id;
            params.product2_text = p4.product2_text;
            params.product3_id = p4.product3_id;
            params.product3_text = p4.product3_text;
            params.product4_id = p4._id;
            params.product4_text = p4.name_cht;
            params.name_cht = c.name_cht;
            params.name_eng = c.name_eng;
            params.contactnum = c.cellnum || c.addr1_phone;
            params.email = c.email;
            params.provider_id = p4.provider_id;
            params.provider_chttext = p4.provider_chttext;
            params.provider_engtext = p4.provider_engtext;
            params.template_id = p4.template_id;
            params.agent_id = c.agent_id || "";
            params.agent_text = c.agent_text || "";

            params.ps = "";
            params.review0_total_se = 0;
            params.review1_total_se = 0;
            params.review2_total_se = 0;
            params.review3_total_se = 0;
            params.review4_total_se = 0;
            params.review5_total_se = 0;
            params.review6_total_se = 0;
            params.review7_total_se = 0;
            params.review8_total_se = 0;
            params.review0_start_time = "";
            params.review1_start_time = new Date();
            params.review2_start_time = "";
            params.review3_start_time = "";
            params.review4_start_time = "";
            params.review5_start_time = "";
            params.review6_start_time = "";
            params.review7_start_time = "";
            params.review8_start_time = "";
            params.review0_end_time = "";
            params.review1_end_time = "";
            params.review2_end_time = "";
            params.review3_end_time = "";
            params.review4_end_time = "";
            params.review5_end_time = "";
            params.review6_end_time = "";
            params.review7_end_time = "";
            params.review8_end_time = "";
            params.review0_by = "";
            params.review1_by = "";
            params.review2_by = "";
            params.review3_by = "";
            params.review4_by = "";
            params.review5_by = "";
            params.review6_by = "";
            params.review7_by = "";
            params.review8_by = "";
            params.review0_name = "";
            params.review1_name = "";
            params.review2_name = "";
            params.review3_name = "";
            params.review4_name = "";
            params.review5_name = "";
            params.review6_name = "";
            params.review7_name = "";
            params.review8_name = "";
            params.review_count = 0;

            params.insertedAt = new Date();
            // console.log(params);

            var id = Portfolios.insert(params);

            var obj = {
                parent_id: id,
                from_review_id: "0",
                from_review_text: "草稿",
                to_review_id: "0",
                to_review_text: "草稿",
                interval: 0,
                review_count: 0,
                insertedBy: "",
                insertedName: "",
                insertedAt: new Date(),
                ps: "",
                p_uid: uid,
                p_name_cht: params.name_cht,
                // p_product: data.product1+"-"+data.product2,
            }
            Review2.insert(obj);

            // params.fullname = params.countryname+" - "+params.value;
            if (id) {
                params._id = id;
                return {
                    insert: 'success',
                    data: params
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    // put: {
    //     // roleRequired: ['author', 'admin'],
    //     action: function() {
    //         console.log("Rest Portfolios update PUT");
    //         // console.log(this.bodyParams);
    //         var params = this.bodyParams;
    //         var id = params._id;
    //         delete params._id;
    //         // console.log(params);

    //         if (params.current_Status == params.next_Status) {
    //             params.no_updated_hook = 1; //兩個一樣 就不要進hook
    //         } else {
    //             params.no_updated_hook = 0;
    //         }

    //         // params.insertedAt_yyyy = Number(params.insertedAt_yyyy);
    //         // params.insertedAt_mm = Number(params.insertedAt_mm);
    //         // params.insertedAt_dd = Number(params.insertedAt_dd);

    //         // params.current_Status = params.next_Status;
    //         // params.current_status_days = Workdays.findOne({ _id: params.current_Status }).work_days;

    //         return {
    //             // data: Portfolios.find(queryVar, {
    //             //     sort: sortVar,
    //             //     skip: pageVar.skip,
    //             //     limit: pageVar.limit
    //             // }).fetch(),
    //             // // data: arr_new_p,
    //             // // itemsCount: arr_new_p.length
    //             // itemsCount: Portfolios.find(queryVar).count()
    //         }
    //     },
    // },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Portfolios update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);

            if (params.current_Status == params.next_Status) {
                params.no_updated_hook = 1; //兩個一樣 就不要進hook
            } else {
                params.no_updated_hook = 0;
            }

            if (Portfolios.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },

    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Portfolios DELETE");
            // console.log(this.bodyParams);
            if (Portfolios.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('clients', {
    authRequired: false
}, {
    get: {
        // roleRequired: ['agent', 'admin'],
        action: function () {
            // console.log("Rest clients GET");
            var queryVar, sortVar = {},
                pageVar = {};

            queryVar = this.queryParams || {};
            Api.getSortParams(queryVar, sortVar);
            Api.getPageParams(queryVar, pageVar);

            let bith = queryVar.bith;
            delete queryVar.bith;
            if (!!queryVar.findData) {
                var searchValue = queryVar.findData;
                delete queryVar.findData;

                // console.log(searchValue);
                queryVar = {
                    $and: [{
                        $or: [{
                            name_cht: {
                                $regex: ".*" + searchValue + ".*",
                                $options: '-i'
                            }
                        },
                        {
                            name_eng: {
                                $regex: searchValue,
                                $options: 'i'
                            }
                        },
                        {
                            uid: {
                                $regex: searchValue,
                                $options: 'i'
                            }
                        },
                        ]
                    },
                        queryVar,
                    ]
                };
            }
            // if (!!queryVar.findData) {
            //   var searchValue = queryVar.findData;
            //   delete queryVar.findData;
            //   queryVar = {
            //     $and: [{
            //         $or: [{
            //             name_cht: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //           {
            //             identify: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //           {
            //             passport: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //           {
            //             addr1_phone: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //           {
            //             addr2_phone: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //           {
            //             cellnum: {
            //               $regex: ".*" + searchValue + ".*",
            //               $options: 'i'
            //             }
            //           },
            //         ]
            //       },
            //       queryVar,
            //     ]
            //   };
            //
            // } else {
            //   delete queryVar.findData;
            // }

            if (bith == "1") {
                let d = new Date();
                let nd_month = d.getMonth() + 1; //現在月份
                let nd_date = d.getDate(); //現在月份
                let nd_next_month = d.getMonth() + 2; //下個月月份

                if (nd_next_month > 12) {
                    nd_next_month = 1;
                }

                queryVar = {
                    $and: [{
                        $or: [{
                            birthday_mm: {
                                $eq: nd_next_month
                            }
                        }, //日期等於下個月
                        {
                            $and: [{
                                birthday_mm: {
                                    $eq: nd_month
                                }
                            }, //日期等於這個月
                            {
                                birthday_dd: {
                                    $gte: nd_date
                                }
                            }, //日期大於等於今天
                            ]
                        }
                        ]
                    },
                        queryVar,
                    ]
                };
                // console.log("aaaaaa");
                // console.log(queryVar);
                sortVar = {
                    birthday_mm: 1,
                    birthday_dd: 1
                };
            }

            // console.log("queryVar: ");
            // console.log(queryVar);
            // console.log("sortVar: ");
            // console.log(sortVar);
            // console.log("pageVar: ");
            // console.log(pageVar);

            if (isEmpty(sortVar))
                sortVar = {
                    uid: 1
                };
            return {
                data: Clients.find(queryVar, {
                    sort: sortVar,
                    skip: pageVar.skip,
                    limit: pageVar.limit
                }).fetch(),
                itemsCount: Clients.find(queryVar).count()
            }
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Clients insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = "H" + funcStrPad(getNextSequence('clients'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            var id = Clients.insert(params);
            params._id = id;
            if (id) {
                return {
                    insert: 'success',
                    data: params
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Clients update PUT");
            //console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);

            if (!!params && Clients.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Clients DELETE");
            // console.log(this.bodyParams);
            if (Clients.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('authority', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (queryVar.is_auth == "0") {
            queryVar = {
                'auth.is_auth': "0"
            };
            delete queryVar.is_auth;
        } else if (queryVar.is_auth == "1") {

            queryVar = {
                $or: [{
                    'auth.is_auth': '1'
                }, {
                    'auth.is_auth': {
                        $exists: false
                    }
                }]
            };
            delete queryVar.is_auth;

        }


        var object = Meteor.users.find(queryVar, {
            sort: sortVar,
            // skip: pageVar.skip,
            // limit: pageVar.limit
        }).fetch();

        var new_obj = [];
        var skip = Number(pageVar.skip);
        var number = skip + Number(pageVar.limit);

        // console.log(skip);
        // console.log(number);
        // console.log(object);

        // object = object.reverse();
        for (var i = skip; i < number; i++) {
            if (typeof object[i] !== 'undefined') {
                var attr = object[i];

                var obj = {
                    worknumber: attr.profile.worknumber || "",
                    chtname: attr.profile.chtname || "",
                    engname: attr.profile.engname || "",
                    onbroad_date: attr.profile.onbroad_date || "",
                    jobtitle: attr.profile.jobtitle || "",
                    tw_id: attr.profile.tw_id || "",
                    birthday_date: attr.profile.birthday_date || "",
                    cellphone: attr.profile.cellphone || "",
                    email: attr.emails[0].address || "",
                    email2: attr.profile.email2 || "",
                    aan: attr.profile.aan || "",
                    department_id: attr.profile.department_id || "",
                    _id: attr._id || "",

                    // when: attr.when.toString(),
                    // hashedToken: attr.hashedToken,
                }
                if (!attr.auth) {
                    attr.auth = {};
                }
                obj.is_auth = attr.auth.is_auth || "1", // 1 允許登入
                    obj.auth_fin = attr.auth.auth_fin || "0",
                    obj.auth_hr = attr.auth.auth_hr || "0",
                    obj.auth_cus = attr.auth.auth_cus || "0",
                    obj.auth_as = attr.auth.auth_as || "0",
                    obj.auth_prod = attr.auth.auth_prod || "0",
                    obj.auth_web = attr.auth.auth_web || "0",

                    new_obj.push(obj);
                // new_obj.unshift(attr);
                // new_obj.unshift(obj);
            }
        }
        // console.log('new_obj', new_obj)
        return {
            data: new_obj,
            itemsCount: object.length
        }
    },
    /*post: {
      // roleRequired: ['author', 'admin'],
      action: function () {
        console.log("Rest Salary insert POST");
        console.log(this.bodyParams);
 
        var params = this.bodyParams;
        params.uid = getNextSequence('Salary');
        // params.fullname = params.countryname+" - "+params.value;
        if (Salary.insert(params)) {
          return {insert: 'success', data: {message: 'Teams insert'}};
        }
        return {
          statusCode: 404,
          body: {status: 'fail', message: 'Article not found'}
        };
      }
    },*/
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary update PUT");
            //console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;

            // Roles.addUsersToRoles(bobsUserId, ['manage-team','schedule-game'])

            params.auth = {};
            params.auth.auth_fin = params.auth_fin;
            params.auth.auth_hr = params.auth_hr;
            params.auth.auth_cus = params.auth_cus;
            params.auth.auth_as = params.auth_as;
            params.auth.auth_prod = params.auth_prod;
            params.auth.auth_web = params.auth_web;
            params.auth.is_auth = params.is_auth;

            delete params.auth_fin;
            delete params.auth_hr;
            delete params.auth_cus;
            delete params.auth_as;
            delete params.auth_prod;
            delete params.auth_web;
            delete params.is_auth;

            if (params.auth.auth_fin == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_fin-read', 'auth_fin-edit', 'auth_fin-open']);
            } else if (params.auth.auth_fin == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_fin-read']);
                Roles.removeUsersFromRoles(id, ['auth_fin-edit', 'auth_fin-open']);
            } else if (params.auth.auth_fin == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-edit']);
                Roles.removeUsersFromRoles(id, ['auth_fin-open']);
            } else if (params.auth.auth_fin == "3") { // 讀取+開帳
                Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-open']);
                Roles.removeUsersFromRoles(id, ['auth_fin-edit']);
            }

            if (params.auth.auth_hr == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_hr-read', 'auth_hr-edit']);
            } else if (params.auth.auth_hr == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_hr-read']);
                Roles.removeUsersFromRoles(id, ['auth_hr-edit']);
            } else if (params.auth.auth_hr == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_hr-read', 'auth_hr-edit']);
            }

            if (!!params.current_Status && !!params.next_Status && params.current_Status != params.next_Status) {
                params.current_Status = params.next_Status;
                params.current_status_days = Workdays.findOne({
                    _id: params.current_Status
                }).work_days;
            }
            // console.log(params); === === =
            if (params.auth.auth_cus == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_cus-read', 'auth_cus-edit', 'auth_cus-del']);
            } else if (params.auth.auth_cus == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_cus-read']);
                Roles.removeUsersFromRoles(id, ['auth_cus-edit', 'auth_cus-del']);
            } else if (params.auth.auth_cus == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_cus-read', 'auth_cus-edit']);
                Roles.removeUsersFromRoles(id, ['auth_cus-del']);
            } else if (params.auth.auth_cus == "3") { // 讀取+編輯+刪除
                Roles.addUsersToRoles(id, ['auth_cus-read', 'auth_cus-edit', 'auth_cus-del'])
            }
            if (params.auth.auth_as == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_as-read', 'auth_as-edit']);
            } else if (params.auth.auth_as == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_as-read']);
                Roles.removeUsersFromRoles(id, ['auth_as-edit']);
            } else if (params.auth.auth_as == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_as-read', 'auth_as-edit']);
            }
            if (params.auth.auth_prod == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_prod-read', 'auth_prod-edit']);
            } else if (params.auth.auth_prod == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_prod-read']);
                Roles.removeUsersFromRoles(id, ['auth_prod-edit']);
            } else if (params.auth.auth_prod == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_prod-read', 'auth_prod-edit']);
            }
            if (params.auth.auth_web == "0") { // 禁止
                Roles.removeUsersFromRoles(id, ['auth_web-read', 'auth_web-edit']);
            } else if (params.auth.auth_web == "1") { // 讀取
                Roles.addUsersToRoles(id, ['auth_web-read']);
                Roles.removeUsersFromRoles(id, ['auth_web-edit']);
            } else if (params.auth.auth_web == "2") { // 讀取+編輯
                Roles.addUsersToRoles(id, ['auth_web-read', 'auth_web-edit']);
            }

            if (params.auth.is_auth == "0") { // 讀取+編輯
                Roles.removeUsersFromRoles(id, [
                    'auth_fin-read', 'auth_fin-edit', 'auth_fin-open',
                    'auth_hr-read', 'auth_hr-edit',
                    'auth_cus-read', 'auth_cus-edit',
                    'auth_as-read', 'auth_as-edit',
                    'auth_prod-read', 'auth_prod-edit',
                    'auth_web-read', 'auth_web-edit', 'auth_cus-del'
                ]);
                params.auth.auth_fin = "0";
                params.auth.auth_hr = "0";
                params.auth.auth_cus = "0";
                params.auth.auth_as = "0";
                params.auth.auth_prod = "0";
                params.auth.auth_web = "0";
            }

            //console.log(params);
            if (Meteor.users.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('authority2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (queryVar.is_auth == "0") {
            queryVar = {
                'auth.is_auth': "0"
            };
            delete queryVar.is_auth;
        } else if (queryVar.is_auth == "1") {

            queryVar = {
                $or: [{
                    'auth.is_auth': '1'
                }, {
                    'auth.is_auth': {
                        $exists: false
                    }
                }]
            };
            delete queryVar.is_auth;

        }


        var object = Meteor.users.findOne(queryVar, {
            sort: sortVar,
            // skip: pageVar.skip,
            // limit: pageVar.limit
        });

        var new_obj = [];
        var skip = Number(pageVar.skip);
        var number = skip + Number(pageVar.limit);

        // console.log(skip);
        // console.log(number);
        // console.log(object);
        // console.log('object', object.roles);
        // 財務與績效管理, 人資管理, 客戶管理, 售後服務, 產品中心, 網站管理,
        var roles = object.roles
        // object = object.reverse();
        for (var i = 0; i < roles.length; i++) {
            const attr = roles[i];
            const index = attr.indexOf('-');
            const name = attr.slice(0, index);
            const key = attr.slice(index + 1);

            // console.log(index, name , key);

            obj = {
                name,
                [key]: '1',
            }
            new_obj.push(obj);
        }

        var res_obj = [];

        new_obj.forEach(function (item, i) {
            var existing = res_obj.filter(function (v, i) {
                return v.name == item.name;
            });
            // console.log(existing)
            if (existing.length) {
                var existingIndex = res_obj.indexOf(existing[0]);
                Object.assign(res_obj[existingIndex], item);
            } else {
                res_obj.push(Object.assign({}, item));
            }
        });

        objAuthority.forEach(function (item, i) {
            var existing = res_obj.filter(function (v, i) {
                return v.name == item.id;
            });
            if (!existing.length) {
                res_obj.push(Object.assign({}, { name: item.id }));
            }
        })

        //console.log('res_obj', res_obj)

        return {
            data: res_obj,
            itemsCount: object.length
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary update PUT");
            //console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;

            // Roles.addUsersToRoles(bobsUserId, ['manage-team','schedule-game'])

            // params.auth = {};
            // params.auth.auth_fin = params.auth_fin;
            // params.auth.auth_hr = params.auth_hr;
            // params.auth.auth_cus = params.auth_cus;
            // params.auth.auth_as = params.auth_as;
            // params.auth.auth_prod = params.auth_prod;
            // params.auth.auth_web = params.auth_web;
            // params.auth.is_auth = params.is_auth;

            // delete params.name;
            // delete params.read;
            // delete params.add;
            // delete params.edit;
            // delete params.del;
            // delete params.auth_web;
            // delete params.is_auth;

            // console.log('before', params);

            if (params.name == `${params.name}`) {
                if (params.read == "0") {
                    Roles.removeUsersFromRoles(id, [`${params.name}-read`]);
                }
                if (params.add == "0") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read']);
                    Roles.removeUsersFromRoles(id, [`${params.name}-add`]);
                }
                if (params.edit == "0") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-edit']);
                    Roles.removeUsersFromRoles(id, [`${params.name}-edit`]);
                }
                if (params.del == "0") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-open']);
                    Roles.removeUsersFromRoles(id, [`${params.name}-del`]);
                }
                if (params.read == "1") {
                    Roles.addUsersToRoles(id, [`${params.name}-read`]);
                }
                if (params.add == "1") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read']);
                    Roles.addUsersToRoles(id, [`${params.name}-add`]);
                }
                if (params.edit == "1") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-edit']);
                    Roles.addUsersToRoles(id, [`${params.name}-edit`]);
                }
                if (params.del == "1") {
                    // Roles.addUsersToRoles(id, ['auth_fin-read', 'auth_fin-open']);
                    Roles.addUsersToRoles(id, [`${params.name}-del`]);
                }
            }

            // if (params.auth.is_auth == "0") { // 讀取+編輯
            //     Roles.removeUsersFromRoles(id, [
            //         'auth_fin-read', 'auth_fin-edit', 'auth_fin-open',
            //         'auth_hr-read', 'auth_hr-edit',
            //         'auth_cus-read', 'auth_cus-edit',
            //         'auth_as-read', 'auth_as-edit',
            //         'auth_prod-read', 'auth_prod-edit',
            //         'auth_web-read', 'auth_web-edit', 'auth_cus-del'
            //     ]);
            //     params.auth.auth_fin = "0";
            //     params.auth.auth_hr = "0";
            //     params.auth.auth_cus = "0";
            //     params.auth.auth_as = "0";
            //     params.auth.auth_prod = "0";
            //     params.auth.auth_web = "0";
            // }
            // delete params.name;
            // delete params.read;
            // delete params.add;
            // delete params.edit;
            // delete params.del;
            // console.log('after', id, params);
            // if (
            //     Meteor.users.update(id, {
            //         $set: params
            //     })
            // ) {
            //     params._id = id;
            return params;
            //     // return {status: 'success', data: {message: 'Teams updated'}};
            // }
            // return {
            //     statusCode: 404,
            //     body: {
            //         status: 'fail',
            //         message: 'Article not found'
            //     }
            // };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('employeelist', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        if (!!queryVar.bg_id) {
            var bg_id = queryVar.bg_id;
            delete queryVar.bg_id;
            if (queryVar.is_auth == "0") {
                // queryVar = {'auth.is_auth':"0"};
                queryVar = {
                    $and: [{
                        'auth.is_auth': '0'
                    },
                    {
                        'profile.department_id': bg_id
                    },
                    ]
                };
                delete queryVar.is_auth;
            } else if (queryVar.is_auth == "1") {
                queryVar = {
                    $and: [{
                        $or: [{
                            'auth.is_auth': '1'
                        }, {
                            'auth.is_auth': {
                                $exists: false
                            }
                        }]
                    },
                    {
                        'profile.department_id': bg_id
                    }
                    ]
                };
                delete queryVar.is_auth;
            }
        } else {
            if (queryVar.is_auth == "0") {
                // queryVar = {'auth.is_auth':"0"};
                queryVar = {
                    $and: [{
                        'auth.is_auth': '0'
                    },]
                };
                delete queryVar.is_auth;
            } else if (queryVar.is_auth == "1") {
                queryVar = {
                    $and: [{
                        $or: [{
                            'auth.is_auth': '1'
                        }, {
                            'auth.is_auth': {
                                $exists: false
                            }
                        }]
                    },]
                };
                delete queryVar.is_auth;
            }
        }

        var object = Meteor.users.find(queryVar, {
            sort: sortVar,
            // skip: pageVar.skip,
            // limit: pageVar.limit
        }).fetch();

        var new_obj = [];
        var skip = Number(pageVar.skip);
        var number = skip + Number(pageVar.limit);

        // console.log(skip);
        // console.log(number);
        // console.log(object);
        // object = object.reverse();
        for (var i = skip; i < number; i++) {
            if (typeof object[i] !== 'undefined') {
                var attr = object[i];

                var obj = {
                    worknumber: attr.profile.worknumber || "",
                    chtname: attr.profile.chtname || "",
                    engname: attr.profile.engname || "",
                    onbroad_date: attr.profile.onbroad_date || "",
                    jobtitle: attr.profile.jobtitle || "",
                    tw_id: attr.profile.tw_id || "",
                    birthday_date: attr.profile.birthday_date || "",
                    cellphone: attr.profile.cellphone || "",
                    email: attr.emails[0].address || "",
                    email2: attr.profile.email2 || "",
                    aan: attr.profile.aan || "",
                    department_id: attr.profile.department_id || "",
                    auth_substitutego: attr.auth_substitutego || "0",
                    auth_supervisorgo: attr.auth_supervisorgo || "0",
                    auth_hrgo: attr.auth_hrgo || "0",
                    _id: attr._id || "",
                    // last_year_dayoff: attr.last_year_dayoff || "",
                    // when: attr.when.toString(),
                    // hashedToken: attr.hashedToken,
                }
                new_obj.push(obj);
                // new_obj.unshift(attr);
                // new_obj.unshift(obj);
            }
        }

        return {
            data: new_obj,
            itemsCount: object.length
        }
    },
    /*post: {
          // roleRequired: ['author', 'admin'],
          action: function () {
            console.log("Rest Salary insert POST");
            console.log(this.bodyParams);
    
            var params = this.bodyParams;
            params.uid = getNextSequence('Salary');
            // params.fullname = params.countryname+" - "+params.value;
            if (Salary.insert(params)) {
              return {insert: 'success', data: {message: 'Teams insert'}};
            }
            return {
              statusCode: 404,
              body: {status: 'fail', message: 'Article not found'}
            };
          }
        },*/
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest employeelist update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Meteor.users.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});


Api.addRoute('loginlist', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        // Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        var object = Meteor.users.find(queryVar, {
            // sort: sortVar,
            // skip: pageVar.skip,
            // limit: pageVar.limit
        }).fetch()[0].services.resume.loginTokens;

        var new_obj = [];
        var skip = Number(pageVar.skip);
        var number = skip + Number(pageVar.limit);

        // console.log(skip);
        // console.log(number);
        // console.log(object);

        object = object.reverse();
        for (var i = skip; i < number; i++) {
            if (typeof object[i] !== 'undefined') {
                var attr = object[i];

                var obj = {
                    when: attr.when.toString(),
                    hashedToken: attr.hashedToken,
                }
                new_obj.push(obj);
                // new_obj.unshift(attr);
                // new_obj.unshift(obj);
            }
        }

        return {
            data: new_obj,
            itemsCount: object.length
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Salary');
            // params.fullname = params.countryname+" - "+params.value;
            if (Salary.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    // put: {
    //   // roleRequired: ['author', 'admin'],
    //   action: function () {
    //     console.log("Rest Salary update PUT");
    //     // console.log(this.bodyParams);
    //     var params = this.bodyParams;
    //     var id = params._id;
    //     delete params._id;
    //     console.log(params);
    //     if (Salary.update(id, {$set: params })) {
    //       params._id = id;
    //       return params;
    //       // return {status: 'success', data: {message: 'Teams updated'}};
    //     }
    //     return {
    //       statusCode: 404,
    //       body: {status: 'fail', message: 'Article not found'}
    //     };
    //   }
    // },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('salary', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (!!queryVar.start || !!queryVar.end) {
            var start = "";
            var end = "";

            if (queryVar.start) {
                start = new Date(queryVar.start);
            }
            if (queryVar.end) {
                end = new Date(queryVar.end);
            }

            delete queryVar.start;
            delete queryVar.end;

            if (start != "" && end == "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $gte: start
                        }
                    },
                        queryVar,
                    ]
                };
            } else if (start == "" && end != "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $lt: end
                        }
                    },
                        queryVar,
                    ]
                };
            } else if (start != "" && end != "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $gte: start,
                            $lt: end
                        }
                    },
                        queryVar,
                    ]
                };
            }

            // console.log(queryVar);
        }


        return {
            data: Salary.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Salary.find(queryVar).count()
        }

    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Salary');
            // params.fullname = params.countryname+" - "+params.value;
            if (Salary.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);

            // params.healthcare_pe = Number(params.healthcare_pe) || 0;
            // params.healthcare_co = Number(params.healthcare_co) || 0;
            // params.labor_pe      = Number(params.labor_pe) || 0;
            // params.labor_co      = Number(params.labor_co) || 0;
            // params.labor_retreat = Number(params.labor_retreat) || 0;
            // console.log("after");
            // console.log(params);

            if (Salary.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('commission', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (!!queryVar.start || !!queryVar.end) {
            var start = "";
            var end = "";

            if (queryVar.start) {
                start = new Date(queryVar.start);
            }
            if (queryVar.end) {
                end = new Date(queryVar.end);
            }

            delete queryVar.start;
            delete queryVar.end;

            if (start != "" && end == "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $gte: start
                        }
                    },
                        queryVar,
                    ]
                };
            } else if (start == "" && end != "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $lt: end
                        }
                    },
                        queryVar,
                    ]
                };
            } else if (start != "" && end != "") {
                queryVar = {
                    $and: [{
                        'dateObj': {
                            $gte: start,
                            $lt: end
                        }
                    },
                        queryVar,
                    ]
                };
            }
            // console.log(queryVar);
        }

        return {
            data: Commission.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Commission.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Commission insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Commission');
            // params.fullname = params.countryname+" - "+params.value;
            if (Commission.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Salary update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Commission.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Commission DELETE");
            // console.log(this.bodyParams);
            if (Commission.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('cmsget', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Cmsget.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Cmsget.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmsget insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('cmsget');
            // params.fullname = params.countryname+" - "+params.value;
            if (Cmsget.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmsget update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Cmsget.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmsget DELETE");
            // console.log(this.bodyParams);
            if (Cmsget.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('workdays', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Workdays.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Workdays.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Workdays insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            // params.fullname = params.countryname+" - "+params.value;
            if (Workdays.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Workdays update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Workdays.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Workdays DELETE");
            // console.log(this.bodyParams);
            if (Workdays.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('cmslevel', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Cmslevel.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Cmslevel.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmslevel insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('cmslevel');
            // params.fullname = params.countryname+" - "+params.value;
            if (Cmslevel.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmslevel update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Cmslevel.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Cmslevel DELETE");
            // console.log(this.bodyParams);
            if (Cmslevel.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('oprec', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        return {
            data: Oprec.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Oprec.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Oprec insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('oprec');
            // params.fullname = params.countryname+" - "+params.value;
            if (Oprec.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Oprec update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Oprec.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Oprec DELETE");
            // console.log(this.bodyParams);
            if (Oprec.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('interestcondition', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };
        if (isEmpty(queryVar))
            queryVar = {
                companyReceivables: { $ne: '1' }
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Interestcondition.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Interestcondition.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Interestcondition insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Interestcondition.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Interestcondition.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Interestcondition.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('account', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Account.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Account.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Account.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Account.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Account.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('account1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Account1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Account1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Account1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Account1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Account1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('account2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Account2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Account2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Account2.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Account2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Account2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('account3', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Account3.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Account3.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('account');
            // params.fullname = params.countryname+" - "+params.value;
            if (Account3.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Account3.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Account3.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('dayoff_set', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Dayoff_set.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Dayoff_set.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Dayoff_set');
            // params.fullname = params.countryname+" - "+params.value;
            if (Dayoff_set.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Dayoff_set.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Dayoff_set.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('dayoff', {
    authRequired: false
}, {
    get: function () {
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        if (!!queryVar.findyear) {
            var findyear = queryVar.findyear;
            delete queryVar.findyear;

            queryVar.start_time = {
                $regex: ".*/" + findyear + " .*",
                $options: 'i'
            };
        }


        if (!!queryVar.isrecord) {
            var isrecord = queryVar.isrecord;
            queryVar.dayoff_status = {
                $ne: "1"
            };
            delete queryVar.isrecord;
        }
        /*else {
          queryVar = {
            $and: [
              {$or: [
                {dayoff_status: {$exists: false}},
                // {dayoff_status: { $eq: "1" }},
                {dayoff_status: "1"},
              ]},
              queryVar,
            ]
          };
        }*/

        /*if(!!queryVar.isrecord && queryVar.isrecord == "1"){ // 下面的 開始時間大於現在時間
    
              queryVar.start_time_d = {
                  $lt: new Date()
    
              }
              delete queryVar.isrecord;
              // console.log("queryVar 1: ");
            }
            else{
              queryVar = {
                $and: [
                  {$or: [
                    {start_time_d: {$exists: false}},
                    {start_time_d: {$gte: new Date()}},
                  ]},
                  queryVar,
                ]
              };
              // console.log("queryVar 2: ");
            }*/

        // console.log("Rest dayoff GET");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);
        sortVar = {
            start_time: -1
        };

        return {
            data: Dayoff.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Dayoff.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest dayoff insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('dayoff');
            // params.fullname = params.countryname+" - "+params.value;
            if (Dayoff.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest dayoff update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);

            function parseDateTime(str) {
                if (!str)
                    return "";
                else
                    return new Date(Date.parse(str, "mm/dd/yyyy hh:MM tt"));
            }

            params.start_time_d = parseDateTime(params.start_time);
            params.end_time_d = parseDateTime(params.end_time);

            params.start_time_year = parseDateTime(params.start_time_d).getFullYear();
            params.start_time_month = parseDateTime(params.start_time_d).getMonth() + 1;

            let userdata = Meteor.users.findOne({
                _id: params.apply_id
            });

            delete params.no_dayoff;

            //total_hours 請假時數
            //dayoff13 可用補休時數
            //day_off_class10 特休假
            //day_off_class12 補休假
            //day_off_class13 補休申請

            //補休天數不足
            if (params.day_off_class == "12") {
                var getDayoffSelYear = Number(new Date().getFullYear());
                var userData = Meteor.users.findOne({
                    "_id": params.apply_id
                });
                if (!!userData.arr_dayoff && userData.arr_dayoff.length > 0) { // users有arr_dayoff[]
                    var arr = userData.arr_dayoff;
                    var data = {};
                    var isData = 0;

                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].year == getDayoffSelYear) {
                            isData = 1;
                            data = arr[i];
                            break;
                        }
                    }
                    var day_surplus = (Number(data.dayoff13)) - (Number(data.dayoff12))
                    if (isData == 1 && (Number(day_surplus)) < (Number(params.total_hours))) {
                        params.dayoff_status = "1";
                        params.no_dayoff = "1";
                    }
                }
            }

            //特休天數不足或不再有效日期
            // if (params.day_off_class == "10") {
            //   var userData = Meteor.users.findOne({"_id": params.apply_id});
            //   var special_dayoff_ava = 0;     // 可用
            //   var special_dayoff_used = 0;    // 已用
            //   var special_dayoff_surplus = 0; // 剩餘
            //   params.total_hours //申請時數

            //   if(!userData.onbroad_date){
            //       special_dayoff_ava = 0;
            //   }
            //   var JobDays = (new Date() - new Date(userData.onbroad_date))/1000/3600/24; //365.2422
            //   var year = parseInt( JobDays / 365 );
            //   var day = parseInt(JobDays % 365 );
            //   if(year < 1 ){
            //       if(day < 183){
            //           special_dayoff_ava = 0;
            //       }
            //       special_dayoff_ava = 3;
            //   }
            //   else if(year >= 1 && year < 2){
            //       special_dayoff_ava = 7;
            //   }
            //   else if(year >= 2 && year < 3){
            //       special_dayoff_ava = 10;
            //   }
            //   else if(year >= 3 && year < 5){
            //       special_dayoff_ava = 14;
            //   }
            //   else if(year >= 5 && year < 10){
            //       special_dayoff_ava = 15;
            //   }
            //   else if(year >= 10){
            //       special_dayoff_ava =(year - 10) + 16;
            //       if(special_dayoff_ava > 30){
            //           special_dayoff_ava = 30;
            //       }
            //   }
            //   special_dayoff_ava_hour = special_dayoff_ava * 8;//可用時數
            //   var d = new Date();
            //   var n = new Date(userData.onbroad_date);
            //   var now_time = currentdate(d)//現在時間 ex:2017/3/22
            //   var ava_start_date = new Date((d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate());//有效開始日期
            //   var add_half = ava_start_date;
            //   var ava_end_date = new Date((add_half.getFullYear() + 1)+"/"+ (add_half.getMonth()+1) +"/"+ add_half.getDate());//有效結束日期
            //   var submit_dayoff = new Date(params.start_time);//申請特休假的開始時間

            //   if (submit_dayoff < ava_start_date || submit_dayoff > ava_end_date) {//如果申請時間不在有效日期範圍內
            //     params.dayoff_status = "1";
            //     params.no_dayoff = "1";
            //   }
            //   console.log("special_dayoff_ava")
            //   console.log(special_dayoff_ava)
            //   console.log("special_dayoff_used")
            //   console.log(special_dayoff_used)
            // }

            if (params.dayoff_status == "5") { // 從人資審核4 到 核可5

                let year = params.start_time_d.getFullYear();
                let userdata = Meteor.users.findOne({
                    _id: params.apply_id
                });

                if (!userdata.arr_dayoff) {
                    userdata.arr_dayoff = [];
                }

                function findDayoffByYear(arr, year) {
                    if (arr.length == 0) return 0;

                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].year == year) {
                            return 1;
                        }
                    }
                    return 0;
                }

                // console.log("userdata");
                // console.log(userdata);

                if (!findDayoffByYear(userdata.arr_dayoff, year)) { // 如果沒有找到有存過該年度的資料的話，就要新增
                    var element = {
                        year: year,
                        dayoff1: 0,
                        dayoff2: 0,
                        dayoff3: 0,
                        dayoff4: 0,
                        dayoff5: 0,
                        dayoff6: 0,
                        dayoff7: 0,
                        dayoff8: 0,
                        dayoff9: 0,
                        dayoff10: 0,
                        dayoff11: 0,
                        dayoff12: 0,
                        dayoff13: 0,
                    }
                    userdata.arr_dayoff.push(element);
                    Meteor.users.update({
                        _id: params.apply_id
                    }, userdata);
                }

                // console.log("userdata after");
                // console.log(userdata);

                switch (params.day_off_class) {
                    case "1":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff1': Number(params.total_hours)
                            }
                        });
                        break;
                    case "2":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff2': Number(params.total_hours)
                            }
                        });
                        break;
                    case "3":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff3': Number(params.total_hours)
                            }
                        });
                        break;
                    case "4":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff4': Number(params.total_hours)
                            }
                        });
                        break;
                    case "5":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff5': Number(params.total_hours)
                            }
                        });
                        break;
                    case "6":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff6': Number(params.total_hours)
                            }
                        });
                        break;
                    case "7":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff7': Number(params.total_hours)
                            }
                        });
                        break;
                    case "8":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff8': Number(params.total_hours)
                            }
                        });
                        break;
                    case "9":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff9': Number(params.total_hours)
                            }
                        });
                        break;
                    case "10":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff10': Number(params.total_hours)
                            }
                        });
                        break;
                    case "11":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff11': Number(params.total_hours)
                            }
                        });
                        break;
                    case "12":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff12': Number(params.total_hours)
                            }
                        });
                        break;
                    case "13":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff13': Number(params.total_hours)
                            }
                        });
                        break;
                    default:
                    //console.log("none: " + params.day_off_class);
                }

            }

            if (params.no_dayoff != "1" && params.dayoff_status >= "2" && params.dayoff_status <= "5") { // 進來的話就是成功請假的意思
                let to = subject = context = cc = bcc = "";
                let data = {};
                let to_id = "";
                if (params.dayoff_status == "2") { // 請代理人覆核
                    data = Emailauto.findOne({
                        type: "3a"
                    });

                    to_id = params.substitute;
                } else if (params.dayoff_status == "3") { // 送交主管簽核
                    data = Emailauto.findOne({
                        type: "3b"
                    });

                    to_id = params.supervisor;
                } else if (params.dayoff_status == "4") { // 送交人資簽核
                    data = Emailauto.findOne({
                        type: "3c"
                    });

                    to_id = params.hr;
                } else if (params.dayoff_status == "5") { // 人資提交已生效，cc給主管等
                    data = Emailauto.findOne({
                        type: "3d"
                    });
                    to_id = params.apply_id;

                    // console.log("params:");
                    // console.log(params);
                    let sub_mail = Meteor.users.findOne({
                        "_id": params.substitute
                    }).emails[0].address;
                    let sup_mail = Meteor.users.findOne({
                        "_id": params.supervisor
                    }).emails[0].address;
                    cc = sub_mail + "," + sup_mail + "," + data.cc;
                    bcc = data.bcc;
                }

                to = Meteor.users.findOne({
                    "_id": to_id
                }).emails[0].address;
                subject = data.subject;
                context = data.context;

                let apply_name = params.apply_chtname;
                if (context.indexOf("[apply_name]") != -1) {
                    context = context.replace(/\[apply_name]/g, apply_name);
                }
                let dayoff_type = funcObjFind(objDay_off, params.day_off_class);
                if (context.indexOf("[dayoff_type]") != -1) {
                    context = context.replace(/\[dayoff_type]/g, dayoff_type);
                }
                let start_time = params.start_time;
                if (context.indexOf("[start_time]") != -1) {
                    context = context.replace(/\[start_time]/g, start_time);
                }
                let end_time = params.end_time;
                if (context.indexOf("[end_time]") != -1) {
                    context = context.replace(/\[end_time]/g, end_time);
                }

                let day = parseInt(params.total_hours / 8);
                let hour = params.total_hours % 8;
                let dayoff_hours = day + "天 " + hour + "時";
                if (context.indexOf("[dayoff_hours]") != -1) {
                    context = context.replace(/\[dayoff_hours]/g, dayoff_hours);
                }

                let url = "https://system.hanburyifa.com/";
                if (context.indexOf("[url]") != -1) {
                    context = context.replace(/\[url]/g, url);
                }
                // bcc = data.bcc || "";
                Meteor.call("sendMailHtml", to, subject, context, cc, bcc);

            }
            // params.leave_start_time_d = parseDateTime(params.leave_start_time);
            // params.leave_end_time_d   = parseDateTime(params.leave_end_time);

            if (Dayoff.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE2");
            // console.log(this.bodyParams);
            function parseDateTime(str) {
                if (!str)
                    return "";
                else
                    return new Date(Date.parse(str, "mm/dd/yyyy hh:MM tt"));
            }

            var params = this.bodyParams;
            params.start_time_d = parseDateTime(params.start_time);

            if (params.dayoff_status == "5")
                var year = params.start_time_d.getFullYear();

            if (Dayoff.remove(this.bodyParams._id)) {
                switch (params.day_off_class) {
                    case "1":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff1': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "2":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff2': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "3":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff3': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "4":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff4': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "5":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff5': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "6":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff6': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "7":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff7': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "8":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff8': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "9":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff9': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "10":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff10': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "11":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff11': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "12":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff12': -Number(params.total_hours)
                            }
                        });
                        break;
                    case "13":
                        Meteor.users.update({
                            "_id": params.apply_id,
                            "arr_dayoff.year": year
                        }, {
                            $inc: {
                                'arr_dayoff.$.dayoff13': -Number(params.total_hours)
                            }
                        });
                        break;
                    default:
                    //console.log("none: " + params.day_off_class);
                }
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('salary_structure', {
    authRequired: false
}, {
    get: function () {
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        if (!!queryVar.findyear) {
            var findyear = queryVar.findyear;
            // delete queryVar.findyear;

            // queryVar.year = {$regex : ".*/"+findyear+" .*", $options: 'i'};
        }

        var arr_ret = [];

        var arr = Salary.find(queryVar, {
            sort: sortVar,
            skip: pageVar.skip,
            limit: pageVar.limit
        }).fetch();

        // 這邊是每一個薪水資料的
        for (var i = 0; i < arr.length; i++) {
            var entry = arr[i];
            // console.log("Salary:");
            // console.log(entry);

            var arr_dayoff = Dayoff.find({
                apply_id: entry.user_id,
                start_time_year: Number(entry.year),
                start_time_month: Number(entry.month),
                dayoff_status: "5"
            }).fetch();

            var total_deductions = 0;
            var sub_arr_do = [];
            // 這邊有進來的話 就是有請假的資料
            for (var j = 0; j < arr_dayoff.length; j++) {
                // console.log("Dayoff:");
                // console.log(arr_dayoff[j]);

                /*
                本薪base_salary     請假日期
                津貼allowance       假別 day_off_class
                計算equation        應扣薪資計算（要把天數算進去）
                應扣薪資deductions   請假天數
                */

                var equ = "";
                var b = Number(entry.base_salary) || 0;
                var a = Number(entry.allowance) || 0;

                var ds = Dayoff_set.findOne({
                    all_day_off_class: arr_dayoff[j].day_off_class
                });
                var ths = arr_dayoff[j].total_hours;
                // console.log(ds);
                var ded_days = 22;
                var ded = 0;
                if (!!ds) {
                    if (ds.deductions == "3") { // 不扣薪
                        ded = 0;
                    } else if (ds.deductions == "1") {
                        ded = Math.round((b + a) / ded_days / 8 * ths);
                    } else if (ds.deductions == "2") { // halt
                        ded = Math.round((b + a) / ded_days / 2 / 8 * ths);
                    }
                    ded_days = Number(ds.equation);
                } else {
                    ded = Math.round((b + a) / ded_days / 8 * ths);
                }

                if (!!b || !!a) {
                    equ = "(" + b + "+" + a + ") ÷ " + ded_days + "÷ 8 *" + ths + " = " + ded;
                }

                var element = {
                    base_salary: arr_dayoff[j].start_time,
                    allowance: funcObjFind(objDay_off, arr_dayoff[j].day_off_class),
                    equation: equ,
                    deductions: ded,
                    is_dayoff: "1"
                };
                total_deductions += ded; // 要算
                sub_arr_do.push(element);

            }
            // console.log("sub_arr_do");
            // console.log(sub_arr_do);

            if (!!total_deductions) {
                // 把累積應扣薪資 寫回資料庫
                var s_params = {}
                s_params.total_deductions = total_deductions;
                Salary.update(entry._id, {
                    $set: s_params
                });
            }
            entry.total_deductions = total_deductions;


            arr_ret.push(entry);
            arr_ret = arr_ret.concat(sub_arr_do);

            // console.log("arr_ret");
            // console.log(arr_ret);
        }
        return {
            data: arr_ret,
            /*data: Salary.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
              }).fetch(),*/
            // itemsCount: Salary.find(queryVar).count()
            itemsCount: arr_ret.length
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('dayoff');
            // params.fullname = params.countryname+" - "+params.value;
            if (Salary.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Account insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest salary_structure update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);

            // function parseDateTime(str){
            //   if(!str)
            //     return "";
            //   else
            //     return new Date(Date.parse(str, "mm/dd/yyyy hh:MM tt"));
            // }

            if (Salary.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Account DELETE");
            // console.log(this.bodyParams);
            if (Salary.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Account removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('accountyear', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };
        return {
            data: Accountyear.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Accountyear.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Accountyear insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Accountyear');
            // params.fullname = params.countryname+" - "+params.value;
            if (Accountyear.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Accountyear insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Bankacc update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Accountyear.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Bankacc updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Bankacc DELETE");
            // console.log(this.bodyParams);
            if (Accountyear.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Bankacc removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('bankacc', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };
        return {
            data: Bankacc.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Bankacc.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Bankacc insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Bankacc');
            // params.fullname = params.countryname+" - "+params.value;
            if (Bankacc.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Bankacc insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Bankacc update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Bankacc.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Bankacc updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Bankacc DELETE");
            // console.log(this.bodyParams);
            if (Bankacc.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Bankacc removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('formcenter1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Formcenter1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Formcenter1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Formcenter');
            // params.fullname = params.countryname+" - "+params.value;
            if (Formcenter1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Formcenter insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Formcenter1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter DELETE");
            // console.log(this.bodyParams);
            if (Formcenter1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Formcenter removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('formcenter2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Formcenter2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Formcenter2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Formcenter2');
            // params.fullname = params.countryname+" - "+params.value;
            if (Formcenter2.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Formcenter insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Formcenter2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter DELETE");
            // console.log(this.bodyParams);
            if (Formcenter2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Formcenter removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('reportcenter1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Reportcenter1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Reportcenter1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Formcenter');
            // params.fullname = params.countryname+" - "+params.value;
            if (Reportcenter1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Formcenter insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Reportcenter1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter DELETE");
            // console.log(this.bodyParams);
            if (Reportcenter1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Formcenter removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('reportcenter2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Reportcenter2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Reportcenter2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Formcenter2');
            // params.fullname = params.countryname+" - "+params.value;
            if (Reportcenter2.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Formcenter insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Reportcenter2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Formcenter DELETE");
            // console.log(this.bodyParams);
            if (Reportcenter2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Formcenter removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('businessgroup', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        if (isEmpty(sortVar))
            sortVar = {
                order_id: 1
            };

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Businessgroup.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Businessgroup.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Businessgroup insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('Businessgroup');
            // params.fullname = params.countryname+" - "+params.value;
            if (Businessgroup.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Businessgroup insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Businessgroup update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Businessgroup.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Account updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Businessgroup DELETE");
            // console.log(this.bodyParams);
            if (Businessgroup.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Businessgroup removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('booking', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        //console.log("queryVar: ");
        //console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        if (isEmpty(sortVar))
            sortVar = {
                invoice_date: -1,
                date: 1,
                uid: 1
            };
        return {
            data: Booking.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Booking.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Booking insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = funcStrPad(getNextSequence('booking'), 7);
            // params.fullname = params.countryname+" - "+params.value;
            if (Booking.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Booking update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);

            // params.total = params.cost0_money + params.cost1_money*params.rate + params.cost2_money*params.rate + params.cost3_money*params.rate + params.cost4_money*params.rate + params.cost5_money*params.rate;

            // params.total = Number(params.cost0_money) +
            //     Number(params.cost1_money*params.rate) +
            //     Number(params.cost2_money*params.rate) +
            //     Number(params.cost3_money*params.rate) +
            //     Number(params.cost4_money*params.rate) +
            //     Number(params.cost5_money*params.rate);


            if (!!params.cost0_money && !!params.rate) {
                // params.total = Number(params.cost0_money) * Number(params.rate);
                params.total = Math.round(Number(params.cost0_money) * Number(params.rate));
            }
            if (!!params.account3_id && !!Account3.findOne({
                _id: params.account3_id
            })) {
                params.account2_id = Account3.findOne({
                    _id: params.account3_id
                }).a2_id;
            }

            if (Booking.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Booking updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Booking DELETE");
            // console.log(this.bodyParams);
            if (Booking.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Booking removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('country', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Country.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Country.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Country insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('accounting');
            // params.fullname = params.countryname+" - "+params.value;
            if (Country.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Country insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Country update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Country.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Country DELETE");
            // console.log(this.bodyParams);
            if (Country.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Country removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('accounting', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Accounting.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Accounting.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Accounting insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('accounting');
            // params.fullname = params.countryname+" - "+params.value;
            if (Accounting.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Accounting insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Teams update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            //console.log(params);
            if (Accounting.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest Teams DELETE");
            // console.log(this.bodyParams);
            if (Accounting.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('clientlist', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};

        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);


        var object = Meteor.users.find(queryVar, {
            sort: sortVar,
            skip: pageVar.skip,
            limit: pageVar.limit
        }).fetch()

        var new_obj = [];
        for (var index = 0; index < object.length; index++) {
            var attr = object[index];


            var d1 = "";
            if (attr.services.resume.loginTokens.length > 0)
                d1 = attr.services.resume.loginTokens[attr.services.resume.loginTokens.length - 1].when;
            var d2 = attr.createdAt;

            var obj = {
                _id: attr._id,
                // uid: attr.uid,
                team: attr.profile.team,
                username: attr.username,
                email: attr.emails[0].address,
                name: attr.profile.name,
                name2: attr.profile.name2,
                roles: attr.profile.roles,
                cms1: attr.profile.cms1,
                cms2: attr.profile.cms2,
                cms3: attr.profile.cms3,
                loginedAt: Api.mydate(d1),
                createdAt: Api.mydate(d2),
            }
            new_obj.push(obj);

        }

        // console.log(new_obj);

        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: new_obj,
            itemsCount: Meteor.users.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest User insert POST");
            //console.log(this.bodyParams);

            var params = this.bodyParams;
            // params.uid = getNextSequence('provider');
            // params.uid = getNextSequence('users');
            // params.fullname = params.countryname+" - "+params.value;
            /*  if (Teams.insert(params)) {
    
                    return {insert: 'success', data: {message: 'Teams insert'}};
                  }
                  return {
                    statusCode: 404,
                    body: {status: 'fail', message: 'Article not found'}
                  };*/
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            //console.log("Rest User update PUT");
            //console.log(this.bodyParams);
            var params = this.bodyParams;
            // var id = params._id;
            // delete params._id;
            //console.log(params);
            // if (Teams.update(id, {$set: params })) {
            Meteor.users.update(params._id, {
                $set: {
                    "username": params.username,
                    // "emails[0].address": params.email,
                    "profile.name": params.name,
                    "profile.name2": params.name2,
                    "profile.team": params.team,
                    "profile.cms1": params.cms1,
                    "profile.cms2": params.cms2,
                    "profile.cms3": params.cms3,
                    "profile.roles": params.roles,
                }
            });

            var arrRoles = [];
            if (params.roles == 1) {
                arrRoles.push('guest');
            } else if (params.roles == 10) {
                arrRoles.push('agent');
            } else if (params.roles == 20) {
                arrRoles.push('agent');
                arrRoles.push('agentleader');
            } else if (params.roles == 30) {
                arrRoles.push('commission');
            } else if (params.roles == 40) {
                arrRoles.push('commission');
                arrRoles.push('director1');
            } else if (params.roles == 41) {
                arrRoles.push('commission');
                arrRoles.push('director2');
            } else if (params.roles == 42) {
                arrRoles.push('commission');
                arrRoles.push('director1');
                arrRoles.push('director2');
                arrRoles.push('director4');
            } else if (params.roles == 50) {
                arrRoles.push('agent');
                arrRoles.push('commission');
                arrRoles.push('director');
                arrRoles.push('supervisor');
            } else if (params.roles == 90) {
                arrRoles.push('administrative');
            } else if (params.roles == 95 || params.roles == 99) {
                arrRoles.push('agent');
                arrRoles.push('agentleader');
                arrRoles.push('commission');
                arrRoles.push('director');
                arrRoles.push('supervisor');
                arrRoles.push('administrative');
                arrRoles.push('admin');
            }

            // Roles.addUsersToRoles(params._id, arrRoles);
            Roles.setUserRoles(params._id, arrRoles);

            if (1) {

                // var d1 = params.services.resume.loginTokens[params.services.resume.loginTokens.length-1].when;
                // var d2 = params.createdAt;

                var obj = {
                    _id: params._id,
                    // uid: params.uid,
                    team: params.team,
                    username: params.username,
                    email: params.email,
                    name: params.name,
                    name2: params.name2,
                    roles: params.roles,
                    cms1: params.cms1,
                    cms2: params.cms2,
                    cms3: params.cms3,
                    loginedAt: params.loginedAt,
                    createdAt: params.createdAt,
                    message: 'User updated'
                }
                // console.log(obj);
                // return { status: 'success', data: obj };
                return obj;
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    /* delete: {
       // roleRequired: ['author', 'admin'],
       action: function () {
         console.log("Rest Teams DELETE");
         // console.log(this.bodyParams);
         if (Teams.remove(this.bodyParams._id)) {
           return {status: 'success', data: {message: 'Teams removed'}};
         }
         return {
           statusCode: 404,
           body: {status: 'fail', message: 'Article not found'}
         };
       }
     },*/
});
Api.addRoute('teams', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Teams.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Teams.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Teams insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('provider');
            // params.fullname = params.countryname+" - "+params.value;
            if (Teams.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Teams insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Teams update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Teams.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Teams DELETE");
            // console.log(this.bodyParams);
            if (Teams.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Teams removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('provider', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest Teams GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Provider.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Provider.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // params.uid = getNextSequence('provider');
            // params.fullname = params.countryname+" - "+params.value;
            if (Provider.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Provider insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Provider update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Provider.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Teams updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Provider not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Provider DELETE");
            // console.log(this.bodyParams);
            if (Provider.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('products', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Products.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Products.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            params.uid = getNextSequence('provider');
            params.fullname = params.countryname + " - " + params.value;
            // console.log(params);
            if (Products.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest products update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Products.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest products DELETE");
            // console.log(this.bodyParams);
            if (Products.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('review1', {
    authRequired: false
}, {
    get: function () {
        //console.log("Rest review1 GET");
        // console.log(this.urlParams);
        // console.log(this.queryParams);

        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Review1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Review1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review1 insert POST");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            // params.uid = getNextSequence('Review1');
            // params.fullname = params.countryname+" - "+params.value;

            if (Review1.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Article removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review1 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;

            // params.fullname = params.country+" - "+params.countryname,
            var id = params._id;
            delete params._id;
            // console.log(params);
            // params.fullname = params.countryname+" - "+params.value;

            if (Review1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Provider updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review1 DELETE");
            // console.log(this.bodyParams);
            if (Review1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Review1 removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('review2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest review1 GET");
        // console.log(this.urlParams);
        // console.log(this.queryParams);

        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Review2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Review2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review2 insert POST");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            // params.uid = getNextSequence('Review2');
            // params.fullname = params.countryname+" - "+params.value;

            if (Review2.insert(params)) {
                return {
                    insert: 'success',
                    data: {
                        message: 'Article removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review2 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;

            // params.fullname = params.country+" - "+params.countryname,
            var id = params._id;
            delete params._id;
            // console.log(params);
            // params.fullname = params.countryname+" - "+params.value;

            if (Review2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest Review2 DELETE");
            // console.log(this.bodyParams);
            if (Review2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Review2 removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('fundconfigure', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: FundConfigure.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: FundConfigure.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (FundConfigure.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product1 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            var isShow = params.isShow;
            delete params.isShow;

            // checkbox 要改 null 才能work
            // console.log('res', isMark)
            if (isShow == 'true') {
                params.isShow = 'true';
            } else {
                params.isShow = null;
            }

            // Portfolios.update({
            //     product1_id: id
            // }, {
            //     $set: {
            //         product1_text: params.value,
            //     }
            // }, {
            //     multi: true
            // });

            delete params._id;
            // console.log(params);
            // console.log(id);
            if (FundConfigure.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product1 DELETE");
            // console.log(this.bodyParams);
            if (FundConfigure.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('product1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Product1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Product1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Product1.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product1 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;

            Portfolios.update({
                product1_id: id
            }, {
                $set: {
                    product1_text: params.value,
                }
            }, {
                multi: true
            });

            delete params._id;
            // console.log(params);
            if (Product1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product1 DELETE");
            // console.log(this.bodyParams);
            if (Product1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('product2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Product2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Product2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Product2.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product2 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;

            Portfolios.update({
                product2_id: id
            }, {
                $set: {
                    product2_text: params.name_cht,
                }
            }, {
                multi: true
            });

            delete params._id;
            // console.log(params);
            if (Product2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product2 DELETE");
            // console.log(this.bodyParams);
            if (Product2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('product3', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Product3.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Product3.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Product3.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product3 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;

            Portfolios.update({
                product3_id: id
            }, {
                $set: {
                    product3_text: params.name_cht,
                }
            }, {
                multi: true
            });

            delete params._id;
            // console.log(params);
            if (Product3.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product3 DELETE");
            // console.log(this.bodyParams);
            if (Product3.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('product4', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest products GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Product4.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Product4.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest provider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Product4.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Products insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product4 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;

            var p4 = Product4.findOne({
                _id: id
            });
            params.provider_chttext = p4.provider_chttext || "";
            params.provider_engtext = p4.provider_engtext || "";

            Portfolios.update({
                product4_id: id
            }, {
                $set: {
                    product4_text: params.name_cht,
                    template_id: params.template_id,
                    provider_id: params.provider_id,
                    provider_chttext: params.provider_chttext,
                    provider_engtext: params.provider_engtext,
                }
            }, {
                multi: true
            });

            delete params._id;
            // console.log(params);
            if (Product4.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Products updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest product4 DELETE");
            // console.log(this.bodyParams);
            if (Product4.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Provider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});
Api.addRoute('oproduct1', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest oproducts GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Oproduct1.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Oproduct1.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oprovider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Oproduct1.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Oproducts insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct1 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Oproduct1.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Oproducts updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct1 DELETE");
            // console.log(this.bodyParams);
            if (Oproduct1.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Oprovider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('oproduct2', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest oproducts GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Oproduct2.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Oproduct2.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oprovider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Oproduct2.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Oproducts insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct2 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Oproduct2.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Oproducts updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct2 DELETE");
            // console.log(this.bodyParams);
            if (Oproduct2.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Oprovider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('oproduct3', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest oproducts GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Oproduct3.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Oproduct3.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oprovider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Oproduct3.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Oproducts insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct3 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Oproduct3.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Oproducts updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct3 DELETE");
            // console.log(this.bodyParams);
            if (Oproduct3.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Oprovider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});

Api.addRoute('oproduct4', {
    authRequired: false
}, {
    get: function () {
        // console.log("Rest oproducts GET");
        var queryVar, sortVar = {},
            pageVar = {};

        queryVar = this.queryParams || {};
        Api.getSortParams(queryVar, sortVar);
        Api.getPageParams(queryVar, pageVar);
        // console.log("queryVar: ");
        // console.log(queryVar);
        // console.log("sortVar: ");
        // console.log(sortVar);
        // console.log("pageVar: ");
        // console.log(pageVar);

        return {
            data: Oproduct4.find(queryVar, {
                sort: sortVar,
                skip: pageVar.skip,
                limit: pageVar.limit
            }).fetch(),
            itemsCount: Oproduct4.find(queryVar).count()
        }
    },
    post: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oprovider insert POST");
            // console.log(this.bodyParams);

            var params = this.bodyParams;
            // console.log(params);
            if (Oproduct4.insert(params)) {

                return {
                    insert: 'success',
                    data: {
                        message: 'Oproducts insert'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    put: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct4 update PUT");
            // console.log(this.bodyParams);
            var params = this.bodyParams;
            var id = params._id;
            delete params._id;
            // console.log(params);
            if (Oproduct4.update(id, {
                $set: params
            })) {
                params._id = id;
                return params;
                // return {status: 'success', data: {message: 'Oproducts updated'}};
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
    delete: {
        // roleRequired: ['author', 'admin'],
        action: function () {
            // console.log("Rest oproduct4 DELETE");
            // console.log(this.bodyParams);
            if (Oproduct4.remove(this.bodyParams._id)) {
                return {
                    status: 'success',
                    data: {
                        message: 'Oprovider removed'
                    }
                };
            }
            return {
                statusCode: 404,
                body: {
                    status: 'fail',
                    message: 'Article not found'
                }
            };
        }
    },
});