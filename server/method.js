import { Email } from 'meteor/email'
import { check } from 'meteor/check'

Meteor.methods({
  checkIsLoginable: function (id) {
    var user = Meteor.users.findOne(id);

    if (!!user.auth && user.auth.is_auth == "0") {
      return 0;
    }
    else {
      return 1;
    }
  },
  getEmailByWorknumber: function (worknumber) {
    var arr = Meteor.users.find().fetch();
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].profile.worknumber == worknumber) {
        return arr[i].username;
      }
      if (arr[i].worknumber == worknumber) {
        return arr[i].username;
      }
    }
    return 0;
    // return Clients.findOne({_id: client_id});
  },
  getEmailByUserId: function (userId) {
    return Meteor.users.findOne({ _id: userId });
  },
  getClientData: function (client_id) {
    return Clients.findOne({ _id: client_id });
  },
  changePeriodNumArr: function (id, arrname, years, per_year) {
    var p = Portfolios.findOne({ _id: id });

    // console.log(arrname);
    // console.log(p[arrname]);
    if (typeof p[arrname] == "undefined") {
      p[arrname] = [{ order_id: 1 }];
    }

    years = Number(years);
    var tmpArr = p[arrname];
    // console.log("before");
    // console.log(tmpArr);
    for (var i = tmpArr.length + 1; i <= years; i++) {
      tmpArr.push({ order_id: i });
    }

    var i = years;
    while (tmpArr.length > i) {
      // delete p[arrname][i];
      tmpArr.splice(i, 1);
      // console.log(tmpArr);
    }

    p[arrname] = tmpArr;
    // console.log("after");
    // console.log(p[arrname]);

    Portfolios.update({ _id: id }, { $set: p });
  },
  'saveAllFundConfigure': function (csv_text) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var arrLineData = csv_text.split('\n');
      // console.log(id);
      /*if(!id || !Portfolios.findOne(id)){
        return 0;
      }*/
      var return_err = "";
      var return_suc = "";

      // for (var i = 1; i < arrLineData.length; i++) {
      //   var arr = arrLineData[i].split(',');

      //   var obj = {};
      //   obj.LifePlanReference      = arr[4]; // 要當寫入的id用

      //   var p = Portfolios.findOne({account_num: obj.LifePlanReference});
      //   Portfolios.update({account_num: obj.LifePlanReference}, {$set: {'arrFundConfigure': []} });
      // }
      for (var i = 1; i < arrLineData.length; i++) {
        var arr = arrLineData[i].split(',');

        var obj = {};

        //     PlanID  PolicyId  LifePlanReference
        // PriorLifePlanReference
        //  FundCurrencyID    PlanCurrencyID  PlanCurrency
        // ValuationCurrencyID ValuationCurrency
        obj.FundDescription = arr[7];
        obj.UnitType = arr[8];
        obj.PriceDate = arr[11];
        obj.BidPrice = arr[10];
        obj.UnitsHeld = arr[9];
        obj.FundCurrency = arr[12];
        obj.FundCurrencyValue = arr[13];
        obj.ValuationCurrencyValue = arr[17];

        obj.LifePlanReference = arr[3]; // 要當寫入的id用

        var arrExTime = arr[0].split(" ");
        var exTime = "";
        if (arrExTime[1] == "¤U¤È") {
          exTime = arrExTime[0] + " PM " + arrExTime[2];
        }
        else {
          exTime = arrExTime[0] + " AM " + arrExTime[2];
        }

        // obj.ExtractTime            = arr[0];
        obj.ExtractTime = exTime;
        obj.insertAt = new Date();

        // console.log(obj);
        var p = Portfolios.findOne({ account_num: obj.LifePlanReference });

        if (!!p && !p.arrFundConfigure) {
          // Portfolios.update({_id: id}, {$set: {'arrFundConfigure': []} });
          Portfolios.update({ account_num: obj.LifePlanReference }, { $set: { 'arrFundConfigure': [] } });
          p.arrFundConfigure = [];
        }
        if (!p) {
          return_err = return_err + "無此計劃號碼：" + obj.LifePlanReference + "<BR>";
        } else {
          var f = FundConfigure.findOne({ FundDescription: obj.FundDescription });
          var fundConfigureAll = p.arrFundConfigure;
          obj.order_id = p.arrFundConfigure.length + 1;
          console.log("////////2////////");
          console.log(obj)
          var index = fundConfigureAll.findIndex(item => item.PriceDate === obj.PriceDate && item.BidPrice === obj.BidPrice
            && item.UnitType === obj.UnitType && item.FundDescription === obj.FundDescription)
          if (index === -1) {
            console.log("push");
            Portfolios.update({ account_num: obj.LifePlanReference }, { $push: { 'arrFundConfigure': obj } });
          }

          if (!f) {
            console.log("insert");
            FundConfigure.insert(obj);
            FundConfigure.update({ FundDescription: obj.FundDescription }, { $set: { 'arrRecord': [obj] } });
          }
          // 不同日期新增
          var cnt = FundConfigure.find({ FundDescription: obj.FundDescription, 'arrRecord.PriceDate': obj.PriceDate }, {}).fetch()

          if (f && f['arrRecord'] && cnt.length == 0) {
            FundConfigure.update({ FundDescription: obj.FundDescription }, { $push: { 'arrRecord': obj } });
          }
          return_suc = return_suc + '匯入計劃號碼：<a target="_blank" href="/portfolio/' + p._id + '">' + obj.LifePlanReference
            + "</a> PriceDate: " + obj.PriceDate
            + "</a> UnitType: " + obj.UnitType
            + " ValuationCurrencyValue: " + obj.ValuationCurrencyValue + "<BR>";

        }
      }
    }
    return { success: return_suc, error: return_err };
  },
  'saveFundConfigure': function (id, csv_text) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var arrLineData = csv_text.split('\n');
      console.log(id);
      if (!id || !Portfolios.findOne(id)) {
        return 0;
      }

      for (var i = 1; i < arrLineData.length; i++) {
        var arr = arrLineData[i].split(',');

        var obj = {};

        //     PlanID  PolicyId  LifePlanReference
        // PriorLifePlanReference
        //  FundCurrencyID    PlanCurrencyID  PlanCurrency
        // ValuationCurrencyID ValuationCurrency
        obj.FundDescription = arr[6];
        obj.UnitType = arr[7];
        obj.PriceDate = arr[10];
        obj.BidPrice = arr[9];
        obj.UnitsHeld = arr[8];
        obj.FundCurrency = arr[12];
        obj.FundCurrencyValue = arr[17];
        obj.ValuationCurrencyValue = arr[1];

        var arrExTime = arr[0].split(" ");
        var exTime = "";
        if (arrExTime[1] == "¤U¤È") {
          exTime = arrExTime[0] + " PM " + arrExTime[2];
        }
        else {
          exTime = arrExTime[0] + " AM " + arrExTime[2];
        }

        // obj.ExtractTime            = arr[0];
        obj.ExtractTime = exTime;
        obj.insertAt = new Date();

        // console.log(obj);

        var p = Portfolios.findOne(id);
        if (!!p && !p.arrFundConfigure) {
          Portfolios.update({ _id: id }, { $set: { 'arrFundConfigure': [] } });
        }

        var p = Portfolios.findOne(id);
        obj.order_id = p.arrFundConfigure.length + 1;

        // console.log("////////2////////");
        Portfolios.update({ _id: id }, { $push: { 'arrFundConfigure': obj } });
      }
    }
    return 1;
  },
  'removeUser': function (id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      // console.log(id);
      Meteor.users.remove({ _id: id });
    }
    return 1;
  },

  'updateBookingUpload': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var id = Booking.update(id, { $set: formVar });
    }
    return 1;
  },
  'updateCommissionUpload': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var id = Commission.update(id, { $set: formVar });
    }
    return 1;
  },
  'updateLocktable': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      formVar['lockbook' + formVar.month] = "1";
      formVar['lockbook' + formVar.month + "_time"] = new Date();
      formVar['lockbook' + formVar.month + "_userid"] = currentUserId;

      Accountyear.update(id, { $set: formVar });
    }
    return 1;
  },
  'updateUnLocktable': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      formVar['lockbook' + formVar.month] = "0";
      formVar['lockbook' + formVar.month + "_un_time"] = new Date();
      formVar['lockbook' + formVar.month + "_un_userid"] = currentUserId;

      Accountyear.update(id, { $set: formVar });
    }
    return 1;
  },
  'delSalary': function (id, upload_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.file_url = "";
      formVar.file = {};

      Salary.remove(id, { $set: formVar });
      console.log(upload_id);
      Uploads.remove(upload_id);
    }
    return 1;
  },
  'delSalaryUrl': function (id, upload_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.file_url = "";
      formVar.file = {};

      Salary.update(id, { $set: formVar });
      console.log(upload_id);
      Uploads.remove(upload_id);
    }
    return 1;
  },
  'updateSalaryUrl': function (id, selVar, fileVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.year = selVar.year.toString();
      formVar.month = selVar.month.toString();
      formVar.employee = selVar.employee;
      formVar.employee_id = selVar.employee_id;
      formVar.salary_id = selVar.salary_id;
      formVar.upload_id = selVar.upload_id;

      formVar.file_url = fileVar.url;
      formVar.file_rurl = fileVar.file.relative_url;
      formVar.file = fileVar;

      Salary.update(id, { $set: formVar });
    }
    return 1;
  },
  addSalaryMember: function (formVar) {

    // console.log(formVar);
    if (typeof formVar.name == "string") {
      var tmparr = []
      tmparr.push(formVar.name);
      formVar.name = tmparr;
    }
    // console.log("formVar2");
    // console.log(formVar);
    for (var i = 0; i < formVar.name.length; i++) {
      var obj = {};
      obj.bg_id = formVar.bg_id;
      obj.year = formVar.year;
      obj.month = formVar.month;
      obj.user_id = formVar.name[i];
      obj.healthcare_pe = 0;
      obj.healthcare_co = 0;
      obj.labor_pe = 0;
      obj.labor_co = 0;
      obj.labor_retreat = 0;

      // 找名字
      var arrUser = Meteor.users.find().fetch();
      for (var j = 0; j < arrUser.length; j++) {
        if (arrUser[j]._id == obj.user_id) {
          obj.engname = arrUser[j].profile.engname;
          obj.chtname = arrUser[j].profile.chtname;
          break;
        }
      }

      let lest_month = (Number(formVar.month) - 1).toString();
      let pre_salary = Salary.findOne({ year: formVar.year, month: lest_month, user_id: formVar.name[i] });

      if (!!pre_salary) {
        obj.healthcare_pe = pre_salary.healthcare_pe;
        obj.healthcare_co = pre_salary.healthcare_co;
        obj.labor_pe = pre_salary.labor_pe;
        obj.labor_co = pre_salary.labor_co;
        obj.labor_retreat = pre_salary.labor_retreat;
      }

      Salary.insert(obj);
    }
    // for()
  },
  addSalesMember: function (formVar) {

    // console.log(formVar);
    if (typeof formVar.name == "string") {
      var tmparr = []
      tmparr.push(formVar.name);
      formVar.name = tmparr;
    }
    console.log("formVar sales");
    console.log(formVar);
    for (var i = 0; i < formVar.name.length; i++) {
      var obj = {};
      obj.year = formVar.year;
      obj.user_id = formVar.name[i];

      var arrUser = Meteor.users.find().fetch();
      for (var j = 0; j < arrUser.length; j++) {
        if (arrUser[j]._id == obj.user_id) {
          obj.engname = arrUser[j].profile.engname;
          obj.chtname = arrUser[j].profile.chtname;
          obj.depart_text = arrUser[j].profile.depart_text;
          break;
        }
      }

      Fin_sales.insert(obj);
    }
    // for()
  },
  addSalesMember1: function (formVar) {

    // console.log(formVar);
    if (typeof formVar.name == "string") {
      var tmparr = []
      tmparr.push(formVar.name);
      formVar.name = tmparr;
    }
    console.log("formVar 2");
    console.log(formVar);
    for (var i = 0; i < formVar.name.length; i++) {
      var obj = {};
      obj.year = formVar.year;
      obj.user_id = formVar.name[i];

      var arrUser = Meteor.users.find().fetch();
      for (var j = 0; j < arrUser.length; j++) {
        if (arrUser[j]._id == obj.user_id) {
          obj.engname = arrUser[j].profile.engname;
          obj.chtname = arrUser[j].profile.chtname;
          obj.department_id = arrUser[j].department_id;
          break;
        }
      }

      Fin_sales1.insert(obj);
    }
    // for()
  },
  'delCommission': function (id, upload_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.file_url = "";
      formVar.file = {};

      Commission.remove(id, { $set: formVar });
      console.log(upload_id);
      Uploads.remove(upload_id);
    }
    return 1;
  },
  'delCommissionUrl': function (id, upload_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.file_url = "";
      formVar.file = {};

      Commission.update(id, { $set: formVar });
      console.log(upload_id);
      Uploads.remove(upload_id);
    }
    return 1;
  },
  'updateCommissionUrl': function (id, selVar, fileVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var formVar = {};
      formVar.year = selVar.year.toString();
      formVar.month = selVar.month.toString();
      formVar.employee = selVar.employee;
      formVar.employee_id = selVar.employee_id;
      formVar.commission_id = selVar.commission_id;
      formVar.upload_id = selVar.upload_id;

      formVar.file_url = fileVar.url;
      formVar.file_rurl = fileVar.file.relative_url;
      formVar.file = fileVar;

      Commission.update(id, { $set: formVar });
    }
    return 1;
  },
  addCommissionMember: function (formVar) {

    // console.log(formVar);
    if (typeof formVar.name == "string") {
      var tmparr = []
      tmparr.push(formVar.name);
      formVar.name = tmparr;
    }
    console.log("formVar 2");
    console.log(formVar);
    for (var i = 0; i < formVar.name.length; i++) {
      var obj = {};
      obj.bg_id = formVar.bg_id;
      obj.year = formVar.year;
      obj.month = formVar.month;
      obj.user_id = formVar.name[i];

      var arrUser = Meteor.users.find().fetch();
      for (var j = 0; j < arrUser.length; j++) {
        if (arrUser[j]._id == obj.user_id) {
          obj.engname = arrUser[j].profile.engname;
          obj.chtname = arrUser[j].profile.chtname;
          break;
        }
      }

      Commission.insert(obj);
    }
    // for()
  },
  /*addCommissionMember: function (formVar) {

    // console.log(formVar);
    if(typeof formVar.name == "string"){
      var tmparr = []
      tmparr.push(formVar.name);
      formVar.name = tmparr;
    }
    console.log("formVar 2");
    console.log(formVar);
    for(var i=0; i<formVar.name.length; i++){
      var obj = {};
      obj.bg_id = formVar.bg_id;
      obj.year  = formVar.year;
      obj.month = formVar.month;
      obj.user_id = formVar.name[i];

      var arrUser = Meteor.users.find().fetch();
      for(var j=0; j<arrUser.length; j++){
        if(arrUser[j]._id == obj.user_id ){
          obj.engname = arrUser[j].profile.engname;
          obj.chtname = arrUser[j].profile.chtname;
          break;
        }
      }

      Commission.insert(obj);
    }
    // for()
  },*/
  insertNewPortfolioArr: function (id, arrname) {
    var p = Portfolios.findOne({ _id: id });

    // console.log(p);
    // console.log(arrname);
    // console.log("before");
    // console.log(p[arrname]);
    if (typeof p[arrname] == "undefined") {
      p[arrname] = [{ order_id: 1 }];
    }
    else {
      // p[arrname] = [];
      var len = p[arrname].length + 1;
      p[arrname].push({ order_id: len });
    }
    // console.log("after");
    // console.log(p[arrname]);
    // console.log(p);

    Portfolios.update({ _id: id }, { $set: p });
  },
  getPeriodMailList: function (id) {
    var clients = [];
    switch (id) {
      case "2":
        clients = Clients.find({ periodmail2: '1' }).fetch();
        break;
      case "3":
        clients = Clients.find({ periodmail3: '1' }).fetch();
        break;
      case "4":
        clients = Clients.find({ periodmail4: '1' }).fetch();
        break;
      case "5":
        clients = Clients.find({ periodmail5: '1' }).fetch();
        break;
      case "6":
        clients = Clients.find({ periodmail6: '1' }).fetch();
        break;
      case "7":
        clients = Clients.find({ periodmail7: '1' }).fetch();
        break;
      case "8":
        clients = Clients.find({ periodmail8: '1' }).fetch();
        break;
      default:

    }

    let list = [];
    for (var i = 0; i < clients.length; i++) {
      if (!!clients[i].email) {
        list.push(clients[i].email);
      }
    }
    if (list.length) {
      return list.join();
    }
    else {
      return "";
    }
  },
  sendBirthdayReminder: function (arr, table_html) {
    // var p = Portfolios.findOne({_id: id});

    if (!arr || !arr.length) {
      return "";
    }
    // console.log(arr[0]);
    // console.log(arrname);
    // console.log("before");
    // console.log(p[arrname]);
    let to = subject = context = cc = bcc = "";

    var ag = Agents.findOne({ _id: arr[0].agent_id });
    // console.log(ag);

    if (!!Meteor.users.findOne({ "_id": ag.user_id }) && !!Meteor.users.findOne({ "_id": ag.user_id }).emails) {
      to = Meteor.users.findOne({ "_id": ag.user_id }).emails[0].address;
    }
    // console.log(to);
    // to = "yinchen618@gmail.com";

    if (!to) {
      return "錯誤：Agent 信箱為空\nagent id:" + arr[0].agent_id;
    }

    let data = {};
    data = Emailauto.findOne({ type: "1" });

    subject = data.subject;
    context = data.context;
    cc = data.cc;
    bcc = data.bcc;

    // cc += ",yinchen618@gmail.com";

    // let agent = arr[0].agent_text;
    let agent = ag.name;
    if (context.indexOf("[agent]") != -1) {
      context = context.replace(/\[agent]/g, agent);
    }

    // context = context + "<BR><BR>";
    /*
        context = "<table border='1'><tr><td width=120>Agent</td><td width=200>客戶名稱</td><td width=200>生日</td></tr>";
    
        for (var i = 0; i < arr.length; i++) {
          // context = context + arr[i].name_cht + " "+ arr[i].birthday + "<BR>";
          context = context + '<tr><td>' + arr[i].agent_text +'</td><td>' + arr[i].name_cht + "</td><td>"+ arr[i].birthday + "</td>";
        }
        context = context + "</table><BR><BR>";
    */

    context = context + '<style>\
.row-warning > td { background-color: #fcf8e3 !important; }\
.row-danger > td { background-color: #f2dede !important; }\
.my-form { margin-bottom: 10px !important; }\
th, td{ border: thin #000 solid; }</style>';
    context = context + table_html;
    // context = context + $(table_html).contents().unwrap().html();

    // cc = "";
    // bcc = "";

    Meteor.call("sendMailHtml", to, subject, context, cc, bcc);

    return "成功：Agent 信箱為 " + to;
  },
  sendFollowNewCaseReminder: function (arr, table_html) {
    // var p = Portfolios.findOne({_id: id});

    if (!arr || !arr.length) {
      return "";
    }
    // console.log(arr[0]);
    // console.log(arrname);
    // console.log("before");
    // console.log(p[arrname]);
    let to = subject = context = cc = bcc = "";

    var ag = Agents.findOne({ _id: arr[0].agent_id });
    // console.log(ag);

    if (!!Meteor.users.findOne({ "_id": ag.user_id }) && !!Meteor.users.findOne({ "_id": ag.user_id }).emails) {
      to = Meteor.users.findOne({ "_id": ag.user_id }).emails[0].address;
    }
    // console.log(to);
    // to = "yinchen618@gmail.com";

    if (!to) {
      return "錯誤：Agent 信箱為空\nagent id:" + arr[0].agent_id;
    }

    let data = {};
    data = Emailauto.findOne({ type: "4" });

    subject = data.subject;
    context = data.context;
    cc = data.cc;
    bcc = data.bcc;

    // cc += ",yinchen618@gmail.com";

    // let agent = arr[0].agent_text;
    let agent = ag.name;
    if (context.indexOf("[agent]") != -1) {
      context = context.replace(/\[agent]/g, agent);
    }

    context = context + "<table border='1'><tr><td width=120>客戶名稱</td><td width=120>Agent</td><td width=120>投資項目</td><td width=120>目前進度</td><td width=150>開始時間</td></tr>";

    // context = context + "<BR><BR>";
    for (var i = 0; i < arr.length; i++) {

      var status_text = "";
      var status_date = 0;
      if (!!arr[i].current_Status) {
        status_text = Workdays.findOne({ _id: arr[i].current_Status }).status;
        status_date = Workdays.findOne({ _id: arr[i].current_Status }).work_days;
      }

      if (!status_text) {
        status_text = "初始中";
      }
      var start_day = new Date(arr[i].insertedAt).yyyymmddhm();

      context = context + '<tr><td>' + arr[i].name_cht + '</td><td>' + arr[i].agent_text + '</td><td>' + arr[i].product4_text + '</td><td>' + status_text + "</td><td>" + start_day + "</td>";
      // context = context + arr[i].name_cht + " 進度："+ status_text + " 開始時間："+ new Date(arr[i].insertedAt).yyyymmddhm() + " 應完成天數："+ status_date + "<BR>";
    }
    context = context + "</table><BR><BR>";
    // context = context + "<BR><BR>";


    //     context = context + '<style>\
    // .row-warning > td { background-color: #fcf8e3 !important; }\
    // .row-danger > td { background-color: #f2dede !important; }\
    // .my-form { margin-bottom: 10px !important; }\
    // th, td{ border: thin #000 solid; }</style>';
    //     context = context + table_html;
    // context = context + $(table_html).contents().unwrap().html();

    Meteor.call("sendMailHtml", to, subject, context, cc, bcc);

    return "成功：Agent 信箱為 " + to;
  },
  sendFollowservice: function (arr, table_html) {
    // var p = Portfolios.findOne({_id: id});

    if (!arr || !arr.length) {
      return "";
    }
    // console.log(arr[0]);
    // console.log(arrname);
    // console.log("before");
    // console.log(p[arrname]);
    let to = subject = context = cc = bcc = "";

    var ag = Agents.findOne({ _id: arr[0].agent_id });
    // console.log(ag);

    if (!!Meteor.users.findOne({ "_id": ag.user_id }) && !!Meteor.users.findOne({ "_id": ag.user_id }).emails) {
      to = Meteor.users.findOne({ "_id": ag.user_id }).emails[0].address;
    }

    if (!to) {
      return "錯誤：Agent 信箱為空\nagent id:" + arr[0].agent_id;
    }

    let data = {};
    data = Emailauto.findOne({ type: "5" });

    subject = data.subject;
    context = data.context;
    cc = data.cc;
    bcc = data.bcc;

    // cc += ",yinchen618@gmail.com";
    // console.log(to);
    // to = "yinchen618@gmail.com";
    // cc = "";
    // bcc = "";

    // let agent = arr[0].agent_text;
    let agent = ag.name;
    if (context.indexOf("[agent]") != -1) {
      context = context.replace(/\[agent]/g, agent);
    }

    // context = "<table border='1'><tr><td width=120>客戶名稱</td> \
    // <td width=120>目前進度</td>\
    // <td width=150>開始時間</td>\
    // <td width=100>應完成天數</td></tr>";
    /*
        context = '<tr class="jsgrid-header-row"><th class="jsgrid-header-cell jsgrid-align-center" style="width: 95px;">Case Date</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 140px;">Client Name</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 100px;">Provider</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 85px;">單號</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 100px;">Service Item</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 95px;">Stage Date</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 70px;">Status</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 150px;">Description</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 100px;">Action</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 100px;">Action Owner</th><th class="jsgrid-header-cell jsgrid-align-center" style="width: 100px;">Agent</th></tr>';
    
        // context = context + "<BR><BR>";
        for (var i = 0; i < arr.length; i++) {
    
          var status_text = "";
          var status_date = 0;
          if(!!arr[i].current_Status){
            status_text = Workdays.findOne({_id: arr[i].current_Status}).status;
            status_date = Workdays.findOne({_id: arr[i].current_Status}).work_days;
          }
    
          if(!status_text){
            status_text = "初始中";
          }
          var start_day = new Date(arr[i].insertedAt).yyyymmddhm();
    
    // mic: 這邊要加每個欄位的值，用下面的看目前的欄位
    // $("#e_followservice_dis_non").jsGrid("option", "data");
          context = context + '<tr><td>' + currentdate_eff(item.insertedAt) //arr[i].name_cht
          +'</td><td>' + name_eng
          + "</td><td>"+ start_day
          + "</td><td>"+ status_date + "</td>";
          // context = context + arr[i].name_cht + " 進度："+ status_text + " 開始時間："+ new Date(arr[i].insertedAt).yyyymmddhm() + " 應完成天數："+ status_date + "<BR>";
        }
        context = context + "</table><BR><BR>";
        // context = context + "<BR><BR>";
    */
    context = context + '<style>\
.row-warning > td { background-color: #fcf8e3 !important; }\
.row-danger > td { background-color: #f2dede !important; }\
.my-form { margin-bottom: 10px !important; }\
th, td{ border: thin #000 solid; }</style>';
    context = context + table_html;
    // context = context + $(table_html).contents().unwrap().html();


    Meteor.call("sendMailHtml", to, subject, context, cc, bcc);
    // console.log(context);
    // console.log(to);
    // console.log(cc);

    return "成功：Agent 信箱為 " + to;
  },
  sendPayReminder: function (arr, table_html) {
    // var p = Portfolios.findOne({_id: id});

    // console.log(p);
    // console.log(arrname);
    // console.log("before");
    // console.log(p[arrname]);
    // console.log(arr[0]);
    let to = subject = context = cc = bcc = "";

    if (!arr || !arr.length) {
      return "";
    }

    var ag = Agents.findOne({ _id: arr[0].p_agent_id });

    if (!!Meteor.users.findOne({ "_id": ag.user_id }) && !!Meteor.users.findOne({ "_id": ag.user_id }).emails) {
      to = Meteor.users.findOne({ "_id": ag.user_id }).emails[0].address;
    }


    // console.log(to);
    // to = "yinchen618@gmail.com";

    if (!to) {
      return "錯誤：Agent 信箱為空\nagent id:" + arr[0].agent_id;
    }

    let data = {};
    data = Emailauto.findOne({ type: "2" });
    subject = data.subject;
    context = data.context;
    cc = data.cc;
    bcc = data.bcc;

    // cc += ",yinchen618@gmail.com";

    // let agent = arr[0].agent_text;
    let agent = ag.name;
    if (context.indexOf("[agent]") != -1) {
      context = context.replace(/\[agent]/g, agent);
    }

    // context = context + "<BR><BR>";
    // for (var i = 0; i < arr.length; i++) {
    //   context = context + arr[i].p_name_cht + " 規劃繳費日期："+ arr[i].pmr_payment1date  + " 規劃繳費金額："+ arr[i].pmr_payment1money + "<BR>";
    // }
    // context = context + "<BR><BR>";

    context = context + '<style>\
    .row-warning > td { background-color: #fcf8e3 !important; }\
    .row-danger > td { background-color: #f2dede !important; }\
    .my-form { margin-bottom: 10px !important; }\
    th, td{ border: thin #000 solid; }</style>';
    context = context + table_html;
    // context = context + $(table_html).contents().unwrap().html();

    Meteor.call("sendMailHtml", to, subject, context, cc, bcc);

    return "成功：Agent 信箱為 " + to;

  },
  sendMailHtml: function (to, subject, text, cc, bcc) {
    /* 其他可以用的mail的
    https://github.com/VeliovGroup/Meteor-Files
    講附件的
    https://github.com/nodemailer/mailcomposer/blob/7c0422b2de2dc61a60ba27cfa3353472f662aeb5/README.md#add-attachments
    */

    // check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    var from = "notice@hanburyifa.com";

    var cid_value1 = Date.now() + '.image1.jpg';
    var cid_value2 = Date.now() + '.image2.jpg';

    var attachments = [{
      filename: "mail_header.jpg",
      // path: "https://s3-ap-northeast-1.amazonaws.com/hanbury/mail_header.jpg",
      path: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/rY66GsLbHnTm9Tvrx-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Header.png",
      // filePath: "mail_header.jpg",
      cid: cid_value1
    }, {
      filename: "mail_footer.jpg",
      // path: "https://s3-ap-northeast-1.amazonaws.com/hanbury/mail_footer.jpg",
      path: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/MFL94578AMBKprg9w-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Logo.png",
      // filePath: "mail_footer.jpg",
      cid: cid_value2
    }];

    // var html_header = '<img src="cid:' + cid_value1 + '" />';
    // var html_header = '<img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/mail_header.jpg" />';
    var html_header = '<img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/rY66GsLbHnTm9Tvrx-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Header.png" />';
    // var html_content = '<h1>Embedded image testing</h1><p>123123<BR>gfgfg</p>';
    var html_content = text;
    // var html_footer = '<img src="cid:' + cid_value2 + '" />';;
    /*
    var html_footer = '<BR><BR>\
    <p class="MsoNormal"><span style="font-size: 10pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">\
    *** 此封電子郵件由系統自動發送，請勿直接回覆此封電子郵件。若您有任何問題請與本公司聯繫，謝謝。***<BR><BR></span></p>\
<table>\
  <tr>\
    <td style="width:150px; padding-right:40px;border-right: 1px solid #ccc;">\
        <img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/MFL94578AMBKprg9w-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Logo.png" />\
    </td>\
    <td style="width:620px; padding: 10px; padding-left:20px; vertical-align:text-top;">\
<strong><span style="color:#e94a48; font-size:14px">HANBURY</span> <span style="color:#f08000; font-size:14px">IFA </span> <span style="color:#4e4e4e; font-size:14px">Corporation Limited</span></strong><BR>\
<span style="color:#4e4e4e; font-size:11px">台灣台北市信義區基隆路一段400號5樓<BR>\
5F, No. 400, SEC.1, KEELONG RD., XINYI DIST., TAIPEI CITY 11051, Taiwan (R.O.C)<BR>\
Tel: +886 2 2758 2239 Email: <a href="mailto:info@hanburyifa.com">info@hanburyifa.com</a></span>\
    </td>\
  </tr>\
</table>\
<p class="" style="font-size:10px; color:#919191;">This message, along with any attachments, may be confidential or legally privileged.  It is intended only for the named person(s), who is/are the only authorized recipient(s). If this message has reached you in error, please delete it without review and notify the sender immediately.</p>';
*/
    var html_footer = '<BR><img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/yKNhmTQoRepiYtRSj-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Footer.png" />';


    // var mailcomposer = new MailComposer();
    /*    var mailObj = {
            from: from,
            to: "yinchen618@gmail.com",
            subject: "test",
            body: "Hello world!",
            envelope: {
                from: 'Daemon <notice@hanburyifa.com>',
                to: 'Michael <yinchen618@gmail.com>'
            },
            html: html_header+html_content+html_footer,
            attachments: attachments
        };*/

    // var mc = new EmailInternals.NpmModules.mailcomposer.module.MailComposer;
    // mc.setMessageOption(mailObj);
    // Email.send({mailComposer: mc});
    // var subject = formVar.subject;
    // var text = formVar.content;

    // console.log("bcc 4");
    // console.log(bcc);
    Email.send({
      // mailComposer: mailcomposer
      // mailComposer: stream//.pipe(process.stdout)
      // mailComposer: mailcomposer
      // to: "yinchen618@gmail.com",
      to: to,
      from: from,
      // subject: "test",
      subject: subject,
      cc: cc,
      bcc: bcc,
      html: html_header + html_content + html_footer,
      /*attachments: [{
        fileName: "image.png",
        // filePath: "/static/images/image.png", // "Volumes/Macintosh\ HD/Users/Opal/Desktop/<filename>"
        // filePath: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        contents: buffer,
        encoding: 'base64'
      }]*/
    });
  },
  // Meteor.call("sendEmail", "yinchen618@gmail.com", "aaa@han.com.tw", "test", "this is a testing mail");
  sendGroupEmail: function (formVar) {
    /* 其他可以用的mail的
    https://github.com/VeliovGroup/Meteor-Files
    講附件的
    https://github.com/nodemailer/mailcomposer/blob/7c0422b2de2dc61a60ba27cfa3353472f662aeb5/README.md#add-attachments
    */
    // take a cfs file and return a base64 string

    var email = [];

    function getEachMainInContact2(arr_id, arr) {
      if (typeof arr_id == "string") {
        var tmp = [];
        tmp.push(arr_id)
        arr_id = tmp;
      }

      for (var i = 0; i < arr_id.length; i++) {
        var arr_entry = Contact2.find({ contact1_id: arr_id[i] }).fetch();

        for (var j = 0; j < arr_entry.length; j++) {
          arr.push({
            name: arr_entry[j].name,
            email: arr_entry[j].email
          });
        }
      }
    }
    function getEachMailInCollection(arr_id, arr) {
      if (typeof arr_id == "string") {
        var tmp = [];
        tmp.push(arr_id)
        arr_id = tmp;
      }

      for (var i = 0; i < arr_id.length; i++) {
        var entry = Clients.findOne({ _id: arr_id[i] });
        arr.push({
          name: entry.name_cht,
          email: entry.email
        });
      }
    }
    function getEachMailInString(str, arr) {
      var newarr = str.split(",");
      for (var i = 0; i < newarr.length; i++) {
        arr.push({
          name: "",
          email: newarr[i]
        });
      }
    }

    if (!!formVar.email1)
      getEachMainInContact2(formVar.email1, email);
    if (!!formVar.email2)
      getEachMailInCollection(formVar.email2, email);
    if (!!formVar.email3)
      getEachMailInString(formVar.email3, email);

    // console.log(email);
    // return;

    var getBase64Data = function (file, callback) {
      // callback has the form function (err, res) {}
      var readStream = file.createReadStream();
      var buffer = [];
      readStream.on('data', function (chunk) {
        buffer.push(chunk);
      });
      readStream.on('error', function (err) {
        callback(err, null);
      });
      readStream.on('end', function () {
        callback(null, buffer.concat()[0].toString('base64'));
      });
    };

    // wrap it to make it sync
    var getBase64DataSync = Meteor.wrapAsync(getBase64Data);

    // get a cfs file
    // var file = Files.findOne();
    var file = Uploads.findOne({ _id: "ScFZcvfhehAYdiBM8" }).file.getFileRecord();

    // get the base64 string
    var base64str = getBase64DataSync(file);

    // get the buffer from the string
    var buffer = new Buffer(base64str, 'base64');

    // console.log(buffer);



    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    var from = "notice@hanburyifa.com";
    var subject = formVar.subject;
    var text = formVar.content;

    for (var i = 0; i < email.length; i++) {
      var to = email[i].email;
      check([to, from, subject, text], [String]);

      console.log("bcc 5");
      console.log(bcc);
      Email.send({
        to: to,
        from: from,
        subject: subject,
        text: text,
        /*attachments: [{
          fileName: "image.png",
          // filePath: "/static/images/image.png", // "Volumes/Macintosh\ HD/Users/Opal/Desktop/<filename>"
          // filePath: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
          // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
          // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
          contents: buffer,
          encoding: 'base64'
        }]*/
      });
    }

  },
  // sendEmail: function (to, from, subject, text) {
  sendEmail: function (to, subject, text) {
    /* 其他可以用的mail的
    https://github.com/VeliovGroup/Meteor-Files
    講附件的
    https://github.com/nodemailer/mailcomposer/blob/7c0422b2de2dc61a60ba27cfa3353472f662aeb5/README.md#add-attachments
    */

    // take a cfs file and return a base64 string
    /*  var getBase64Data = function(file, callback) {
        // callback has the form function (err, res) {}
        var readStream = file.createReadStream();
        var buffer = [];
        readStream.on('data', function(chunk) {
          buffer.push(chunk);
        });
        readStream.on('error', function(err) {
          callback(err, null);
        });
        readStream.on('end', function() {
          callback(null, buffer.concat()[0].toString('base64'));
        });
      };*/

    // wrap it to make it sync
    // var getBase64DataSync = Meteor.wrapAsync(getBase64Data);

    // get a cfs file
    // var file = Files.findOne();
    // var file = Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord();

    // get the base64 string
    // var base64str = getBase64DataSync(file);

    // get the buffer from the string
    // var buffer = new Buffer(base64str, 'base64');

    // console.log(buffer);


    // check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();


    var from = "notice@hanburyifa.com";
    // var subject = formVar.subject;
    // var text = formVar.content;
    Email.send({
      to: to,
      from: from,
      subject: subject,
      html: text,
      /*attachments: [{
        fileName: "image.png",
        // filePath: "/static/images/image.png", // "Volumes/Macintosh\ HD/Users/Opal/Desktop/<filename>"
        // filePath: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        // path: Uploads.findOne({_id: "ScFZcvfhehAYdiBM8"}).file.getFileRecord().url()
        contents: buffer,
        encoding: 'base64'
      }]*/
    });
  },
  sendEmails: function (formVar) { //to, from, subject, text, files) {
    function replaceDynamicText(mail, context) {
      var name_cht = "";

      if (context.indexOf("[name_cht]") != -1) {
        if (Clients.find({ email: mail }).count()) {
          name_cht = Clients.findOne({ email: mail }).name_cht;
          // console.log("replaceDynamicText: "+ mail + " name: "+name_cht);
        }
      }
      // return context;
      return context.replace(/\[name_cht]/g, name_cht);
    }
    function replaceDynamicText2(name, p3, p4, context) {
      if (context.indexOf("[name_cht]") != -1) {
        context = context.replace(/\[name_cht]/g, name);
      }
      if (context.indexOf("[product3]") != -1) {
        context = context.replace(/\[product3]/g, p3);
      }
      if (context.indexOf("[product4]") != -1) {
        context = context.replace(/\[product4]/g, p4);
      }
      return context;
    }

    // take a cfs file and return a base64 string
    var getBase64Data = function (file, callback) {
      // callback has the form function (err, res) {}
      var readStream = file.createReadStream();
      var buffer = [];
      readStream.on('data', function (chunk) {
        buffer.push(chunk);
      });
      readStream.on('error', function (err) {
        callback(err, null);
      });
      readStream.on('end', function () {
        callback(null, buffer.concat()[0].toString('base64'));
      });
    };

    // wrap it to make it sync
    var getBase64DataSync = Meteor.wrapAsync(getBase64Data);

    var from = "notice@hanburyifa.com";
    var subject = formVar.title;
    // var text = formVar.context;


    var html_header = '<img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/rY66GsLbHnTm9Tvrx-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Header.png" />';
    var html_content = '<div style="padding:10px;">' + formVar.context + '</div>';

    /*var html_footer = '<BR><p class="MsoNormal"><span lang="EN-US" style="font-size: 10pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">\
    *** 此封電子郵件由系統自動發送，請勿直接回覆此封電子郵件。若您有任何問題請與本公司聯繫，謝謝。***<BR><BR></span></p>\
<table>\
  <tr>\
    <td style="width:100px; padding-right:20px;">\
        <img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/MFL94578AMBKprg9w-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Logo.png" />\
    </td>\
    <td style="width:320px; padding: 10px;border-right: 1px solid #000;">\
<strong>Hanbury IFA Corporation Limited</strong><BR>\
Hong Kong: Room 1502, 15/F, Wing On House, 71 Des Voeux Road, Central<BR>\
<a href="http://www.hanburyifa.com" target="_blank">www.hanburyifa.com</a><BR>\
    </td>\
    <td style="width:330px; padding: 10px;border-left: 1px solid #000;">\
    <BR>\
Taipei: 5F, No. 400, Sec.1, Keelung Road, Xinyi District, Taiwan<BR>\
Tel: +886 2 2758 2239<BR>\
Email: <a href="mailto:info@hanburyifa.com">info@hanburyifa.com</a><BR>\
    </td>\
  </tr>\
</table>\
    <p class="MsoNormal"><span lang="EN-US" style="font-size: 10pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">\
    This message, along with any attachments, may be confidential or legally privileged.  It is intended only for the named person(s), who is/are the only authorized recipient(s). \
    If this message has reached you in error, please delete it without review and notify the sender immediately.</span></p>';
    */
    /*
    var html_footer = '<BR><BR>\
    <p class="MsoNormal"><span style="font-size: 10pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">\
    *** 此封電子郵件由系統自動發送，請勿直接回覆此封電子郵件。若您有任何問題請與本公司聯繫，謝謝。***<BR><BR></span></p>\
<table>\
  <tr>\
    <td style="width:150px; padding-right:40px;border-right: 1px solid #ccc;">\
        <img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/MFL94578AMBKprg9w-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Logo.png" />\
    </td>\
    <td style="width:620px; padding: 10px; padding-left:20px; vertical-align:text-top;">\
<strong><span style="color:#e94a48; font-size:14px">HANBURY</span> <span style="color:#f08000; font-size:14px">IFA </span> <span style="color:#4e4e4e; font-size:14px">Corporation Limited</span></strong><BR>\
<span style="color:#4e4e4e; font-size:11px">台灣台北市信義區基隆路一段400號5樓<BR>\
5F, No. 400, SEC.1, KEELONG RD., XINYI DIST., TAIPEI CITY 11051, Taiwan (R.O.C)<BR>\
Tel: +886 2 2758 2239 Email: <a href="mailto:info@hanburyifa.com">info@hanburyifa.com</a></span>\
    </td>\
  </tr>\
</table>\
<p class="" style="font-size:10px; color:#919191;">This message, along with any attachments, may be confidential or legally privileged.  It is intended only for the named person(s), who is/are the only authorized recipient(s). If this message has reached you in error, please delete it without review and notify the sender immediately.</p>';
*/
    /*
        var html_footer = '<BR><BR>\
        <p class="MsoNormal"><span style="font-size: 10pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">\
        *** 此封電子郵件由系統自動發送，請勿直接回覆此封電子郵件。若您有任何問題請與本公司聯繫，謝謝。***<BR><BR></span></p>\
    <table>\
      <tr>\
        <td style="width:150px; padding-right:40px;border-right: 1px solid #ccc;">\
            <img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/MFL94578AMBKprg9w-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Logo.png" />\
        </td>\
        <td style="width:620px; padding: 10px; padding-left:20px; vertical-align:text-top;">\
    <strong><span style="color:#e94a48; font-size:14px">HANBURY</span> <span style="color:#f08000; font-size:14px">IFA </span> <span style="color:#4e4e4e; font-size:14px">Corporation Limited</span></strong><BR>\
    <span style="color:#4e4e4e; font-size:11px">台灣台北市信義區基隆路一段400號5樓<BR>\
    5F, No. 400, SEC.1, KEELONG RD., XINYI DIST., TAIPEI CITY 11051, Taiwan (R.O.C)<BR>\
    Tel: +886 2 2758 2239 Email: <a href="mailto:info@hanburyifa.com">info@hanburyifa.com</a></span>\
        </td>\
      </tr>\
    </table>\
    <p class="" style="font-size:10px; color:#919191;">This message, along with any attachments, may be confidential or legally privileged.  It is intended only for the named person(s), who is/are the only authorized recipient(s). If this message has reached you in error, please delete it without review and notify the sender immediately.</p>';
    */
    var html_footer = '<BR><img style="width:100%" src="https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/yKNhmTQoRepiYtRSj-%E6%A0%B8%E5%BF%83%E7%B3%BB%E7%B5%B1%E7%B0%BD%E5%90%8D%E6%AA%94_Footer.png" />';


    // var html = html_header + html_content + html_footer;
    var html = "";//html_header + html_content + html_footer;


    var date_send = new Date();
    if (!!formVar.send_datetime_utc) {
      date_send = new Date(formVar.send_datetime_utc);
    }

    var date_now = new Date();
    var send_now = "2"; // 排程中

    if (date_now >= date_send) { // 要寄的時間，在之前
      send_now = "1";
    }

    var cc = formVar.cc_mail || "";
    if (cc.indexOf(",") != -1) {
      cc = cc.split(",");
    }
    var bcc = formVar.bcc_mail || "";
    if (bcc.indexOf(",") != -1) {
      bcc = bcc.split(",");
    }
    // console.log("formVar");
    // console.log(formVar);
    // console.log("bcc1");
    // console.log(bcc);
    // console.log("bcc2");


    // 先判斷附件的東西 是不是有自帶email，有的話就直接送了
    var currentUserId = Meteor.userId();

    // console.log("currentUserId", currentUserId);

    if (!!formVar.use_auto_mail && formVar.use_auto_mail == "1") { // 如果要用自動的mail時(上傳附件自動帶出的)

      if (Uploads.find({ email: "1", insertedById: currentUserId }).count() > 0) {
        var arrU = Uploads.find({ email: "1", insertedById: currentUserId, receiver_email: { $exists: true, $ne: '' } }).fetch();
        for (var i = 0; i < arrU.length; i++) {
          var mail = arrU[i];
          if (!!mail.receiver_name && !!mail.receiver_email) { // 先得到上傳檔案的mail

            var arrMail = mail.receiver_email.split(",");

            for (var j = 0; j < arrMail.length; j++) { // 對每一個mail的主人寄信 (有兩個信箱 就寄兩次)
              if (typeof arrMail[j] == "string" && arrMail[j].length > 3) {
                var to = arrMail[j];
                var sender_name = Meteor.users.findOne(Meteor.userId()).profile.engname || "";
                var uid = "M" + funcStrPad(getNextSequence('mailbox'), 7);

                // console.log("mail: " + j);
                // console.log(mail);
                // console.log("encodeURI mail.url");
                // console.log(encodeURI(mail.url));

                // var file = Uploads.findOne({_id: mail._id});
                // 這邊進來的 是一定有檔案的
                var attachments_send = [];
                attachments_send.push({
                  // fileName: mail.name,
                  // filePath: encodeURI(mail.url),
                  filename: mail.name,
                  path: encodeURI(mail.url),
                });
                Uploads.update({ _id: mail._id }, { $set: { email: "2" } });

                html = html_header + replaceDynamicText(to, html_content) + html_footer;

                if (1 && send_now == "1") {

                  console.log("bcc 2");
                  console.log(bcc);
                  this.unblock();
                  Email.send({
                    to: to,
                    // to: "yinchen618@gmail.com",
                    from: from,
                    cc: cc,
                    bcc: bcc,
                    subject: subject,
                    html: html,
                    attachments: attachments_send
                  });
                }

                var obj = {
                  uid: uid,
                  subject: subject,
                  receiver: to,
                  cc: cc,
                  bcc: bcc,
                  sent_status_id: send_now,
                  context: html,
                  attachments: attachments_send,
                  sent_time: date_send,
                  sender_id: Meteor.userId(),
                  sender_name: sender_name,
                };
                // console.log(obj);

                Emails.insert(obj);
              }
            }
          }
        }
      }
    }
    else if (!!formVar.email4) { // mail4 是投資單號的欄位
      //
      // 把上面整理的兩個mail連在一起，對每一個收件者寄信
      //
      var attachments_send = [];
      if (Uploads.find({ email: "1", insertedById: currentUserId }).count() > 0) {
        var files = Uploads.find({ email: "1", insertedById: currentUserId, receiver_email: { $exists: true, $ne: '' } }).fetch();
        // console.log(files);

        for (var j = 0; j < files.length; j++) { // 把所有有傳的檔案 一次放進來
          var nn = files[j].name;
          var uu = files[j].url;
          if (!!nn && !!uu && typeof nn == "string" && typeof uu == "string") {
            attachments_send.push({
              // fileName: nn,
              // filePath: encodeURI(uu),
              filename: nn,
              path: encodeURI(uu),
            });
            Uploads.update({ _id: files[j]._id }, { $set: { email: "2" } });
          }
        }
      }

      var arrMail4 = formVar.email4.split(",");

      if (typeof arrMail4 == "string") {
        arrMail4 = [arrMail4];
      }
      // console.log("arrMail4", arrMail4);
      // return;

      for (var i = 0; i < arrMail4.length; i++) {
        var uid = arrMail4[i];
        var arrMail2 = [];

        // 抓出所有要寄出的mail
        var p = Portfolios.findOne({ uid: uid });
        if (!p) continue;

        var mail = Portfolios.findOne({ uid: uid }).email;
        if (!mail) continue;

        // 整理mail
        if (mail.indexOf(",") != "-1") { // 有兩個mail以上的
          var arr = mail.split(",");
          for (var j = 0; j < arr.length; j++) {
            var str = arr[j].trim();
            arrMail2.push(str);
          }
        }
        else {
          arrMail2.push(mail);
        }

        var p3 = p.product3_text || "";
        var p4 = p.product4_text || "";
        var name = p.name_cht || "";

        for (var j = 0; j < arrMail2.length; j++) {
          var to = arrMail2[j].trim();

          html = html_header + replaceDynamicText2(name, p3, p4, html_content) + html_footer;

          // if(0)
          if (send_now == "1") {
            console.log("bcc 1");
            console.log(bcc);
            this.unblock();
            Email.send({
              to: to,
              from: from,
              cc: cc,
              bcc: bcc,
              subject: subject,
              html: html,
              attachments: attachments_send
            });
          }

          var sender_name = Meteor.users.findOne(Meteor.userId()).profile.engname || "";
          var m_uid = "M" + funcStrPad(getNextSequence('mailbox'), 7);

          var obj = {
            uid: m_uid,
            subject: subject,
            receiver: to,
            sent_status_id: send_now,
            cc: cc,
            bcc: bcc,
            context: html,
            sent_time: date_send,
            sender_id: Meteor.userId(),
            sender_name: sender_name,
            attachments: attachments_send
          };
          // console.log(obj);

          Emails.insert(obj);
        }
      }
    }

    if (!!formVar.email2 || !!formVar.email3) {
      // mail2 是上面一個一個用選的的欄位
      var arrMail2 = [];

      if (typeof formVar.email2 == "string") {
        formVar.email2 = [formVar.email2];
      }
      // console.log(formVar);

      if (!!formVar.email2 && formVar.email2.length > 0) {
        for (var i = 0; i < formVar.email2.length; i++) {
          var mail = "";
          var id = formVar.email2[i];
          if (Clients.findOne({ _id: id })) {
            mail = Clients.findOne({ _id: id }).email;

            if (mail.indexOf(",") != "-1") { // 有兩個mail以上的
              var arr = mail.split(",");
              for (var j = 0; j < arr.length; j++) {
                var str = arr[j];
                if (!!str && typeof str == "string") {
                  str = str.trim();
                  arrMail2.push(str);
                }
              }
            }
            else {
              arrMail2.push(mail);
            }
          }
        }
      }
      // console.log(arrMail2);

      // mail3 是使用者輸入的欄位
      var arrTo = formVar.email3.split(",");
      arrTo = arrTo.concat(arrMail2).unique();
      arrTo = arrTo.clean();

      if (!arrTo.length) {
        return 1;
      }

      // console.log('arrTo', arrTo)
      //
      // 把上面整理的兩個mail連在一起，對每一個收件者寄信
      //
      var attachments_send = [];
      if (Uploads.find({ email: "1", insertedById: currentUserId }).count() > 0) {
        // var files = Uploads.find({ email: "1", insertedById: currentUserId, receiver_email: { $exists: true, $ne: '' } }).fetch();
        var files = Uploads.find({ email: "1", insertedById: currentUserId }).fetch();
        console.log(files);

        for (var j = 0; j < files.length; j++) { // 把所有有傳的檔案 一次放進來
          var nn = files[j].name;
          var uu = files[j].url;
          if (!!nn && !!uu && typeof nn == "string" && typeof uu == "string") {
            attachments_send.push({
              // fileName: nn,
              // filePath: encodeURI(uu),
              filename: nn,
              path: encodeURI(uu),
            });
            Uploads.update({ _id: files[j]._id }, { $set: { email: "2" } });
          }
        }
      }
      console.log("attachments_send");
      console.log(attachments_send);

      for (var i = 0; i < arrTo.length; i++) {
        var to = arrTo[i];
        if (!to || typeof to != "string") {
          continue;
        }
        else {
          to = to.trim();
          html = html_header + replaceDynamicText(to, html_content) + html_footer;

          if (send_now == "1") {
            console.log("bcc 3");
            console.log(bcc);
            this.unblock();
            Email.send({
              to: to,
              from: from,
              cc: cc,
              bcc: bcc,
              subject: subject,
              html: html,
              attachments: attachments_send
            });
          }

          /*
          sent_status_id
              { id:"1", value:"已寄送"},
              { id:"2", value:"排程中"},
              { id:"3", value:"取消排程"},
           */
          var sender_name = Meteor.users.findOne(Meteor.userId()).profile.engname || "";
          var uid = "M" + funcStrPad(getNextSequence('mailbox'), 7);

          var obj = {
            uid: uid,
            subject: subject,
            receiver: to,
            sent_status_id: send_now,
            cc: cc,
            bcc: bcc,
            context: html,
            sent_time: date_send,
            sender_id: Meteor.userId(),
            sender_name: sender_name,
            attachments: attachments_send
          };
          // console.log(obj);

          var findObj = {
            subject: subject,
            receiver: to,
            cc: cc,
            bcc: bcc,
            sent_status_id: send_now,
            context: html,
            // attachments: attachments_send,
            sent_time: date_send,
            sender_id: Meteor.userId(),
          };

          var findEmail = Emails.find(findObj).fetch();
          // console.log(findEmail.length);

          if (!findEmail.length) {
            Emails.insert(obj);
          }
        }
      }
    }
    console.log(bcc);
  },
  getMailAddrs: function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      funcObjDelNullProp(formVar);
      // console.log(formVar);

      var arr = Portfolios.find(formVar, {
        // sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
      }).fetch();

      var arrEmails = "";
      arrEmails = arr.map(function (a) { return a.email; });

      // console.log(arrEmails);

      return arrEmails.clean();
    }
    return [];
  },
  getMailAddrsUid: function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      funcObjDelNullProp(formVar);
      // console.log(formVar);

      var arr = Portfolios.find(formVar, {
        // sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
      }).fetch();

      var arrEmails = "";
      arrEmails = arr.map(function (a) { return a.uid; });

      // console.log(arrEmails);

      return arrEmails.clean();
    }
    return [];
  },
  getMailAddrs2: function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      funcObjDelNullProp(formVar);
      // console.log(formVar);

      var arr = Portfolios.find(formVar, {
        // sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
      }).fetch();

      var arrEmails = "";
      // arrEmails = arr.map(function(a) {return a.email;});

      var str = "";
      for (var i = 0; i < arr.length; i++) {
        var entry = arr[i];
        if (!entry.email) {
          if (!str) {
            str = "無mail者：\n";
          }
          str = str + entry.uid + " " + entry.name_cht + "\n";
        }
      }

      // console.log(arrEmails);

      return str;
    }
    return "";
  },
  getArrClientPortfolios: function (client_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var arr = Portfolios.find({ client_id: client_id }, {
        fields: {
          product3_text: 1,
          product4_text: 1,
          account_num: 1,
          insurance_status: 1,
          nowphase: 1
        }
        // sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
      }).fetch();
      return arr.clean();
    }
    return [];
  },
  getArrPortfolios: function (p_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var arr = Portfolios.find({ _id: p_id }, {
        fields: {
          // product3_text: 1,
          // product4_text: 1,
          // account_num: 1,
        }
        // sort: sortVar,
        // skip: pageVar.skip,
        // limit: pageVar.limit
      }).fetch();
      return arr.clean();
    }
    return [];
  },
  insertContact: function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var id = Contact1.insert({ name: formVar.name });

      for (var i = 0; i < formVar.arrid.length; i++) {

        var client = Clients.findOne(formVar.arrid[i]);

        Contact2.insert({
          contact1_id: id,
          contact1_text: formVar.name,
          client_id: formVar.arrid[i],
          name: client.name_cht,
          email: client.email
        });
      }

      return id;
    }
    return [];
  },
  delContact1: function (id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      Contact2.remove({ contact1_id: id });
      Contact1.remove({ _id: id });

      return id;
    }
    return [];
  },
  'uploadFile': function (file) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      Images.insert(file, function (err, fileObj) {
        if (err) {
          console.log(err);
        } else {
          console.log(fileObj);
        }
      });
      return fileObj;
    }
    return [];
  },
  'getFileUrl': function (id) {
    // var currentUserId = Meteor.userId();
    // if(currentUserId){

    var url = Uploads.findOne({ _id: id }).file.getFileRecord().url();
    console.log("getFileUrl: " + url);

    return url;
    // }
    // return [];
  },
  'getAddr3a': function (addr1, addr2) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var arr = [];
      var arr1 = [];

      Addr2a.find({ addr1: addr1, addr2: addr2 }, { sort: { addr3: 1 } }).forEach(function (obj) {
        if (arr1.indexOf(obj.addr3) == -1) {
          arr1.push(obj.addr3);
          arr.push(obj);
        }
      });
      return arr;
    }
    return [];
  },
  'getAddr4a': function (addr1, addr2, addr3) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var arr = [];
      arr = Addr2a.find({ addr1: addr1, addr2: addr2, addr3: addr3 }, { sort: { addr4: 1 } }).fetch();
      return arr;
    }
    return [];
  },
  getAgents: function () {
    var results = [];

    var results = Roles.getUsersInRole(['agent']).map(function (user, index, originalCursor) {
      var result = {
        _id: user._id,
        emails: user.emails,
        name: user.profile.name,
        insertedAt: user.insertedAt,
        roles: user.roles
      };
      // console.log("result: ", result);
      return result;
    });

    // console.log("all results - this needs to get returned: ", results);

    return results;
  },
  'getUserRevStatus': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var id = Clients.update(id, { $set: formVar });
    }
    return 1;
  },
  'getProduct2': function (product1_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var f = Products.find({ product1_id: product1_id }, { sort: { code: 1 } }).fetch();
      return f;
    }
    return [];
  },
  'insertClient': function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var arr_start = ["", "", "", "", "", "", "", "", ""];
      var arr_end = ["", "", "", "", "", "", "", "", ""];
      var arr_by = ["", "", "", "", "", "", "", "", ""];
      var arr_name = ["", "", "", "", "", "", "", "", ""];

      var reviewid = Number(formVar.review_id);
      arr_start[reviewid] = new Date();
      arr_by[reviewid] = currentUserId;
      arr_name[reviewid] = Meteor.user().profile.name;

      var review_text = "";
      for (var i = 0; i < objReviewRes.length; i++) {  // 讀要儲存狀態的文字是什麼
        if (objReviewRes[i].id == reviewid) {
          review_text = objReviewRes[i].value;
          break;
        }
      }

      var obj = {
        uid: "H" + funcStrPad(getNextSequence('clients'), 7),
        review_id: formVar.review_id || "0",
        review_text: review_text || "草稿",
        name_cht: formVar.name_cht || "",
        name_eng: formVar.name_eng || "",
        sexual_id: formVar.sexual_id || "",
        sexual_text: formVar.sexual_text || "",
        title_id: formVar.title_id || "",
        title_text: formVar.title_text || "",
        birthday: formVar.birthday || "",
        identify: formVar.identify || "",
        passport: formVar.passport || "",
        marriage_id: formVar.marriage_id || "",
        marriage_text: formVar.marriage_text || "",
        country_id: formVar.country_id || "",
        country_text: formVar.country_text || "",
        cellnum: formVar.cellnum || "",
        faxnum: formVar.faxnum || "",
        addr1_phone: formVar.addr1_phone || "",
        addr1_post5: formVar.addr1_post5 || "",
        addr1_cht: formVar.addr1_cht || "",
        addr1_eng: formVar.addr1_eng || "",
        addr2_phone: formVar.addr2_phone || "",
        addr2_post5: formVar.addr2_post5 || "",
        addr2_cht: formVar.addr2_cht || "",
        addr2_eng: formVar.addr2_eng || "",
        email: formVar.email || "",
        email2: formVar.email2 || "",
        fin_type: formVar.fin_type || "",
        fin_company_cht: formVar.fin_company_cht || "",
        fin_company_eng: formVar.fin_company_eng || "",
        fin_company_tel: formVar.fin_company_tel || "",
        fin_addr_cht: formVar.fin_addr_cht || "",
        fin_addr_eng: formVar.fin_addr_eng || "",
        fin_yearsalary: formVar.fin_yearsalary || "",
        fin_invexp_id: formVar.fin_invexp_id || "",
        fin_invexp_text: formVar.fin_invexp_text || "",
        fin_purpose_id: formVar.fin_purpose_id || "",
        fin_purpose_text: formVar.fin_purpose_text || "",
        ps: formVar.ps || "",
        agent_id: formVar.agent_id || "",
        agent_text: formVar.agent_text || "",
        review0_total_sec: 0,
        review1_total_sec: 0,
        review2_total_sec: 0,
        review3_total_sec: 0,
        review4_total_sec: 0,
        review5_total_sec: 0,
        review6_total_sec: 0,
        review7_total_sec: 0,
        review8_total_sec: 0,
        review0_start_time: arr_start[0],
        review1_start_time: arr_start[1],
        review2_start_time: arr_start[2],
        review3_start_time: arr_start[3],
        review4_start_time: arr_start[4],
        review5_start_time: arr_start[5],
        review6_start_time: arr_start[6],
        review7_start_time: arr_start[7],
        review8_start_time: arr_start[8],
        review0_end_time: arr_end[0],
        review1_end_time: arr_end[1],
        review2_end_time: arr_end[2],
        review3_end_time: arr_end[3],
        review4_end_time: arr_end[4],
        review5_end_time: arr_end[5],
        review6_end_time: arr_end[6],
        review7_end_time: arr_end[7],
        review8_end_time: arr_end[8],
        review0_by: arr_by[0],
        review1_by: arr_by[1],
        review2_by: arr_by[2],
        review3_by: arr_by[3],
        review4_by: arr_by[4],
        review5_by: arr_by[5],
        review6_by: arr_by[6],
        review7_by: arr_by[7],
        review8_by: arr_by[8],
        review0_name: arr_name[0],
        review1_name: arr_name[1],
        review2_name: arr_name[2],
        review3_name: arr_name[3],
        review4_name: arr_name[4],
        review5_name: arr_name[5],
        review6_name: arr_name[6],
        review7_name: arr_name[7],
        review8_name: arr_name[8],
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        review_count: 0,
      }
      var id = Clients.insert(obj);

      var obj2 = {
        // uid: "h"+funcStrPad(getNextSequence("h"+obj.uid), 3),
        parent_id: id, //formVar._id,
        from_review_id: "0",
        from_review_text: "草稿",
        to_review_id: formVar.review_id,
        to_review_text: formVar.review_text,
        interval: 0,
        review_count: 0,
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        ps: formVar.ps,
        p_name_cht: formVar.name_cht,
        p_uid: obj.uid,
      }
      Review1.insert(obj2);

      // formVar._id = id;
      // obj._id = id;
      // console.log(formVar);
      // console.log(obj);
      return formVar;
    }
    return 1;
  },
  'updateClient': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      // console.log(formVar);
      var arr_start = ["", "", "", "", "", "", "", "", ""];
      var arr_end = ["", "", "", "", "", "", "", "", ""];
      var arr_by = ["", "", "", "", "", "", "", "", ""];
      var arr_name = ["", "", "", "", "", "", "", "", ""];
      var arr_sec = ["", "", "", "", "", "", "", "", ""];

      // 讀原來的
      var ori_data = Clients.findOne(id);

      arr_sec[0] = ori_data.review0_total_sec;
      arr_sec[1] = ori_data.review1_total_sec;
      arr_sec[2] = ori_data.review2_total_sec;
      arr_sec[3] = ori_data.review3_total_sec;
      arr_sec[4] = ori_data.review4_total_sec;
      arr_sec[5] = ori_data.review5_total_sec;
      arr_sec[6] = ori_data.review6_total_sec;
      arr_sec[7] = ori_data.review7_total_sec;
      arr_sec[8] = ori_data.review8_total_sec;
      arr_start[0] = ori_data.review0_start_time;
      arr_start[1] = ori_data.review1_start_time;
      arr_start[2] = ori_data.review2_start_time;
      arr_start[3] = ori_data.review3_start_time;
      arr_start[4] = ori_data.review4_start_time;
      arr_start[5] = ori_data.review5_start_time;
      arr_start[6] = ori_data.review6_start_time;
      arr_start[7] = ori_data.review7_start_time;
      arr_start[8] = ori_data.review8_start_time;
      arr_end[0] = ori_data.review0_end_time;
      arr_end[1] = ori_data.review1_end_time;
      arr_end[2] = ori_data.review2_end_time;
      arr_end[3] = ori_data.review3_end_time;
      arr_end[4] = ori_data.review4_end_time;
      arr_end[5] = ori_data.review5_end_time;
      arr_end[6] = ori_data.review6_end_time;
      arr_end[7] = ori_data.review7_end_time;
      arr_end[8] = ori_data.review8_end_time;
      arr_by[0] = ori_data.review0_by;
      arr_by[1] = ori_data.review1_by;
      arr_by[2] = ori_data.review2_by;
      arr_by[3] = ori_data.review3_by;
      arr_by[4] = ori_data.review4_by;
      arr_by[5] = ori_data.review5_by;
      arr_by[6] = ori_data.review6_by;
      arr_by[7] = ori_data.review7_by;
      arr_by[8] = ori_data.review8_by;
      arr_name[0] = ori_data.review0_name;
      arr_name[1] = ori_data.review1_name;
      arr_name[2] = ori_data.review2_name;
      arr_name[3] = ori_data.review3_name;
      arr_name[4] = ori_data.review4_name;
      arr_name[5] = ori_data.review5_name;
      arr_name[6] = ori_data.review6_name;
      arr_name[7] = ori_data.review7_name;
      arr_name[8] = ori_data.review8_name;


      // 算間隔了多久
      var starttime = "";
      var ori_review_id = Number(ori_data.review_id);
      if (ori_review_id == 0) starttime = ori_data.review0_start_time;
      else if (ori_review_id == 1) starttime = ori_data.review1_start_time;
      else if (ori_review_id == 2) starttime = ori_data.review2_start_time;
      else if (ori_review_id == 3) starttime = ori_data.review3_start_time;
      else if (ori_review_id == 4) starttime = ori_data.review4_start_time;
      else if (ori_review_id == 5) starttime = ori_data.review5_start_time;
      else if (ori_review_id == 6) starttime = ori_data.review6_start_time;
      else if (ori_review_id == 7) starttime = ori_data.review7_start_time;
      else if (ori_review_id == 8) starttime = ori_data.review8_start_time;

      var interval = 0;
      // console.log("1arr_sec[ori_review_id]: " + arr_sec[ori_review_id]);

      if (isNaN(arr_sec[ori_review_id])) { // if not-a-number
        arr_sec[ori_review_id] = 0;
      }

      interval = Math.round((new Date() - starttime) / 1000); //ori_data['review1_start_time']
      arr_sec[ori_review_id] = Number(arr_sec[ori_review_id]) + Number(interval);
      arr_end[ori_review_id] = new Date();

      // console.log("starttime: " + starttime);
      // console.log("interval: " + interval);
      // console.log("2arr_sec[ori_review_id]: " + arr_sec[ori_review_id]);
      // console.log("arr_end[ori_review_id]: " + arr_end[ori_review_id]);

      // 處理新的資料
      var review_count = Number(formVar.review_count) + 1;
      var reviewid = Number(formVar.review_id);
      var review_text = "";
      for (var i = 0; i < objReviewRes.length; i++) {  // 讀要儲存狀態的文字是什麼
        if (objReviewRes[i].id == reviewid) {
          review_text = objReviewRes[i].value;
          break;
        }
      }
      var obj = {
        parent_id: id,
        from_review_id: ori_data.review_id,
        from_review_text: ori_data.review_text,
        to_review_id: reviewid,
        to_review_text: review_text,
        interval: interval,
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        review_count: review_count,
        ps: formVar.ps,
        p_name_cht: formVar.name_cht,
        p_uid: formVar.uid,
      }
      Review1.insert(obj);

      // 新的狀態
      arr_start[reviewid] = new Date();
      arr_end[reviewid] = "";
      arr_by[reviewid] = currentUserId;
      arr_name[reviewid] = Meteor.user().profile.name;

      var obj = {
        review_id: formVar.review_id || "",
        review_text: review_text || "",
        name_cht: formVar.name_cht || "",
        name_eng: formVar.name_eng || "",
        sexual_id: formVar.sexual_id || "",
        sexual_text: formVar.sexual_text || "",
        title_id: formVar.title_id || "",
        title_text: formVar.title_text || "",
        birthday: formVar.birthday || "",
        identify: formVar.identify || "",
        passport: formVar.passport || "",
        marriage_id: formVar.marriage_id || "",
        marriage_text: formVar.marriage_text || "",
        country_id: formVar.country_id || "",
        country_text: formVar.country_text || "",
        cellnum: formVar.cellnum || "",
        faxnum: formVar.faxnum || "",
        addr1_phone: formVar.addr1_phone || "",
        addr1_post5: formVar.addr1_post5 || "",
        addr1_cht: formVar.addr1_cht || "",
        addr1_eng: formVar.addr1_eng || "",
        addr2_phone: formVar.addr2_phone || "",
        addr2_post5: formVar.addr2_post5 || "",
        addr2_cht: formVar.addr2_cht || "",
        addr2_eng: formVar.addr2_eng || "",
        email: formVar.email || "",
        email2: formVar.email2 || "",
        fin_type: formVar.fin_type || "",
        fin_company_cht: formVar.fin_company_cht || "",
        fin_company_eng: formVar.fin_company_eng || "",
        fin_company_tel: formVar.fin_company_tel || "",
        fin_addr_cht: formVar.fin_addr_cht || "",
        fin_addr_eng: formVar.fin_addr_eng || "",
        fin_yearsalary: formVar.fin_yearsalary || "",
        fin_invexp_id: formVar.fin_invexp_id || "",
        fin_invexp_text: formVar.fin_invexp_text || "",
        fin_purpose_id: formVar.fin_purpose_id || "",
        fin_purpose_text: formVar.fin_purpose_text || "",
        ps: formVar.ps || "",
        agent_id: formVar.agent_id || "",
        agent_text: formVar.agent_text || "",
        review0_total_sec: arr_sec[0],
        review1_total_sec: arr_sec[1],
        review2_total_sec: arr_sec[2],
        review3_total_sec: arr_sec[3],
        review4_total_sec: arr_sec[4],
        review5_total_sec: arr_sec[5],
        review6_total_sec: arr_sec[6],
        review7_total_sec: arr_sec[7],
        review8_total_sec: arr_sec[8],
        review0_start_time: arr_start[0],
        review1_start_time: arr_start[1],
        review2_start_time: arr_start[2],
        review3_start_time: arr_start[3],
        review4_start_time: arr_start[4],
        review5_start_time: arr_start[5],
        review6_start_time: arr_start[6],
        review7_start_time: arr_start[7],
        review8_start_time: arr_start[8],
        review0_end_time: arr_end[0],
        review1_end_time: arr_end[1],
        review2_end_time: arr_end[2],
        review3_end_time: arr_end[3],
        review4_end_time: arr_end[4],
        review5_end_time: arr_end[5],
        review6_end_time: arr_end[6],
        review7_end_time: arr_end[7],
        review8_end_time: arr_end[8],
        review0_by: arr_by[0],
        review1_by: arr_by[1],
        review2_by: arr_by[2],
        review3_by: arr_by[3],
        review4_by: arr_by[4],
        review5_by: arr_by[5],
        review6_by: arr_by[6],
        review7_by: arr_by[7],
        review8_by: arr_by[8],
        review0_name: arr_name[0],
        review1_name: arr_name[1],
        review2_name: arr_name[2],
        review3_name: arr_name[3],
        review4_name: arr_name[4],
        review5_name: arr_name[5],
        review6_name: arr_name[6],
        review7_name: arr_name[7],
        review8_name: arr_name[8],
        review_count: review_count,
        updatedBy: currentUserId,
        updatedName: Meteor.user().profile.name,
        updatedAt: new Date(),
      }
      Clients.update(id, { $set: obj });

      // console.log("in updateClient");
      // console.log(formVar);
      // console.log(obj);
      return { data: Clients.findOne(id) };
    }
    return 1;
  },
  'insertPortfolio': function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var arr_start = ["", "", "", "", "", "", "", "", ""];
      var arr_end = ["", "", "", "", "", "", "", "", ""];
      var arr_by = ["", "", "", "", "", "", "", "", ""];
      var arr_name = ["", "", "", "", "", "", "", "", ""];

      var reviewid = Number(formVar.review_id);
      arr_start[reviewid] = new Date();
      arr_by[reviewid] = currentUserId;
      arr_name[reviewid] = Meteor.user().profile.name;

      var review_text = "";
      for (var i = 0; i < objReviewRes.length; i++) {  // 讀要儲存狀態的文字是什麼
        if (objReviewRes[i].id == reviewid) {
          review_text = objReviewRes[i].value;
          break;
        }
      }

      var uid = "";
      formVar.product1_text = arrProductClass[formVar.product1_id];
      if (formVar.product1_id == "1") {
        uid = "R" + funcStrPad(getNextSequence('portfolio_r'), 7);
      }
      else if (formVar.product1_id == "2") {
        uid = "A" + funcStrPad(getNextSequence('portfolio_a'), 7);
      }
      else if (formVar.product1_id == "3") {
        uid = "I" + funcStrPad(getNextSequence('portfolio_i'), 7);
      }
      else if (formVar.product1_id == "4") {
        uid = "E" + funcStrPad(getNextSequence('portfolio_e'), 7);
      }
      else if (formVar.product1_id == "5") {
        uid = "V" + funcStrPad(getNextSequence('portfolio_v'), 7);
      }

      var obj = {
        uid: uid,
        review_id: formVar.review_id,
        review_text: review_text || "草稿",
        /////////
        product1_id: formVar.product1_id,
        product2: formVar.product2,
        remit_date: formVar.remit_date || "",
        item_start: formVar.item_start || "",
        submit_date: formVar.submit_date || "",
        invest_money: formVar.invest_money || "",
        first_rate: formVar.first_rate || "",
        first_prepay: formVar.first_prepay || "",

        fpi_num: formVar.fpi_num || "",
        ins_price: formVar.ins_price || "",
        ins_tp: formVar.ins_tp || "",
        ins_first_price: formVar.ins_first_price || "",
        ins_pay_years: formVar.ins_pay_years || "",
        ins_bodycheck_date: formVar.ins_bodycheck_date || "",
        ins_is_trust: formVar.ins_is_trust || "",
        ins_beneficiaries_cht: formVar.ins_beneficiaries_cht || "",
        ins_beneficiaries_eng: formVar.ins_beneficiaries_eng || "",
        ins_beneficiaries_rel: formVar.ins_beneficiaries_rel || "",
        ins_beneficiaries_birth: formVar.ins_beneficiaries_birth || "",
        ins_beneficiaries_id: formVar.ins_beneficiaries_id || "",
        ins_beneficiaries_passport: formVar.ins_beneficiaries_passport || "",
        ins_beneficiaries_addr: formVar.ins_beneficiaries_addr || "",
        ins_beneficiaries_email: formVar.ins_beneficiaries_email || "",
        //////////
        ps: formVar.ps,
        client_id: formVar.client_id,
        ps: formVar.ps || "",
        agent_id: formVar.agent_id || "",
        agent_text: formVar.agent_text || "",
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        review_count: 0,
        review0_total_sec: 0,
        review1_total_sec: 0,
        review2_total_sec: 0,
        review3_total_sec: 0,
        review4_total_sec: 0,
        review5_total_sec: 0,
        review6_total_sec: 0,
        review7_total_sec: 0,
        review8_total_sec: 0,
        review0_start_time: arr_start[0],
        review1_start_time: arr_start[1],
        review2_start_time: arr_start[2],
        review3_start_time: arr_start[3],
        review4_start_time: arr_start[4],
        review5_start_time: arr_start[5],
        review6_start_time: arr_start[6],
        review7_start_time: arr_start[7],
        review8_start_time: arr_start[8],
        review0_end_time: arr_end[0],
        review1_end_time: arr_end[1],
        review2_end_time: arr_end[2],
        review3_end_time: arr_end[3],
        review4_end_time: arr_end[4],
        review5_end_time: arr_end[5],
        review6_end_time: arr_end[6],
        review7_end_time: arr_end[7],
        review8_end_time: arr_end[8],
        review0_by: arr_by[0],
        review1_by: arr_by[1],
        review2_by: arr_by[2],
        review3_by: arr_by[3],
        review4_by: arr_by[4],
        review5_by: arr_by[5],
        review6_by: arr_by[6],
        review7_by: arr_by[7],
        review8_by: arr_by[8],
        review0_name: arr_name[0],
        review1_name: arr_name[1],
        review2_name: arr_name[2],
        review3_name: arr_name[3],
        review4_name: arr_name[4],
        review5_name: arr_name[5],
        review6_name: arr_name[6],
        review7_name: arr_name[7],
        review8_name: arr_name[8],
      }
      var id = Portfolios.insert(obj);

      var obj2 = {
        // uid: "h"+funcStrPad(getNextSequence("h"+obj.uid), 3),
        parent_id: id, //formVar._id,
        from_review_id: "0",
        from_review_text: "草稿",
        to_review_id: formVar.review_id,
        to_review_text: review_text,
        interval: 0,
        review_count: 0,
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        ps: formVar.ps,
        p_name_cht: formVar.name_cht,
        p_uid: obj.uid,
        p_product: arrProductClass[formVar.product1_id] + "-" + formVar.product2,
      }
      Review2.insert(obj2);

      // formVar._id = id;
      // obj._id = id;
      // console.log(formVar);
      // console.log(obj);
      return formVar;
    }
    return 1;
  },
  'updateP4Pic1': function (p4id, url, file_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var obj = {};
      obj.showpic1 = url;
      obj.filepic1 = file_id;
      console.log(obj);
      Oproduct4.update(p4id, { $set: obj });
    }
    return Oproduct4.findOne(p4id);
  },
  'updateP4Pic2': function (p4id, url, file_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var obj = {};
      obj.showpic2 = url;
      obj.filepic2 = file_id;
      Oproduct4.update(p4id, { $set: obj });
      // console.log(url);
    }
    return Oproduct4.findOne(p4id);
  },
  'getP4obj': function (p4id) {
    return Oproduct4.findOne({ _id: p4id });
  },
  'getP4CaroImg': function (p4id) {
    return Uploads.find({ oproduct4_id: p4id, carousel: "1" }).fetch();
  },
  'updateP4obj': function (p4id, obj) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      Oproduct4.update(p4id, { $set: obj });
    }
    return 1; // Oproduct4.findOne(p4id);
  },
  'updatePortfolio': function (id, formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      // console.log(formVar);
      var arr_start = ["", "", "", "", "", "", "", "", ""];
      var arr_end = ["", "", "", "", "", "", "", "", ""];
      var arr_by = ["", "", "", "", "", "", "", "", ""];
      var arr_name = ["", "", "", "", "", "", "", "", ""];
      var arr_sec = ["", "", "", "", "", "", "", "", ""];

      // 讀原來的
      var ori_data = Portfolios.findOne(id);

      arr_sec[0] = ori_data.review0_total_sec;
      arr_sec[1] = ori_data.review1_total_sec;
      arr_sec[2] = ori_data.review2_total_sec;
      arr_sec[3] = ori_data.review3_total_sec;
      arr_sec[4] = ori_data.review4_total_sec;
      arr_sec[5] = ori_data.review5_total_sec;
      arr_sec[6] = ori_data.review6_total_sec;
      arr_sec[7] = ori_data.review7_total_sec;
      arr_sec[8] = ori_data.review8_total_sec;
      arr_start[0] = ori_data.review0_start_time;
      arr_start[1] = ori_data.review1_start_time;
      arr_start[2] = ori_data.review2_start_time;
      arr_start[3] = ori_data.review3_start_time;
      arr_start[4] = ori_data.review4_start_time;
      arr_start[5] = ori_data.review5_start_time;
      arr_start[6] = ori_data.review6_start_time;
      arr_start[7] = ori_data.review7_start_time;
      arr_start[8] = ori_data.review8_start_time;
      arr_end[0] = ori_data.review0_end_time;
      arr_end[1] = ori_data.review1_end_time;
      arr_end[2] = ori_data.review2_end_time;
      arr_end[3] = ori_data.review3_end_time;
      arr_end[4] = ori_data.review4_end_time;
      arr_end[5] = ori_data.review5_end_time;
      arr_end[6] = ori_data.review6_end_time;
      arr_end[7] = ori_data.review7_end_time;
      arr_end[8] = ori_data.review8_end_time;
      arr_by[0] = ori_data.review0_by;
      arr_by[1] = ori_data.review1_by;
      arr_by[2] = ori_data.review2_by;
      arr_by[3] = ori_data.review3_by;
      arr_by[4] = ori_data.review4_by;
      arr_by[5] = ori_data.review5_by;
      arr_by[6] = ori_data.review6_by;
      arr_by[7] = ori_data.review7_by;
      arr_by[8] = ori_data.review8_by;
      arr_name[0] = ori_data.review0_name;
      arr_name[1] = ori_data.review1_name;
      arr_name[2] = ori_data.review2_name;
      arr_name[3] = ori_data.review3_name;
      arr_name[4] = ori_data.review4_name;
      arr_name[5] = ori_data.review5_name;
      arr_name[6] = ori_data.review6_name;
      arr_name[7] = ori_data.review7_name;
      arr_name[8] = ori_data.review8_name;


      // 算間隔了多久
      var starttime = "";
      var ori_review_id = Number(ori_data.review_id);
      if (ori_review_id == 0) starttime = ori_data.review0_start_time;
      else if (ori_review_id == 1) starttime = ori_data.review1_start_time;
      else if (ori_review_id == 2) starttime = ori_data.review2_start_time;
      else if (ori_review_id == 3) starttime = ori_data.review3_start_time;
      else if (ori_review_id == 4) starttime = ori_data.review4_start_time;
      else if (ori_review_id == 5) starttime = ori_data.review5_start_time;
      else if (ori_review_id == 6) starttime = ori_data.review6_start_time;
      else if (ori_review_id == 7) starttime = ori_data.review7_start_time;
      else if (ori_review_id == 8) starttime = ori_data.review8_start_time;

      var interval = 0;
      // console.log("1arr_sec[ori_review_id]: " + arr_sec[ori_review_id]);

      if (isNaN(arr_sec[ori_review_id])) { // if not-a-number
        arr_sec[ori_review_id] = 0;
      }

      interval = Math.round((new Date() - starttime) / 1000); //ori_data['review1_start_time']
      arr_sec[ori_review_id] = Number(arr_sec[ori_review_id]) + Number(interval);
      arr_end[ori_review_id] = new Date();

      // console.log("starttime: " + starttime);
      // console.log("interval: " + interval);
      // console.log("2arr_sec[ori_review_id]: " + arr_sec[ori_review_id]);
      // console.log("arr_end[ori_review_id]: " + arr_end[ori_review_id]);

      // 處理新的資料
      var review_count = Number(formVar.review_count) + 1;
      var reviewid = Number(formVar.review_id);
      var review_text = "";
      for (var i = 0; i < objReviewRes.length; i++) { // 讀要儲存狀態的文字是什麼
        if (objReviewRes[i].id == reviewid) {
          review_text = objReviewRes[i].value;
          break;
        }
      }
      var obj = {
        parent_id: id,
        from_review_id: ori_data.review_id,
        from_review_text: ori_data.review_text,
        to_review_id: reviewid,
        to_review_text: review_text,
        interval: interval,
        insertedBy: currentUserId,
        insertedName: Meteor.user().profile.name,
        insertedAt: new Date(),
        review_count: review_count,
        ps: formVar.ps,
        p_name_cht: formVar.name_cht,
        p_uid: formVar.uid,
        // p_product: formVar.product1+"-"+formVar.product2,
        p_product: arrProductClass[formVar.product1_id] + "-" + formVar.product2,
      }
      Review2.insert(obj);

      // 新的狀態
      arr_start[reviewid] = new Date();
      arr_end[reviewid] = "";
      arr_by[reviewid] = currentUserId;
      arr_name[reviewid] = Meteor.user().profile.name;

      var obj = {
        review_id: formVar.review_id || "",
        review_text: review_text || "",
        ps: formVar.ps || "",
        agent_id: formVar.agent_id || "",
        agent_text: formVar.agent_text || "",
        review0_total_sec: arr_sec[0],
        review1_total_sec: arr_sec[1],
        review2_total_sec: arr_sec[2],
        review3_total_sec: arr_sec[3],
        review4_total_sec: arr_sec[4],
        review5_total_sec: arr_sec[5],
        review6_total_sec: arr_sec[6],
        review7_total_sec: arr_sec[7],
        review8_total_sec: arr_sec[8],
        review0_start_time: arr_start[0],
        review1_start_time: arr_start[1],
        review2_start_time: arr_start[2],
        review3_start_time: arr_start[3],
        review4_start_time: arr_start[4],
        review5_start_time: arr_start[5],
        review6_start_time: arr_start[6],
        review7_start_time: arr_start[7],
        review8_start_time: arr_start[8],
        review0_end_time: arr_end[0],
        review1_end_time: arr_end[1],
        review2_end_time: arr_end[2],
        review3_end_time: arr_end[3],
        review4_end_time: arr_end[4],
        review5_end_time: arr_end[5],
        review6_end_time: arr_end[6],
        review7_end_time: arr_end[7],
        review8_end_time: arr_end[8],
        review0_by: arr_by[0],
        review1_by: arr_by[1],
        review2_by: arr_by[2],
        review3_by: arr_by[3],
        review4_by: arr_by[4],
        review5_by: arr_by[5],
        review6_by: arr_by[6],
        review7_by: arr_by[7],
        review8_by: arr_by[8],
        review0_name: arr_name[0],
        review1_name: arr_name[1],
        review2_name: arr_name[2],
        review3_name: arr_name[3],
        review4_name: arr_name[4],
        review5_name: arr_name[5],
        review6_name: arr_name[6],
        review7_name: arr_name[7],
        review8_name: arr_name[8],
        review_count: review_count,
        updatedBy: currentUserId,
        updatedName: Meteor.user().profile.name,
        updatedAt: new Date(),
      }
      Portfolios.update(id, { $set: obj });

      // console.log("in updateClient");
      // console.log(formVar);
      // console.log(obj);
      return { data: Portfolios.findOne(id) };
    }
    return 1;
  },
  'insertProductData': function (productVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var id = Products.insert({
        uid: getNextSequence('products'),
        product1_id: productVar.product1_id,
        product1_text: productVar.product1_text,
        providerid: productVar.providerid,
        providername: productVar.providername,
        code: productVar.code,
        name: productVar.name,
        ps: productVar.ps,
        insertedBy: currentUserId
      });
      // console.log("insert ok " +id);
      // console.log(productVar);

    }
    return 1;
  },
  'insertAfterservice': function (productVar) {
    var currentUserId = Meteor.userId();
    var getClients = Clients.findOne({ _id: productVar.client_id });
    // var getPortfolio = Portfolios.findOne({ _id: productVar._id });
    var getPortfolio = productVar;
    var t_obj = {};
    if (currentUserId) {
      if (!!getPortfolio.arrPaymentRecord && getPortfolio.arrPaymentRecord.length) {
        t_obj.pay_rec = getPortfolio.arrPaymentRecord.find(function (item) {
          var monShoudPay = moment(item.pmr_payment1date).format('YYYY-MM');
          var thisMonth = moment().format('YYYY-MM');
          return monShoudPay == thisMonth;
        });
        //   t_obj.contactnum = '+886-' + ((getPortfolio.contactnum).slice(1));
      }
      // console.log('t_obj', t_obj);
      var id = Afterservice.insert(Object.assign({}, {
        uid: "S" + funcStrPad(getNextSequence('afterservice'), 6),
        status_text: "申請中",
        status: "1",
        // url : $(".form-modal2").serialize(),
        as_template_id: 'Nhx2ZDsK7D2dAnmpe',
        as_template_text: 'Voya保費繳費提醒',

        portfolio: getPortfolio,
        portfolio_id: productVar._id,
        as_owner_id: Meteor.userId(),
        as_owner_text: Meteor.user().profile.engname,
        as_action: "2",
        as_action_owner: "4",
        updatedAt1: moment(getPortfolio.effective_date).set('year', moment().get('year')).subtract(7, 'days').toISOString(),
        // Client
        addr1_eng: getClients.addr1_eng,
        identify: getClients.identify,
        policy_currency: getClients.policy_currency,
        policyxx_num: getClients.policyxx_num,
        birthday: getClients.birthday,
        // Portfolio
        contactnum: getPortfolio.contactnum || '',
        account_num: getPortfolio.account_num,
        account_num2: getPortfolio.account_num2,
        hsbc_account_name: getPortfolio.hsbc_account_name,
        hsbc_account_name: getPortfolio.hsbc_account_name,
        email: getPortfolio.email,
        name_cht: getPortfolio.name_cht,
        name_eng: getPortfolio.name_eng,
        agent_text: getPortfolio.agent_text,
        policy_num: getPortfolio.policy_num,
        insertedBy_CE: getPortfolio.product4_text + '（' + getPortfolio.product4_engtext + "）",
        insertedBy: getPortfolio.product4_engtext,
        trust_name: getPortfolio.trust_name,
        trust_trustee: getPortfolio.trust_trustee,
        trust_ssnitin: getPortfolio.trust_ssnitin,
        trust_lawyer: getPortfolio.trust_lawyer,
        trust_grantor: getPortfolio.trust_grantor,
        beneficial1_name: getPortfolio.beneficial1_name,
        beneficial1_name2: getPortfolio.beneficial1_name2,
        beneficial1_relationship: getPortfolio.beneficial1_relationship,
        beneficial2_name: getPortfolio.beneficial2_name,
        beneficial3_name: getPortfolio.beneficial3_name,
        beneficial4_name: getPortfolio.beneficial4_name,
        beneficial1_percent: getPortfolio.beneficial1_percent,
        beneficial2_percent: getPortfolio.beneficial2_percent,
        beneficial3_percent: getPortfolio.beneficial3_percent,
        beneficial4_percent: getPortfolio.beneficial4_percent,
        last_name: getPortfolio.name_eng,
        first_name: getPortfolio.name_eng,
        apply_passport: getPortfolio.apply_passport,
        apply_telephone: getPortfolio.apply_telephone,
        apply_cellphone: getPortfolio.apply_cellphone,
        hsbc_account_name: getPortfolio.hsbc_account_name,
        hsbc_account_num: getPortfolio.hsbc_account_num,
        creditcard_num: getPortfolio.creditcard_num,
        creditcard_bank: getPortfolio.creditcard_bank,
        creditcard_bankname: getPortfolio.creditcard_bankname,
        fpi_now_same_payment: getPortfolio.fpi_now_same_payment,
        policy_currency_type: funcObjFind2(objSalaryCash_eng, getPortfolio.policy_currency_type),
        paymethod_fpi: funcObjFind2(objSalaryCash_eng, getPortfolio.paymethod_fpi),
        payperiod_fpi: funcObjFind2(objSalaryCash_eng, getPortfolio.payperiod_fpi),
        beneficiary_bank_location: getPortfolio.beneficiary_bank_location,
        beneficiary_bank_name: getPortfolio.beneficiary_bank_name,
        beneficiary_bank_address: getPortfolio.beneficiary_bank_address,
        swift_code: getPortfolio.swift_code,
        beneficiary_account_number: getPortfolio.beneficiary_account_number,
        beneficiary_name: getPortfolio.beneficiary_name,
        policy_firstperson: getPortfolio.policy_firstperson,
        policy_secondperson: getPortfolio.policy_secondperson,
        agent_id: getPortfolio.agent_id,
        creditcard_name: getPortfolio.creditcard_name,
        creditcard_addr: getPortfolio.creditcard_addr,
        ///
        wdba_bankname: "",
        wdba_bankaddr: "",
        // wda_holdernum : "",
        // wda_swift_bic : "",
        creditcard_term_month: (getPortfolio.creditcard_date || '').slice(0, 2),
        creditcard_term_year: (getPortfolio.creditcard_date || '').slice(3),
        fpi_now_same_payment_words: numToWords(getPortfolio.fpi_now_same_payment),
        "custom_1": "custom_1",
        "custom_2": "custom_2",
        "custom_3": "custom_3",
        "custom_4": "custom_4",
        "custom_5": "custom_5",
        "custom_6": "custom_6",
        "custom_7": "custom_7",
        "custom_8": "custom_8",
        "custom_9": "custom_9",
        "custom_10": "custom_10",
        "custom_11": "custom_11",
        "custom_12": "custom_12",
        "custom_13": "custom_13",
        "custom_14": "custom_14",
        "custom_15": "custom_15",
      }));

      // console.log(productVar);
      // console.log("insert ok " + id);

    }
    return 1;
  },
  'insertSalary': function (objVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      objVar.uid = "AC" + funcStrPad(getNextSequence('salary'), 7);
      objVar.insertedBy = currentUserId;
      objVar.insertedName = Meteor.user().profile.name;
      objVar.insertedAt = new Date();
      objVar.dateObj = new Date(objVar.date);
      objVar.money_predict = Number(objVar.money_predict);
      objVar.money_real = Number(objVar.money_real);

      var id = Salary.insert(objVar);
      console.log("insert ok " + id);
      console.log(objVar);
    }
    return 1;
  },
  'insertCommission': function (objVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      objVar.uid = "AC" + funcStrPad(getNextSequence('commission'), 7);
      objVar.insertedBy = currentUserId;
      objVar.insertedName = Meteor.user().profile.name;
      objVar.insertedAt = new Date();
      objVar.dateObj = new Date(objVar.date);
      objVar.money_predict = Number(objVar.money_predict);
      objVar.money_real = Number(objVar.money_real);

      var id = Commission.insert(objVar);
      console.log("insert ok " + id);
      console.log(objVar);
    }
    return 1;
  },
  'insertProvider': function (productVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      var id = Provider.insert({
        uid: getNextSequence('provider'),
        country: productVar.country,
        countryname: productVar.countryname,
        fullname: productVar.countryname + " - " + productVar.value,
        value: productVar.value,
        ps: productVar.ps,
        insertedBy: currentUserId
      });

      console.log("insert ok " + id);
      console.log(productVar);

    }
    return 1;
  },
  'insertBooking': function (formVar) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var dd = new Date(formVar.date);
      // month = dd.getFullYear()+ '-' + (dd.getMonth()+1);

      var yyyy = dd.getFullYear().toString();
      var mm = (dd.getMonth() + 1).toString();
      month = yyyy + "-" + (mm[1] ? mm : "0" + mm[0]);

      formVar.uid = "B" + funcStrPad(getNextSequence('booking'), 7);
      formVar.month_id = month;

      if (Bookingmonth.find({ value: month }).count() == 0) {
        Bookingmonth.insert({ value: month });
      }
      var id = Booking.insert(formVar);

      console.log("insert ok " + id);
      console.log(formVar);
      return month;
    }
    return 1;
  },
  'transAddrChtEng': function (addr) {
    // console.log("////// 第 "+ i + " 筆 ////////");
    // console.log(addr);
    var addr_eng = "";
    var arrAddr1 = Addr1.find().fetch();

    for (var j = 0; j < arrAddr1.length; j++) {
      if (arrAddr1[j].cht == addr.substr(0, arrAddr1[j].cht.length)) { // ○○市○○區
        addr = addr.substr(arrAddr1[j].cht.length);
        addr_eng = arrAddr1[j].eng;

        // console.log(addr);
        // console.log(addr_eng);

        var arrAddr2 = Addr2.find().fetch();
        for (var k = 0; k < arrAddr2.length; k++) {
          if (arrAddr2[k].cht == addr.substr(0, arrAddr2[k].cht.length)) { // ○○里 或 ○○村
            addr = addr.substr(arrAddr2[k].cht.length);
            addr_eng = arrAddr2[k].eng + ", " + addr_eng;

            // console.log(addr);
            // console.log(addr_eng);

            var neb = addr.indexOf("鄰");
            if (neb == 1 || neb == 2) {
              neb_num = addr.substring(0, neb);

              addr = addr.substr(neb + 1);
              addr_eng = "Neighborhood " + neb_num + ", " + addr_eng;

              // console.log(addr);
              // console.log(addr_eng);
            }
            break;
          }
        }
        var arrAddr3 = Addr3.find().fetch();
        for (var l = 0; l < arrAddr3.length; l++) {
          if (arrAddr3[l].cht == addr.substr(0, arrAddr3[l].cht.length)) { // 路
            addr = addr.substr(arrAddr3[l].cht.length);
            // addr_eng = arrAddr3[l].eng + ", " + addr_eng;
            var tmp_eng = arrAddr3[l].eng;

            if (addr.indexOf("東") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " E.";
            }
            else if (addr.indexOf("西") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " W.";
            }
            else if (addr.indexOf("南") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " S.";
            }
            else if (addr.indexOf("北") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " N.";
            }

            if (addr.indexOf("路") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " Rd.";
            }
            else if (addr.indexOf("街") == 0) {
              addr = addr.substr(1);
              tmp_eng = tmp_eng + " St.";
            }
            addr_eng = tmp_eng + ", " + addr_eng;

            // console.log(addr);
            // console.log(addr_eng);
            break;
          }
        }

        var tmp_eng = "";
        if (addr.indexOf("一") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 1st";
        }
        else if (addr.indexOf("二") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 2nd";
        }
        else if (addr.indexOf("三") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 3rd";
        }
        else if (addr.indexOf("四") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 4th";
        }
        else if (addr.indexOf("五") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 5th";
        }
        else if (addr.indexOf("六") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 6th";
        }
        else if (addr.indexOf("七") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 7th";
        }
        else if (addr.indexOf("八") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 8th";
        }
        else if (addr.indexOf("九") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 9th";
        }
        else if (addr.indexOf("十") == 0) {
          addr = addr.substr(1);
          tmp_eng = " 10th";
        }

        if (addr.indexOf("路") == 0) {
          addr = addr.substr(1);
          tmp_eng = "Rd." + tmp_eng;
        }
        else if (addr.indexOf("街") == 0) {
          addr = addr.substr(1);
          tmp_eng = "St." + tmp_eng;
        }
        else if (addr.indexOf("段") == 0) {
          addr = addr.substr(1);
          tmp_eng = "Sec." + tmp_eng;
        }
        if (tmp_eng)
          addr_eng = tmp_eng + ", " + addr_eng;

        // 這邊後，就剩下巷/弄/號/樓/之/室了

        var tmp_eng = "";
        if (addr.indexOf("段") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("段")); // 數字
          addr = addr.substr(addr.indexOf("段") + 1);
          tmp_eng = "Sec. " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }
        if (addr.indexOf("巷") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("巷")); // 數字
          addr = addr.substr(addr.indexOf("巷") + 1);
          tmp_eng = "Ln. " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }
        if (addr.indexOf("弄") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("弄")); // 數字
          addr = addr.substr(addr.indexOf("弄") + 1);
          tmp_eng = "Aly. " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }
        if (addr.indexOf("衖") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("衖")); // 數字
          addr = addr.substr(addr.indexOf("衖") + 1);
          tmp_eng = "Sub-Alley " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }


        var tmp_eng = "";
        if (addr.indexOf("巷") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("巷")); // 數字
          addr = addr.substr(addr.indexOf("巷") + 1);
          tmp_eng = "Ln. " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }

        var isDash = 0;
        if (addr.indexOf("之") != -1) {
          addr = addr.replace("之", "-");
          isDash = 1;
        }
        if (addr.indexOf("號") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("號")); // 數字
          addr = addr.substr(addr.indexOf("號") + 1);
          tmp_eng = "No." + tmp_eng;

          if (isDash && addr.indexOf("樓") == -1) {
            tmp_eng = tmp_eng + addr;
          }

          addr_eng = tmp_eng + ", " + addr_eng;
        }
        if (addr.indexOf("樓") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("樓")); // 數字
          addr = addr.substr(addr.indexOf("樓") + 1);
          tmp_eng = tmp_eng + "F.";


          if (isDash && addr.indexOf("室") == -1) {
            tmp_eng = tmp_eng + addr;
          }

          addr_eng = tmp_eng + ", " + addr_eng;
        }
        if (addr.indexOf("室") != -1) {
          tmp_eng = addr.substr(0, addr.indexOf("室")); // 數字
          addr = addr.substr(addr.indexOf("室") + 1);
          tmp_eng = "Rm. " + tmp_eng;

          addr_eng = tmp_eng + ", " + addr_eng;
        }

        break;
      }
    }// 這邊出來

    // console.log(addr);
    // console.log(addr_eng);
    return addr_eng;
  },
  'updateSortHrform': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Hrform_management.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortEmployee': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Meteor.users.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortReportcenter': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Reportcenter1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortBlankform': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Blank_form.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortAccountyear': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Accountyear.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortBusinessgroup': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Businessgroup.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortReportcenter2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Reportcenter2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortdFormcenter1': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Formcenter1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSort_sales_year': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Fin_sales_year.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortNopdfform': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Nopdfform.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortdFormcenter2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Formcenter2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortUploads': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Uploads.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortCounrty': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Country.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortBankacc': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Bankacc.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortProducts': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Products.update(arr[i]._id, { $set: { order: ord } });
      }
    }
    return "1";
  },

  'updateSortAccount1': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Account1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },

  'updateSortAccount2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Account2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },

  'updateSortAccount3': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Account3.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },

  'updateSortProduct1': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Product1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortProduct2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Product2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortProduct3': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Product3.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortProduct4': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Product4.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateProduct4': function (obj) {
    var currentUserId = Meteor.userId();
    // console.log(obj.provider_id);
    if (currentUserId) {
      obj.provider_chttext = Provider.findOne({ _id: obj.provider_id }).name_cht;
      obj.provider_engtext = Provider.findOne({ _id: obj.provider_id }).name_eng;
      // console.log(obj);
      Product4.update(obj._id, { $set: obj });
    }
    return "1";
  },
  'updateSortOproduct1': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct3': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct3.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct4': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct4.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct1': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct1.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct2': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct2.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct3': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct3.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortOproduct4': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Oproduct4.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'updateSortInterestcondition': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {
      for (var i = 0; i < arr.length; i++) {
        var ord = Number(i) + 1;
        Interestcondition.update(arr[i]._id, { $set: { order_id: ord } });
      }
    }
    return "1";
  },
  'getAllAgents': function (arr) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      var queryVar = {};

      var object = Meteor.users.find(queryVar, {
      }).fetch();

      var arrObj = [];
      for (var i = 0; i < object.length; i++) {
        var obj = {
          value: object[i]._id,
          text: object[i].profile.engname || "",
        }
        arrObj.push(obj);
      }

      return arrObj;
    }
    return "1";
  },
  'getOProductPics': function (p4_id) {
    var currentUserId = Meteor.userId();
    if (currentUserId) {

      return Uploads.find({
        carousel: "1",
        oproduct4_id: p4_id
      }).fetch();

      // return arrObj;
    }
    return "1";
  },
});
