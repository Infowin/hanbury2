Meteor.publish("uploads", function () { return Uploads.find(); });
Meteor.publish("Homepics", function () {
  return Uploads.find({ homepic: "1" });
});

Meteor.publish("images", function () { return Images.find(); });

Meteor.publish('Emailauto', function () {
  return Emailauto.find();
});
Meteor.publish('Addr1a', function () {
  return Addr1a.find();
});

Meteor.publish("currentAccessToken", function () {
  return Meteor.users.find(this.userId, { fields: { 'services.resume.loginTokens': 1 } });
});

Meteor.publish("userList", function () {
  // return Meteor.users.find({}, {fields: {emails: 1, profile: 1}});
  return Meteor.users.find();
});
Meteor.publish('Country', function () {
  return Country.find({});
});
Meteor.publish('Accountyear', function (id) {
  if (!id) return Accountyear.find();
  return Accountyear.find({ bg_id: id });
});
Meteor.publish('Interestcondition', function (id) {
  return Interestcondition.find();
});
Meteor.publish('BusinessgroupOne', function (id) {
  return Businessgroup.find({ _id: id });
});
Meteor.publish('Businessgroup', function () {
  return Businessgroup.find();
});
Meteor.publish('FundConfigure', function () {
  return FundConfigure.find();
});
Meteor.publish('Formcenter1', function () {
  return Formcenter1.find();
});
Meteor.publish('Formcenter2', function () {
  return Formcenter2.find();
});
Meteor.publish('Reportcenter1', function () {
  return Reportcenter1.find();
});
Meteor.publish('Reportcenter2', function () {
  return Reportcenter2.find();
});
Meteor.publish('Workdays', function () {
  return Workdays.find();
});
Meteor.publish('MailTitle', function () {
  return MailTitle.find();
});
Meteor.publish('MailContext', function () {
  return MailContext.find();
});
Meteor.publish('Calendar', function () {
  return Calendar.find();
});
Meteor.publish('Product1', function () {
  return Product1.find();
});
Meteor.publish('Product2', function () {
  return Product2.find();
});
Meteor.publish('Product3', function () {
  return Product3.find();
});
Meteor.publish('Product4', function () {
  return Product4.find();
});

Meteor.publish('Oproduct1', function () {
  return Oproduct1.find();
});
Meteor.publish('Oproduct2', function () {
  return Oproduct2.find();
});
Meteor.publish('Oproduct3', function () {
  return Oproduct3.find();
});
Meteor.publish('Oproduct4', function () {
  return Oproduct4.find();
});
Meteor.publish('Provider', function () {
  return Provider.find();
});
Meteor.publish('Agents', function () {
  return Agents.find();
});
Meteor.publish('Account', function (id) {
  if (!id) return Account.find();
  return Account.find({ bg_id: id });
});
Meteor.publish('Account1', function (id) {
  return Account1.find({ bg_id: id });
});
Meteor.publish('Account2', function (id) {
  return Account2.find({ bg_id: id });
});

Meteor.publish('Account3', function (id) {
  return Account3.find({ bg_id: id });
});
Meteor.publish('Nopdfform', function () {
  return Nopdfform.find();
});
Meteor.publish('Bankacc', function (id) {
  if (!id) return Bankacc.find();
  return Bankacc.find({ bg_id: id });
});
Meteor.publish('Salary', function (id) {
  return Salary.find({ bg_id: id });
});
Meteor.publish('Fin_sales_year', function () {
  return Fin_sales_year.find();
});

Meteor.publish('Commission', function (id) {
  if (!id) return Commission.find();
  return Commission.find({ bg_id: id });
});

Meteor.publish('Dayoff', function () {
  return Dayoff.find();
});

Meteor.publish('Dayoff_set', function () {
  return Dayoff_set.find();
});

// Meteor.publish('Dayoff_management', function() {
//   return Dayoff_management.find();
// });

Meteor.publish('Bookingmonth', function () {
  return Bookingmonth.find();
});

Meteor.publish('Booking', function () {
  return Booking.find();
});

Meteor.publish('Contact1', function () {
  return Contact1.find();
});

Meteor.publish('Cmsget', function () {
  return Cmsget.find();
});

Meteor.publish('Cmslevel', function () {
  return Cmslevel.find();
});

Meteor.publish('Teams', function () {
  return Teams.find();
});

Meteor.publish('Clients', function () {
  return Clients.find();
});

Meteor.publish('Client', function (client_id) {
  // console.log("client_id: "+client_id);
  return Clients.find({ _id: client_id });
}); 
Meteor.publish('ClientPortfolio', function (client_id) {
  // console.log("client_id: "+client_id);
  return Portfolios.find({ client_id: client_id });
});

Meteor.publish('Interviews', function () {
  return Interviews.find();
});

Meteor.publish('Portfolio', function (portfolio_id) {
  return Portfolios.find({ _id: portfolio_id });
});
Meteor.publish('Oproduct4One', function (p4_id) {
  return Oproduct4.find({ _id: p4_id });
});

Meteor.publish('Oproduct4Photos', function (p4_id) {
  return Uploads.find({
    carousel: "1",
    oproduct4_id: p4_id
  });
});
Meteor.publish('Oproduct4Menu', function () {
  return Oproduct4.find({}, {
    fields: {
      value_cht: 1,
      product1_id: 1,
      product1_text: 1,
      product2_id: 1,
      product2_text: 1,
      product3_id: 1,
      product3_text: 1,
      order_id: 1,
    }
  });
});

Meteor.publish('Portfolios', function () {
  return Portfolios.find();
});

Meteor.publish('Products', function () {
  return Products.find();
});


Meteor.publish('CustomerLoc', function () {
  return CustomerLoc.find();
});

Meteor.publish('TeamLoc', function () {
  return TeamLoc.find();
});

Meteor.publish('CommissionLevel', function () {
  return CommissionLevel.find();
});
