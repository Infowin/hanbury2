Meteor.startup(function () {
  // process.env.MAIL_URL = 'smtp://yinchen618%40gmail.com:tp6up4tp6@smtp.gmail.com:587';
  // process.env.MAIL_URL = 'smtp://notice%40hanburyifa.com:Hanbury99@smtp.office365.com:587';
  process.env.MAIL_URL = 'smtp://notice%40hanburyifa.com:Hanbury%40%40@smtp.office365.com:587';
});
/*// server code
Mandrill.config({
  username: process.env.MANDRILL_API_USER,  // the email address you log into Mandrill with. Only used to set MAIL_URL.
  key: process.env.MANDRILL_API_KEY  // get your Mandrill key from https://mandrillapp.com/settings/index
  // port: 587,  // defaults to 465 for SMTP over TLS
  host: 'smtp.gmail.com',  // the SMTP host
  // baseUrl: 'https://mandrillapp.com/api/1.0/'  // update this in case Mandrill changes its API endpoint URL or version
});
*/
// Meteor method code
// this.unblock();
// try {
//   [result = ]Mandrill.<category>.<call>(options, [callback]);
// } catch (e) {
//   // handle error
// }



Accounts.emailTemplates.siteName = "HanburyIFA";
Accounts.emailTemplates.from = "HanburyIFA Info <notice@hanburyifa.com>";
Accounts.emailTemplates.enrollAccount.subject = function (user) {
  return "Welcome to HanburyIFA, " + user.profile.name;
};
Accounts.emailTemplates.enrollAccount.text = function (user, url) {
  return "You have been selected to participate in building a better future!"
    + " To activate your account, simply click the link below:\n\n"
    + url;
};
Accounts.emailTemplates.resetPassword.from = function () {
  // Overrides value set in Accounts.emailTemplates.from when resetting passwords
  return "HanburyIFA Password Reset <notice@hanburyifa.com>";
};