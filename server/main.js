import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  //
  if(Country.find().count() == 0){
  	Country.insert({value: "台灣"});
  	Country.insert({value: "中國"});
  	Country.insert({value: "英國"});
  	Country.insert({value: "西班牙"});
  	Country.insert({value: "日本"});
  }

  if(Bankacc.find().count() == 0){
  	var bg_id = "";
  	if(Businessgroup.findOne({"value" : "Hanbury"})){
  		bg_id = Businessgroup.findOne({"value" : "Hanbury"})._id;
  	}
  	else{
  		bg_id = Businessgroup.insert({"value" : "Hanbury", "uid" : 1});
  	}

  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "公司" });
  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "公司-富邦" });
  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "公司-上海" });
  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "公司-零用金" });
  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "Aaron" });
  	Bankacc.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury", "value" : "Stephy" });
  }

  if(Account1.find().count() == 0){
  	var bg_id = "";
  	if(Businessgroup.findOne({"value" : "Hanbury"})){
  		bg_id = Businessgroup.findOne({"value" : "Hanbury"})._id;
  	}
  	else{
  		bg_id = Businessgroup.insert({"value" : "Hanbury", "uid" : 1});
  	}

  	var a1_id = Account1.insert({
	    "bg_id" : bg_id,
	    "bg_text" : "Hanbury",
	    "value" : "公司支出",
	    "order_id" : 1
	});

  	var a2_id = Account2.insert({
	    "bg_id" : bg_id,
	    "bg_text" : "Hanbury",
	    "a1_id" : a1_id,
	    "a1_text" : "公司支出",
	    "value" : "薪資福利",
	    "order_id" : 1
	});
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "薪資津貼", "ps" : "薪資、獎金、年終", "order_id" : 1 });
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "職工福利", "ps" : "慶生會、員工聚餐、三節禮品、家庭日、尾牙、員工旅遊、奠儀", "order_id" : 2 });
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "勞健保費", "ps" : "勞保、勞退、健保、二代健保", "order_id" : 3 });
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "股東紅利", "ps" : "", "order_id" : 4 });
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "佣金費用", "ps" : "", "order_id" : 5 });
  	Account3.insert({
	    "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出",
	    "a2_id" : a2_id, "a2_text" : "薪資福利 ",
	    "value" : "退休金", "ps" : "", "order_id" : 6 });


  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "value" : "營運費用", "order_id" : 2
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "管理費用", "ps" : "管理費", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "租金費用", "ps" : "租金", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "水電費用", "ps" : "", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "辦公(事務)用品", "ps" : "文具、名片、裝訂、印刷、華生桶裝水", "order_id" : 4 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "設備", "ps" : "", "order_id" : 5 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "書報雜誌", "ps" : "訂閱報紙、雜誌及購買書籍之支出", "order_id" : 6 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "郵票、快遞費用", "ps" : "郵寄、快遞、超商或宅急便等物品運送", "order_id" : 7 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "電話費用", "ps" : "市話、傳真電話、節費電話", "order_id" : 8 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "設備軟、硬體租金及使用費", "ps" : "影印機、MIS", "order_id" : 9 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "清潔費", "ps" : "", "order_id" : 10 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "修繕費", "ps" : "固定資產之維修保養、更換零件等支出", "order_id" : 11 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "交際費用", "ps" : "因業務需要之餽贈、招待所產生之費用。不包含客群經營", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "稅捐", "ps" : "租賃稅、牌照稅、房屋稅、地價稅等各項稅捐", "order_id" : 12 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "所得稅", "ps" : "本期應負擔之所得稅費用", "order_id" : 13 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "營運費用 ",
	    "value" : "營業稅", "ps" : "本期應負擔之營業稅費用", "order_id" : 14 });

  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "value" : "會費、專業費用", "order_id" : 3
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "會費、專業費用",
	    "value" : "律師、會計師費", "ps" : "", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "會費、專業費用",
	    "value" : "會費", "ps" : "各項入會之會費", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "會費、專業費用",
	    "value" : "風水堪輿、修法費用", "ps" : "", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "會費、專業費用",
	    "value" : "權利金", "ps" : "使用他人著作權所付的代價", "order_id" : 4 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "會費、專業費用",
	    "value" : "承包費用", "ps" : "公司營運項目向外承包。(系統、人資)", "order_id" : 5 });


  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "value" : "差旅費", "order_id" : 4
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "差旅費",
	    "value" : "差旅費-交通", "ps" : "員工因公出差之交通費用", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "差旅費",
	    "value" : "差旅費-住宿", "ps" : "員工因公出差之住宿費用", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "差旅費",
	    "value" : "差旅費-餐點", "ps" : "員工因公出差之用餐費用", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "差旅費",
	    "value" : "差旅費-差旅津貼", "ps" : "差旅津貼", "order_id" : 4 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "差旅費",
	    "value" : "差旅費-什項或其他費用", "ps" : "電信加值、用品", "order_id" : 5 });

  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "value" : "活動、廣告宣傳", "order_id" : 5
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "活動、廣告宣傳",
	    "value" : "活動費用", "ps" : "經營會議、記者會、招待會", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "活動、廣告宣傳",
	    "value" : "廣告費", "ps" : "與營業有關之登報等廣告費用 (以印有公司名稱之物品贈送顧客亦屬)", "order_id" : 2 });

  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "value" : "其他費用", "order_id" : 6
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "其他費用",
	    "value" : "保單移轉費用", "ps" : "", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "其他費用",
	    "value" : "保單補償費用", "ps" : "", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "其他費用",
	    "value" : "公司代墊費用", "ps" : "為負項，業務回補公司代墊費用", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "公司支出", "a2_id" : a2_id, "a2_text" : "其他費用",
	    "value" : "什項或其他費用", "ps" : "", "order_id" : 4 });


  	var a1_id = Account1.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "value" : "餐費", "order_id" : 2 });
  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "餐費", "value" : "餐費", "order_id" : 1
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "餐費", "a2_id" : a2_id, "a2_text" : "餐費",
	    "value" : "早餐", "ps" : "", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "餐費", "a2_id" : a2_id, "a2_text" : "餐費",
	    "value" : "午餐", "ps" : "", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "餐費", "a2_id" : a2_id, "a2_text" : "餐費",
	    "value" : "下午茶", "ps" : "", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "餐費", "a2_id" : a2_id, "a2_text" : "餐費",
	    "value" : "晚餐", "ps" : "", "order_id" : 4 });

  	var a1_id = Account1.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "value" : "交通", "order_id" : 3 });
  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "value" : "交通", "order_id" : 1
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "飛機", "ps" : "", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "高鐵", "ps" : "", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "大眾運輸", "ps" : "", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "計程車", "ps" : "", "order_id" : 4 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "加油油資", "ps" : "", "order_id" : 5 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "停車費", "ps" : "", "order_id" : 6 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "交通",
	    "value" : "租賃車", "ps" : "", "order_id" : 7 });

  	var a1_id = Account1.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "value" : "客群經營", "order_id" : 4 });
  	var a2_id = Account2.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "客群經營", "value" : "客群經營", "order_id" : 1
	});
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "餐費", "ps" : "", "order_id" : 1 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "住宿", "ps" : "", "order_id" : 2 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "交通", "ps" : "", "order_id" : 3 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "贈禮（中秋）", "ps" : "", "order_id" : 4 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "贈禮（年節）", "ps" : "", "order_id" : 5 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "贈禮（個人特殊節日）", "ps" : "", "order_id" : 6 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "伴手禮", "ps" : "", "order_id" : 8 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "紅白包", "ps" : "", "order_id" : 9 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "CPA Letter", "ps" : "", "order_id" : 10 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "信託/公證費用", "ps" : "", "order_id" : 11 });
  	Account3.insert({ "bg_id" : bg_id, "bg_text" : "Hanbury",
	    "a1_id" : a1_id, "a1_text" : "交通", "a2_id" : a2_id, "a2_text" : "客群經營",
	    "value" : "雜項", "ps" : "", "order_id" : 12 });
  }
});
