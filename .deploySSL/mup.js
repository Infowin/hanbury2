module.exports = {
  servers: {
    one: {
      // host: "ec2-52-197-160-117.ap-northeast-1.compute.amazonaws.com",
      // username: 'ubuntu',
      // pem: "/Users/michaelchen/Dropbox/_code/lb0505.pem"
      host: "40.115.164.125",
      username: 'infowintech',
      password: 'Info24934467'
    }
  },

  meteor: {
    name: 'hanbury2',
    path: "~/Documents/_code/hanbury2",
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
      debug: true,
      // debug: false,
    },
    env: {
      // PORT: '8000',
      // ROOT_URL: "http://ec2-52-197-160-117.ap-northeast-1.compute.amazonaws.com",
      ROOT_URL: "https://40.115.164.125",
      MONGO_URL: 'mongodb://localhost:27017/meteor'
    },
    //    dockerImage: 'abernix/meteord:base',
    dockerImage: 'abernix/meteord:node-8.9.1-base',
    enableUploadProgressBar: true,
    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },

  mongo: {
    version: '3.4.0',
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
  plugins: ['mup-disk'],
  proxy: {
    domains: 'hanbury.infowin.com.tw',
    ssl: {
      forceSSL: true,
      letsEncryptEmail: 'michael@infowin.com.tw'
    },
    "shared": {
      "envLetsEncrypt": {
        "DEBUG": true
      }
    }
  }
};
