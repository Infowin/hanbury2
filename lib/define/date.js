years = [];
var year = new Date();
for (var i = 0; i <= (year.getFullYear() - 1911); i++)
    years.push({ id: i, value: i });

months = [];
for (var i = 1; i <= 12; i++)
    months.push({ id: i, value: i });

days = [];
for (var i = 1; i <= 31; i++)
    days.push({ id: i, value: i });

dateDiff = function (d1, d2 = new Date()) {
    //   console.log(d1, d2)
    const date1 = new Date(d1);
    const date2 = new Date(d2);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
}

currentdate = function () {
    var dd = new Date();
    return dd.getFullYear() + '/' + (dd.getMonth() + 1) + '/' + dd.getDate(); // +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds()
}
currentdate_eff = function (e) {
    var dd = new Date(e);
    return dd.getFullYear() + '/' + (dd.getMonth() + 1) + '/' + dd.getDate(); // +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds()
}
currentdate2 = function () {
    var dd = new Date();
    return dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate(); // +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds()
}
Date.prototype.yyyymmdd = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();
    return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
};
Date.prototype.yyyymmddhm = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();
    var h = this.getHours().toString();
    var m = this.getMinutes().toString();
    // var s  = this.getSeconds().toString();
    return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]) + " " + (h[1] ? h : "0" + h[0]) + ":" + (m[1] ? m : "0" + m[0]); // padding
};
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

funcHourToDayText = function (hours) {

    var day = parseInt(hours / 8);
    var hour = hours % 8;
    return day + "天 " + hour + "時";

}
funcTimeInterval = function (sec) {
    var ret;
    /*
    小时：h=time/3600（整除）
    分钟：m=(time-h*3600)/60 （整除）
    秒：s=(time-h*3600) mod 60 （取余）
     */
    sec = parseInt(sec);

    if (isNaN(sec)) {
        return "";
    }

    if (sec < 60) { // 一分鐘內
        ret = sec + " 秒";
    }
    else if (sec < 3600) { // 一小時內
        ret = Math.floor(sec / 60) + " 分";
    }
    else if (sec < 86400) { // 一天內
        var h = Math.floor(sec / 3600);
        ret = h + " 時 ";
        sec = sec - h * 3600;

        var m = Math.floor(sec / 60);
        ret = ret + m + " 分";
    }
    else {
        var d = Math.floor(sec / 86400); // 幾天
        ret = d + " 天 ";
        sec = sec - d * 86400;

        var h = Math.floor(sec / 3600);
        ret = ret + h + " 時 ";
        sec = sec - h * 3600;

        // var m = Math.floor(sec/60);
        // ret = ret + m + " 分";
    }

    return ret;
}

// 加半年
add_helf = function (d) {
    return (new Date(d)).addDays(183);
}
// // 7/1 or 1/1
// next_start_date = function(d) {
//     var r = new Date();
//     if (d.getMonth() < 6) {
//         r.setFullYear(d.getFullYear(), 6, 1);
//     } else {
//         r.setFullYear(d.getFullYear() + 1, 0, 1);
//     }
//     return r;
// }
// // 下個月一號
// next_helfyearly_start_date = function(d) {
//     var r = new Date();
//     r.setFullYear(d.getFullYear(), d.getMonth() + 1, 1)
//     return r;
// }
// // 下一年同天
// next_yearly_start_date = function (d) {
//     var r = new Date();
//     r.setFullYear(d.getFullYear(), d.getMonth() + 12, d.getDate())
//     return r;
// }