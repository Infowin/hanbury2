objSexual = [{id:0, value:""}, {id:1, value:"男"},{id:2, value:"女"}];
objSexual2 = [{id:1, value:"男"},{id:2, value:"女"}];
objSexual3 = [{id:"1", value:"男"},{id:"2", value:"女"}];
objYesNo = [{id:0, value:""}, {id:1, value:"是"},{id:2, value:"否"}];
objYes = [{id:0, value:""}, {id:1, value:"是"}];
arrSexual2 = ["", "男", "女"];

arrSelEmpty = [{ id:"", value:""}];
objGraduate = [{ id:"1", value:"國小"}, { id:"2", value:"國中"},
    { id:"3", value:"高中"}, { id:"4", value:"大學"}, { id:"5", value:"碩士"}, { id:"6", value:"博士"}];

objMarriage = [{ id:"1", value:"未婚"}, { id:"2", value:"已婚"}];

objTitle = [
    { id:"1", value:"先生"},
    { id:"2", value:"小姐"},
    { id:"3", value:"女士"},
    { id:"6", value:"博士"}
];

objAuth0 = [
    { id:"0", value: "關閉" },
    { id:"1", value:"允許"},
];
objAuth1 = [
    { id:"0", value:"禁止"},
    { id:"1", value:"讀取"},
    { id:"2", value:"讀取+編輯"},
    { id:"3", value:"讀取+開帳"},
];
objAuth2 = [
    { id:"0", value:"禁止"},
    { id:"1", value:"讀取"},
    { id:"2", value:"讀取+編輯"},
];
objAuth4 = [
    { id:"0", value:"禁止"},
    { id:"1", value:"讀取"},
    { id:"2", value:"讀取+編輯"},
    { id:"3", value:"讀取+編輯+刪除"},
];
objAuth3 = [
    { id:"0", value:"無"},
    { id:"1", value:"有"},
];

objMailsending = [
    { id:"1", value:"直接寄送"},
    { id:"2", value:"排程"},
    // { id:"3", value:"取消"},
];
objMailsended = [
    { id:"1", value:"已寄送"},
    { id:"2", value:"排程中"},
    { id:"3", value:"取消排程"},
];

objASAction = [
    { id:"1", value:"Awaiting for Provider's confirmation"},
    { id:"2", value:"Awaiting for Agent's Instruction"},
];
objASActionOwner = [
    { id:"1", value:"GrandTag"},
    { id:"2", value:"Panorama"},
    { id:"3", value:"Agent"},
    { id:"4", value:"Hanbury Operation"},
    { id:"5", value:"Provider"},
];

objFollownewcaseOwner = [
    { id:"1", value:"Operation"},
    { id:"2", value:"Grandtag"},
    { id:"3", value:"Agent"},
    { id:"4", value:"Client"},
];

objASPhase = [
    {id: "1", value: '申請中'},
    {id: "2", value: '已完成'},
    {id: "3", value: '已取消'},
    {id: "4", value: 'Pending'},
];
objICLeave = [
    {id: "1", value: 'end term return'},
    {id: "2", value: 'buy back'}, // 預設
    {id: "3", value: 'no buy back'},
    {id: "4", value: 'none'},
];
objICStartDay = [
    {id: "1", value: '匯款日 (可能有多個)'},
    {id: "2", value: '生效日'}, // 預設
];
objICPeriod = [
    {id: "0", value: '不固定'},
    {id: "1", value: 'Half-yearly(6月底/12月底)'},
    // {id: "2", value: 'Half-yearly(生效日)'},
    {id: "3", value: 'Yearly(12月底)'}, // 預設
    {id: "4", value: 'Yearly(生效日)'}, // 預設
];
objICPeriod2 = [
    {id: "0", value: '每年'},
    {id: "1", value: '每半年'},
    // {id: "2", value: 'Half-yearly(生效日)'},
    {id: "3", value: '每季'}, // 預設
    {id: "4", value: '每月'}, // 預設
];
objICCutOff = [
    {id: "1", value: '6月底/12月底'},
    {id: "2", value: '12月底'}, // 預設
    {id: "3", value: '生效日'}, // 預設
];

objICPayDate = [
    {id: "1", value: '預計1月/7月'},
    {id: "2", value: '預計1月'}, // 預設
    {id: "3", value: '生效日'}, // 預設
];

objPay_status = [
    {id: "1", value: '已繳費'},
    {id: "2", value: '未繳費'},
    {id: "3", value: '其他'},
];


/*
新案申請_保險-Pru Life    1
新案申請_保險-VOYA    2
新案申請_英國房產-股權    3
新案申請_英國房產-產權    4
新案申請_美國.加拿大 5
FPI架構   6
股票  7
AVIVA/Zurich/Standardlife/MassMutual/AIA 架構 8
 */
objPortfolioTemplate = [
    { id:"0", value:"顯示樣版無定義"},
    { id:"1", value:"保險-Pru Life"}, // ok
    { id:"2", value:"保險-VOYA"},     // ok like 1 ok
    { id:"3", value:"英國房產-股權"},  // ok
    { id:"4", value:"英國房產-產權"},   // ok like 3
    { id:"5", value:"美國、加拿大房產"},    // ok like 3
    { id:"6", value:"FPI"}, // ok
    { id:"7", value:"股票"},                  // ok like 3
    { id:"8", value:"AVIVA/Zurich/Standardlife/MassMutual/AIA"}, // ok like 6
    // { id:"9", value:"(AS)Zurich架構"},
    // { id:"10", value:"(AS)AVIVA架構"},
    // { id:"11", value:"(AS)FPI架構"},
];

// 申請中/已生效/無效
objNowPhase = [
    {value: 1, text: '申請中'},
    {value: 2, text: '已生效'},
    {value: 3, text: '無效'},
];
objYN = [
    {value: 1, text: 'Y'},
    {value: 2, text: 'N'},
];
objSexual4 = [
    {value: 1, text: '男'},
    {value: 2, text: '女'},
];
objIsRevoke = [
    {value: 1, text: '可撤銷'},
    {value: 2, text: '不可撤銷'},
];
objInsuranceStatus = [
    {value: 1, text: '生效 In force'},
    {value: 2, text: '解約 Surrender'},
];
objInsurancePayStatus = [
    {value: 1, text: '繳款中Premium paying'},
    {value: 2, text: '終止繳費Paid up'},
];

objPayMethodFPI = [
    {value: 1, text: 'Credit Card'},
    {value: 2, text: 'BSO'},
    {value: 3, text: 'Direct Paying'}
];
objPayPeriodFPI = [
    {value: 1, text: 'Annually'},
    {value: 2, text: 'Six Monthly'},
    {value: 3, text: 'Quarterly'},
    {value: 4, text: 'Monthly'},
    {value: 5, text: 'Single Premium'}
];

// 2 VOYA
// 現況       Active / Lapes
objNowStatus = [
    {value: 1, text: 'Active'},
    {value: 2, text: 'Lapse'},
];

objDepartment = [
    {value: "1", text: 'Hanbury'},
    {value: "2", text: 'Hanbursing'},
    {value: "3", text: 'Breidin'},
];

/*
核保體位
(SPNT/PNT/SNT/PT/ST)
(Table 0/Table A/Table B/Table C/Table D)
 */
objBodyCheckType1 = [
    {value: 1, text: 'SPNT'},
    {value: 2, text: 'PNT'},
    {value: 3, text: 'SNT'},
    {value: 4, text: 'PT'},
    {value: 5, text: 'ST'},
];
objBodyCheckType2 = [
    {value: 1, text: 'Table 0'},
    {value: 2, text: 'Table A'},
    {value: 3, text: 'Table B'},
    {value: 4, text: 'Table C'},
    {value: 5, text: 'Table D'},
];

objBodyCheckType3 = [
    {value: 1, text: 'Preferred Best'},
    {value: 2, text: 'Preferred Non-Tobacco'},
    {value: 3, text: 'Non-Somker Plus'},
    {value: 4, text: 'Non-Smoker'},
    {value: 5, text: 'Preferred Smoker'},
    {value: 6, text: 'Smoker'},
];


objSalaryCash = [
    { value:"0", text:"台幣NTD"},
    { value:"1", text:"港幣HKD"},
    { value:"2", text:"人民幣RMB"},
    { value:"3", text:"美金USD"},
    { value:"4", text:"英磅GBP"},
    { value:"5", text:"歐元EUR"},
    { value:"6", text:"日圓JPY"},
];

objSalaryCash_eng = [
    { value:"0", text:"NTD"},
    { value:"1", text:"HKD"},
    { value:"2", text:"RMB"},
    { value:"3", text:"USD"},
    { value:"4", text:"GBP"},
    { value:"5", text:"EUR"},
    { value:"6", text:"JPY"},
];
objLockMonth = [
    { value:"", text:""},
    { value:"18", text:"18"},
    { value:"24", text:"24"},
    { value:"30", text:"30"},
];

objPruPayMethod = [
    {value: 1, text: 'FPI提款'},
    {value: 2, text: 'HSBC 取款單'},
    {value: 3, text: 'TT'},
    {value: 4, text: 'Non-Smoker'},
    {value: 5, text: 'Preferred Smoker'},
    {value: 6, text: 'Smoker'},
];

objStockItem = [
    {value: 1, text: 'Hanburfore & Colonial Realty Co., Ltd'},
    {value: 2, text: 'Hanburfore & Colonial II Realty Co., Ltd'},
    {value: 3, text: 'Hanburfore & Colonial III Realty Co., Ltd'},
];

objYearNum =  [
    { value:"2000", text:"2000"},
    { value:"2001", text:"2001"},
    { value:"2002", text:"2002"},
    { value:"2003", text:"2003"},
    { value:"2004", text:"2004"},
    { value:"2005", text:"2005"},
    { value:"2006", text:"2006"},
    { value:"2007", text:"2007"},
    { value:"2008", text:"2008"},
    { value:"2009", text:"2009"},
    { value:"2010", text:"2010"},
    { value:"2011", text:"2011"},
    { value:"2012", text:"2012"},
    { value:"2013", text:"2013"},
    { value:"2014", text:"2014"},
    { value:"2015", text:"2015"},
    { value:"2016", text:"2016"},
    { value:"2017", text:"2017"},
    { value:"2018", text:"2018"},
    { value:"2019", text:"2019"},
    { value:"2020", text:"2020"},
    { value:"2021", text:"2021"},
    { value:"2022", text:"2022"},
    { value:"2023", text:"2023"},
    { value:"2024", text:"2024"},
    { value:"2025", text:"2025"},
    { value:"2026", text:"2026"},
    { value:"2027", text:"2027"},
    { value:"2028", text:"2028"},
    { value:"2029", text:"2029"},
    { value:"2030", text:"2030"},
    { value:"2031", text:"2031"},
    { value:"2032", text:"2032"},
    { value:"2033", text:"2033"},
    { value:"2034", text:"2034"},
    { value:"2035", text:"2035"},
    { value:"2036", text:"2036"},
    { value:"2037", text:"2037"},
    { value:"2038", text:"2038"},
    { value:"2039", text:"2039"},
    { value:"2040", text:"2040"},
];
objPayYearNum =  [
    { value:"1",  text:"1年"},
    { value:"2",  text:"2年"},
    { value:"3",  text:"3年"},
    { value:"4",  text:"4年"},
    { value:"5",  text:"5年"},
    { value:"6",  text:"6年"},
    { value:"7",  text:"7年"},
    { value:"8",  text:"8年"},
    { value:"9",  text:"9年"},
    { value:"10", text:"10年"},
    { value:"11", text:"11年"},
    { value:"12", text:"12年"},
    { value:"13", text:"13年"},
    { value:"14", text:"14年"},
    { value:"15", text:"15年"},
    { value:"16", text:"16年"},
    { value:"17", text:"17年"},
    { value:"18", text:"18年"},
    { value:"19", text:"19年"},
    { value:"20", text:"20年"},
];

objPayMonthNum =  [
    { value:"0",  text:"0月"},
    { value:"1",  text:"1月"},
    { value:"2",  text:"2月"},
    { value:"3",  text:"3月"},
    { value:"4",  text:"4月"},
    { value:"5",  text:"5月"},
    { value:"6",  text:"6月"},
    { value:"7",  text:"7月"},
    { value:"8",  text:"8月"},
    { value:"9",  text:"9月"},
    { value:"10", text:"10月"},
    { value:"11", text:"11月"},
    { value:"12", text:"12月"},
];
objPaymentFreqYear =  [
    { value:"1", text:"一次"},
    { value:"2", text:"一年"},
    { value:"3", text:"半年"},
    { value:"4", text:"無"},
];
objHalfYear =  [
    { value:"1", text:"一次"},
    { value:"2", text:"一年"},
    { value:"3", text:"上半年"},
    { value:"4", text:"下半年"},
];

objInvestType =  [
    { value:"1", text:"產權"},
    { value:"2", text:"股權"},
    { value:"3", text:"無"},
];

objCompanyType =  [
    { value:"1", text:"LP"},
    { value:"2", text:"LLC"},
    { value:"3", text:"無"},
];
objUnitType =  [
    { value:"A", text:"A"},
    { value:"I", text:"I"},
];

objMailsendresult = [
    { id:"1", value:"成功"},
    { id:"2", value:"失敗"},
    { id:"3", value:"等待中"},
];

objDepartment2 = [
    { id:"1", value:"全體同仁"},
    { id:"2", value:"營運"},
    { id:"3", value:"財務"},
    { id:"4", value:"業務"},
];

objIsOpen = [
    { id:"1", value:"開啟"},
    { id:"2", value:"關閉"},
];

objBankAccount = [
    { id:"1", value:"公司"},
    { id:"2", value:"公司-富邦"},
    { id:"3", value:"公司-上海"},
    { id:"4", value:"公司-零用金"},
    { id:"5", value:"Aaron"},
    { id:"6", value:"Stephy"}
];
objSalaryType = [
    // { id:"0", value:""},
    { id:"1", value:"薪資"},
    { id:"2", value:"發佣"},
    { id:"3", value:"補貼"},
    { id:"10", value:"其他"},
];
objPayType = [
    // { id:"0", value:""},
    { id:"1", value:"現金"},
    { id:"2", value:"支票"},
    { id:"3", value:"電匯"},
    { id:"10", value:"其他"},
];

objSalaryCash2 = [
    { id:"0", value:"NTD"},
    { id:"1", value:"HKD"},
    { id:"2", value:"RMB"},
    { id:"3", value:"USD"},
    { id:"4", value:"GBP"},
    { id:"5", value:"EUR"},
    { id:"6", value:"JPY"},
];

objSalaryCash3 = [
    { id:"0", value:"台幣NTD"},
    { id:"1", value:"港幣HKD"},
    { id:"2", value:"人民幣RMB"},
    { id:"3", value:"美金USD"},
    { id:"4", value:"英磅GBP"},
    { id:"5", value:"歐元EUR"},
    { id:"6", value:"日圓JPY"},
];
objCountries = [
    // { id:"", value:""},
    { id:"1", value:"台灣"},
    { id:"2", value:"香港"},
    { id:"3", value:"美國"},
    { id:"4", value:"英國"},
    { id:"5", value:"歐洲"},
    { id:"6", value:"中國"}
];

objCountries2 = [
    // { id:"", value:""},
    { id:"1", value:"台灣"},
    { id:"2", value:"香港"},
    { id:"3", value:"上海"},
    { id:"4", value:"杭州"},
    { id:"5", value:"大陸"},
    { id:"6", value:"英國"},
    { id:"7", value:"歐洲"},
    { id:"8", value:"日本"},
    { id:"9", value:"美國"},
];

objDay_off = [
    // { id:"", value:""},
    { id:"1", value:"公假"},
    { id:"2", value:"公傷病假"},
    { id:"3", value:"事假"},
    { id:"4", value:"病假"},
    { id:"5", value:"婚假"},
    { id:"6", value:"傷假"},
    { id:"7", value:"產假"},
    { id:"8", value:"生理假"},
    { id:"9", value:"陪產假"},
    { id:"10", value:"特休假"},
    { id:"11", value:"育嬰假"},
    { id:"12", value:"補休假"},
    { id:"13", value:"補休申請"},
];

objDay_off_set = [
    // { id:"", value:""},
    { id:"1", value:"公假"},
    { id:"2", value:"公傷病假"},
    { id:"3", value:"事假"},
    { id:"4", value:"病假"},
    { id:"5", value:"婚假"},
    { id:"6", value:"傷假"},
    { id:"7", value:"產假"},
    { id:"8", value:"生理假"},
    { id:"9", value:"陪產假"},
    { id:"10", value:"特休假"},
    { id:"11", value:"育嬰假"},
    { id:"12", value:"補休假"},
    { id:"13", value:"補休申請"},
];

objDeductions = [
    { id:"1", value:"全薪"},
    { id:"2", value:"半薪"},
    { id:"3", value:"不扣薪"},
];

objDay_off_status = [
    // { id:"", value:""},
    { id:"1", value:"填寫中"},
    { id:"2", value:"代理人覆核中"},
    { id:"3", value:"主管簽核中"},
    { id:"4", value:"人資簽核中"},
    { id:"5", value:"已生效"},
    { id:"6", value:"已取消"},
];

objDay_off_status_first = [
    // { id:"", value:""},
    { id:"1", value:"填寫中"},
    { id:"2", value:"請代理人覆核"},
    // { id:"3", value:"主管簽核中"},
    // { id:"4", value:"人資簽核中"},
    // { id:"5", value:"已生效"},
    // { id:"6", value:"已取消"},
];
objDay_off_status_second = [
    // { id:"", value:""},
    { id:"2", value:"代理人覆核中"},
    { id:"3", value:"送交主管簽核"},
    // { id:"4", value:"人資簽核中"},
    // { id:"5", value:"已生效"},
    { id:"1", value:"退回給申請人"},
    { id:"6", value:"取消本申請"},
];
objDay_off_status_third = [
    // { id:"", value:""},
    // { id:"2", value:"退給代理人"},
    { id:"3", value:"主管簽核中"},
    { id:"4", value:"送交人資簽核"},
    // { id:"5", value:"已生效"},
    { id:"1", value:"退回給申請人"},
    { id:"6", value:"取消本申請"},
];
objDay_off_status_fourth = [
    // { id:"", value:""},
    // { id:"2", value:"代理人覆核中"},
    // { id:"3", value:"退給主管"},
    { id:"4", value:"人資簽核中"},
    { id:"5", value:"提交已生效"},
    { id:"1", value:"退回給申請人"},
    { id:"6", value:"取消本申請"},
];

/*obj = [
    { id:"1", value:"台灣"},
    { id:"2", value:"香港"}
];*/

objInvExp = [{ id:"0", value:"沒有"}, { id:"1", value:"一年以下"}, { id:"2", value:"一至五年"}, { id:"3", value:"五年以上"}];

objInvPurpose = [{ id:"1", value:"退休準備"}, { id:"2", value:"教育"},
    { id:"3", value:"遺產規劃"}, { id:"4", value:"儲蓄"}, { id:"5", value:"資本增值"}, { id:"6", value:"其他"}];

objInvRisk = [{ id:"0", value:"10%以下"}, { id:"1", value:"11%~20%"}, { id:"2", value:"20%以上"}];

objInvPurRet = [{ id:"0", value:"5%至10%"}, { id:"1", value:"11%至20%"}, { id:"2", value:"20%以上"}];

objUserRole = [
    // { id:"", value:""},
    { id:"1", role:"guest", value:"訪客"},
    { id:"10", role:"agent", value:"Agent"},
    { id:"20", role:"agentleader", value:"Agent Leader"},
    { id:"30", role:"commission", value:"一般行政"},
    { id:"40", role:"director1", value:"行政主管"},
    { id:"41", role:"director2", value:"財務主管"},
    { id:"42", role:"director4", value:"財務行政主管"},
    { id:"50", role:"supervisor", value:"總監"},
    { id:"90", role:"administrative", value:"佣金管理者"},
    { id:"95", role:"designer", value:"系統開發者"},
    { id:"99", role:"admin", value:"管理者"}
];


// 狀態
objStatusRes = [
    { id:"3", value:"無效"},
    { id:"1", value:"申請中"}, // 用objReviewRes的歷程
    { id:"2", value:"已生效"},  // 已完成的時候
];

// p5
objStatusP5 = [
    { value:"1", text:"申請中"},
    { value:"2", text:"已完成"},
    { value:"3", text:"待處理"},
];
objP5Country = [
    { value:"1", text:"美國"},
    { value:"2", text:"加拿大"},
];
objP5Dollar = [
    { value:"1", text:"美金"},
    { value:"2", text:"加幣"},
];
objSeason = [
    { value:"1", text:"第一季"},
    { value:"2", text:"第二季"},
    { value:"3", text:"第三季"},
    { value:"4", text:"第四季"},
];

// 申請中
objReviewRes = [
    { id:"0", value:"草稿"},
    { id:"1", value:"已建檔"},
    { id:"2", value:"已覆核"},
    { id:"3", value:"處理中 (廣達)"},
    { id:"4", value:"處理中 (Hanbury)"},
    { id:"5", value:"已收件"},
    { id:"6", value:"已交付文件"},
    { id:"7", value:"已完成"},
    { id:"8", value:"取消"}];

objProductClass = [
    // { id:"", value:""},
    { id:"1", value:"房產"},
    { id:"2", value:"安養院"},
    { id:"3", value:"保險"},
    { id:"4", value:"第三方資產管理"},
    { id:"5", value:"投資"}
];
arrProductClass = ["", "房產", "安養院", "保險", "第三方資產管理", "投資"];

objTalkType = [
    // { id:"", value:""},
    { id:"1", value:"面談"},
    { id:"2", value:"電話"},
    { id:"3", value:"E-Mail"},
    { id:"4", value:"郵寄"},
];

objCostType = [
    // { id:"", value:""},
    { id:"1", value:"無"},
    { id:"2", value:"客群經營 - 餐費"},
    { id:"3", value:"客群經營 - 贈禮(中秋)"},
    { id:"4", value:"客群經營 - 贈禮(年節)"},
    { id:"5", value:"客群經營 - 贈禮(個人特殊節日)"},
    { id:"6", value:"客群經營 - 住宿"},
    { id:"7", value:"客群經營 - 交通"},
    { id:"8", value:"客群經營 - 雜項"},
];

objTalkResult = [
    // { id:"", value:""},
    { id:"1", value:"僅維繫客戶"},
    { id:"2", value:"成交"},
    { id:"3", value:"考慮中(產品不熟悉)"},
    { id:"4", value:"考慮中(入場時機點)"},
    { id:"5", value:"考慮中(資金不夠)"},
    { id:"6", value:"婉拒(家人反對)"},
    { id:"7", value:"婉拒(擔憂市場風險)"},
    { id:"8", value:"婉拒(已擁有太多相似產品)"},
    { id:"9", value:"售後服務"},
];

objCommissionLevel = [{ id:"1", value:"Green"}, { id:"2", value:"Sliver"},
    { id:"3", value:"Gold"}, { id:"4", value:"Diamond"}];

objAuthority = [
    { id: 'auth_fin', value:'1 財務&績效管理'},
    { id: 'auth_hr', value:'2 人資管理'},
    { id: 'auth_cus', value:'3 客戶管理'},
    { id: 'auth_as', value:'4 售後服務'},
    { id: 'auth_prod', value:'5 產品中心'},
    { id: 'auth_web', value: '6 網站管理' },

    { id: 'auth_as1', value: '4-1 案件追蹤' },
    { id: 'auth_as2', value: '4-2 保費提醒' },
    { id: 'auth_as3', value: '4-3 產品配息調整' },
    // { id: 'auth_web', value: '網站管理' },
    // { id: 'auth_web', value: '網站管理' },
    // { id: 'auth_web', value:'網站管理'},
]

objPaymentFreq =  [
    // { id:"0", value:""},
    { id:"1", value:"一次性"},
    { id:"2", value:"半年"},
    { id:"3", value:"一年"},
];

objPayMethod =  [
    { id:"90", value:"生效"},
    { id:"99", value:"終止繳費"}
];

objManagedTeam =  [{ id:"1", value:"Hanbury"}];

objCmsStatus = [{ id:"1", value:"未發佣"}, { id:"2", value:"已發佣"}];


arrPageSize = ["", "A3", "B4", "A4"];
objPageSize = [{ id:0, value:"" }, { id:1, value:"A3" }, { id:2, value:"B4" }, { id:3, value:"A4" } ];
