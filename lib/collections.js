Workdays = new Mongo.Collection("workdays"); // 預估天數
PortfolioStatus = new Mongo.Collection("portfoliostatus");

News = new Mongo.Collection("news"); // 公告欄
Calendar = new Mongo.Collection("calendar"); // 行事曆
Contact1 = new Mongo.Collection("contact1"); // 通訊錄
Contact2 = new Mongo.Collection("contact2"); // 通訊錄

Emails = new Mongo.Collection("emails"); // 行事曆
Emailauto = new Mongo.Collection("emailauto"); // 行事曆
Emailtemplate = new Mongo.Collection("emailtemplate"); // 行事曆

Account = new Mongo.Collection("account"); // 會計科目
Account1 = new Mongo.Collection("account1"); // 會計科目1
Account2 = new Mongo.Collection("account2"); // 會計科目2
Account3 = new Mongo.Collection("account3"); // 會計科目3 科目細項

Country = new Mongo.Collection("country"); // 會計科目

Bookingmonth = new Mongo.Collection("bookingmonth"); // 會計科目
Booking = new Mongo.Collection("booking"); // 會計科目
Bankacc = new Mongo.Collection("bankacc"); // 出款帳戶
Businessgroup = new Mongo.Collection("businessgroup"); // 隸屬事業群
Accountyear = new Mongo.Collection("accountyear"); //會計年度

Afterservice = new Mongo.Collection("afterservice");
Interestcondition = new Mongo.Collection("interestcondition");

Salary = new Mongo.Collection("salary");
Commission = new Mongo.Collection("commission");
Dayoff = new Mongo.Collection("dayoff"); //差勤申請
Dayoff_management = new Mongo.Collection("dayoff_management"); //差勤總覽
Dayoff_set = new Mongo.Collection("dayoff_set"); //差勤設定

Sales_level = new Mongo.Collection("sales_level"); //佣金等級計算
Fin_sales1 = new Mongo.Collection("fin_sales1"); //佣金等級
Fin_sales = new Mongo.Collection("fin_sales"); //業績目標
Fin_sales_year = new Mongo.Collection("fin_sales_year"); //業績管理年度


Nopdfform = new Mongo.Collection("nopdfform");
Hrform_management = new Mongo.Collection("hrform_management");//人事規章管理
Blank_form = new Mongo.Collection("blank_form");//空白表單

Agents = new Mongo.Collection("agents");
Clients = new Mongo.Collection("clients");
Portfolios = new Mongo.Collection("portfolios");
Interviews = new Mongo.Collection("interviews");
FundConfigure = new Mongo.Collection("fundconfigure");

MailTitle = new Mongo.Collection("mailtitle");
MailContext = new Mongo.Collection("mailcontent");

Review1 = new Mongo.Collection("review1"); // clients
Review2 = new Mongo.Collection("review2"); // portfolios

Formcenter1 = new Mongo.Collection("formcenter1");
Formcenter2 = new Mongo.Collection("formcenter2");

Reportcenter1 = new Mongo.Collection("reportcenter1");
Reportcenter2 = new Mongo.Collection("reportcenter2");

Oproduct1 = new Mongo.Collection("oproduct1");
Oproduct2 = new Mongo.Collection("oproduct2");
Oproduct3 = new Mongo.Collection("oproduct3");
Oproduct4 = new Mongo.Collection("oproduct4");

Products = new Mongo.Collection("products");
Product1 = new Mongo.Collection("product1");
Product2 = new Mongo.Collection("product2");
Product3 = new Mongo.Collection("product3");
Product4 = new Mongo.Collection("product4");

Provider = new Mongo.Collection("provider");

CustomerLoc = new Mongo.Collection("customerloc");
Teams = new Mongo.Collection("teams");

Cmslevel = new Mongo.Collection("cmslevel");
Cmsget = new Mongo.Collection("cmsget");
Accounting = new Mongo.Collection("accounting");

Files = new Mongo.Collection("files");
Oprec = new Mongo.Collection("oprec");

Addr1 = new Mongo.Collection("addr1");
Addr2 = new Mongo.Collection("addr2");
Addr3 = new Mongo.Collection("addr3");

Addr1a = new Mongo.Collection("addr1a");
Addr2a = new Mongo.Collection("addr2a");