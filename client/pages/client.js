Template.client.rendered = function () {
    // var $ = jQuery.noConflict();
    Template.semicolon.loadjs();

    var $container = $('#portfolio');

    $container.isotope({
        transitionDuration: '0.65s'
    });

    $('.portfolio-filter a').click(function () {
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });

    $('.portfolio-shuffle').click(function () {
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function () {
        $container.isotope('layout');
    });


    var ocImages = $("#oc-images");

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });
    var data = Clients.findOne();
    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        editing: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                // var item = Session.get("client");
                // var p4_id = Session.get("nowrow_oproduct4")._id;
                filter.client = "1"; // 1:尚未送出 2: 已送出
                filter.client_id = data._id;
                // filter.insertedById = Meteor.userId();

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            { name: "ctrl_field", type: "control", width: 40 }, // , editButton:false
            {
                name: "download", width: 100, align: "left", title: "檔案",
                itemTemplate: function (value, item) {
                    return '<a href="#" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
                },
            },
            // { name: "size", title: "檔案大小", type: "text", width:70, align:"right" },
            { name: "createdByName", width: 70, align: "left", title: "檔案建立者", type: "text" },
            { name: "insertedByName", width: 70, align: "left", title: "上傳者", type: "text", editing: false },
            {
                name: "insertedAt", title: "上傳日期", width: 70, editing: false,
                itemTemplate: function (value, item) {
                    if (!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
        ],
        onItemDeleting: function (args) {
            // console.log(args);
            var data = args.item;
            S3.delete(data.file.relative_url, function (e, r) {
                if (!!e) console.log(e);
                else {
                    // Materialize.toast('資料已刪除 '+ data.name, 3000, 'rounded');
                    console.log("資料已刪除" + data.name);
                }
            });
            // $("#cFiles").jsGrid("loadData");
        },
        onRefreshed: function () {
            var $gridData = $("#cFiles .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function (e, ui) {
                    // arrays of items
                    var items = $.map($gridData.find("tr"), function (row) {
                        return $(row).data("JSGridItem");
                    });
                    console.log("Reordered items", items);
                    Meteor.call("updateSortUploads", items, function (error, result) {
                        if (error) {
                            console.log("error from updateSortUploads: ", error);
                        }
                        else {
                            $("#cFiles").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });

    $("#input-3").fileinput({
        'language': "zh-TW",
    });
    $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);

    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        // console.log(button);
        // console.log(button.data("id"));

        var index = button.data("index");
        var data = $("#cFiles").data("JSGrid").data[index];

        var url = button.data("url");
        $("#modal2-link").html(' <a target="_blank" href="' + data.url + '">' + data.name + ' (開新視窗)</a>');

        $("#file_iframe").attr("src", url);
    });
    $('#myModalFile').on('hide.bs.modal', function (event) {
        $("#file_iframe").attr("src", "");
    });
    $.fn.editable.defaults.mode = 'inline';
    $("#unedit-form-btn").hide();

    $("#uid").text(this.data.uid);
    $("#notice").text(this.data.notice);
    $("#notice").attr('style', 'color:red');
    $("#title_text").text(this.data.title_text);
    $("#name_cht").text(this.data.name_cht);
    $("#name_eng").text(this.data.name_eng);
    $("#identify").text(this.data.identify);
    $("#passport").text(this.data.passport);
    $("#email").text(this.data.email);
    $("#cellnum").text(this.data.cellnum);
    $("#email2").text(this.data.email2);
    $("#addr1_phone").text(this.data.addr1_phone);
    $("#sexual_text").text(this.data.sexual_text);
    // $("#addr_eng").text(this.data.addr_eng);
    $("#addr2_post5").text(this.data.addr2_post5);
    $("#addr2_cht").text(this.data.addr2_cht);
    $("#addr2_eng").text(this.data.addr2_eng);
    $("#addr1_post5").text(this.data.addr1_post5);
    $("#addr1_cht").text(this.data.addr1_cht);
    $("#addr1_eng").text(this.data.addr_eng || this.data.addr1_eng);
    $("#birthday").text(this.data.birthday);
    $("#country2_text").text(this.data.country2_text);
    $("#country_text").text(this.data.country_text);
    $("#marriage").text(this.data.marriage);

    $("#fin_com_name").text(this.data.fin_com_name);
    $("#fin_com_name_eng").text(this.data.fin_com_name_eng);
    $("#fin_invest_exp").text(this.data.fin_invest_exp);
    $("#fin_com_type").text(this.data.fin_com_type);
    $("#fin_invest_purpose").text(this.data.fin_invest_purpose);
    $("#fin_title").text(this.data.fin_title);
    $("#fin_ps").text(this.data.fin_ps);
    $("#fin_com_tele").text(this.data.fin_com_tele);
    $("#fin_com_addrcht").text(this.data.fin_com_addrcht);
    $("#fin_com_addreng").text(this.data.fin_com_addreng);
    $("#fin_com_fax").text(this.data.fin_com_fax);

    $("#account_num").text(this.data.account_num);
    $("#policyxx_num").text(this.data.policy_num);

    $("#agent_id").text(this.data.agent_text);
    $("#creditcard_bank").text(this.data.creditcard_bank);
    $("#creditcard_bank_country").text(this.data.creditcard_bank_country);
    $("#creditcard_num").text(this.data.creditcard_num);
    $("#creditcard_term_month").text(this.data.creditcard_term_month);
    $("#creditcard_term_year").text(this.data.creditcard_term_year);
    $("#existing_defined_contribution").text(this.data.existing_defined_contribution);
    $("#existing_defined_contribution_words").text(this.data.existing_defined_contribution_words);
    $("#policy_currency").text(this.data.policy_currency);
    $("#last_name").text(this.data.last_name);
    $("#first_name").text(this.data.first_name);
    $("#hsbc_account_num").text(this.data.hsbc_account_num);

    if (data.periodmail2 && data.periodmail2 == "1") { $("#periodmail2").text("Y"); } else { $("#periodmail2").text("N"); }
    if (data.periodmail3 && data.periodmail3 == "1") { $("#periodmail3").text("Y"); } else { $("#periodmail3").text("N"); }
    if (data.periodmail4 && data.periodmail4 == "1") { $("#periodmail4").text("Y"); } else { $("#periodmail4").text("N"); }
    if (data.periodmail5 && data.periodmail5 == "1") { $("#periodmail5").text("Y"); } else { $("#periodmail5").text("N"); }
    if (data.periodmail6 && data.periodmail6 == "1") { $("#periodmail6").text("Y"); } else { $("#periodmail6").text("N"); }
    if (data.periodmail7 && data.periodmail7 == "1") { $("#periodmail7").text("Y"); } else { $("#periodmail7").text("N"); }
    if (data.periodmail8 && data.periodmail8 == "1") { $("#periodmail8").text("Y"); } else { $("#periodmail8").text("N"); }


    /* this.autorun(function(){
         var data = Blaze.getData();
         // var s = Session.get("portfolio");
         Session.set("client", data);
         // console.log(data);
 
         $("#cFiles").jsGrid("loadData");
     }); */
};

Template.client.helpers({
    // homepics: objHomePics
    portfolios: Portfolios.find(),
    Product1: Product1.find(),
    "files": function () {
        return S3.collection.find();
    }
    // client: Clients.findOne().uid,
});

Template.client.events({
    'click .btn-showlogin': function () {
        // Session.set('counter', Session.get('counter') + 1);
    },
    'click .my-btn-submit1': function () { // 新增投資項目
        if (!$("#eproduct4").val()) {
            alert("請選擇 產品類別4 再新增");
            return;
        }
        var formVar = $(".form-modal1").serializeObject();
        formVar.client_id = this._id;
        // console.log(formVar);
        $('#myModal1').modal("hide");
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();


        $.ajax({
            url: '/api/portfolio',
            type: 'POST',
            data: formVar,
            success: function (response) {
                Router.go("/portfolio/" + response.data._id);
            }
        });
    },
    'click #del-form-btn': function () {
        if (!confirm("確認要刪除本筆嗎?")) {
            return;
        }
        var now_id = this._id;
        $.ajax({
            url: '/api/clients',
            type: 'DELETE',
            data: {
                _id: now_id
            },
            success: function (response) {
                Router.go("/clientslist");
            }
        });
    },
    'click #newclient-form-btn': function () {
        if (!confirm("確認要新增客戶嗎?")) {
            return;
        }
        $.ajax({
            url: '/api/clients',
            type: 'POST',
            data: {
                user_name: $('#user_name').val()
            },
            success: function (response) {
                Router.go("/client/" + response.data._id);
            }
        });
    },
    'click #edit-form-btn': function () {
        $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", true);

        var now_id = this._id;
        //
        $(".my-xedit-text").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xeditclient',
                // title: 'Enter username'
            });
        });
        $(".my-xedit-date").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xeditclient',
                format: 'YYYY/MM/DD',
                viewformat: 'YYYY/MM/DD',
                template: 'YYYY / MMMM / D',
                combodate: {
                    minYear: 1900,
                    maxYear: 2020,
                    minuteStep: 1
                }
            });
        });
        $(".my-xedit-sel").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'select',
                name: domId,
                pk: now_id,
                url: '/api/xeditclient',
                // source: ,
                source: function () {
                    var objname = $(this).attr("data-obj");
                    if (objname == "objBodyCheckType1") return objBodyCheckType1;
                    else if (objname == "objAgents") return $.map(Agents.find({}).fetch(), function (item) { return { value: item._id, text: item.name }; });
                    else if (objname == "objYN") return objYN;
                },
                value: $(this).text()
            });
        });

        $(".on-editing").show();
        $("#unedit-form-btn").show();
        $("#edit-form-btn").hide();


        $('.editable').on('hidden', function (e, reason) {
            // console.log("aaa " + reason);
            if (reason === 'save' || reason === 'nochange') {
                var $next = "";
                if ($(this).next('.editable').length) { // 同td的下一個
                    $next = $(this).next('.editable');
                }
                else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
                    $next = $(this).parent().nextAll().children(".editable").eq(0);
                }
                else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
                    $next = $(this).closest('tr').next().find('.editable').eq(0);
                }

                if ($next) {
                    setTimeout(function () {
                        $next.editable('show');
                    }, 300);
                }
            }
        });
    },
    'click #unedit-form-btn': function () {
        // Session.set('counter', Session.get('counter') + 1);
        $('.my-xedit').editable('destroy');
        $(".on-editing").hide();
        $("#unedit-form-btn").hide();
        $("#edit-form-btn").show();
        $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);
    },
    'change #client-portfolio-id': function (event) {
        // Session.set('counter', Session.get('counter') + 1);
        var port_id = $(event.target).val();
        // console.log(port_id);
        Router.go("/portfolio/" + port_id);
    },
    "change select": function (event, template) {
        // console.log(event.currentTarget);
        // console.log(event.target);
        // console.log(template);
        if ($(event.target).hasClass("product")) {
            // $("#eproduct2").attr("disabled", true);
            // $("#eproduct3").attr("disabled", true);
            // $("#eproduct4").attr("disabled", true);

            var nowid = $(event.target).attr("id");
            if (nowid == "eproduct1") {
                $("#eproduct2").removeAttr("disabled");
                $("#eproduct3").attr("disabled", true);
                $("#eproduct4").attr("disabled", true);

                $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#eproduct2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                var fieldtype = $('#eproduct1 :selected').val();

                var type = Product2.find({
                    product1_id: fieldtype.toString()
                }, { sort: { product2_id: 1 } }).fetch();

                $.each(type, function (i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.name_cht
                    }));
                });
            } else if (nowid == "eproduct2") {
                $("#eproduct3").removeAttr("disabled");
                $("#eproduct4").attr("disabled", true);

                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 無 --</option>');

                var fieldtype = $('#eproduct2 :selected').val();

                var type = Product3.find({
                    product2_id: fieldtype.toString()
                }, { sort: { product3_id: 1 } }).fetch();

                if (!type.length) {
                    $("#eproduct3").attr("disabled", true);
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else {
                    $("#eproduct3").removeAttr("disabled");
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht
                        }));
                    });
                }
            } else if (nowid == "eproduct3") {
                var fieldtype = $('#eproduct3 :selected').val();

                var type = Product4.find({
                    product3_id: fieldtype.toString()
                }).fetch();

                if (!type.length) {
                    $("#eproduct4").attr("disabled", true);
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else {
                    $("#eproduct4").removeAttr("disabled");
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht
                        }));
                    });
                }
            }
            $("#portfolio").jsGrid("loadData");
        }
    },
    // "change .myFileInput": function(event, template) {
    //     // console.log("change .myFileInput");
    //     var now_id = this._id;
    //
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 var formVar = {
    //                     // oproduct4_id: item._id,
    //                     client: "1", // 1:尚未送出 2: 已送出
    //                     client_id: now_id,
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 };
    //                 // console.log(formVar);
    //
    //                 var pic_id = Uploads.insert(formVar);
    //                 Meteor.setTimeout(function(){$("#cFiles").jsGrid("loadData");}, 500);
    //             }
    //         });
    //     });
    // },
    "click a.upload": function () {
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
            files: files,
            path: "subfolder"
        }, function (e, r) {
            console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                client: "1", // 1:尚未送出 2: 已送出
                client_id: now_id,
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    }
});
