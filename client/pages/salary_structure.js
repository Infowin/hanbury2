Template.salary_structure.rendered = function(){
    Template.semicolon.loadjs();
    var data = this.data;
    $("#title").text(data.value);
    Session.set("bankacc_bg", data);

    $("#e_salary_structure").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        // editing: false,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.year = $("#year_id").val();
                filter.month = $("#month_id").val();
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                return jsgridAjax(filter, "salary_structure", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "salary_structure", "POST");
            },
            updateItem: function(item) {
                // console.log(item);
                if(item.is_dayoff == "1"){
                    return "";
                }
                return jsgridAjax(item, "salary_structure", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "salary_structure", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:30,
                itemTemplate: function(value, item) {
                    // console.log(item);
                    if(item.is_dayoff == "1"){
                        return "";
                    }

                    var $result = $([]);

                    if(this.editButton) {
                        $result = $result.add(this._createEditButton(item));
                    }

                    if(this.deleteButton) {
                        $result = $result.add(this._createDeleteButton(item));
                    }

                    return $result;
                },
                editTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return "";
                    }
                    return this._createUpdateButton().add(this._createCancelEditButton());
                },
            },
          { name: "", title: "員工編號", width: 50, editing: false, align: "center" },
            { name: "chtname", title: "員工姓名", width:50, editing: false, align: "center"},
            { name: "base_salary", title: "基本薪資", type: "text", width:40, align: "center",
                itemTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.base_salary;
                    }
                        return commaSeparateNumber(item.base_salary);
                },
                editTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.base_salary;
                    }
                    // return "";
                    // return this._createUpdateButton().add(this._createCancelEditButton());
                    //
                    if(!this.editing)
                        return this.itemTemplate.apply(this, arguments);

                    var $result = this.editControl = this._createTextBox();
                    $result.val(value);
                    return $result;
                },
                editValue: function() {
                    if(!this.editControl.val()){
                        return "";
                    }
                    return this.editControl.val();
                },
            },
            { name: "allowance", title: "交通津貼", type: "text", width:40, align: "center",
                itemTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.allowance;
                    }
                        return commaSeparateNumber(item.allowance);
                },
                editTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.allowance;
                    }
                    // return "";
                    // return this._createUpdateButton().add(this._createCancelEditButton());
                    //
                    if(!this.editing)
                        return this.itemTemplate.apply(this, arguments);

                    var $result = this.editControl = this._createTextBox();
                    $result.val(value);
                    return $result;
                },
                editValue: function() {
                    if(!this.editControl.val()){
                        return "";
                    }
                    return this.editControl.val();
                },
            },
          { name: "allowance2", title: "伙食津貼", type: "text", width: 40, align: "center" },
          { name: "allowance3", title: "職務加給", type: "text", width: 40, align: "center"},
          { name: "allowance4", title: "其他", type: "text", width: 40, align: "center" },
            
            { name: "equation", title: "應領合計", editing: false, align: "center",
                // itemTemplate: function(value, item) {
                //     if(!item.base_salary && !item.allowance) return "";
                //     if(!!item.base_salary || !!item.allowance) {
                //         return "(" + Number(item.base_salary) + "+" + Number(item.allowance) + ") ÷ 22 = " + Math.round((Number(item.base_salary) + Number(item.allowance))/22);
                //     }
                // },
            },
            { name: "deductions", title: "[應扣]請假", editing: false, width:40, align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.deductions);
                }
                // itemTemplate: function(value, item) {
                //     if(!item.base_salary && !item.allowance) return "";
                //     if(!!item.base_salary || !!item.allowance) {
                //         return Math.round((Number(item.base_salary) + Number(item.allowance))/22);
                //     }
                // }
            },
          { name: "healthcare_pe", title: "[應扣]健保", editing: false, width: 40, align: "center"},
          { name: "labor_pe", title: "[應扣]勞保", editing: false, width: 40, align: "center" },
          { name: "first_pay", title: "[應扣]代扣稅款", editing: false, width: 40, align: "center" },            
            { name: "total_deductions", title: "應扣合計", width:50, editing: false, align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.total_deductions);
                }
                // itemTemplate: function(value, item) {
                //     if(!item.deductions && !item.first_pay) return "";
                //     if(!!item.deductions || !!item.first_pay) {
                //         return (Number(Math.round((Number(item.base_salary) + Number(item.allowance))/22)) + Number(item.first_pay));
                //     }
                // }
            },
          { name: "real_salary", title: "實發金額", editing: false, width: 40, align: "center" },
          { name: "paybank1", title: "[付款銀行] 中國信託", editing: false, width: 40, align: "center" },
          { name: "paybank2", title: "[付款銀行] 土銀", editing: false, width: 40, align: "center" },
          { name: "ps", title: "備註（扣款帳號）", editing: false, width: 40, align: "center" },
            
            /*{ name: "first_pay", title: "代墊款項", type: "text", width:40, editing: true, align: "center",
                itemTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.first_pay;
                    }
                        return commaSeparateNumber(item.first_pay);
                },
                editTemplate: function(value, item) {
                    if(item.is_dayoff == "1"){
                        return item.first_pay;
                    }
                    // return "";
                    // return this._createUpdateButton().add(this._createCancelEditButton());
                    //
                    if(!this.editing)
                        return this.itemTemplate.apply(this, arguments);

                    var $result = this.editControl = this._createTextBox();
                    $result.val(value);
                    return $result;
                },
                editValue: function() {
                    if(!this.editControl.val()){
                        return "";
                    }
                    return this.editControl.val();
                },
            },*/
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
    });
    $("#e_salary_structure_2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.year = $("#year_id").val();
                filter.month = $("#month_id").val();
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                return jsgridAjax(filter, "salary", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "salary", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "salary", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "salary", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:60, deleteButton:false,
                itemTemplate: function(_, item) {
                if(item.IsTotal)
                    return "";
                return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                }
            },
            { name: "chtname", title: "員工姓名", align: "center"},
            { name: "healthcare_pe", title: "健保<br>個人負擔", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.healthcare_pe);
                }
                // itemTemplate: function(value, item) {
                //     if(!!item.IsTotal)
                //         return "個人+單位健保"+((Number(item.healthcare_pe) || 0) + (Number(item.healthcare_co) || 0));
                //     return (Number(item.healthcare_pe) || 0) + (Number(item.healthcare_co) || 0);
                // }
            },
            { name: "healthcare_co", title: "健保<br>單位負擔", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.healthcare_co);
                }
            },
            { name: "labor_pe", title: "勞保<br>個人負擔", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.labor_pe);
                }
            },
            { name: "labor_co", title: "勞保<br>單位負擔", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.labor_co);
                }
            },
            { name: "labor_retreat", title: "勞退<br>單位負擔", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.labor_retreat);
                }
            },
            { name: "total_pe", title: "個人負擔合計", align: "center",
                itemTemplate: function(value, item) {
                    if(!!item.IsTotal)
                        return "";
                    return commaSeparateNumber((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0));
                }
            },
            { name: "total_co", title: "單位負擔合計", align: "center",
                itemTemplate: function(value, item) {
                    if(!!item.IsTotal)
                        return "";
                    return commaSeparateNumber((Number(item.healthcare_co) || 0) + (Number(item.labor_co) || 0) + (Number(item.labor_retreat) || 0));
                }
            },
            { name: "total_labor_heal", title: "勞健保合計<br>(不含勞退)", align: "center",
                itemTemplate: function(value, item) {
                    return commaSeparateNumber((Number(item.healthcare_pe) || 0) + (Number(item.healthcare_co) || 0) + (Number(item.labor_pe) || 0) + (Number(item.labor_co) || 0));
                }
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
        onRefreshed: function(args) {
            var items = args.grid.option("data");
            var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "labor_retreat": 0, IsTotal: true };

            items.forEach(function(item) {
                total.healthcare_pe += ((Number(item.healthcare_pe)||0) + (Number(item.healthcare_co)||0));
                total.labor_pe += ((Number(item.labor_pe)||0) + (Number(item.labor_co)||0));
                total.labor_retreat += (Number(item.labor_retreat)||0);
            });

            var $totalRow = $("<tr>").addClass("total-row");

            args.grid._renderCells($totalRow, total);

            args.grid._content.append($totalRow);
        },
    });
    $("#e_salary_structure_3").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: false,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.year = $("#year_id").val();
                filter.month = $("#month_id").val();
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                return jsgridAjax(filter, "salary", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "salary", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "salary", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "salary", "DELETE");
            },
        },
        fields: [
            // { type: "control", title: "刪除", width:10, deleteButton:false, editButton:false,
            //     itemTemplate: function(_, item) {
            //     if(item.IsTotal)
            //         return "";
            //     return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
            //     }
            // },
            { name: "chtname", title: "員工姓名", align: "center", width: 60},
            // { name: "total_salary_list", title: "", align: "center",
            //     itemTemplate: function(value, item) {
            //         item.total_salary_list = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
            //         if(!!item.IsTotal)
            //             // return item.total_salary_list;
            //             return "";
            //         return "本薪"+(Number(item.base_salary) || 0)+" + "+"津貼"+(Number(item.allowance) || 0)+" + "+"代墊款項"+(Number(item.first_pay) || 0)+" - "+"累積應扣薪資"+(Number(item.total_deductions) || 0)+" - "+"個人負擔合計"+((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))+" = "+item.total_salary_list;
            //         // return "本薪"+(Number(item.base_salary) || 0)+" + "+"津貼"+(Number(item.allowance) || 0)+" + "+"代墊款項"+(Number(item.first_pay) || 0)+" - "+"累積應扣薪資"+(Number(item.total_deductions) || 0)+" - "+"個人負擔合計"+((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0));
            //     }
            // },
            { name: "total_base_salary", title: "本薪", align: "center",
                itemTemplate: function(value, item) {
                    item.total_base_salary = (Number(item.base_salary) || 0)
                    return (commaSeparateNumber(item.base_salary) || 0)
                }
            },
            { name: "total_allowance", title: "津貼", align: "center",
                itemTemplate: function(value, item) {
                    item.total_allowance = (Number(item.allowance) || 0)
                    return (commaSeparateNumber(item.allowance) || 0)
                }
            },
            { name: "total_first_pay", title: "代墊款項", align: "center",
                itemTemplate: function(value, item) {
                    item.total_first_pay = (Number(item.first_pay) || 0)
                    return (commaSeparateNumber(item.first_pay) || 0)
                }
            },
            { name: "total_total_deductions", title: "累積應扣薪資", align: "center",
                itemTemplate: function(value, item) {
                    item.total_total_deductions = (Number(item.total_deductions) || 0)
                    return (commaSeparateNumber(item.total_deductions) || 0)
                }
            },
            { name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
                itemTemplate: function(value, item) {
                    item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
                    return (commaSeparateNumber(item.total_pe) || 0)
                }
            },
            { name: "total_salary", title: "薪資總額", align: "center", width: 60,
                itemTemplate: function(value, item) {
                    item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
                    // if(!!item.IsTotal)
                    //     return item.total_salary;
                    return commaSeparateNumber(item.total_salary);
                }
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_salary_structure_").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        },
        onRefreshed: function(args) {
            var items = args.grid.option("data");
            var total = { chtname: "Total","healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

            items.forEach(function(item) {
                total.healthcare_pe += (Number(item.healthcare_pe)||0);
                total.labor_pe += (Number(item.labor_pe)||0);
                total.base_salary += (Number(item.base_salary)||0);
                total.allowance += (Number(item.allowance)||0);
                total.first_pay += (Number(item.first_pay)||0);
                total.total_deductions += (Number(item.total_deductions)||0);
            });

            var $totalRow = $("<tr>").addClass("total-row");

            args.grid._renderCells($totalRow, total);

            args.grid._content.append($totalRow);
        },
    });

    var d = new Date();
    $("#year_id").val(d.getFullYear());
    $(".sel_filter").trigger("change");

    $('#myModal2').on('show.bs.modal', function (event) {
        var option = $(event.relatedTarget) // Button that triggered the modal
        var bg = option.data("bg");
        $("#bg_id").val(bg);
        $(".check").prop('checked',false);
    });
    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));
    });

    // $('#csv_1').click(function(){
    //     var data = JSON.stringify($("#e_salary_structure").jsGrid("option", "data"));
    //     if(data == '')
    //         return;
    //     JSONToCSVConvertor(data, "Vehicle Report", true);
    // });

    $("#csv_1").on('click', function (event) {
        var csv_month = $("#month_id").val() + "月"
        var csv_year = $("#year_id").val() + "年"
        var args = [$("#e_salary_structure"), ''+data.value + csv_year + csv_month+'薪資設定.csv'];
        exportTableToCSV.apply(this, args);
    });
    $("#csv_2").on('click', function (event) {
        var csv_month = $("#month_id").val() + "月 "
        var csv_year = $("#year_id").val() + "年"
        var args = [$("#e_salary_structure_2"), ''+data.value + csv_year + csv_month+'勞健保.csv'];
        exportTableToCSV.apply(this, args);
    });
    $("#csv_3").on('click', function (event) {
        var csv_month = $("#month_id").val() + "月"
        var csv_year = $("#year_id").val() + "年"
        var args = [$("#e_salary_structure_3"), ''+data.value + csv_year + csv_month+'發放薪資總額.csv'];
        exportTableToCSV.apply(this, args);
    });
};

Template.salary_structure.helpers({
    acc_year: Accountyear.find({}, {sort:{value:1}}),
    get_bg: function(){
        var r = Router.current().params;
        return r.f1_id;
    },
    get_user: function(){
      var arr = Meteor.users.find(
        {$and: [
            {$or: [{'auth.is_auth':'1'},{'auth.is_auth':{ $exists : false }}]},
            {'profile.department_id':Router.current().params.f1_id}
        ]}).fetch();
      var resArr = [];
      for (var i = 0; i < arr.length; i++) {
        var obj = {};
        obj._id = arr[i]._id;
        obj.engname = arr[i].profile.engname;
        obj.chtname = arr[i].profile.chtname;
        resArr.push(obj);
      }

      return resArr;
    },
});

Template.salary_structure.events({
    'click .btn-m2-new': function() {
        var formVar = $(".form-modal2").serializeObject();
        formVar.year = $("#year_id").val();
        formVar.month = $("#month_id").val();
        formVar.bg_id = $("#bg_id").val();
        Meteor.call("addSalaryMember", formVar, function(error, result){
            if(error){
                console.log("error from updateSortUploads: ", error);
            }
            else{
                $("#e_salary_structure").jsGrid("loadData");
                $("#e_salary_structure_2").jsGrid("loadData");
                $("#e_salary_structure_3").jsGrid("loadData");
            }
        });
    },
    "change #type_id": function(event, template) {
        if ($(event.target).hasClass("type_id")) {
            $("#e_salary_structure").jsGrid("loadData");
            $("#e_salary_structure_2").jsGrid("loadData");
            $("#e_salary_structure_3").jsGrid("loadData");
        }
    },
    "change .sel_filter": function(event, template) {
        $("#e_salary_structure").jsGrid("loadData");
        $("#e_salary_structure_2").jsGrid("loadData");
        $("#e_salary_structure_3").jsGrid("loadData");
    },
});