clickHereToPrint = function () {
  try {
    var oIframe = document.getElementById('ifrmPrint');
    var oContent = document.getElementById('printarea').innerHTML;
    var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
    if (oDoc.document) oDoc = oDoc.document;
    oDoc.write("<head><title>title</title>");
    oDoc.write("</head><body onload='this.focus(); this.print();'>");
    oDoc.write(oContent + "</body>");
    oDoc.close();
  } catch (e) {
    self.print();
  }
}
Template.portfolio.PrintDiv = function () {
  var contents = document.getElementById("printarea").innerHTML;
  var frame1 = document.createElement('iframe');
  frame1.name = "frame1";
  frame1.style.position = "absolute";
  frame1.style.top = "-1000000px";
  document.body.appendChild(frame1);
  var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
  frameDoc.document.open();
  frameDoc.document.write('<html><head><title>投資組合表單</title>');
  frameDoc.document.write('</head><body>');
  frameDoc.document.write(contents);
  frameDoc.document.write('</body></html>');
  frameDoc.document.close();
  setTimeout(function () {
    window.frames["frame1"].focus();
    window.frames["frame1"].print();
    document.body.removeChild(frame1);
  }, 500);
  return false;
}
// Create a jquery plugin that prints the given element.
jQuery.fn.print = function () {
  // NOTE: We are trimming the jQuery collection down to the
  // first element in the collection.
  if (this.size() > 1) {
    this.eq(0).print();
    return;
  } else if (!this.size()) {
    return;
  }

  // ASSERT: At this point, we know that the current jQuery
  // collection (as defined by THIS), contains only one
  // printable element.

  // Create a random name for the print frame.
  var strFrameName = ("printer-" + (new Date()).getTime());

  // Create an iFrame with the new name.
  var jFrame = $("<iframe name='" + strFrameName + "'>");

  // Hide the frame (sort of) and attach to the body.
  jFrame
    .css("width", "1px")
    .css("height", "1px")
    .css("position", "absolute")
    .css("left", "-9999px")
    .appendTo($("body:first"));

  // Get a FRAMES reference to the new frame.
  var objFrame = window.frames[strFrameName];

  // Get a reference to the DOM in the new frame.
  var objDoc = objFrame.document;

  // Grab all the style tags and copy to the new
  // document so that we capture look and feel of
  // the current document.

  // Create a temp document DIV to hold the style tags.
  // This is the only way I could find to get the style
  // tags into IE.
  var jStyleDiv = $("<div>").append(
    $("style").clone()
  );

  // Write the HTML for the document. In this, we will
  // write out the HTML of the current element.
  objDoc.open();
  objDoc.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
  objDoc.write("<html>");
  objDoc.write("<body>");
  objDoc.write("<head>");
  objDoc.write("<title>");
  objDoc.write(document.title);
  objDoc.write("</title>");
  objDoc.write(jStyleDiv.html());
  objDoc.write("</head>");
  objDoc.write(this.html());
  objDoc.write("</body>");
  objDoc.write("</html>");
  objDoc.close();

  // Print the document.
  objFrame.focus();
  objFrame.print();

  // Have the frame remove itself in about a minute so that
  // we don't build up too many of these frames.
  setTimeout(
    function () {
      jFrame.remove();
    },
    (60 * 1000)
  );
}
Template.portfolio.editForm = function (now_id) {
  $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", true);
  $(".my-xedit-sel-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).attr("data-order");
    var field = $(this).parent().parent().attr("data-id");
    // var field = $(this).attr("data-id");
    // console.log(field);
    $(this).editable({
      type: 'select',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
      // source: ,
      source: function () {
        var objname = $(this).attr("data-obj");
        if (objname == "objYearNum") return objYearNum;
        else if (objname == "objPaymentFreqYear") return objPaymentFreqYear;
        else if (objname == "objHalfYear") return objHalfYear;
      },
      value: $(this).text()
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-text-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var field = $(this).parent().parent().attr("data-id");
    console.log(order_id);
    console.log(field);
    $(this).editable({
      type: 'text',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-text-arr2").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var order_id2 = $(this).parent().parent().attr("data-order2");
    var field = $(this).parent().parent().attr("data-id");
    // console.log(order_id);s
    // console.log(field);
    $(this).editable({
      type: 'text',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr2',
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id,
        "order_id2": order_id2
      })
    });
  });
  $(".my-xedit-number-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var field = $(this).parent().parent().attr("data-id");
    // console.log(order_id);
    // console.log(field);
    $(this).editable({
      type: 'text',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
      display: function (value, response) {
        $(this).text(commaSeparateNumber(value));
      }
    }).on('shown', function (e, editable) {
      editable.input.$input.val(commaSeparateNumber(editable.value));
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-year-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var field = $(this).parent().parent().attr("data-id");
    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
      format: 'YYYY/MM/DD',
      viewformat: 'YYYY/MM/DD',
      template: 'YYYY / MMMM / D',
      combodate: {
        yearDescending: false,
        minYear: 2000,
        maxYear: 2050,
        minuteStep: 1
      }
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-date-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var field = $(this).parent().parent().attr("data-id");

    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
      format: 'YYYY/MM/DD',
      viewformat: 'YYYY/MM/DD',
      template: 'YYYY / MMMM / D',
      combodate: {
        yearDescending: false,
        minYear: 2000,
        maxYear: 2050,
        minuteStep: 1
      }
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-date-arr2").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var order_id2 = $(this).parent().parent().attr("data-order2");
    var field = $(this).parent().parent().attr("data-id");

    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr2',
      format: 'YYYY/MM/DD',
      viewformat: 'YYYY/MM/DD',
      template: 'YYYY / MMMM / D',
      combodate: {
        yearDescending: false,
        minYear: 2000,
        maxYear: 2050,
        minuteStep: 1
      }
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id,
        "order_id2": order_id2
      })
    });
  });
  $(".my-xedit-ymdate-arr").each(function () {
    var domId = $(this).attr("id");
    var order_id = $(this).parent().parent().attr("data-order");
    var field = $(this).parent().parent().attr("data-id");
    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolioarr',
      format: 'YYYY/MM/DD',
      viewformat: 'YYYY/MM/DD',
      template: 'YYYY / MMMM / DDDD',
      combodate: {
        yearDescending: false,
        minYear: 2000,
        maxYear: 2050,
        minuteStep: 1
      }
    }).on('shown', function (e, editable) {
      editable.option('params', {
        "field": field,
        "order_id": order_id
      })
    });
  });
  $(".my-xedit-text").each(function () {
    var domId = $(this).attr("id");
    $(this).editable({
      type: 'text',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolio',
      // title: 'Enter username'
    });
  });
  $(".my-xedit-number").each(function () {
    var domId = $(this).attr("id");
    $(this).editable({
      type: 'text',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolio',
      display: function (value, response) {
        $(this).text(commaSeparateNumber(value));
      }
    }).on('shown', function (e, editable) {
      editable.input.$input.val(commaSeparateNumber(editable.value));
    });
  });
  $(".my-xedit-date").each(function () {
    var domId = $(this).attr("id");
    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolio',
      format: 'YYYY/MM/DD',
      viewformat: 'YYYY/MM/DD',
      template: 'YYYY / MMMM / D',
      combodate: {
        // yearDescending: false,
        minYear: 1900,
        maxYear: 2050,
        minuteStep: 1
      }
    });
  });
  $(".my-xedit-credit-date").each(function () {
    var domId = $(this).attr("id");
    $(this).editable({
      type: 'combodate',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolio',
      format: 'MM/YYYY',
      viewformat: 'MM/YYYY',
      template: 'MM / YYYY',
      combodate: {
        maxYear: 2050,
        minYear: 2000,
        minuteStep: 1
      }
    });
  });
  $(".my-xedit-sel").each(function () {
    var domId = $(this).attr("id");
    $(this).editable({
      type: 'select',
      name: domId,
      pk: now_id,
      url: '/api/xeditportfolio',
      // source: ,
      source: function () {
        var objname = $(this).attr("data-obj");
        if (objname == "objBodyCheckType1") return objBodyCheckType1;
        else if (objname == "objBodyCheckType2") return objBodyCheckType2;
        else if (objname == "objNowPhase") return objNowPhase;
        else if (objname == "objNowStatus") return objNowStatus;
        else if (objname == "objYN") return objYN;
        else if (objname == "objIsRevoke") return objIsRevoke;
        else if (objname == "Provider234") return $.map(Provider.find({
          $or: [{
            provider_oid: "2"
          }, {
            provider_oid: "3"
          }, {
            provider_oid: "4"
          }]
        }).fetch(), function (item) {
          return {
            value: item._id,
            text: item.name_cht
          };
        });
        else if (objname == "Provider") return $.map(Provider.find({}).fetch(), function (item) {
          return {
            value: item._id,
            text: item.name_cht
          };
        });
        else if (objname == "Interestcondition") return $.map(Interestcondition.find({}, {
          sort: {
            order_id: 1
          }
        }).fetch(), function (item) {
          return {
            value: item._id,
            text: item.interestcondition
          };
        });
        else if (objname == "objPruPayMethod") return objPruPayMethod;
        else if (objname == "objStockItem") return objStockItem;
        else if (objname == "objInsuranceStatus") return objInsuranceStatus;
        else if (objname == "objInsurancePayStatus") return objInsurancePayStatus;
        else if (objname == "objPayMethodFPI") return objPayMethodFPI;
        else if (objname == "objPayPeriodFPI") return objPayPeriodFPI;
        else if (objname == "objPayYearNum") return objPayYearNum;
        else if (objname == "objPayMonthNum") return objPayMonthNum;
        else if (objname == "objUnitType") return objUnitType;
        else if (objname == "objSalaryCash") return objSalaryCash;
        else if (objname == "objPaymentFreqYear") return objPaymentFreqYear;
        else if (objname == "objInvestType") return objInvestType;
        else if (objname == "objCompanyType") return objCompanyType;
        else if (objname == "objLockMonth") return objLockMonth;
        else if (objname == "objYearNum") return objYearNum;
        else if (objname == "objStatusP5") return objStatusP5;
        else if (objname == "objP5Dollar") return objP5Dollar;
        else if (objname == "objP5Country") return objP5Country;
        else if (objname == "objSeason") return objSeason;
      },
      value: $(this).text(),
      success: function (response, newValue) {
        // if(!response.success) return response.msg;
        if (response.data.name == "pay_year_num") {
          console.log(response.data);
          Template.portfolio.changePeriodNum(response.data.pk, response.data.value, 1);
        }
      }
    });
  });

  if (Session.get('editing-portfolio')) {
    $(".on-editing").show();
  }


  $('.editable').on('hidden', function (e, reason) {
    // console.log("aaa " + reason);
    if (reason === 'save' || reason === 'nochange') {
      var $next = "";
      if ($(this).next('.editable').length) { // 同td的下一個
        $next = $(this).next('.editable');
      }
      else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
        $next = $(this).parent().nextAll().children(".editable").eq(0);
      }
      else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
        $next = $(this).closest('tr').next().find('.editable').eq(0);
      }

      if ($next) {
        setTimeout(function () {
          $next.editable('show');
        }, 300);
      }
    }
  });

};
Template.portfolio.changePeriodNum = function (id, years, per_year) {
  Meteor.call("changePeriodNumArr", id, "arrPaymentRecord", years, per_year, function () {
    Template.portfolio.loadForm();
    if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
      // console.log("click");
      // $("#unedit-form-btn").trigger("click");
      // $("#edit-form-btn").trigger("click");
      Template.portfolio.editForm(id);
    }
  });
};
Template.portfolio.loadForm = function () {
  // var data = Blaze.getData();
  var data = Portfolios.findOne();
  // Session.set("portfolio", data);

  console.log("Template.portfolio.loadForm");

  if (data.template_id == "1" || data.template_id == "2") {

    var arrData = data.arrPaymentRecord;
    // console.log(arrData);
    $("#arrPaymentRecord").empty();

    if (typeof arrData == "object")
      for (var i = 0; i < arrData.length; i++) {
        var entry = arrData[i];

        var status_text = "未繳";

        if (!entry.pmr_payment2money) {
          status_text = "未繳";
        } else if (!!entry.pmr_payment2money) {
          if (commaToNumber(entry.pmr_payment2money) == "0") {
            status_text = "展延";
          } else if (commaToNumber(entry.pmr_payment2money) < commaToNumber(entry.pmr_payment1money)) {
            status_text = "部分繳";
          } else if (commaToNumber(entry.pmr_payment2money) == commaToNumber(entry.pmr_payment1money)) {
            status_text = "已繳";
          } else if (commaToNumber(entry.pmr_payment2money) > commaToNumber(entry.pmr_payment1money)) {
            status_text = "溢繳";
          }
        }

        var dom = $('\
            <tr id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrPaymentRecord">\
                <td>第' + entry.order_id + '年&nbsp;</td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-ymdate-arr" id="pmr_payment1date">' + (entry.pmr_payment1date || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="pmr_payment1money">' + commaSeparateNumber(entry.pmr_payment1money || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="pmr_payment2date">' + (entry.pmr_payment2date || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="pmr_payment2money">' + commaSeparateNumber(entry.pmr_payment2money || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="pmr_paymethod">' + (entry.pmr_paymethod || "") + '</span></td>\
                <td><span data-mode="popup" class="" id="pmr_paystatus">' + (status_text || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="pmr_ps">' + (entry.pmr_ps || "") + '</span></td>\
            </tr>');
        $("#arrPaymentRecord").append(dom);
      }
  } else if (data.template_id == "3" || data.template_id == "4" || data.template_id == "5" || data.template_id == "7") {
    // 配息記錄
    $("#arrGiveInterestRecord").empty();

    //利率
    function calInterest(day, principal, rate) {
      if (!principal || principal == "")
        return "0";

      var interest = 0;

      // principal = principal.replace(/\,/g, '');
      interest = commaToNumber(principal) * Number(rate) / 100 / 365 * Number(day);
      interest = interest.toFixed(2);

      // if(typeof interest != "number"){
      //     return "0";
      // }

      return interest.toString();
    }
    // 6/30 or 12/31
    function next_end_date(d) {
      var r = new Date();
      if (d.getMonth() < 6) {
        r.setFullYear(d.getFullYear(), 5, 30);
      } else {
        r.setFullYear(d.getFullYear(), 11, 31);
      }
      return r;
    }
    // 7/1 or 1/1
    function next_start_date(d) {
      var r = new Date();
      if (d.getMonth() < 6) {
        r.setFullYear(d.getFullYear(), 6, 1);
      } else {
        r.setFullYear(d.getFullYear() + 1, 0, 1);
      }
      return r;
    }
    // 下個月一號
    function next_helfyearly_start_date(d) {
      var r = new Date();
      r.setFullYear(d.getFullYear(), d.getMonth() + 1, 1)
      return r;
    }
    // 下一年同天
    function next_yearly_start_date(d) {
      var r = new Date();
      r.setFullYear(d.getFullYear(), d.getMonth() + 12, d.getDate())
      return r;
    }

    function getDatePeriod(d) {
      if (d.getMonth() < 6) {
        return "上半年";
      } else {
        return "下半年";
      }
      return "";
    }
    // console.log("asd");

    // start
    var arr_payment = data.arrPaymentDateMoney;
    var ic = Interestcondition.findOne({
      _id: data.interest_condition
    });


    $("#arrGiveInterestRecord-footer").html("");
    if (!ic) {
      // console.log("請選擇利率條件");
      $("#arrGiveInterestRecord-footer").html('請先選擇<a href="#ic-link">利率條件</a>');
    } else if (ic.interest_period == "0") { //不固定
      var arrData = data.arrGiveInterestRecord;
      console.log(arrData);

      if (typeof arrData == "object")
        // if (!!arrData)
        for (var i = 0; i < arrData.length; i++) {
          var entry = arrData[i];

          var start_date = new Date(arrData[i].gir_start_date);
          var end_date = new Date(arrData[i].gir_end_date);

          var timeDiff = Math.abs(start_date.getTime() - end_date.getTime());
          // var gir_diff_day = Math.ceil(timeDiff / (1000 * 3600 * 24))+1;
          var gir_diff_day = Math.ceil(timeDiff / (1000 * 3600 * 24));
          // arrData[i].gir_interest = calInterest(arrData[i].gir_diff_day, arrData[i].gir_principal, arrData[i].gir_interest_rate);
          var gir_interest = calInterest(arrData[i].gir_diff_day, arrData[i].gir_principal, arrData[i].gir_interest_rate) || "0";

          var dom = $('\
                <tr id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrGiveInterestRecord">\
                    <td><a href="#" class="del-arrId-btn on-editing">[x]</a>&nbsp;&nbsp;<span data-mode="popup" class="my-xedit my-xedit-sel-arr" id="gir_year" data-obj="objYearNum" data-id="objYearNum" data-order="' + entry.order_id + '">' + (entry.gir_year || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-sel-arr" id="gir_halfyear" data-obj="objHalfYear" data-id="objHalfYear" data-order="' + entry.order_id + '">' + (funcObjFind2(objHalfYear, entry.gir_halfyear) || "") + '</span></td>\
                    <td>$<span data-mode="popup" class="my-xedit my-xedit-text-arr" id="gir_principal">' + (commaSeparateNumber(entry.gir_principal) || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="gir_start_date">' + (entry.gir_start_date || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="gir_end_date">' + (entry.gir_end_date || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="gir_interest_rate">' + (entry.gir_interest_rate || "") + '</span> %</td>\
                    <td><span data-mode="popup" id="gir_diff_day">' + (gir_diff_day || "") + '</span></td>\
                    <td>$<span data-mode="popup" id="gir_interest">' + (commaSeparateNumber(gir_interest) || "") + '</span></td>\
                    <td>$<span data-mode="popup" class="my-xedit my-xedit-text-arr" id="gir_amount_dispensed">' + (commaSeparateNumber(entry.gir_amount_dispensed) || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="gir_amount_dispensed_date">' + (entry.gir_amount_dispensed_date || "") + '</span></td>\
                    <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="gir_marks">' + (entry.gir_marks || "") + '</span></td>\
                </tr>');
          $("#arrGiveInterestRecord").append(dom);
        }
    } else { // 固定配息
      var all_total_principal = 0;
      var all_total_diff_day = 0;
      var all_total_interest = 0;
      var all_total_amount_dispensed = 0;

      var all_total_payment = 0;
      if (typeof arr_payment == "object")
        for (var i = 0; i < arr_payment.length; i++) { // 有幾筆投資的資料
          all_total_payment += Number(arr_payment[i].money);
        }
      var arr_retprincipal = data.arrReturnPrincipal; // 本金返還 date, money

      var str_interest = "";

      if (typeof arr_payment == "object")
        for (var i = 0; i < arr_payment.length; i++) { // 有幾筆投資的資料
          var order_id = arr_payment[i].order_id;
          var principal = arr_payment[i].money || 0;
          // console.log(arr_payment[i]);

          var now_date = new Date();
          var start_date = "";
          var end_date = "";
          var principal_show = "";
          var j = 0;

          var total_principal = 0;
          var total_diff_day = 0;
          var total_interest = 0;
          var total_amount_dispensed = 0;

          total_principal += Number(principal);
          do {
            if (!start_date) { // 是第一次進來的時候
              start_date = new Date(arr_payment[i].date);
              end_date = next_end_date(start_date);
              principal_show = "$" + commaSeparateNumber(principal);
            } else {
              start_date = next_start_date(start_date);
              end_date = next_end_date(start_date);
              principal_show = "";
            }

            if (now_date.getTime() < end_date.getTime()) {
              // 現在的日子比之前少的話 就跳掉
              // end date會是之前的日子 如果之前的結束時間 大於現在的時間 就不用再新增下一筆資料
              break;
            }
            // console.log("start date");
            // console.log(start_date);
            // console.log("end date");
            // console.log(end_date);

            var interest = 0;

            // 找配息的利率
            if (!!ic) {
              var i1 = (parseInt(j / 2) + 1).toString();
              if (!!ic['y' + i1]) {
                interest_rate = ic['y' + i1];
                // console.log("got interest_rate "+ interest_rate +" "+i1);
              }
              // console.log("interest_rate: "+ interest_rate);

              var timeDiff = Math.abs(start_date.getTime() - end_date.getTime());
              // var diff_day = Math.ceil(timeDiff / (1000 * 3600 * 24))+1;
              var diff_day = Math.ceil(timeDiff / (1000 * 3600 * 24));
              if (diff_day >= 180 && diff_day <= 185) {
                diff_day = 182.5;
              }

              interest = calInterest(diff_day, principal, interest_rate); //money*interest_rate/100/365*diff_day;
              str_interest = "$" + commaSeparateNumber(interest);
            }
            // console.log("principal: "+ principal);
            // console.log("interest: "+ interest);

            var datePeriod = getDatePeriod(start_date);

            var amount_dispensed = 0; // 當期分派金額
            var amount_dispensed_date = 0; // 配息日期
            var marks = ""; // 備註
            if (!!data.arrGiveInterestRecord && typeof data.arrGiveInterestRecord == "object") {
              for (var k = 0; k < data.arrGiveInterestRecord.length; k++) {
                var tobj = data.arrGiveInterestRecord[k];
                if (Number(tobj.order_id) == Number(order_id) && Number(tobj.order_id2) == Number(j)) {
                  amount_dispensed = tobj.amount_dispensed;
                  amount_dispensed_date = tobj.amount_dispensed_date;
                  marks = tobj.marks;

                  break;
                }
              }
            }
            // 本金返還
            // console.log("start_date: " + start_date);
            if (typeof arr_retprincipal == "object")
              for (var k = 0; k < arr_retprincipal.length; k++) {
                var dd = arr_retprincipal[k].date;
                if (!!dd) {
                  var ddd = new Date(dd);
                  if (ddd >= start_date && ddd < end_date) {
                    // console.log("dd: " + dd);

                    // var ret_principal = commaToNumber(arr_retprincipal[k].money)/arr_payment.length; // 每次的返還金 都平均還給各筆
                    // 本次匯款金額* (本次返還金/總匯款金額)
                    var ret_principal = principal * (commaToNumber(arr_retprincipal[k].money) / all_total_payment);

                    // commaToNumber(arr_retprincipal[k].money)/arr_payment.length; // 每次的返還金 都平均還給各筆
                    marks = marks + dd + "<BR>返: $" + commaSeparateNumber(ret_principal);
                    // if(!!principal_show){
                    //     principal_show = principal_show + "<BR>" + dd + "<BR>返: $"+commaSeparateNumber(ret_principal);
                    // }
                    // else{
                    //     principal_show = dd + "<BR>返: $"+commaSeparateNumber(ret_principal);
                    // }
                    // arr_retprincipal.splice(i, 1);

                    // 如果在這個時候 有本金返還，本金和利息要重算
                    var timeDiff1 = Math.abs(start_date.getTime() - ddd.getTime());
                    var diff_day1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24)) + 1;
                    // var diff_day1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24));
                    var interest1 = calInterest(diff_day1, principal, interest_rate);;

                    // principal = commaToNumber(principal) - commaToNumber(arr_retprincipal[k].money); // 減去已領的
                    principal = commaToNumber(principal) - commaToNumber(ret_principal); // 減去已領的
                    principal_show = "$" + commaSeparateNumber(principal);

                    var timeDiff2 = Math.abs(ddd.getTime() - end_date.getTime());
                    var diff_day2 = Math.ceil(timeDiff2 / (1000 * 3600 * 24)) - 1;
                    var interest2 = calInterest(diff_day2, principal, interest_rate);;

                    // console.log("interest1: " + interest1 + " interest2: " + interest2);
                    interest = parseFloat(interest1) + parseFloat(interest2);
                    str_interest = ("$" + commaSeparateNumber(interest) || "");
                    str_interest = str_interest + "<BR>($" + commaSeparateNumber(interest1) + " + $" + commaSeparateNumber(interest2) + ")";
                    break;
                  }
                }
              }

            total_diff_day += Number(diff_day);
            total_interest += parseFloat(interest);
            total_amount_dispensed += parseFloat(amount_dispensed);
            //<td><span data-mode="popup" id="years">'+(start_date.getFullYear() || "")+'</span></td>\
            var dom = $('\
                    <tr id="wdba-tr' + order_id + '" data-order="' + order_id + '" data-order2="' + j + '"  data-id="arrGiveInterestRecord">\
                        <td><span id="years">第' + (Math.ceil((j + 1) / 2)) + '年</span></td>\
                        <td><span data-mode="popup" id="datePeriod">' + datePeriod + '</span></td>\
                        <td><span data-mode="popup" id="principal">' + (principal_show || "") + '</span></td>\
                        <td><span data-mode="popup" id="start_date">' + (start_date.yyyymmdd() || "") + '</span></td>\
                        <td><span data-mode="popup" id="end_date">' + (end_date.yyyymmdd() || "") + '</span></td>\
                        <td><span data-mode="popup" id="interest_rate">' + (interest_rate || "") + ' %</span></td>\
                        <td><span data-mode="popup" id="diff_day">' + (diff_day || "") + '</span></td>\
                        <td><span data-mode="popup" id="interest">' + str_interest + '</span></td>\
                        <td>$<span data-mode="popup" id="amount_dispensed" class="my-xedit my-xedit-text-arr2">' + (commaSeparateNumber(amount_dispensed) || "0") + '</span></td>\
                        <td>$<span data-mode="popup" id="acc_amount_dispensed" class="">' + (commaSeparateNumber(total_amount_dispensed) || "0") + '</span></td>\
                        <td><span data-mode="popup" id="amount_dispensed_date" class="my-xedit my-xedit-date-arr2">' + (amount_dispensed_date || "") + '</span></td>\
                        <td><span data-mode="popup" id="marks" class="my-xedit my-xedit-text-arr2">' + (marks || "") + '</span></td>\
                    </tr>');
            $("#arrGiveInterestRecord").append(dom);
            j++;
          } while (now_date.getTime() > end_date.getTime());

          // 小計
          var dom = $('\
                <tr id="wdba-tr' + order_id + '" data-order="' + order_id + '" data-order2="' + j + '"  data-id="arrGiveInterestRecord">\
                    <td style="background-color:#a5c2f7"><span id="years">小計</span></td>\
                    <td style="background-color:#a5c2f7"><span id="datePeriod"></span></td>\
                    <td style="background-color:#d0e0fd"><span id="principal">' + ("$" + commaSeparateNumber(principal) || "") + '</span></td>\
                    <td style="background-color:#d0e0fd"><span id="start_date"> - </span></td>\
                    <td style="background-color:#d0e0fd"><span id="end_date"> - </span></td>\
                    <td style="background-color:#d0e0fd"><span id="interest_rate"> - </span></td>\
                    <td style="background-color:#d0e0fd"><span id="diff_day">' + (commaSeparateNumber(total_diff_day) || "") + '</span></td>\
                    <td style="background-color:#d0e0fd"><span id="interest">' + ("$" + commaSeparateNumber(total_interest) || "") + '</span></td>\
                    <td style="background-color:#d0e0fd">$<span id="amount_dispensed" class="">' + (commaSeparateNumber(total_amount_dispensed) || "0") + '</span></td>\
                    <td style="background-color:#d0e0fd">$<span id="acc_amount_dispensed" class="">' + (commaSeparateNumber(total_amount_dispensed) || "0") + '</span></td>\
                    <td style="background-color:#d0e0fd"><span id="amount_dispensed_date" class=""> - </span></td>\
                    <td style="background-color:#d0e0fd"><span id="marks" class=""> - </span></td>\
                </tr>');
          $("#arrGiveInterestRecord").append(dom);

          // all_total_principal += total_principal;
          all_total_principal += commaToNumber(principal);
          all_total_diff_day += total_diff_day;
          all_total_interest += total_interest;
          all_total_amount_dispensed += total_amount_dispensed;
        }
      // commaSeparateNumber(all_total_diff_day)
      var dom = $('\
            <tr id="wdba-tr' + order_id + '" data-order="' + order_id + '" data-order2="' + j + '"  data-id="arrGiveInterestRecord">\
                <td style="background-color:#6f9ef3"><span id="years">總計</span></td>\
                <td style="background-color:#6f9ef3"><span id="datePeriod"></span></td>\
                <td style="background-color:#aec8f7"><span id="principal">' + ("$" + commaSeparateNumber(all_total_principal) || "") + '</span></td>\
                <td style="background-color:#aec8f7"><span id="start_date"> - </span></td>\
                <td style="background-color:#aec8f7"><span id="end_date"> - </span></td>\
                <td style="background-color:#aec8f7"><span id="interest_rate"> - </span></td>\
                <td style="background-color:#aec8f7"><span id="diff_day">' + ("") + '</span></td>\
                <td style="background-color:#aec8f7"><span id="interest">' + ("$" + commaSeparateNumber(all_total_interest) || "") + '</span></td>\
                <td style="background-color:#aec8f7">$<span id="amount_dispensed" class="">' + (commaSeparateNumber(all_total_amount_dispensed) || "0") + '</span></td>\
                <td style="background-color:#aec8f7">$<span id="acc_amount_dispensed" class="">' + (commaSeparateNumber(all_total_amount_dispensed) || "0") + '</span></td>\
                <td style="background-color:#aec8f7"><span id="amount_dispensed_date" class=""> - </span></td>\
                <td style="background-color:#aec8f7"><span id="marks" class=""> - </span></td>\
            </tr>');
      $("#arrGiveInterestRecord").append(dom);

    }
    if (Session.get('editing-portfolio')) {
      $(".on-editing").show();


      $('.editable').on('hidden', function (e, reason) {
        // console.log("aaa " + reason);
        if (reason === 'save' || reason === 'nochange') {
          var $next = "";
          if ($(this).next('.editable').length) { // 同td的下一個
            $next = $(this).next('.editable');
          }
          else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
            $next = $(this).parent().nextAll().children(".editable").eq(0);
          }
          else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
            $next = $(this).closest('tr').next().find('.editable').eq(0);
          }

          if ($next) {
            setTimeout(function () {
              $next.editable('show');
            }, 300);
          }
        }
      });
    }
  } else if (data.template_id == "6" || data.template_id == "8") {
    var arrData = data.arrWithdrawBankAccount;
    // console.log(arrData);
    $("#arrWithdrawBankAccount").empty();

    if (typeof arrData == "object")
      for (var i = 0; i < arrData.length; i++) {
        var entry = arrData[i];
        var dom = $('\
            <tr id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrWithdrawBankAccount">\
                <td><a href="#" class="del-arrId-btn on-editing">[刪除]</a> <span>' + (i + 1) + '</span>&nbsp;</td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_bankname">' + (entry.wdba_bankname || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_bankaddr">' + (entry.wdba_bankaddr || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_holdername">' + (entry.wdba_holdername || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_holdernum">' + (entry.wdba_holdernum || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_swift_bic">' + (entry.wdba_swift_bic || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_abacode">' + (entry.wdba_abacode || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="wdba_ps">' + (entry.wdba_ps || "") + '</span></td>\
            </tr>');
        $("#arrWithdrawBankAccount").append(dom);
      }

    var arrData = data.arrCustomerFollowItems;
    $("#arrCustomerFollowItems").empty();

    if (typeof arrData == "object")
      for (var i = 0; i < arrData.length; i++) {
        var entry = arrData[i];
        var dom = $('\
            <tr id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrCustomerFollowItems">\
                <td><a href="#" class="del-arrId-btn on-editing">[刪除]</a> <span>' + (i + 1) + '</span>&nbsp;</td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="cfi_date">' + (entry.cfi_date || "") + '</span>\</td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="cfi_serviceitem">' + (entry.cfi_serviceitem || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="cfi_status">' + (entry.cfi_status || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="cfi_description">' + (entry.cfi_description || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="cfi_action">' + (entry.cfi_action || "") + '</span></td>\
                <td><span data-mode="popup" class="my-xedit my-xedit-text-arr" id="cfi_actionowner">' + (entry.cfi_actionowner || "") + '</span></td>\
            </tr>');
        $("#arrCustomerFollowItems").append(dom);
      }

    if (Session.get('editing-portfolio')) {
      $(".on-editing").show();


      $('.editable').on('hidden', function (e, reason) {
        // console.log("aaa " + reason);
        if (reason === 'save' || reason === 'nochange') {
          var $next = "";
          if ($(this).next('.editable').length) { // 同td的下一個
            $next = $(this).next('.editable');
          }
          else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
            $next = $(this).parent().nextAll().children(".editable").eq(0);
          }
          else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
            $next = $(this).closest('tr').next().find('.editable').eq(0);
          }

          if ($next) {
            setTimeout(function () {
              $next.editable('show');
            }, 300);
          }
        }
      });
    }
  }
  if (data.template_id == "3" || data.template_id == "4" || data.template_id == "5") {
    var arrData = data.arrReturnPrincipal;
    // console.log("arrReturnPrincipal");
    // console.log(arrData);
    // 本金返還
    $("#arrReturnPrincipal").empty();
    if (typeof arrData == "object")
      for (var i = 0; i < arrData.length; i++) {
        var entry = arrData[i];
        var dom = $('\
            <div id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrReturnPrincipal">\
                <span class="on-editing"><a href="#" class="del-arrId-btn-div">[x]</a>&nbsp;&nbsp;</span>\
                <span><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="date" data-mode="popup">' + (entry.date || "") + '</span>&nbsp;&nbsp;&nbsp;\
                $<span data-mode="popup" class="my-xedit my-xedit-number-arr" id="money" data-mode="popup">' + (commaSeparateNumber(entry.money) || "0") + '</span>\
            </span></div>');
        $("#arrReturnPrincipal").append(dom);
      }

    var arrData = data.arrPaymentDateMoney;
    // console.log(arrData);
    // 匯款日/金額
    $("#arrPaymentDateMoney").empty();
    if (typeof arrData == "object")
      for (var i = 0; i < arrData.length; i++) {
        var entry = arrData[i];
        var dom = $('\
            <div id="wdba-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrPaymentDateMoney">\
                <span class="on-editing"><a href="#" class="del-arrId-btn-div">[x]</a>&nbsp;&nbsp;</span>\
                <span><span data-mode="popup" class="my-xedit my-xedit-date-arr" id="date" data-mode="popup">' + (entry.date || "") + '</span>&nbsp;&nbsp;&nbsp;\
                $<span data-mode="popup" class="my-xedit my-xedit-number-arr" id="money" data-mode="popup">' + (commaSeparateNumber(entry.money) || "0") + '</span>\
            </span></div>');
        $("#arrPaymentDateMoney").append(dom);
      }
    if (Session.get('editing-portfolio')) {
      $(".on-editing").show();


      $('.editable').on('hidden', function (e, reason) {
        // console.log("aaa " + reason);
        if (reason === 'save' || reason === 'nochange') {
          var $next = "";
          if ($(this).next('.editable').length) { // 同td的下一個
            $next = $(this).next('.editable');
          }
          else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
            $next = $(this).parent().nextAll().children(".editable").eq(0);
          }
          else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
            $next = $(this).closest('tr').next().find('.editable').eq(0);
          }

          if ($next) {
            setTimeout(function () {
              $next.editable('show');
            }, 300);
          }
        }
      });
    }
  }
  if (data.template_id == "6") { // 基金配置
    var arrData = data.arrFundConfigure;
    // console.log(arrData);
    /*
    obj.FundDescription        = arr[6];
    obj.UnitType               = arr[7];
    obj.PriceDate              = arr[10];
    obj.BidPrice               = arr[9];
    obj.UnitsHeld              = arr[8];
    obj.FundCurrency           = arr[12];
    obj.FundCurrencyValue      = arr[17];
    obj.ValuationCurrencyValue = arr[1];
     */
    var total_ValuationCurrencyValue = 0;
    var total_A_ValuationCurrencyValue = 0;
    var last_fpi_data_updatedate = "";

    $("#arrFundConfigure").empty();
    if (typeof arrData == "object") {
      // 撈出最新的
      // var arrDataTmp = arrData.sort((a, b) => ('' + b.PriceDate).localeCompare(a.PriceDate));
      var arrDataTmp = arrData.sort((a, b) => (new Date(b.PriceDate || 0).getTime() - new Date(a.PriceDate || 0).getTime()));
      // console.log(arrDataTmp);
      var latestDate = arrDataTmp[0].PriceDate;
      var arrData2 = arrData.filter(item => item.PriceDate === latestDate);
      for (var i = 0; i < arrData2.length; i++) {
        var entry = arrData2[i];
        total_ValuationCurrencyValue = total_ValuationCurrencyValue + Number(entry.ValuationCurrencyValue);
      }

      for (var i = 0; i < arrData2.length; i++) {
        var entry = arrData2[i];
        var dom = $('\
            <tr id="fudc-tr' + entry.order_id + '" data-order="' + entry.order_id + '" data-id="arrFundConfigure">\
                <td><a href="#" class="del-arrId-btn on-editing">[刪除]</a> <span>(' + (i + 1) + ')</span>&nbsp;<span class="" id="FundDescription">' + (entry.FundDescription || "") + '</span></td>\
                <td><span class="" id="UnitType">' + (entry.UnitType || "") + '</span></td>\
                <td><span class="" id="PriceDate">' + (entry.PriceDate || "") + '</span></td>\
                <td><span class="" id="BidPrice">' + (entry.BidPrice || "") + '</span></td>\
                <td><span class="" id="UnitsHeld">' + (commaSeparateNumber(entry.UnitsHeld) || "") + '</span></td>\
                <td><span class="" id="FundCurrency">' + (entry.FundCurrency || "") + '</span></td>\
                <td><span class="" id="FundCurrencyValue">$' + (commaSeparateNumber(entry.FundCurrencyValue) || "") + '</span></td>\
                <td><span class="" id="ValuationCurrencyValue">$' + (commaSeparateNumber(entry.ValuationCurrencyValue) || "") + '</span></td>\
                <td><span class="" id="Percentage">' + (commaSeparateNumber(entry.ValuationCurrencyValue / total_ValuationCurrencyValue * 100)) + "%" + '</span></td>\
            </tr>');

        last_fpi_data_updatedate = entry.ExtractTime;
        // total_ValuationCurrencyValue = total_ValuationCurrencyValue + Number(entry.ValuationCurrencyValue);

        if (entry.UnitType == "A") {
          total_A_ValuationCurrencyValue = total_A_ValuationCurrencyValue + Number(entry.ValuationCurrencyValue);
        }

        $("#arrFundConfigure").append(dom);
      }
    }
    if (!!arrData2 && arrData2.length) {
      $("#total_ValuationCurrencyValue").text("$" + commaSeparateNumber(total_ValuationCurrencyValue));
      $("#fpi_able_withdraw_money").text(commaSeparateNumber(total_A_ValuationCurrencyValue)); // 所有A的總額加起來
      $("#fpi_data_updatedate").text(last_fpi_data_updatedate);

      if (!data.fpi_done_withdraw_money) {
        $("#account_invest_per").html('<span style="color: #cc0000">請先輸入「已供款總額」</span>');
      } else if (!data.fpi_all_withdraw_money) {
        $("#account_invest_per").html('<span style="color: #cc0000">請先輸入「總提款金額」</span>');
      } else {
        var account_invest_per = 0;
        account_invest_per = (total_ValuationCurrencyValue - Number(data.fpi_done_withdraw_money.toString().replace(/,/g, '')) + Number(data.fpi_all_withdraw_money.toString().replace(/,/g, ''))) / Number(data.fpi_done_withdraw_money.toString().replace(/,/g, ''));
        $("#account_invest_per").text(commaSeparateNumber(account_invest_per));
      }
    }

    /*
    if(!total_ValuationCurrencyValue || !data.fpi_done_withdraw_money || !data.fpi_all_withdraw_money || !data.fpi_done_withdraw_money){
        $("#account_invest_per").text("");
    }
    else{
        $("#account_invest_per").text(commaSeparateNumber(account_invest_per));

    }*/
  }

  /*  if(Session.get('editing-portfolio')){
        // console.log('editing-portfolio');
        $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", true);
    }
    else{
        $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);
    }*/
  // console.log("cFiles");
  $("#cFiles").jsGrid("loadData");

  $("#input-3").fileinput({
    'language': "zh-TW",
    'dropZoneEnabled': true
  });
  $('#myModalFile').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    // console.log(button);
    // console.log(button.data("id"));

    var index = button.data("index");
    var data = $("#cFiles").data("JSGrid").data[index];

    var url = button.data("url");
    $("#modal2-link").html(' <a target="_blank" href="' + data.url + '">' + data.name + ' (開新視窗)</a>');

    $("#file_iframe").attr("src", url);
  });
  $('#myModalFile').on('hide.bs.modal', function (event) {
    $("#file_iframe").attr("src", "");
  });
}
Template.portfolio.rendered = function () {
  Template.semicolon.loadjs();

  var data = Blaze.getData();
  // var s = Session.get("portfolio");
  Session.set("portfolio", data);

  this.autorun(function () {
    var data = Blaze.getData();
    // var s = Session.get("portfolio");
    Session.set("portfolio", data);
    // console.log("autorun reload data");
    // console.log(data);
    if (Session.get('editing-portfolio')) { // 編輯中
      // console.log('editing-portfolio');
      // $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", true);
    } else {
      Template.portfolio.loadForm();
      // $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);
    }

    // $("#cFiles").jsGrid("loadData");
    // Template.portfolio.loadForm();
  });
  // $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);
  Session.set('editing-portfolio', 0);
  $(".on-editing").hide();

  var sel = $('#client-portfolio-id')
    .find('option')
    .remove()
    .end()
    // .append('<option value="" disabled selected>請選擇</option>')
    ;

  var now_port_id = this.data._id;
  Meteor.call("getArrClientPortfolios", this.data.client_id, function (error, result) {
    $.each(result, function (i, item) {
      if (!item.account_num) {
        sel.append($('<option>', {
          value: item._id,
          text: item.product3_text,
        }));
      } else {
        sel.append($('<option>', {
          value: item._id,
          text: item.product3_text + " " + item.account_num + " " + funcObjFind2(objInsuranceStatus, item.insurance_status) + " " + funcObjFind2(objNowPhase, item.nowphase),
        }));
      }
    });
    sel.val(now_port_id);
  });

  $.fn.editable.defaults.mode = 'inline';
  $("#unedit-form-btn").hide();


  $("#cFiles").jsGrid({
    // height: "90%",
    width: "100%",
    loadIndication: true,
    editing: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        // var item = Session.get("nowrow");
        // var p4_id = Session.get("nowrow_oproduct4")._id;
        filter.portfolio = "1"; // 1:尚未送出 2: 已送出

        var s = Session.get("portfolio");
        // console.log("cFiles load");

        if (!s) {
          var data = Blaze.getData();
          // var s = Session.get("portfolio");
          Session.set("portfolio", data);
          s = Session.get("portfolio");
        }
        filter.portfolio_id = s._id;
        // console.log("cFile load: "+ s._id);
        // filter.insertedById = Meteor.userId();

        filter.sortField = "order_id";
        filter.sortOrder = "asc";
        return jsgridAjax(filter, "uploads", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "uploads", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "uploads", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "uploads", "DELETE");
      },
    },
    fields: [{
      name: "ctrl_field",
      type: "control",
      width: 40
    }, {
      name: "download",
      width: 100,
      align: "left",
      title: "檔案",
      itemTemplate: function (value, item) {
        return '<a href="#" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
      },
    },
    // { name: "size", title: "檔案大小", type: "text", width:70, align:"right" },
    {
      name: "createdByName",
      width: 70,
      align: "left",
      title: "檔案建立者",
      type: "text"
    },
    {
      name: "insertedByName",
      width: 70,
      align: "left",
      title: "上傳者",
      type: "text",
      editing: false
    }, {
      name: "insertedAt",
      title: "上傳日期",
      width: 70,
      itemTemplate: function (value, item) {
        if (!!item.insertedAt)
          return (new Date(item.insertedAt)).yyyymmddhm();
        else
          return "";
      },
    },
    ],
    onItemDeleting: function (args) {
      // console.log(args);
      var data = args.item;
      S3.delete(data.file.relative_url, function (e, r) {
        if (!!e) console.log(e);
        else {
          // Materialize.toast('資料已刪除 '+ data.name, 3000, 'rounded');
          console.log("資料已刪除" + data.name);
        }
      });
      // $("#cFiles").jsGrid("loadData");
    },
    onRefreshed: function () {
      var $gridData = $("#cFiles .jsgrid-grid-body tbody");
      $gridData.sortable({
        update: function (e, ui) {
          // arrays of items
          var items = $.map($gridData.find("tr"), function (row) {
            return $(row).data("JSGridItem");
          });
          // console.log("Reordered items", items);
          Meteor.call("updateSortUploads", items, function (error, result) {
            if (error) {
              console.log("error from updateSortUploads: ", error);
            } else {
              $("#cFiles").jsGrid("loadData");
            }
          });
        }
      });
    },
  });
  $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);

};

Template.portfolio.helpers({
  // homepics: objHomePics
  getTemplateName: function () {
    var p4 = Product4.findOne({
      _id: this.product4_id
    });
    // console.log("this.product4_id");
    // console.log(this.product4_id);
    var template_id = "0";
    if (p4 && p4.template_id) {
      template_id = p4.template_id;
      // console.log("p4.template_id");
      // console.log(p4.template_id);
      if (p4.name_cht == '洽談中') {
        return '私募基金'
      }
    }

    return objPortfolioTemplate[template_id].value;
  },
  /*    getPolicyNo: function() {
          var p4 = Product4.findOne({ _id: this.product4_id });
          // console.log("this.product4_id");
          // console.log(this.product4_id);
          var template_id = "0";
          if (p4 && p4.template_id) {
              template_id = p4.template_id;
              // console.log("p4.template_id");
              // console.log(p4.template_id);
          }

          return objPortfolioTemplate[template_id].value;
      },
  */
  Product1: Product1.find(),
  portfolios: Portfolios.find(),
  getInsuranceStatus: function () {
    return funcObjFind2(objInsuranceStatus, this.insurance_status);
  },
  getNowPhase: function () {
    console.log(this);
    return funcObjFind2(objNowPhase, this.nowphase);
  },
  getPanel: function () {

    // var controller = Router.current();
    // var por = Portfolio.findOne({_id: controller.params.portfolio_id});

    // console.log(Router.current().params);
    // var get_p4id = Router.current().params.portfolio_id;
    var p4 = Product4.findOne({
      _id: this.product4_id
    });
    // console.log("this.product4_id");
    // console.log(this.product4_id);

    var template_id = "0";
    if (p4 && p4.template_id) {
      template_id = p4.template_id;
      // console.log("p4.template_id");
      // console.log(p4.template_id);
    }
    var result = "portfolio" + template_id;
    return result; //.panelName;
  },
  "files": function () {
    return S3.collection.find();
  }
});

Template.portfolio.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
  'click .addPaymentDateMoney': function () {
    var id = this._id;
    Meteor.call("insertNewPortfolioArr", id, "arrPaymentDateMoney", function () {
      Template.portfolio.loadForm();
      if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
        Template.portfolio.editForm(id);
      }
    });
  },
  'click .addReturnPrincipal': function () {
    var id = this._id;
    Meteor.call("insertNewPortfolioArr", id, "arrReturnPrincipal", function () {
      Template.portfolio.loadForm();
      if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
        Template.portfolio.editForm(id);
      }
    });
  },
  'click .addGiveInterestRecord': function () {
    var id = this._id;
    Meteor.call("insertNewPortfolioArr", id, "arrGiveInterestRecord", function () {
      Template.portfolio.loadForm();
      if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
        Template.portfolio.editForm(id);
      }
    });
  },
  'click .addWithdrawBankAccount': function () {
    var id = this._id;
    Meteor.call("insertNewPortfolioArr", id, "arrWithdrawBankAccount", function () {
      Template.portfolio.loadForm();
      if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
        Template.portfolio.editForm(id);
      }
    });
  },
  'click .addCustomerFollowItems': function () {
    var id = this._id;
    Meteor.call("insertNewPortfolioArr", id, "arrCustomerFollowItems", function () {
      Template.portfolio.loadForm();
      if ($("#edit-form-btn").css('display') == "none") { // 有編輯的時候 才要重整
        Template.portfolio.editForm(id);
      }
    });
  },
  'click .my-btn-submit1': function () { // 新增投資項目
    var formVar = $(".form-modal1").serializeObject();
    formVar.client_id = this.client_id;
    // console.log(formVar);
    $('#myModal1').modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    $.ajax({
      url: '/api/portfolio',
      type: 'POST',
      data: formVar,
      success: function (response) {
        Router.go("/portfolio/" + response.data._id);
      }
    });
  },
  'click #del-form-btn': function () {
    if (!confirm("確認要刪除本筆嗎?")) {
      return;
    }
    var now_id = this._id;
    var client_id = this.client_id;

    $.ajax({
      url: '/api/portfolio',
      type: 'DELETE',
      data: {
        _id: now_id
      },
      success: function (response) {
        Router.go("/client/" + client_id);
      }
    });
  },
  'click .del-arrId-btn': function (event, template) {
    if (!confirm("確認要刪除本筆嗎?")) {
      return;
    }
    // console.log(event.target);
    var now_id = template.data._id;
    var order_id = $(event.target).parent().parent().attr("data-order");
    var field = $(event.target).parent().parent().attr("data-id");
    // console.log(order_id);
    // console.log(field);
    $.ajax({
      url: '/api/xeditportfolioarr?' + $.param({
        "pk": now_id,
        "order_id": order_id,
        "field": field
      }),
      type: 'DELETE',
      success: function (response) {
        // console.log(response);
        var dom = "#" + response.data.field + " tr[data-order='" + response.data.order_id + "']";
        $(dom).nextAll().each(function (index) {
          console.log(index + ": " + $(this).text());
          console.log(this);
          $(this).children(":first").children("span").text(order_id);
          order_id++;

        });
        $(dom).remove();
        // Router.go("/client/"+client_id);
        // Meteor.setTimeout(function(){
        //     Template.portfolio.loadForm();
        // }, 500);
      }
    });
  },
  'click .del-arrId-btn-div': function (event, template) {
    if (!confirm("確認要刪除本筆嗎?")) {
      return;
    }
    // console.log(event.target);
    var now_id = template.data._id;
    var order_id = $(event.target).parent().parent().attr("data-order");
    var field = $(event.target).parent().parent().attr("data-id");
    // console.log(order_id);
    // console.log(field);
    $.ajax({
      url: '/api/xeditportfolioarr?' + $.param({
        "pk": now_id,
        "order_id": order_id,
        "field": field
      }),
      type: 'DELETE',
      success: function (response) {
        console.log(response);
        var dom = "#" + response.data.field + " div[data-order='" + response.data.order_id + "']";
        $(dom).nextAll().each(function (index) {
          console.log(index + ": " + $(this).text());
          console.log(this);
          $(this).children(":first").children("span").text(order_id);
          order_id++;

        });
        $(dom).remove();
        // Router.go("/client/"+client_id);
        // Meteor.setTimeout(function(){
        //     Template.portfolio.loadForm();
        // }, 500);
      }
    });
  },
  'click #edit-form-btn': function () {
    Session.set('editing-portfolio', 1);
    var now_id = this._id;
    //
    Template.portfolio.editForm(now_id);

    $(".on-editing").show();
    $("#unedit-form-btn").show();
    $("#edit-form-btn").hide();


    $('.editable').on('hidden', function (e, reason) {
      // console.log("aaa " + reason);
      if (reason === 'save' || reason === 'nochange') {
        var $next = "";
        if ($(this).next('.editable').length) { // 同td的下一個
          $next = $(this).next('.editable');
        }
        else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
          $next = $(this).parent().nextAll().children(".editable").eq(0);
        }
        else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
          $next = $(this).closest('tr').next().find('.editable').eq(0);
        }

        if ($next) {
          setTimeout(function () {
            $next.editable('show');
          }, 300);
        }
      }
    });
  },
  'click #unedit-form-btn': function () {
    Session.set('editing-portfolio', 0);
    $('.my-xedit').editable('destroy');
    $("#unedit-form-btn").hide();
    $("#edit-form-btn").show();

    $(".on-editing").hide();
    $("#cFiles").jsGrid("fieldOption", "ctrl_field", "visible", false);

  },
  'click #print-form-btn': function () {
    // $("#printarea").print();
    // $("#printarea").PrintDiv();
    // PrintDiv();
    Template.portfolio.PrintDiv();
  },
  'change #client-portfolio-id': function (event) {
    // Session.set('counter', Session.get('counter') + 1);
    var port_id = $(event.target).val();
    // console.log(port_id);
    // Template.portfolio.loadForm();
    Router.go("/portfolio/" + port_id);
  },
  "change select": function (event, template) {
    // console.log(event.currentTarget);
    // console.log(event.target);
    // console.log(template);
    if ($(event.target).hasClass("product")) {
      // $("#eproduct2").attr("disabled", true);
      // $("#eproduct3").attr("disabled", true);
      // $("#eproduct4").attr("disabled", true);

      var nowid = $(event.target).attr("id");
      if (nowid == "eproduct1") {
        $("#eproduct2").removeAttr("disabled");
        $("#eproduct3").attr("disabled", true);
        $("#eproduct4").attr("disabled", true);

        $('#eproduct3')
          .find('option')
          .remove()
          .end()
          .append('<option value="" disabled selected>-- 無 --</option>');
        $('#eproduct4')
          .find('option')
          .remove()
          .end()
          .append('<option value="" disabled selected>-- 無 --</option>');

        var sel = $('#eproduct2')
          .find('option')
          .remove()
          .end()
          .append('<option value="" selected>-- 請選擇 --</option>');

        var fieldtype = $('#eproduct1 :selected').val();

        var type = Product2.find({
          product1_id: fieldtype.toString()
        }, {
          sort: {
            product2_id: 1
          }
        }).fetch();

        $.each(type, function (i, item2) {
          sel.append($('<option>', {
            value: item2._id,
            text: item2.name_cht
          }));
        });
      } else if (nowid == "eproduct2") {
        $("#eproduct3").removeAttr("disabled");
        $("#eproduct4").attr("disabled", true);

        $('#eproduct4')
          .find('option')
          .remove()
          .end()
          .append('<option value="" selected>-- 無 --</option>');

        var fieldtype = $('#eproduct2 :selected').val();

        var type = Product3.find({
          product2_id: fieldtype.toString()
        }, {
          sort: {
            product3_id: 1
          }
        }).fetch();

        if (!type.length) {
          $("#eproduct3").attr("disabled", true);
          var sel = $('#eproduct3')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>-- 無 --</option>');
        } else {
          $("#eproduct3").removeAttr("disabled");
          var sel = $('#eproduct3')
            .find('option')
            .remove()
            .end()
            .append('<option value="" selected>-- 請選擇 --</option>');

          $.each(type, function (i, item2) {
            sel.append($('<option>', {
              value: item2._id,
              text: item2.name_cht
            }));
          });
        }
      } else if (nowid == "eproduct3") {
        var fieldtype = $('#eproduct3 :selected').val();

        var type = Product4.find({
          product3_id: fieldtype.toString()
        }).fetch();

        if (!type.length) {
          $("#eproduct4").attr("disabled", true);
          var sel = $('#eproduct4')
            .find('option')
            .remove()
            .end()
            .append('<option value="" disabled selected>-- 無 --</option>');
        } else {
          $("#eproduct4").removeAttr("disabled");
          var sel = $('#eproduct4')
            .find('option')
            .remove()
            .end()
            .append('<option value="" selected>-- 請選擇 --</option>');

          $.each(type, function (i, item2) {
            sel.append($('<option>', {
              value: item2._id,
              text: item2.name_cht
            }));
          });
        }
      }
      $("#portfolio").jsGrid("loadData");
    }
  },
  "click a.upload": function () {
    var files = $("input.file_bag")[0].files;
    // var now_id = this._id;
    // console.log(now_id);
    var s = Session.get("portfolio");

    S3.upload({
      files: files,
      path: "subfolder"
    }, function (e, r) {
      // console.log(r);
      $('#input-3').fileinput('clear');
      // var now_id = this._id;

      var formVar = {
        portfolio: "1", // 1:尚未送出 2: 已送出
        portfolio_id: s._id,
        name: r.file.original_name,
        size: r.file.size,
        file: r,
        image_id: r._id,
        url: r.secure_url,
        insertedById: Meteor.userId(),
        insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
        insertedAt: new Date()
      };
      // console.log(formVar);
      $("#cFiles").jsGrid("insertItem", formVar);
    });
  }
});
