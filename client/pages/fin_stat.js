Template.fin_stat.rendered = function () {
  Template.semicolon.loadjs();
  $("#e_stat").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      // loadData: function (filter) {
      //   return jsgridAjax(filter, "provider", "GET");
      // },
      // insertItem: function (item) {
      //   return jsgridAjax(item, "provider", "POST");
      // },
      // updateItem: function (item) {
      //   return jsgridAjax(item, "provider", "PUT");
      // },
      // deleteItem: function (item) {
      //   return jsgridAjax(item, "provider", "DELETE");
      // },
    },
    fields: [
      { type: "control", width: 60, editButton: false },
      // { name: "uid", title: "#", width: 60},
      // { name: "isopen", type: "checkbox", title: "開啟" },
      // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
      { name: "", title: "項目", type: "text" },
      { name: "", title: "一月 件數", type: "text" },
      { name: "", title: "一月 保額", type: "text" },
      { name: "", title: "一月 實收保費", type: "text" },
      { name: "", title: "二月 件數", type: "text" },
      { name: "", title: "二月 保額", type: "text" },
      { name: "", title: "二月 實收保費", type: "text" },
      { name: "", title: "三月 件數", type: "text" },
      { name: "", title: "三月 保額", type: "text" },
      { name: "", title: "三月 實收保費", type: "text" },
      // { name: "", title: "", type: "text" },
      // { name: "", title: "合計", type: "text" },
      // { name: "", title: "備註", type: "text" },
      // { name: "ps", title: "備註", type: "text", width: 200 },
      // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#e_provider").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_provider").jsGrid("loadData");
    },
  });
};
Template.fin_stat.helpers({

  acc_year: Accountyear.find({}, { sort: { value: 1 } }),
  get_bg: function () {
    var r = Router.current().params;
    return r.f1_id;
  },
  get_user: function () {
    var arr = Meteor.users.find(
      {
        $and: [
          { $or: [{ 'auth.is_auth': '1' }, { 'auth.is_auth': { $exists: false } }] },
          { 'profile.department_id': Router.current().params.f1_id }
        ]
      }).fetch();
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
      var obj = {};
      obj._id = arr[i]._id;
      obj.engname = arr[i].profile.engname;
      obj.chtname = arr[i].profile.chtname;
      resArr.push(obj);
    }

    return resArr;
  },
});

Template.fin_stat.events({

});
