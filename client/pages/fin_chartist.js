Template.fin_chartist.rendered = function(){
    Template.semicolon.loadjs();

    var data = {
      series: [5, 3, 4]
    };

    new Chartist.Pie('.ct-chart', data, {
      labelInterpolationFnc: function(value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
      }
    });

    new Chartist.Pie('.ct-chart', data);

};
Template.fin_chartist.helpers({

});

Template.fin_chartist.events({

});
