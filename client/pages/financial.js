Template.financial.rendered = function(){
    Template.semicolon.loadjs();

    var r = Router.current().params;
    if (r.f1_id && r.year && r.month) {
        $("#sel_account2").attr("disabled", true);
        $("#sel_account3").attr("disabled", true);
    } else {
        $('h3').hide();
        $('.m2-locktable').hide();
        $('.m2-unlocktable').hide();
        $('table').hide();
        $('h4').hide();
        $("#search_text").hide();
        $("#sel_account1").hide();
        $("#sel_account2").hide();
        $("#sel_account3").hide();
        $('label').hide();
        $('.my-form').hide();
        // $('.clear').hide();
    }

    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function(value) {
            return new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
        },

        editTemplate: function(value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },

        insertValue: function() {
            return this._insertPicker.datepicker("getDate").toISOString();
        },

        editValue: function() {
            return this._editPicker.datepicker("getDate").toISOString();
        }
    });
    jsGrid.fields.myDateField = MyDateField;

    $("#bCounting").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "120%",

        // sorting: true,
        pageSize: 10000,
        // paging: true,
        pageLoading: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var r = Router.current().params;
                var formVar = $(".form-modal1").serializeObject();
                $.extend(filter, formVar);
                // console.log(Router.current());
                if (r.f1_id && r.year && r.month) {
                    filter.bg_id = r.f1_id;
                    filter.year = r.year;
                    filter.month = r.month;
                } else {
                    filter.createdAt = moment().format('YYYY-MM-DD');
                }
                filter.findData = $("input#search_text").val() || "";                
                // filter.bg_id = r.f1_id;
                // filter.year = r.year;
                // filter.month = r.month;

                console.log(filter);
                return jsgridAjax(filter, "booking", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "booking", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "booking", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "booking", "DELETE");
            },
        },
        fields: [
        /*
        日期  帳戶   地點  顧客姓名    科目1 科目2 備註  單據號
        住宿          餐費          交通          客群經營
        匯率  折合新台幣
         */
            { name: "mycontrol", type: "control", width:50, align: "center" },
            { name: "invoice_date", title: "日期", type: "myDateField", width: 80, align: "center",
                itemTemplate: function(value, item) {
                    return currentdate_eff(item.invoice_date);
                },
            },
            // { name: "bankaccount_id", title: "出款帳戶", type: "select", width: 70, items: Bankacc.find({},{sort:{order_id:1}}).fetch(), valueField: "_id", textField: "value" },
            { name: "bankaccount_id", title: "出款帳戶", type: "select", width: 70, items: [{ _id: "", value: "--- 選擇 ---" }].concat(Bankacc.find({},{sort:{order_id:1}}).fetch()), valueField: "_id", textField: "value",
                itemTemplate: function (value, item) {
                    if (item._id == "") {
                        return "";
                    } else if (!!item && !!item.bankaccount_id && !!Bankacc.findOne({ _id: item.bankaccount_id })) {
                        return Bankacc.findOne({ _id: item.bankaccount_id }).value;
                    } else {
                        return '';
                    }
                }
            },
            // { name: "country_id", title: "地區", width: 60, type: "select", items: [{ id: "", value: "所有" }].concat(objCountries2), valueField: "id", textField: "value" },
            // { name: "country_id", title: "地區", width: 50, type: "select", items: [{ _id: "", value: "所有地區" }].concat(Country.find({},{sort:{order_id:1}}).fetch()), valueField: "_id", textField: "value" },
            { name: "account1_id", title: "會計科目1", width: 50, type: "select", align: "center", css:"account1_id-col", items: [{ _id: "", value: "所有科目" }].concat(Account1.find({},{sort:{order_id:1}}).fetch()), valueField: "_id", textField: "value" },
            // { name: "account2_id", title: "科目細項", type: "select", items: Account2.find({},{sort:{order_id:1}}).fetch(), valueField: "_id", textField: "value" },
            {
                name: "account2_id", title: "會計科目2", width: 80, css: "account2_id-col",
                itemTemplate: function(value, item) {
                    // console.log(value, item);
                    if (value == '無資料'){
                        return "";
                    } else if (!!item && !!item.account2_id && !!Account2.findOne({ _id: item.account2_id })) {
                        return Account2.findOne({ _id: item.account2_id }).value;
                    } else {
                        return "";
                    }
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-account2_id"></select>')
                        .find('option')
                        .remove()
                        .end();
                        // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();

                    if(!fieldtype){
                        fieldtype = Account1.findOne({},{sort:{order_id:1}})._id;
                    }
                    var type = Account2.find({a1_id: fieldtype.toString()}, {sort:{order: 1}} ).fetch();

                    if (type.length >0) {
                        $.each(type, function (i, item2) {
                            sel.append($('<option>', {
                                value: item2._id,
                                text: item2.value //+ " ($" + item2.price + ")",
                            }));
                        });
                    } else {
                        sel.append($('<option>', {
                            value: "",
                            text: "無資料"
                        }));
                    }
                    if(!!value){
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
            { name: "account3_id", title: "科目細項", align: "center", width: 80,
                itemTemplate: function(value, item) {
                    // console.log(value, item)
                    if (value == "無資料"){
                        return "";
                    }else if(!!item && !!item.account3_id) {
                        return Account3.findOne({ _id: item.account3_id }).value;
                    }
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-account3_id"></select>')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();

                    if(!fieldtype){
                        fieldtype = Account1.findOne({},{sort:{order_id:1}})._id;
                    }
                    var type = Account3.find({a1_id: fieldtype.toString()}, {sort:{order: 1}} ).fetch();

                    if(type.length > 0){
                        $.each(type, function (i, item2) {
                            sel.append($('<option>', {
                                value: item2._id,
                                text : item2.value //+ " ($" + item2.price + ")",
                            }));
                        });
                    }
                    else{
                        sel.append($('<option>', {
                            // value: item2._id,
                            text : "無資料"
                        }));
                    }

                    if(!!value){
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                },
            },
            { name: "invoice", title: "單據號碼", type: "text", align: "center", filtering: false },
            // { name: "ps", title: "說明", type: "text", align: "center", width: 150,}
            { name: "ps", width: 150, title: "說明", type: "textarea", filtering: false,
                itemTemplate: function(value, item) {
                    if (!!item.ps) {
                        let t = item.ps;
                        return t.replace(/\r?\n/g, '<br />');
                    }
                }
            },
            // { name: "currency", title: "幣別", width: 60, type: "select", items: objSalaryCash_booking, valueField: "id", textField: "value"  },
            // { name: "currency", title: "幣別", width: 60, type: "select", items: [{ id: "", value: "所有幣別" }].concat(objSalaryCash2), valueField: "id", textField: "value"  },
            { name: "cost0_money", title: "實際消費金額", width: 60, type: "text", align: "center",  filtering: false,
                itemTemplate: function(value, item) {
                    return commaSeparateNumber(item.cost0_money);
                }
            },
            // { name: "rate", title: "匯率", width: 60, type: "text" , align: "center", filtering: false},
            // { name: "cost1_money", title: "折合新台幣", width: 60, type: "text", editing:false, align: "center", filtering: false,
            //     itemTemplate: function(value, item) {
            //         var rate = 1;
            //         // console.log(item);
            //         if(!item) return "";
            //         if(!!item.rate){
            //             rate = Number(item.rate)
            //         }
            //         item.cost1_money = (Math.round(Number(item.cost0_money) * rate)) || 0;
            //         // return Math.round(Number(item.cost0_money) * rate);
            //         return commaSeparateNumber(item.cost1_money);
            //     },
            //  },
            { name: "blank_doc", title: "收據上傳", align:"center", editing:false, width: 90,
                itemTemplate: function(value, item) {
                    var str = "";
                    if(!!item.upload_url){
                        str = str + '<a target="_blank" href="'+item.upload_url+'">'+"[預覽]"+'</a> ';
                    }
                    str = str + '<a href="#" data-id="'+item._id+'" data-toggle="modal" data-target="#myModal2" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+"[上傳]"+'</a>';
                    return str;
                }
            },
             { name: "blank_doc2", title: "收據預覽", align:"center", editing:false, width: 90,
                itemTemplate: function(value, item) {
                    var str = "";
                    if(!!item.upload_url){
                        str = str + '<a target="_blank" href="'+item.upload_url+'">'+"[預覽]"+'</a> ';
                    }
                    // str = str + '<a href="#" data-id="'+item._id+'" data-toggle="modal" data-target="#myModal2" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+"[上傳]"+'</a>';
                    return str;
                }
            },
            /*{ name: "cost2_money", title: "差旅費<br>原始", type: "text" },
            { name: "cost3_money", title: "差旅費<br>NTD", type: "text", editing:false,
                itemTemplate: function(value, item) {
                    var rate = 1;
                    // console.log(item);
                    if(!item) return "";
                    if(!!item.rate){
                        rate = Number(item.rate)
                    }
                    return Number(item.cost2_money) * rate;
                },
            },
            { name: "cost4_money", title: "餐費<br>原始", type: "text" },
            { name: "cost5_money", title: "餐費<br>NTD", type: "text" , editing:false,
                itemTemplate: function(value, item) {
                    var rate = 1;
                    // console.log(item);
                    if(!item) return "";
                    if(!!item.rate){
                        rate = Number(item.rate)
                    }
                    return Number(item.cost4_money) * rate;
                },
            },
            { name: "cost6_money", title: "交通<br>原始", type: "text" },
            { name: "cost7_money", title: "交通<br>NTD", type: "text", editing:false,
                itemTemplate: function(value, item) {
                    var rate = 1;
                    // console.log(item);
                    if(!item) return "";
                    if(!!item.rate){
                        rate = Number(item.rate)
                    }
                    return Number(item.cost6_money) * rate;
                },
            },
            { name: "cost8_money", title: "客群經營<br>原始", type: "text" },
            { name: "cost9_money", title: "客群經營<br>NTD", type: "text", editing:false,
                itemTemplate: function(value, item) {
                    var rate = 1;
                    // console.log(item);
                    if(!item) return "";
                    if(!!item.rate){
                        rate = Number(item.rate)
                    }
                    return Number(item.cost8_money) * rate;
                },
            },*/
/*            { name: "cost1_currency", title: "住宿-幣別", type: "select", items: [{ id: "", value: "" }].concat(objSalaryCash), valueField: "id", textField: "value" },
            { name: "cost1_money", title: "住宿-金額", type: "text" },
            { name: "cost2_currency", title: "餐費-幣別", type: "select", items: [{ id: "", value: "" }].concat(objSalaryCash), valueField: "id", textField: "value" },
            { name: "cost2_money", title: "餐費-金額", type: "text" },
            { name: "cost3_currency", title: "交通-幣別", type: "select", items: [{ id: "", value: "" }].concat(objSalaryCash), valueField: "id", textField: "value" },
            { name: "cost3_money", title: "交通-金額", type: "text" },
            { name: "cost4_currency", title: "客群經營-幣別", type: "select", items: [{ id: "", value: "" }].concat(objSalaryCash), valueField: "id", textField: "value" },
            { name: "cost4_money", title: "客群經營-金額", type: "text" },
            { name: "cost5_currency", title: "雜費-幣別", type: "select", items: [{ id: "", value: "" }].concat(objSalaryCash), valueField: "id", textField: "value" },
            { name: "cost5_money", title: "雜費-金額", type: "text" },*/
            /*{ name: "total_money", title: "合計(新台幣)", editing:false, textField: "value",type: "text",
                itemTemplate:function(value, item) {
                    if(!item) return "";
                    if(!!item) {
                    var total =  Number(item.cost0_money) + Number(item.cost2_money) + Number(item.cost4_money) + Number(item.cost6_money) + Number(item.cost8_money);


                    if(!!item.rate){
                        rate = Number(item.rate)
                    }
                    total = total * rate;
                    return total.toString();
                    }
                }
            },*/
        ],
        // onDataLoading: function(args) {
        //     // $('.jsgrid select').material_select();
        // },
        // onItemInserted: function(args) {
        //     // Materialize.toast('資料已新增!', 3000, 'rounded')
        // },
        rowClick: function (item, itemIndex) {
            $("#bCounting").jsGrid("cancelEdit");
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            alert('資料已更新！');
            $("#bCounting").jsGrid("loadData");
        },
        // onItemDeleted: function(args) {
        //     // Materialize.toast('資料已刪除', 3000, 'rounded')
        //     $("#bCounting").jsGrid("loadData");
        // },
        onRefreshed: function(args) {
            // console.log(args);
            // console.log("onRefreshed");

            var arr = args.grid.data;
            // $(".my-footer-row").remove();
            // var sum_total0 = 0;
            // var sum_total2 = 0;
            // var sum_total4 = 0;
            // var sum_total6 = 0;
            // var sum_total8 = 0;
            // var sum_total = 0;

            // var a1 = [];

            function findValueById(arr, id){
                for (var i = 0; i < arr.length; i++) {
                    if(arr[i].id == id){
                        return arr[i].value;
                    }
                }
            }
            function findIdInsertOrAddAccount1(arr, item){
                var isFind = 0;
                for (var i = 0; i < arr.length; i++) { // 先看有沒有 有的話，就累加
                    if(arr[i].id == item.account1_id){
                        arr[i].value += Number(item.total || 0);
                        isFind = 1;
                    }
                }
                if(isFind == 0){ // 沒有的話，就新增
                    arr.push({
                        id: item.account1_id,
                        value: Number(item.total)
                    });
                }
            }
            function findIdInsertOrAddAccount2(arr, item){
                var isFind = 0;
                for (var i = 0; i < arr.length; i++) { // 先看有沒有 有的話，就累加
                    if(arr[i].id == item.account2_id){
                        arr[i].value += Number(item.total || 0);
                        isFind = 1;
                    }
                }
                if(isFind == 0){ // 沒有的話，就新增
                    arr.push({
                        id: item.account2_id,
                        a1_id: item.account1_id,
                        value: Number(item.total)
                    });
                }
            }

            var a1_arr = [];
            var a2_arr = [];
            var a1_total = 0;
            for(var i=0; i< arr.length; i++){
                findIdInsertOrAddAccount1(a1_arr, arr[i]);
                findIdInsertOrAddAccount2(a2_arr, arr[i]);
                a1_total += Number(arr[i].total || 0);
            }
            // console.log("a1_arr");
            // console.log(a1_arr);
            // console.log("a2_arr");
            // console.log(a2_arr);
            // console.log("a1_total");
            // console.log(a1_total);

            // a1
            for(var i=0; i< a1_arr.length; i++){
                if(!!a1_arr[i].value){
                    $("#a1-b-"+a1_arr[i].id).text("$"+ commaSeparateNumber(a1_arr[i].value));
                    $("#a1-c-"+a1_arr[i].id).text((a1_arr[i].value/a1_total*100).toFixed(2)+"%" );
                }
            }
            // a2
            for(var i=0; i< a2_arr.length; i++){
                var now_a1_total = findValueById(a1_arr, a2_arr[i].a1_id);

                if(!!a2_arr[i].value){
                    $("#a2-b-"+a2_arr[i].id).text("$"+ commaSeparateNumber(a2_arr[i].value));
                    $("#a2-c-"+a2_arr[i].id).text((a2_arr[i].value/now_a1_total*100).toFixed(2)+"%" );
                }
            }
            $("#total_money").text( "$"+ commaSeparateNumber(a1_total) );

            // sum_total = Number(sum_total0)+ Number(sum_total2)+ Number(sum_total4)+ Number(sum_total6)+ Number(sum_total8);

            // $(".jsgrid-grid-body tbody").append('<tr class="my-footer-row jsgrid-row"><td class="jsgrid-cell">總計</td>\
            //     <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class="jsgrid-cell">'+sum_total0+'</td>\
            //     <td></td><td class="jsgrid-cell">'+sum_total2+'</td><td></td><td class="jsgrid-cell">'+sum_total4+'</td>\
            //     <td></td><td class="jsgrid-cell">'+sum_total6+'</td><td></td><td class="jsgrid-cell">'+sum_total8+'</td>\
            //     <td></td><td></td><td class="jsgrid-cell">'+sum_total+'</td><td></td></tr>');

            // $("#total_money").text(sum_total);

            // if(sum_total == 0){
            //     sum_total = 1;
            // }
            // $(".jsgrid-grid-body tbody").append('<tr class="my-footer-row jsgrid-row"><td class="jsgrid-cell">比重</td>\
            //     <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class="jsgrid-cell">'+(sum_total0/sum_total*100).toFixed(2)+'%</td>\
            //     <td></td><td class="jsgrid-cell">'+(sum_total2/sum_total*100).toFixed(2)+'</td><td></td><td class="jsgrid-cell">'+(sum_total4/sum_total*100).toFixed(2)+'</td>\
            //     <td></td><td class="jsgrid-cell">'+(sum_total6/sum_total*100).toFixed(2)+'</td><td></td><td class="jsgrid-cell">'+(sum_total8/sum_total*100).toFixed(2)+'</td>\
            //     <td></td><td></td><td class="jsgrid-cell">100%</td><td></td></tr>');

            // $("#cost0_money").text(sum_total0);
            // $("#cost0_percent").text((sum_total0/sum_total*100).toFixed(2)+"%");

            // $("#cost2_money").text(sum_total2);
            // $("#cost2_percent").text((sum_total2/sum_total*100).toFixed(2)+"%");

            // $("#cost4_money").text(sum_total4);
            // $("#cost4_percent").text((sum_total4/sum_total*100).toFixed(2)+"%");

            // $("#cost6_money").text(sum_total6);
            // $("#cost6_percent").text((sum_total6/sum_total*100).toFixed(2)+"%");

            // $("#cost8_money").text(sum_total8);
            // $("#cost8_percent").text((sum_total8/sum_total*100).toFixed(2)+"%");
        },
    });

    this.autorun(function(){
        var r = Router.current().params;
        var ay = Accountyear.findOne({value: r.year});

        if(!!ay && ay["lockbook"+r.month]=="1"){ // locked
            $("#bCounting").jsGrid("option", "editing", false);
            $("#bCounting").jsGrid("fieldOption", "mycontrol", "deleteButton", false);
            $("#bCounting").jsGrid("fieldOption", "blank_doc", "visible", false);
            $("#bCounting").jsGrid("fieldOption", "blank_doc2", "visible", true);
        }
        else{
            $("#bCounting").jsGrid("option", "editing", true);
            $("#bCounting").jsGrid("fieldOption", "mycontrol", "deleteButton", true);
            $("#bCounting").jsGrid("fieldOption", "blank_doc2", "visible", false);
            $("#bCounting").jsGrid("fieldOption", "blank_doc", "visible", true);
        }
    });
    $("#input-3").fileinput({
        'language': "zh-TW",
    });
    $('#myModal2').on('show.bs.modal', function (e) {
        // console.log(e.relatedTarget);
        var id = $(e.relatedTarget).data("id");
        $("#data_id").val(id);
    })
    $('#myModal2').on('hide.bs.modal', function (event) {
        $(".progress").hide();
    });
    $("#csv_financial").on('click', function (event) {
        var csv_bg = Businessgroup.findOne().value;
        var csv_month = Router.current().params.month + "月";
        var csv_year = Router.current().params.year + "年";
        var args = [$("#bCounting"), ''+csv_bg + csv_year + csv_month+'財務帳本.csv'];
        exportTableToCSV.apply(this, args);
    });
};

Template.financial.helpers({
    get_currency: function() {
        return objSalaryCash;
    },
    get_bank: function() {
        return objBankAccount;
    },
    get_country: function() {
        return Country.find({},{sort:{order_id:1}}).fetch();
    },
    get_account1: function() {
        return Account1.find({},{sort:{order_id:1}});
    },
    get_account2: function(a1) {
        // console.log(a1);
        return Account2.find({a1_id: a1},{sort:{order_id:1}});
    },
    get_bookingmonth: function() {
        return Bookingmonth.find({},{sort:{value:1}});
    },
    now_month: function(){
        var r = Router.current().params;
        return r.month;
    },
    now_year: function(){
        var r = Router.current().params;
        return r.year;
    },
    is_lockbook: function(){
        var r = Router.current().params;
        var obj = Accountyear.findOne({value: r.year});

        var res = 0;
        if(!!obj && obj["lockbook"+r.month] == "1"){ // 1: 鎖住 0: 可編輯
            res = 1;
        }
        return res;
    },
    "files": function(){
        return S3.collection.find();
    }
});
Template.financial.loadPortfolio = function () {
    $("#bCounting").jsGrid("loadData");
    $("#bCounting").jsGrid("openPage", 1);
}

Template.financial.events({
    //上傳檔案
    "click a.upload1": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                uploader: "1",
                path:"subfolder"
        },function(e,r){
            // console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {};

            var file = {
                fin_upload: "1",
                fin_id: $("#data_id").val(),
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };

            formVar.upload_url = r.secure_url;
            formVar.upload = file;
            // console.log("formVar");
            // console.log(formVar);
            // return;/

            Meteor.call("updateBookingUpload", $("#data_id").val(), formVar, function(error, result){
                if(error){
                    console.log("error from updateSortUploads: ", error);
                }
                else {
                    $("#bCounting").jsGrid("loadData");
                }
            });
            // $("#m2-file1_url").val(r.secure_url);
        });
    },
    "change select": function(event, template) {
        // console.log(event.currentTarget);
        // console.log(template);
        if($(event.target).parent().hasClass("account1_id-col")){
            // var sel = $('<select class="my-account2_id"></select>')
            var sel = $('.my-account3_id')
                .find('option')
                .remove()
                .end();
            var sel2 = $('.my-account2_id')
                .find('option')
                .remove()
                .end();
                // .append('<option value="" disabled selected>請選擇</option>');
            var fieldtype = $(event.target).find("option:selected").val(); // $(".account1_id-col select option:selected").val();
            // console.log(fieldtype);

            var type = Account3.find({a1_id: fieldtype.toString()}, {sort:{order: 1}} ).fetch();
            var type2 = Account2.find({a1_id: fieldtype.toString()}, {sort:{order: 1}} ).fetch();
            
            if(type.length > 0){
                $.each(type, function (i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text : item2.value //+ " ($" + item2.price + ")",
                    }));
                });
            } else{
                sel.append($('<option>', {
                    // value: item2._id,
                    text : "無資料"
                }));
            }

            if(type2.length > 0){
                $.each(type2, function (i, item2) {
                    sel2.append($('<option>', {
                        value: item2._id,
                        text : item2.value //+ " ($" + item2.price + ")",
                    }));
                });
            } else{
                sel2.append($('<option>', {
                    // value: item2._id,
                    text : "無資料"
                }));
            }
        } else if ($(event.target).parent().hasClass("account2_id-col")) {
            var sel = $('.my-account3_id')
                .find('option')
                .remove()
                .end();
            var fieldtype = $(event.target).find("option:selected").val();

            var type = Account3.find({ a2_id: fieldtype.toString() }, { sort: { order: 1 } }).fetch();

            if (type.length > 0) {
                $.each(type, function (i, item3) {
                    sel.append($('<option>', {
                        value: item3._id,
                        text: item3.value //+ " ($" + item2.price + ")",
                    }));
                })
            } else {
                sel.append($('<option>', {
                    // value: item2._id,
                    text: "無資料"
                }));
            }
        } else if ($(event.target).hasClass("account")) {
            var nowid = $(event.target).attr("id");
            // console.log(nowid);
            if (nowid == "sel_account1") {
                $('#sel_account2').removeAttr('disabled');
                $('#sel_account3').attr('disabled', true);

                $('#sel_account3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#sel_account4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#sel_account2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --');
                
                var fieldtype = $('#sel_account1').val();

                var type = Account2.find({
                    a1_id: fieldtype.toString()
                }, { sort: { order_id: 1 }}).fetch();

                $.each(type, function (i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.value
                    }));
                });
            } else if (nowid == 'sel_account2') {
                var fieldtype = $('#sel_account2 :selected').val();
                
                var type = Account3.find({
                    a2_id: fieldtype.toString()
                }).fetch();

                if (!type.length) {
                    $('#sel_account3').attr('disabled', true);
                    var sel = $('#sel_account3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                } else {
                    $('#sel_account3').removeAttr("disabled");
                    var sel = $('#sel_account3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.value
                        }));
                    });
                }
            }
            $("#bCounting").jsGrid("loadData");
            $("#bCounting").jsGrid("openPage", 1);
        }
    },
    'click .my-btn-submit': function (event) {
    // 'submit form': function(event) {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        // formVar.productclassname = $('#productclassid :selected').text();
        // formVar.providername = $('#providerid :selected').text();

        // formVar.cost1_money = Math.round(Number(formVar.cost0_money)) || 0;

        // formVar.total = Number(formVar.cost0_money) +
        //     Number(formVar.cost1_money*formVar.rate) +
        //     Number(formVar.cost2_money*formVar.rate) +
        //     Number(formVar.cost3_money*formVar.rate) +
        //     Number(formVar.cost4_money*formVar.rate) +
        //     Number(formVar.cost5_money*formVar.rate);

        // console.log(formVar);

        Meteor.call("insertBooking", formVar, function(error, result){
            $('#modal1').modal('hide');
            $(".form-modal1").trigger('reset');
            // Materialize.toast('資料已新增', 3000, 'rounded');
            $("#m2-bookingmonth").val(result);
            // console.log(result);
            $("#bCounting").jsGrid("loadData");
        });
   /*     $("#bCounting").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                $('#modal1').closeModal();
                $(".form-modal1").trigger('reset');
                $("#bCounting").jsGrid("loadData");
                Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });*/
    },
    'click .m2-locktable': function() {
        // alert("本功能製作中。");
        if(!confirm("確定要鎖定本月財務帳本?")){
            return 0;
        }
        var formVar = {};

        var r = Router.current().params;
        var id = ""; // account year的id
        id = Accountyear.findOne({value: r.year})._id || "";

        formVar.month = r.month; // 這張表的月份


        Meteor.call("updateLocktable", id, formVar, function(error, result){
            console.log(result);
            // $('#myModal2').closeModal();
            // $(".form-modal1").trigger('reset');
            // $("#bCounting").jsGrid("loadData");
            // Materialize.toast('資料已新增', 3000, 'rounded');
        });
        // Materialize.toast('本月帳目已鎖定(尚未啟用)', 3000, 'rounded');
    },
    'click .m2-unlocktable': function() {
        // alert("本功能製作中。");
        var formVar = {};

        var r = Router.current().params;
        var id = ""; // account year的id
        id = Accountyear.findOne({value: r.year})._id || "";

        formVar.month = r.month; // 這張表的月份


        Meteor.call("updateUnLocktable", id, formVar, function(error, result){
            // console.log(result);
            // $('#modal2').closeModal();
            // $(".form-modal1").trigger('reset');
            // $("#bCounting").jsGrid("loadData");
            // Materialize.toast('資料已新增', 3000, 'rounded');
        });
        // Materialize.toast('本月帳目已鎖定(尚未啟用)', 3000, 'rounded');
    },
    'change #m2-bookingmonth': function() {
        $("#bCounting").jsGrid("loadData");
    },
    'click .my-open-modal1': function() {
        $('#modal1').openModal();

        // var $input = $('.datepicker').pickadate();
		// var picker = $input.pickadate('picker');
		// picker.set('select', new Date());

    },
    'click .label-provider': function() {
        $('#modal1').closeModal();
        Meteor.setTimeout(function() { Router.go('e_provider'); }, 10);
    },
    'click .my-btn-cancel': function() {
        $('#modal1').closeModal();
    },
    'change #search_text, keypress #search_text, click #search_text': function (event) { //#
        Template.financial.loadPortfolio();
    },
    'click .btn-new': function() {
        var formVar = {};
        var r = Router.current().params;

        if (r.f1_id && r.year && r.month) {
            formVar.bg_id = r.f1_id;
            formVar.year = r.year;
            formVar.month = r.month;
        } else {
            formVar.bg_id = r.f1_id;
            formVar.year = moment().format('YYYY');
            formVar.month = moment().format('M');
        }

        formVar.country_id = "1";
        formVar.currency = "0";
        formVar.rate = "1.0";
        formVar.cost0_money = "0";
        // formVar.cost2_money = "0";
        // formVar.cost4_money = "0";
        // formVar.cost6_money = "0";
        // formVar.cost8_money = "0";
        formVar.invoice_date = new Date();
        formVar.createdAt = moment().format('YYYY-MM-DD');

        $("#bCounting").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                // $('#myModal').modal('hide');
                // $(".form-modal2").trigger('reset');
                $("#bCounting").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                // $(".modal2-error").text(ret);
            }
        });
    },
});

