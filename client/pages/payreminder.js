Template.payreminder.rendered = function(){
    Template.semicolon.loadjs();
    $("#payreminder").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        // paging: true,
        pageSize: 10000,
        // filtering: true,
        // editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.template_id = "2" //VOYA客戶
                var sa = $("#sel_agent").find("option:selected").val();
                if (sa !== "-1") { filter.agent_id = sa };
                return jsgridAjax(filter, "payreminder", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "payreminder", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "payreminder", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "payreminder", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:50 , deleteButton : false},
            { name: "p_agent_text", title: "Agent"},
            { name: "p_name_cht", title: "Name"},
            { name: "pmr_payment1date", title: "規劃繳費日期"},
            { name: "pmr_payment1money", title: "規劃繳費金額",
                itemTemplate: function(value, item) {
                    if (!!item.pmr_payment1money) {
                        return commaSeparateNumber(item.pmr_payment1money);
                    } else return "";
                }
            },
            { name: "p_account_num", title: "單號",
                itemTemplate: function(value, item) {
                    if (!!item.p_account_num) {
                        return '<a target="_blank" href="/portfolio/'+item.p_id+'">'+item.p_account_num+'</a>';
                    } else return "";
                }
            },
            { name: "pmr_ps", title: "備註", type: "text"},
            { name: "pmr_payment1date", title: "寄件",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    var monShoudPay = moment(item.pmr_payment1date).format('YYYY-MM');
                    var thisMonth = moment().format('YYYY-MM');
                    var nextMonth = moment().add(1, 'M').format('YYYY-MM');
                    if (monShoudPay == thisMonth || monShoudPay == nextMonth) { //這個月與下個月提醒
                        return '<a target="_blank" href="/email/'+item.p_id+'">' + '寄送電子郵件' + '</a>'                   
                    } else {
                        return '';
                    }
                }
            },
            { name: "", title: "追蹤",
                itemTemplate: function (value, item) {
                    var monShoudPay = moment(item.pmr_payment1date).format('YYYY-MM');
                    var thisMonth = moment().format('YYYY-MM');
                    var nextMonth = moment().add(1, 'M').format('YYYY-MM');
                    if (monShoudPay == thisMonth || monShoudPay == nextMonth) { //這個月與下個月提醒
                        return '<a target="_blank" href="/followservice/' + item.p_id + '">' + '新增案件追蹤' + '</a>'                   
                    } else {
                        return '';
                    }
                }
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdating: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            $("#payreminder").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#payreminder").jsGrid("loadData");
        },
    });
};

Template.payreminder.helpers({
  get_agent: Agents.find(),
});

Template.payreminder.events({
    'change #sel_agent': function(event) {
        $("#payreminder").jsGrid("loadData");
    },
    'click .btn-sendEmails': function() {
        if($("#sel_agent").val() == -1){
            alert("請先選擇要寄送的Agent");
            return;
        }
        if(!confirm("確定寄送繳費提醒給Agent?")){
            return;
        }
        var data = $("#payreminder").jsGrid("option", "data");
        /*
        agent_text
        name_cht
        birthday
        */
        // console.log(data);
        // return;
        // alert("已新增");

        $("#payreminder2").html($("#payreminder").html());

        $("#payreminder2 .jsgrid-grid-body table").prepend("<tr class='jsgrid-header-row'>"+$("#payreminder2 .jsgrid-grid-header table tr").html()+"</tr>");
        var row_html = $("#payreminder2 .jsgrid-grid-body").html();

        // var table_html = '<base href="http://system.hanburyifa.com/" />' + $("#payreminder").html().replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");
        var table_html = '<base href="http://system.hanburyifa.com/" />' + row_html.replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");

        Meteor.call("sendPayReminder", data, table_html, function(error, result){
            if(error){
                console.log("error from updateSortUploads: ", error);
            }
            else {
                alert("已成功送出繳費提醒清單\n"+ result);
            }
        });
    },
});
