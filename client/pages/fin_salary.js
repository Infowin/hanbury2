Template.fin_salary.rendered = function () {
  Template.semicolon.loadjs();
  var data = this.data;
  $("#title").text(data.value);

  var year_form = $("#year_id").val();
  $(".year_form").text(year_form + "年");

  Session.set("bankacc_bg", data);


  $("#e_salary_structure_0").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "1"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "", title: "員工編號", align: "center", width: 60 },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      { name: "", title: "12月薪資", align: "center", width: 60 },
      { name: "", title: "12月代扣稅款", align: "center", width: 60 },
      { name: "", title: "1月薪資", align: "center", width: 60 },
      { name: "", title: "1月代扣稅款", align: "center", width: 60 },
      { name: "", title: "2月薪資", align: "center", width: 60 },
      { name: "", title: "2月代扣稅款", align: "center", width: 60 },
      { name: "", title: "3月薪資", align: "center", width: 60 },
      { name: "", title: "3月代扣稅款", align: "center", width: 60 },
      { name: "", title: "4月薪資", align: "center", width: 60 },
      { name: "", title: "4月代扣稅款", align: "center", width: 60 },
      { name: "", title: "5月薪資", align: "center", width: 60 },
      { name: "", title: "5月代扣稅款", align: "center", width: 60 },
      { name: "", title: "6月薪資", align: "center", width: 60 },
      { name: "", title: "6月代扣稅款", align: "center", width: 60 },
      { name: "", title: "7月薪資", align: "center", width: 60 },
      { name: "", title: "7月代扣稅款", align: "center", width: 60 },
      { name: "", title: "8月薪資", align: "center", width: 60 },
      { name: "", title: "8月代扣稅款", align: "center", width: 60 },
      { name: "", title: "9月薪資", align: "center", width: 60 },
      { name: "", title: "9月代扣稅款", align: "center", width: 60 },
      { name: "", title: "10月薪資", align: "center", width: 60 },
      { name: "", title: "10月代扣稅款", align: "center", width: 60 },
      { name: "", title: "11月薪資", align: "center", width: 60 },
      { name: "", title: "11月代扣稅款", align: "center", width: 60 },
      { name: "", title: "薪資合計", align: "center", width: 60 },
      { name: "", title: "代扣稅款合計", align: "center", width: 60 },

    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_1").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "1"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return commaSeparateNumber(item.total_salary);
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_2").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "2"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          // if(!!item.IsTotal)
          //     return commaSeparateNumber(item.total_salary);
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      $("#e_salary_structure_1").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_salary_structure_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_3").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "3"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          // if(!!item.IsTotal)
          //     return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // $("#e_salary_structure_1").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      // $("#e_salary_structure_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_4").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "4"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_5").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "5"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_6").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "6"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_7").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "7"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_8").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "8"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_9").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "6"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_10").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "10"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_11").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "11"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          if (!!item.IsTotal)
            return item.total_salary;
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_salary_structure_12").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: false,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "12"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "salary", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "salary", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "salary", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "salary", "DELETE");
      },
    },
    fields: [
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "total_base_salary", title: "本薪", align: "center",
        itemTemplate: function (value, item) {
          item.total_base_salary = (Number(item.base_salary) || 0)
          return (commaSeparateNumber(item.base_salary) || 0)
        }
      },
      {
        name: "total_allowance", title: "津貼", align: "center",
        itemTemplate: function (value, item) {
          item.total_allowance = (Number(item.allowance) || 0)
          return (commaSeparateNumber(item.allowance) || 0)
        }
      },
      {
        name: "total_first_pay", title: "代墊款項", align: "center",
        itemTemplate: function (value, item) {
          item.total_first_pay = (Number(item.first_pay) || 0)
          return (commaSeparateNumber(item.first_pay) || 0)
        }
      },
      {
        name: "total_total_deductions", title: "累積應扣薪資", align: "center",
        itemTemplate: function (value, item) {
          item.total_total_deductions = (Number(item.total_deductions) || 0)
          return (commaSeparateNumber(item.total_deductions) || 0)
        }
      },
      {
        name: "total_pe", title: "個人負擔合計<br>(勞健保)", align: "center",
        itemTemplate: function (value, item) {
          item.total_pe = ((Number(item.healthcare_pe) || 0) + (Number(item.labor_pe) || 0))
          return (commaSeparateNumber(item.total_pe) || 0)
        }
      },
      {
        name: "total_salary", title: "薪資總額", align: "center", width: 60,
        itemTemplate: function (value, item) {
          item.total_salary = (Number(item.base_salary) || 0) + (Number(item.allowance) || 0) + (Number(item.first_pay) || 0) - (Number(item.total_deductions) || 0) - (Number(item.healthcare_pe) || 0) - (Number(item.labor_pe) || 0);
          return commaSeparateNumber(item.total_salary);
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "healthcare_pe": 0, "labor_pe": 0, "base_salary": 0, "allowance": 0, "first_pay": 0, "total_deductions": 0, IsTotal: true };

      items.forEach(function (item) {
        total.healthcare_pe += (Number(item.healthcare_pe) || 0);
        total.labor_pe += (Number(item.labor_pe) || 0);
        total.base_salary += (Number(item.base_salary) || 0);
        total.allowance += (Number(item.allowance) || 0);
        total.first_pay += (Number(item.first_pay) || 0);
        total.total_deductions += (Number(item.total_deductions) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })

  $('#collapseOne').on('show.bs.collapse', function () {
    $("#e_salary_structure_1").jsGrid("loadData");
    $("#e_salary_structure_2").jsGrid("loadData");
    $("#e_salary_structure_3").jsGrid("loadData");
  })
  $('#collapseTwo').on('show.bs.collapse', function () {
    $("#e_salary_structure_4").jsGrid("loadData");
    $("#e_salary_structure_5").jsGrid("loadData");
    $("#e_salary_structure_6").jsGrid("loadData");
  })
  $('#collapseThree').on('show.bs.collapse', function () {
    $("#e_salary_structure_7").jsGrid("loadData");
    $("#e_salary_structure_8").jsGrid("loadData");
    $("#e_salary_structure_9").jsGrid("loadData");
  })
  $('#collapseFour').on('show.bs.collapse', function () {
    $("#e_salary_structure_10").jsGrid("loadData");
    $("#e_salary_structure_11").jsGrid("loadData");
    $("#e_salary_structure_12").jsGrid("loadData");
  })

  var obj_season_1_salary = Salary.find({
    $and: [
      { $or: [{ "month": "1" }, { "month": "2" }, { "month": "3" }] },
      { "year": $("#year_id").val() }
    ],
  }).fetch();

  var obj_season_2_salary = Salary.find({
    $and: [
      { $or: [{ "month": "4" }, { "month": "5" }, { "month": "6" }] },
      { "year": $("#year_id").val() }
    ],
  }).fetch();

  var obj_season_3_salary = Salary.find({
    $and: [
      { $or: [{ "month": "7" }, { "month": "8" }, { "month": "9" }] },
      { "year": $("#year_id").val() }
    ],
  }).fetch();

  var obj_season_4_salary = Salary.find({
    $and: [
      { $or: [{ "month": "10" }, { "month": "11" }, { "month": "12" }] },
      { "year": $("#year_id").val() }
    ],
  }).fetch();

  var num_season_1_total = 0;
  var num_season_2_total = 0;
  var num_season_3_total = 0;
  var num_season_4_total = 0;

  for (var i = 0; i < obj_season_1_salary.length; i++) {
    var entry = obj_season_1_salary[i];
    entry.base_salary = entry.base_salary || 0;
    entry.allowance = entry.allowance || 0;
    entry.first_pay = entry.first_pay || 0;
    entry.total_deductions = entry.total_deductions || 0;
    entry.healthcare_pe = entry.healthcare_pe || 0;
    entry.labor_pe = entry.labor_pe || 0;
    num_season_1_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
  }

  for (var i = 0; i < obj_season_2_salary.length; i++) {
    var entry = obj_season_2_salary[i];
    entry.base_salary = entry.base_salary || 0;
    entry.allowance = entry.allowance || 0;
    entry.first_pay = entry.first_pay || 0;
    entry.total_deductions = entry.total_deductions || 0;
    entry.healthcare_pe = entry.healthcare_pe || 0;
    entry.labor_pe = entry.labor_pe || 0;
    num_season_2_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
  }

  for (var i = 0; i < obj_season_3_salary.length; i++) {
    var entry = obj_season_3_salary[i];
    entry.base_salary = entry.base_salary || 0;
    entry.allowance = entry.allowance || 0;
    entry.first_pay = entry.first_pay || 0;
    entry.total_deductions = entry.total_deductions || 0;
    entry.healthcare_pe = entry.healthcare_pe || 0;
    entry.labor_pe = entry.labor_pe || 0;
    num_season_3_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
  }

  for (var i = 0; i < obj_season_4_salary.length; i++) {
    var entry = obj_season_4_salary[i];
    entry.base_salary = entry.base_salary || 0;
    entry.allowance = entry.allowance || 0;
    entry.first_pay = entry.first_pay || 0;
    entry.total_deductions = entry.total_deductions || 0;
    entry.healthcare_pe = entry.healthcare_pe || 0;
    entry.labor_pe = entry.labor_pe || 0;
    num_season_4_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
  }

  $("#num_season_1_total").text(commaSeparateNumber(num_season_1_total));
  $("#num_season_2_total").text(commaSeparateNumber(num_season_2_total));
  $("#num_season_3_total").text(commaSeparateNumber(num_season_3_total));
  $("#num_season_4_total").text(commaSeparateNumber(num_season_4_total));
  $("#num_full_year_total").text(commaSeparateNumber(num_season_1_total + num_season_2_total + num_season_3_total + num_season_4_total))

  $("#year_id").change(function () {
    let obj_season_1_salary = Salary.find({
      $and: [
        { $or: [{ "month": "1" }, { "month": "2" }, { "month": "3" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_2_salary = Salary.find({
      $and: [
        { $or: [{ "month": "4" }, { "month": "5" }, { "month": "6" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_3_salary = Salary.find({
      $and: [
        { $or: [{ "month": "7" }, { "month": "8" }, { "month": "9" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_4_salary = Salary.find({
      $and: [
        { $or: [{ "month": "10" }, { "month": "11" }, { "month": "12" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let num_season_1_total = 0;
    let num_season_2_total = 0;
    let num_season_3_total = 0;
    let num_season_4_total = 0;

    for (var i = 0; i < obj_season_1_salary.length; i++) {
      var entry = obj_season_1_salary[i];
      entry.base_salary = entry.base_salary || 0;
      entry.allowance = entry.allowance || 0;
      entry.first_pay = entry.first_pay || 0;
      entry.total_deductions = entry.total_deductions || 0;
      entry.healthcare_pe = entry.healthcare_pe || 0;
      entry.labor_pe = entry.labor_pe || 0;
      num_season_1_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
    }

    for (let i = 0; i < obj_season_2_salary.length; i++) {
      let entry = obj_season_2_salary[i];
      entry.base_salary = entry.base_salary || 0;
      entry.allowance = entry.allowance || 0;
      entry.first_pay = entry.first_pay || 0;
      entry.total_deductions = entry.total_deductions || 0;
      entry.healthcare_pe = entry.healthcare_pe || 0;
      entry.labor_pe = entry.labor_pe || 0;
      num_season_2_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
    }

    for (var i = 0; i < obj_season_3_salary.length; i++) {
      var entry = obj_season_3_salary[i];
      entry.base_salary = entry.base_salary || 0;
      entry.allowance = entry.allowance || 0;
      entry.first_pay = entry.first_pay || 0;
      entry.total_deductions = entry.total_deductions || 0;
      entry.healthcare_pe = entry.healthcare_pe || 0;
      entry.labor_pe = entry.labor_pe || 0;
      num_season_3_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
    }

    for (var i = 0; i < obj_season_4_salary.length; i++) {
      var entry = obj_season_4_salary[i];
      entry.base_salary = entry.base_salary || 0;
      entry.allowance = entry.allowance || 0;
      entry.first_pay = entry.first_pay || 0;
      entry.total_deductions = entry.total_deductions || 0;
      entry.healthcare_pe = entry.healthcare_pe || 0;
      entry.labor_pe = entry.labor_pe || 0;
      num_season_4_total += (Number(entry.base_salary) || 0) + (Number(entry.allowance) || 0) + (Number(entry.first_pay) || 0) - (Number(entry.total_deductions) || 0) - (Number(entry.healthcare_pe) || 0) - (Number(entry.labor_pe) || 0);
    }

    $("#num_season_1_total").text(commaSeparateNumber(num_season_1_total));
    $("#num_season_2_total").text(commaSeparateNumber(num_season_2_total));
    $("#num_season_3_total").text(commaSeparateNumber(num_season_3_total));
    $("#num_season_4_total").text(commaSeparateNumber(num_season_4_total));
    $("#num_full_year_total").text(commaSeparateNumber(num_season_1_total + num_season_2_total + num_season_3_total + num_season_4_total));
  });

  for (let i = 0; i < 13; i++) {
    $("#csv_salary_" + i).on('click', function (event) {
      var csv_year = $("#year_id").val() + "年"
      var args = [$("#e_salary_structure_" + i), '' + data.value + csv_year + i + '月發放薪資總額.csv'];
      exportTableToCSV.apply(this, args);
    });
  }
  // $("#csv_salary_1").on('click', function (event) {
  //     var csv_year = $("#year_id").val() + "年"
  //     var args = [$("#e_salary_structure_1"), ''+data.value + csv_year +'1月薪資管理.csv'];
  //     exportTableToCSV.apply(this, args);
  // });
  //

  var d = new Date();
  $("#year_id").val(d.getFullYear());
  $(".sel_filter").trigger("change");
};

Template.fin_salary.helpers({
  acc_year: Accountyear.find({}, { sort: { value: 1 } }),
  get_bg: function () {
    var r = Router.current().params;
    return r.f1_id;
  },
  get_user: function () {
    var arr = Meteor.users.find(
      {
        $and: [
          { $or: [{ 'auth.is_auth': '1' }, { 'auth.is_auth': { $exists: false } }] },
          { 'profile.department_id': Router.current().params.f1_id }
        ]
      }).fetch();
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
      var obj = {};
      obj._id = arr[i]._id;
      obj.engname = arr[i].profile.engname;
      obj.chtname = arr[i].profile.chtname;
      resArr.push(obj);
    }

    return resArr;
  },
});

Template.fin_salary.events({
  // "change #type_id": function(event, template) {
  //     if ($(event.target).hasClass("type_id")) {
  //         $("#e_salary_structure_1").jsGrid("loadData");
  //         $("#e_salary_structure_2").jsGrid("loadData");
  //         $("#e_salary_structure_3").jsGrid("loadData");
  //         $("#e_salary_structure_4").jsGrid("loadData");
  //     }
  // },
  "change .sel_filter": function (event, template) {
    var year_form = $("#year_id").val();
    $(".year_form").text(year_form + "年");
    $("#e_salary_structure_1").jsGrid("loadData");
    $("#e_salary_structure_2").jsGrid("loadData");
    $("#e_salary_structure_3").jsGrid("loadData");
    $("#e_salary_structure_4").jsGrid("loadData");
    $("#e_salary_structure_5").jsGrid("loadData");
    $("#e_salary_structure_6").jsGrid("loadData");
    $("#e_salary_structure_7").jsGrid("loadData");
    $("#e_salary_structure_8").jsGrid("loadData");
    $("#e_salary_structure_9").jsGrid("loadData");
    $("#e_salary_structure_10").jsGrid("loadData");
    $("#e_salary_structure_11").jsGrid("loadData");
    $("#e_salary_structure_12").jsGrid("loadData");
  },
});

// Template.fin_salary_month.rendered = function() {
//     //全選checkbox
//     $("#checkAll").click(function () {
//         $(".check").prop('checked', $(this).prop('checked'));
//     });
// }

// Template.fin_salary_month.helpers({
//     get_month_member: function(){
//         // console.log(this);
//         var data = this;
//         return Salary.find({year: data.year, month: data.month});
//     }

// });
// Template.fin_salary_month.events({
//     "click .myFileInput": function(event, template) {
//         $("#progress_bar").hide();
//     },
//     "change .myFileInput": function(event, template) {
//         // console.log("change .myFileInput");

//         var files = $(event.currentTarget).find("input")[0].files;
//         var year = $(event.currentTarget).data("year");
//         var month = $(event.currentTarget).data("month");
//         var employee = $(event.currentTarget).data("fid");
//         var employee_id = $(event.currentTarget).data("eid");
//         var salary_id = $(event.currentTarget).data("sid");

//         S3.upload({
//                 files:files,
//                 uploader: "1",
//                 path:"subfolder"
//         },function(e,r){
//             // console.log(r);

//             var selVar = {};
//             selVar.year        = year;
//             selVar.month       = month;
//             selVar.employee    = employee;
//             selVar.employee_id = employee_id;
//             selVar.salary_id   = salary_id;
//             selVar.file_rurl   = r.relative_url;

//             var formVar = {
//                 employee: "2", // 1:尚未送出 2: 已送出
//                 employee_id: employee_id,
//                 year: year,
//                 month: month,

//                 salary: "1",
//                 name: r.file.original_name,
//                 size: r.file.size,
//                 file: r,
//                 image_id: r._id,
//                 url: r.secure_url,
//                 insertedById: Meteor.userId(),
//                 insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
//                 insertedAt: new Date()
//             };
//             $("#cFiles").jsGrid("insertItem", formVar).done(function(ret) {
//                 // console.log("insertion completed");
//                 // console.log(ret);

//                 if (ret.insert == "success") {
//                     var data = ret.data;

//                     selVar.upload_id = data._id;
//                     Meteor.call("updateSalaryUrl", salary_id, selVar, formVar, function(error, result){
//                         if(error){
//                             console.log("error from updateSortUploads: ", error);
//                         }
//                         else {
//                             // $("#cFiles2").jsGrid("loadData");
//                         }
//                     });

//                 } else {
//                     $(".modal2-error").text(ret);
//                 }

//             });

//         });
//     },
//     'click a.del-salary': function(event) {
//         if(!confirm("確認要刪除嗎?")){
//             return;
//         }

//         var id = $(event.currentTarget).data("sid");
//         var upload_id = $(event.currentTarget).data("uid");
//         var file_rurl = $(event.currentTarget).data("rurl");

//         Meteor.call("delSalary", id, upload_id, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");

//             }
//         });
//     },
//     // 'click .my-xedit': function() {
//     //     // Session.set('counter', Session.get('counter') + 1);
//     //     /*$("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", true);*/
//     //     var now_id = this._id;
//     //     //

//     //     $(".my-xedit-text").each(function() {
//     //         var domId = $(this).attr("id");
//     //         $(this).editable({
//     //             type: 'text',
//     //             name: domId,
//     //             pk: now_id,
//     //             url: '/api/xedituser',
//     //             // title: 'Enter username'
//     //         });
//     //     });
//     // },
// });

// Template.fin_salary.rendered = function() {
//     Template.semicolon.loadjs();
//     var ocImages = $("#oc-images");

//     var data = this.data;
//     $("#title").text(data.value);

//     ocImages.owlCarousel({
//         margin: 30,
//         nav: false,
//         autoplayHoverPause: true,
//         dots: true,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             480: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             992: {
//                 items: 4
//             }
//         }
//     });


//     $("#cFiles").jsGrid({
//         // height: "90%",
//         width: "100%",
//         loadIndication: true,
//         autoload: true,
//         pageLoading: true,
//         loadMessage: "讀取中...",
//         deleteConfirm: "確定要刪除此筆資料?",
//         controller:{
//             loadData: function(filter) {
//                 return jsgridAjax(filter, "uploads", "GET");
//             },
//             insertItem: function(item) {
//                 return jsgridAjax(item, "uploads", "POST");
//             },
//             updateItem: function(item) {
//                 return jsgridAjax(item, "uploads", "PUT");
//             },
//             deleteItem: function(item) {
//                 return jsgridAjax(item, "uploads", "DELETE");
//             },
//         },
//         fields: [
//             { name: "ctrl_field", type: "control", width:40, editButton:false},
//             // { type: "control", width:40, editButton:false},
//             { name: "download", width:100, align:"center", title: "檔案",
//                 itemTemplate: function(value, item) {
//                     // console.log(item);
//                     // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
//                     // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
//                     return '<a href="#" data-num="2" data-toggle="modal" data-target="#myModalFile" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+item.name+'</a>';
//                 },
//             },
//             // { name: "uid", title: "#", width: 50 },
//             // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
//             // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
//             // { name: "product2", title: "產品代碼", type: "text" },
//             // { name: "name", title: "檔案名稱", type: "text" },
//             // { name: "receiver_name", title: "收件人", type: "text" },
//             // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
//             // { name: "url", title: "圖片連結", type: "text" },
//             // { name: "size", title: "檔案大小", type: "text" },
//             { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
//         ],
//     });

//     $.fn.editable.defaults.mode = 'inline';
//     $("#unedit-form-btn").hide();

//     // $("#modaluser").serialize()

//     $('#myModal2').on('show.bs.modal', function (event) {
//         var button = $(event.relatedTarget) // Button that triggered the modal
//         // console.log(button);
//         // console.log(button.data("id"));

//         var bg = button.data("bg");
//         var year = button.data("year");
//         var month = button.data("month");

//         $("#m2-bg_id").val(bg);
//         $("#m2-year").val(year);
//         $("#m2-month").val(month);
//     });


// /*    $("#input").fileinput({
//         'language': "zh-TW",
//         'dropZoneEnabled': true
//     });*/

//     $('#myModalFile').on('show.bs.modal', function (event) {
//         var button = $(event.relatedTarget) // Button that triggered the modal
//         // console.log(button);
//         // console.log(button.data("id"));
//         /*var now_data = $(event.target).attr("data-url");*/
//         var index = button.data("index");
//         var num = button.data("num");
//         var url = button.data("url");
//         $("#modal2-link").html( '<a target="_blank" href="'+url+'"> (開新視窗)</a>');

//         $("#file_iframe").attr("src", url);
//     });
//     $('#myModalFile').on('hide.bs.modal', function (event) {
//         $("#file_iframe").attr("src", "");
//     });

//     this.autorun(function(){
//         var arr_ac = Accountyear.find().fetch();

//         for(var i=0; i<arr_ac.length; i++){
//             var entry = arr_ac[i];

//             entry.salary1  = entry.salary1 || "0";
//             entry.salary2  = entry.salary2 || "0";
//             entry.salary3  = entry.salary3 || "0";
//             entry.salary4  = entry.salary4 || "0";
//             entry.salary5  = entry.salary5 || "0";
//             entry.salary6  = entry.salary6 || "0";
//             entry.salary7  = entry.salary7 || "0";
//             entry.salary8  = entry.salary8 || "0";
//             entry.salary9  = entry.salary9 || "0";
//             entry.salary10 = entry.salary10 || "0";
//             entry.salary11 = entry.salary11 || "0";
//             entry.salary12 = entry.salary12 || "0";

//             $("[data-ayid='"+entry._id+"'][data-field='salary1']").text(entry.salary1 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary2']").text(entry.salary2 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary3']").text(entry.salary3 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary4']").text(entry.salary4 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary5']").text(entry.salary5 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary6']").text(entry.salary6 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary7']").text(entry.salary7 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary8']").text(entry.salary8 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary9']").text(entry.salary9 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary10']").text(entry.salary10 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary11']").text(entry.salary11 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='salary12']").text(entry.salary12 || "0");

//             var season_1 = Number(entry.salary1) + Number(entry.salary2) + Number(entry.salary3);
//             var season_2 = Number(entry.salary4) + Number(entry.salary5) + Number(entry.salary6);
//             var season_3 = Number(entry.salary7) + Number(entry.salary8) + Number(entry.salary9);
//             var season_4 = Number(entry.salary10) + Number(entry.salary11) + Number(entry.salary12);
//             var ta_year = season_1 + season_2 + season_3 + season_4 ;

//             if (isNaN(season_1) == false ) {
//                 $("#ta1-"+entry._id).text(season_1);
//             }
//             if (isNaN(season_2) == false ) {
//                 $("#ta2-"+entry._id).text(season_2);
//             }
//             if (isNaN(season_3) == false ) {
//                 $("#ta3-"+entry._id).text(season_3);
//             }
//             if (isNaN(season_4) == false ) {
//                 $("#ta4-"+entry._id).text(season_4);
//             }

//             $("#tayear"+entry._id).text(ta_year)
//         }
//     });

//     $(".my-xedit-text").each(function() {
//         var domId = $(this).data("field");
//         var now_id = $(this).data("ayid");
//         $(this).editable({
//             type: 'text',
//             name: domId, // 欄位名稱
//             pk: now_id,  // account year's id
//             url: '/api/xeditaccountyear',
//             // title: 'Enter username'
//         });
//     });
// };

// Template.fin_salary.helpers({
//     acc_year: Accountyear.find({}, {sort:{value:1}}),
//     get_bg: function(){
//         var r = Router.current().params;
//         return r.f1_id;
//     },
//     get_user: function(){
//       var arr = Meteor.users.find({$or: [
//           {'auth.is_auth':'1'},
//           {'auth.is_auth':
//             { $exists : false }
//           }
//         ]}).fetch();
//       var resArr = [];
//       for (var i = 0; i < arr.length; i++) {
//         var obj = {};
//         obj._id = arr[i]._id;
//         obj.engname = arr[i].profile.engname;
//         obj.chtname = arr[i].profile.chtname;

//         resArr.push(obj);
//       }

//       return resArr;
//     },
//     "files": function(){
//         return S3.collection.find();
//     }
// });
// Template.fin_salary.events({
//     'click .btn-m2-new': function() {

//         var formVar = $(".form-modal2").serializeObject();

//         console.log(formVar);
//         // return;
//         Meteor.call("addSalaryMember", formVar, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");
//             }
//         });
//     },
//     'click #edit-form-btn': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         var now_id = this._id;
//         //
//         $(".my-xedit-text").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 // title: 'Enter username'
//             });
//         });
//         $(".my-xedit-date").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 format: 'YYYY/MM/DD',
//                 viewformat: 'YYYY/MM/DD',
//                 template: 'YYYY / MMMM / D',
//                 combodate: {
//                     minYear: 1900,
//                     maxYear: 2020,
//                     minuteStep: 1
//                 }
//             });
//         });
//         $(".my-xedit-sel").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'select',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 // source: ,
//                 source: function(){
//                     var objname = $(this).attr("data-obj");
//                     if(objname == "objBodyCheckType1") return objBodyCheckType1;
//                     else if(objname == "objDepartment") return objDepartment;
//                 },
//                 value: $(this).text()
//             });
//         });

//         $("#unedit-form-btn").show();
//         $("#edit-form-btn").hide();
//         $(".on-editing").show();
//     },
//     'click #unedit-form-btn': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         $('.my-xedit').editable('destroy');
//         $("#unedit-form-btn").hide();
//         $("#edit-form-btn").show();
//         $(".on-editing").hide();
//     },
//     'click a.del-file': function(event) {
//         if(!confirm("確認要刪除嗎?")){
//             return;
//         }

//         var id = $(event.currentTarget).data("sid");
//         var upload_id = $(event.currentTarget).data("uid");
//         var file_rurl = $(event.currentTarget).data("rurl");

//         S3.delete(file_rurl, function(e, r){
//             if(!!e) console.log(e);
//             else{
//                 // Materialize.toast('資料已刪除 '+ data.name, 3000, 'rounded');
//                 console.log("資料已刪除"+ data.name);
//             }
//         });
//         Meteor.call("delSalaryUrl", id, upload_id, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");

//             }
//         });
//     },
//     'change #sel-fin_salary': function(event, template) {
//         // console.log($(event.target).val());
//         // var u = Meteor.users.findOne({_id:$(event.target).val()});
//         // $("#username").text(u.username);
//         // $("#cht-name").text(u.profile.name);
//         // Session.set('counter', Session.get('counter') + 1);

//         Router.go("/fin_salary/"+$(event.target).val());
//         // Template.fin_salary.loadData(this);

//     },
//   /*  'click .my-xedit': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         var now_id = this._id;

//         $(".my-xedit-text").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xeditaccountyear',
//                 // title: 'Enter username'
//             });
//         });
//     },*/
// });