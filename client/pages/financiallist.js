Template.financiallist.rendered = function(){
    Template.semicolon.loadjs();
    $("#e_country").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "country", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "country", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "country", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "country", "DELETE");
            },
        },
        fields: [
            { type: "control", width:30 },
            { name: "value", title: "國家/地區名", type: "text", align: "center"},
        ],
        onRefreshed: function() {
            var $gridData = $("#e_country .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    Meteor.call("updateSortCounrty", items, function(error, result){
                        if(error){
                            console.log("error from updateSortCounrty: ", error);
                        }
                        else {
                            $("#e_country").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};
Template.financiallist.helpers({
    get_bg: Businessgroup.find({},{sort:{order_id:1}}),
    findBank: function (id) {
        // console.log(id);
        var res = Bankacc.find({ bg_id: id }, { sort: { order_id: 1 } }).fetch();
        // console.log(res);
        return res;
    },
});
Template.financiallist.events({
    'click .btn-new1': function() {
        $("#e_country").jsGrid("insertItem", formVar).done(function(ret) {
            $("#e_country").jsGrid("loadData");
        });
    },
});

