Template.followservice.rendered = function() {
  var p_id = Router.current().params.p_id;

  Template.semicolon.loadjs();
  // var MyDateTimeField = function(config) {
  //     jsGrid.Field.call(this, config);
  // };

  // MyDateTimeField.prototype = new jsGrid.Field({
  //     sorter: function(date1, date2) {
  //         // console.log(date1);
  //         return new Date(date1) - new Date(date2);
  //     },

  //     itemTemplate: function(value) {
  //         // console.log(value);
  //         return value;
  //         // return new Date(value).toDateString();
  //     },

  //     insertTemplate: function(value) {
  //         return this._insertPicker = $("<input>").datetimepicker({ defaultDate: new Date() });
  //     },

  //     editTemplate: function(value) {
  //         // console.log(value);
  //         // return this._editPicker = $("<input>").datetimepicker().datetimepicker("setDate", new Date(value));

  //         var date = new Date();

  //         if (!!value) {
  //             date = new Date(
  //                 Date.parse(
  //                     value,
  //                     "mm/dd/yyyy hh:MM tt"
  //                 )
  //             );
  //         }

  //         this._editPicker = $("<input>").datetimepicker({
  //             // widgetParent:".datetimepicker_z",
  //             // widgetParent:".datetimepicker",
  //             defaultDate: date
  //         });

  //         return this._editPicker;
  //         // ampm: true
  //         // }).datetimepicker("setDate", date);
  //         // }).datetimepicker({defaultDate: date});
  //         // return this._editPicker = $("<input>").val(value);
  //     },
  //     insertValue: function() {
  //         // return this._insertPicker.datetimepicker("getDate").toISOString();
  //         return this._insertPicker.val();
  //     },
  //     editValue: function() {
  //         // return this._editPicker.datetimepicker("getDate").toISOString();
  //         // console.log("editValue:");
  //         // console.log(this._editPicker.val());
  //         // console.log(new Date(this._editPicker.val()));
  //         return this._editPicker.val();
  //     }
  // });
  // jsGrid.fields.myDateTimeField = MyDateTimeField;

  if(p_id) {
    Meteor.call('getArrPortfolios', p_id, function (err, res) {
        p_data = res;
        var p_obj = p_data[0]
        // console.log(p_data);
        Meteor.call('insertAfterservice', p_obj, function (err, res) {
          if (res) {
            // console.log(res);
            $("#e_followservice").jsGrid("loadData");
            alert('已新增案件！');
          }
        })
    })
  }

  var MyDateField = function(config) {
    jsGrid.Field.call(this, config);
  };

  MyDateField.prototype = new jsGrid.Field({
    sorter: function(date1, date2) {
      return new Date(date1) - new Date(date2);
    },

    itemTemplate: function(value) {
      return new Date(value).toDateString();
    },

    insertTemplate: function(value) {
      return this._insertPicker = $("<input>").datepicker({
        defaultDate: new Date()
      });
    },

    editTemplate: function(value) {
      return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },

    insertValue: function() {
      return this._insertPicker.datepicker("getDate").toISOString();
    },

    editValue: function() {
      return this._editPicker.datepicker("getDate").toISOString();
    }
  });

  jsGrid.fields.myDateField = MyDateField;

  $("#e_followservice").jsGrid({
    // height: "90%",
    height: "850px",
    width: "110%",
    sorting: true,
    paging: true,
    // pageSize: 15,
    pageSize: 15,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    selecting: false,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        if (!filter.sortField) {
          filter.sortField = "insertedAt";
          filter.sortOrder = "desc";
        }

        var sa = $("#sel_agent").find("option:selected").val();
        if (sa != "-1") {
          filter.agent_id = sa
        };

        var ss = $("#sel_status").find("option:selected").val();
        if (ss != "") {
          filter.status = ss
        };

        var aao = $("#sel_actionOwner").find("option:selected").val();
        if (aao != "") {
          filter.as_action_owner = aao
        };

        filter.findData = $("input#search_text").val() || "";
        return jsgridAjax(filter, "afterservice", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "afterservice", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "afterservice", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "afterservice", "DELETE");
      }
    },
    rowClick: function (item, itemIndex) {
      $("#e_followservice").jsGrid("cancelEdit");
    },
    rowClass: function(item, itemIndex) {
      // console.log(item);
      var diff = parseInt((new Date() - new Date(item.updatedAt1)) / 1000 / 60 / 60 / 24);

      // console.log( diff );
      // if (diff >= 60) {
      //   return "row-danger";
      // } else if (diff >= 30) {
      //   return "row-warning";
      // }
      if (item.isMark) {
        return "row-danger";
      }
    },
    fields: [
      {
        name: 'isMark',
        title: '重要',
        type: 'checkbox',
        width: 60,
        // itemTemplate: function (value, item) {

        //   return $("<input>").attr("type", "checkbox")
        //     .attr("checked", value || item.isMark)
        //     .on("change", function () {
        //       item.isMark = $(this).is(":checked");
        //     });
        // }
      }
      ,
      {
        type: "control",
        width: 60
      },{
        name: "insertedAt",
        type: "myDateField",
        title: "Case Date",
        width: 95,
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.insertedAt) {
            // return item.insertedAt
            return currentdate_eff(item.insertedAt);
          } else {
            return "";
          }
        }
      }, 
      {
        name: "portfolio.name_cht",
        editing: false,
        title: "客戶名稱",
        width: 100,
        align: "center"
      }, 
      {
        name: "name_eng",
        editing: false,
        title: "Client Name",
        width: 140,
        align: "center"
      }, 
      {
        name: "portfolio.provider_engtext",
        editing: true,
        title: "Provider",
        type: "text",
        align: "center",
        // itemTemplate: function(value, item) {
        //     if(!!item.portfolio && !!item.portfolio.provider_engtext)
        //         return item.portfolio.provider_engtext;
        //     else
        //         return "";
        // },
      }, {
        name: "account_num",
        editing: false,
        title: "單號",
        type: "text",
        width: 85,
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.portfolio && !!item.portfolio.uid)
            return '<a href="/portfolio/' + item.portfolio._id + '" target="_blank">' + item.uid + '</a>';
          else
            return "";
        },
        editTemplate: function(value, item) {
          return item.portfolio.uid;
        }
      }, {
        name: "as_template_text",
        editing: false,
        title: "Service Item",
        type: "text",
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.as_template_id && !!Nopdfform.findOne({
              _id: item.as_template_id
            })) {
            return Nopdfform.findOne({
              _id: item.as_template_id
            }).nopdf
          } else if (!!item.as_template_text) {
            return item.as_template_text
          } else {
            return ""
          }
        }
      }, {
        name: "updatedAt1",
        type: "myDateField",
        // type: "myDateTimeField",
        title: "Due Date",
        width: 95,
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.updatedAt1) {

            return currentdate_eff(item.updatedAt1) + "<br />過期天數: " + dateDiff(item.updatedAt1);
          } else {
            return "";
          }
        }
      }, {
        name: "status",
        title: "Status",
        type: "select",
        items: objASPhase,
        valueField: "id",
        textField: "value",
        width: 70
      },
      // { name: "status", title: "Status", type: "select", items: [{ id: "", value: "-- 全選 --" }].concat(objASPhase), valueField: "id", textField: "value"},
      {
        name: "description",
        width: 150,
        title: "Description",
        type: "textarea",
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.description) {
            let t = item.description;
            return t.replace(/\r?\n/g, '<br />');
          }
        }
      }, 
      // {
      //   name: "as_action",
      //   title: "Action",
      //   type: "select",
      //   items: objASAction,
      //   valueField: "id",
      //   textField: "value"
      // }, 
      {
        name: "agent_text",
        editing: false,
        title: "Agent",
        align: "center",
        itemTemplate: function (value, item) {
          if (!!item.portfolio && !!item.portfolio.agent_text)
            return item.portfolio.agent_text;
          else
            return "";
        },
        editTemplate: function (value, item) {
          return item.portfolio.agent_text;
        }
      },
      {
        name: "as_action_owner",
        title: "Action Owner",
        type: "select",
        items: objASActionOwner,
        valueField: "id",
        textField: "value"
      }
    ],
    onDataLoading: function(args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#e_followservice").jsGrid("loadData");
      $("#e_followservice_dis_non").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_followservice").jsGrid("loadData");
      $("#e_followservice_dis_non").jsGrid("loadData");
    }
  });
  $("#e_followservice_dis_non").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    pageSize: 10000,
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    selecting: false,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        var sa = $("#sel_agent").find("option:selected").val();
        if (sa != "-1") {
          filter.agent_id = sa
        };

        var ss = $("#sel_status").find("option:selected").val();
        if (ss != "") {
          filter.status = ss
        };

        var aao = $("#sel_actionOwner").find("option:selected").val();
        if (aao != "") {
          filter.as_action_owner = aao
        };

        filter.findData = $("input#search_text").val() || "";
        return jsgridAjax(filter, "afterservice", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "afterservice", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "afterservice", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "afterservice", "DELETE");
      }
    },
    rowClass: function(item, itemIndex) {
      // console.log(item);
      var diff = parseInt((new Date() - new Date(item.insertedAt)) / 1000 / 60 / 60 / 24);

      // console.log( diff );
      // if (diff >= 60) {
      //   return "row-danger";
      // } else if (diff >= 30) {
      //   return "row-warning";
      // }
    },
    fields: [{
        name: "insertedAt",
        type: "myDateTimeField",
        title: "Case Date",
        width: 95,
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.insertedAt) {
            return currentdate_eff(item.insertedAt);
          } else {
            return "";
          }
        }
      },
      {
        name: "portfolio.name_cht",
        editing: false,
        title: "客戶名稱",
        width: 100,
        align: "center"
      },  
      {
        name: "name_eng",
        editing: false,
        title: "Client Name",
        width: 140
      }, {
        name: "provider_text",
        editing: true,
        title: "Provider",
        type: "text",
        itemTemplate: function(value, item) {
          if (!!item.portfolio && !!item.portfolio.provider_engtext)
            return item.portfolio.provider_engtext;
          else
            return "";
        }
      }, {
        name: "account_num",
        editing: false,
        title: "單號",
        type: "text",
        width: 85,
        // itemTemplate: function(value, item) {
        //   if (!!item.portfolio && !!item.portfolio.uid)
        //     return '<a href="/portfolio/' + item.portfolio._id + '" target="_blank">' + item.portfolio.uid + '</a>';
        //   else
        //     return "";
        // },
        // editTemplate: function(value, item) {
        //   return item.portfolio.uid;
        // }
      }, {
        name: "as_template_text",
        editing: false,
        title: "Service Item",
        type: "text"
      }, {
        name: "updatedAt1",
        type: "myDateTimeField",
        title: "Due Date",
        width: 95,
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item.updatedAt1) {
            return currentdate_eff(item.updatedAt1);
          } else {
            return "";
          }
        }
      }, {
        name: "status",
        title: "Status",
        type: "select",
        items: objASPhase,
        valueField: "id",
        textField: "value",
        width: 70
      },
      // { name: "status", title: "Status", type: "select", items: [{ id: "", value: "-- 全選 --" }].concat(objASPhase), valueField: "id", textField: "value"},
      {
        name: "description",
        width: 150,
        title: "Description",
        type: "textarea",
        itemTemplate: function(value, item) {
          if (!!item.description) {
            let t = item.description;
            return t.replace(/\r?\n/g, '<br />');
          }
        }
      },
      // {
      //   name: "as_action",
      //   title: "Action",
      //   type: "select",
      //   items: objASAction,
      //   valueField: "id",
      //   textField: "value"
      // }, 
      {
        name: "agent_text",
        editing: false,
        title: "Agent",
        align: "center",
        // itemTemplate: function (value, item) {
        //   if (!!item.portfolio && !!item.portfolio.agent_text)
        //     return item.portfolio.agent_text;
        //   else
        //     return "";
        // },
        // editTemplate: function (value, item) {
        //   return item.portfolio.agent_text;
        // }
      },
      {
        name: "as_action_owner",
        title: "Action Owner",
        type: "select",
        items: objASActionOwner,
        valueField: "id",
        textField: "value"
      },
      // {
      //   name: "agent_text",
      //   editing: false,
      //   title: "Agent",
      //   itemTemplate: function(value, item) {
      //     if (!!item.portfolio && !!item.portfolio.agent_text)
      //       return item.portfolio.agent_text;
      //     else
      //       return "";
      //   },
      //   editTemplate: function(value, item) {
      //     return item.portfolio.agent_text;
      //   }
      // },
      // {
      //   type: "control",
      //   width: 60
      // }
    ],
  });

  $("#csv_followservice").on('click', function(event) {
    var args = [$("#e_followservice_dis_non"), '案件追蹤.csv'];
    exportTableToCSV.apply(this, args);
  });
};

Template.followservice.helpers({
  get_agent: Agents.find()
});

Template.followservice.loadPortfolio = function() {
  $("#e_followservice").jsGrid("loadData");
  $("#e_followservice").jsGrid("openPage", 1);
  $("#e_followservice_dis_non").jsGrid("loadData");
}

Template.followservice.events({
  "change #sel_actionOwner": function(event, template) {
    $("#e_followservice").jsGrid("loadData");
    $("#e_followservice_dis_non").jsGrid("loadData");
  },
  "change #sel_status": function(event, template) {
    $("#e_followservice").jsGrid("loadData");
    $("#e_followservice_dis_non").jsGrid("loadData");
  },
  'change #sel_agent': function(event) {
    $("#e_followservice").jsGrid("loadData");
    $("#e_followservice_dis_non").jsGrid("loadData");
  },
  'change #search_text, keypress #search_text, click #search_text': function(event) { //#
    Template.followservice.loadPortfolio();
  },
  // 'click #csv_followservice': function(event) {
  //   var args = [$("#e_followservice_dis_non"), '案件追蹤.csv'];
  //   exportTableToCSV.apply(this, args);
  // },
  'click #btn-sendEmails': function() {
    if ($("#sel_agent").val() == -1) {
      alert("請先選擇要寄送的Agent");
      return;
    }
    if (!confirm("確定寄送案件追蹤給Agent?")) {
      return;
    }
    // var data = $("#e_followservice").jsGrid("option", "data");
    var data = $("#e_followservice_dis_non").jsGrid("option", "data");

    $("#e_followservice_dis_non2").html($("#e_followservice_dis_non").html());

    $("#e_followservice_dis_non2 .jsgrid-grid-body table").prepend("<tr class='jsgrid-header-row'>"+$("#e_followservice_dis_non2 .jsgrid-grid-header table tr").html()+"</tr>");
    var row_html = $("#e_followservice_dis_non2 .jsgrid-grid-body").html();

    // var table_html = '<base href="http://system.hanburyifa.com/" />' + $("#e_followservice_dis_non").html().replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");
    var table_html = '<base href="http://system.hanburyifa.com/" />' + row_html.replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");

    // table_html =  $(table_html).contents().unwrap().html();
    /*
    agent_text
    name_cht
    birthday
    */
    // console.log(table_html);

    Meteor.call("sendFollowservice", data, table_html, function(error, result) {
      if (error) {
        console.log("error from updateSortUploads: ", error);
      } else {
        alert("已成功送出案件追蹤清單\n" + result);
      }
    });
  },
});
