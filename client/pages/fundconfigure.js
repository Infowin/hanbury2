Template.fundconfigure.rendered = function() {
    Template.semicolon.loadjs();
    $("#jFundConfigure").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 10,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                // filter.sent_status_id = "2";

                if(!filter.sortField){
                    filter.sortField = "FundDescription";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "fundconfigure", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "fundconfigure", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "fundconfigure", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "fundconfigure", "DELETE");
            },
        },
        fields: [
            { type: "control", width:10, deleteButton: false },
            {
                name: 'isShow',
                title: '顯示',
                type: 'checkbox',
                width: 20,
                // itemTemplate: function (value, item) {

                //   return $("<input>").attr("type", "checkbox")
                //     .attr("checked", value || item.isMark)
                //     .on("change", function () {
                //       item.isMark = $(this).is(":checked");
                //     });
                // }
            },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "FundDescription", title: "基金名稱", type: "text", editing: false },
            { name: "fluctuation", title: "波動", type: "text", itemTemplate: function (value, item) {
                    return value ? `${value}%` : '';
                }
            },
            // { name: 'color', type: 'select'}
        ],
        onItemUpdated: function(args) {
            $("#jFundConfigure").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jFundConfigure").jsGrid("loadData");
        },
        rowClick: function(args) {
            Session.set("nowrow_product1", args.item);
            $("#jFundConfigure").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#jFundConfigure .jsgrid-grid-body tbody");
            // $gridData.sortable({
            //     update: function(e, ui) {
            //         var items = $.map($gridData.find("tr"), function(row) {
            //             return $(row).data("JSGridItem");
            //         });
            //         // console.log("Reordered items", items);
            //         Meteor.call("updateSortProduct1", items, function(error, result){
            //             if(error){
            //                 console.log("error from updateSortProduct1: ", error);
            //             }
            //             else {
            //                 $("#jFundConfigure").jsGrid("loadData");
            //             }
            //         });
            //     }
            // });
        }
    });

};

Template.fundconfigure.helpers({
    // homepics: objHomePics
});

Template.fundconfigure.events({
});