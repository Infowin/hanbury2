Template.product3.loadP4 = function(p4_id) {
    // var id = Session.get("nowrow_oproduct4")._id;
    console.log("Template.product3.loadP4 "+ p4_id);
    Meteor.call("getP4obj", p4_id, function(error, result){
        var p4 = result; // Session.get("nowrow_oproduct4");
    
        // console.log("p4: ");
        // console.log(p4);
            
        if(!!p4.showpic1){
            $("#op4-big-pic").attr("src", p4.showpic1 || "");
            $("#op4-big-pic").fadeIn();
        }
        else{
            $("#op4-big-pic").hide();
        }
        if(!!p4.showpic2){
            $("#op4-big-pic2").attr("src", p4.showpic2 || "");
            $("#op4-big-pic2").fadeIn();
        }
        else{
            $("#op4-big-pic2").hide();
        }
        if(!!p4.title1){
            $("#title1").html(p4.title1);
            $("#title1").fadeIn();
        }
        else{
            $("#title1").hide();
        }
        if(!!p4.title2){
            $("#title2").html(p4.title2);
            $("#title2").fadeIn();
        }
        else{
            $("#title2").hide();
        }
        if(!!p4.intro_title1){
            $("#intro_title1").html(p4.intro_title1);
            $("#intro_title1").fadeIn();
        }
        else{
            $("#intro_title1").hide();
        }
        if(!!p4.intro_content1){
            $("#intro_content1").html(p4.intro_content1.replace(/\n/g,"<br>"));
            $("#intro_content1").fadeIn();
        }
        else{
            $("#intro_content1").parent().hide();
        }
        if(!!p4.intro_title2){
            $("#intro_title2").html(p4.intro_title2);
            $("#intro_title2").fadeIn();
        }
        else{
            $("#intro_title2").hide();
        }
        if(!!p4.intro_content2){
            $("#intro_content2").html(p4.intro_content2.replace(/\n/g,"<br>"));
            $("#intro_content2").fadeIn();
        }
        else{
            $("#intro_content2").parent().hide();
        }
        if(!!p4.intro_title3){
            $("#intro_title3").html(p4.intro_title3);
            $("#intro_title3").fadeIn();
        }
        else{
            $("#intro_title3").hide();
        }
        if(!!p4.intro_content3){
            $("#intro_content3").html(p4.intro_content3.replace(/\n/g,"<br>"));
            $("#intro_content3").fadeIn();
        }
        else{
            $("#intro_content3").parent().hide();
        }
        /////////////
        if(!!p4.youtube_url){
            $("#youtube_url").attr("src", "http://www.youtube.com/embed/"+p4.youtube_url);
            $("#youtube_url").show();
        }
        else{
            $("#youtube_url").parent().hide();
        }
        if(!!p4.youtube_title){
            $("#youtube_title").html(p4.youtube_title);
            $("#youtube_title").show();
        }
        else{
            $("#youtube_title").hide();
        }
        if(!!p4.youtube_content){
            $("#youtube_content").html(p4.youtube_content);
            $("#youtube_content").show();
        }
        else{
            $("#youtube_content").hide();
        }
        if(!!p4.video_url){
            $("#video_url").attr("href", p4.video_url);
            $("#video_url").html(p4.video_title);
            $("#video_url").show();
        }
        else{
            $("#video_url").hide();
        }
        if(!!p4.video_content){
            $("#video_content").html(p4.video_content);
            $("#video_content").show();
        }
        else{
            $("#video_content").hide();
        } 
    });

    function retCaro(url, title1, title2){
        var t1 = title1 || "";
        var t2 = title2 || "";
        var str = '\
        <div class="oc-item">\
            <div class="iportfolio">\
                <div class="portfolio-image">\
                    <img src="'+url+'" alt="'+t1+'">\
                    <div class="portfolio-overlay">\
                        <a href="'+url+'" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>\
                    </div>\
                </div>\
                <div class="portfolio-desc">\
                    <h3>'+t1+'</h3>\
                    <span>'+t2+'</span>\
                </div>\
            </div>\
        </div>';
        return $(str);
    };
    $("#oc-portfolio").html("");
    Meteor.call("getP4CaroImg", p4_id, function(error, result){
        // console.log(result);
        if(result.length > 0){
            $(".div-owl").show();   
        }
        else{
            $(".div-owl").hide();   
        }
        for(var i in result){
            var obj = result[i];
            if(!!obj.url){
                var dom = retCaro(obj.url, obj.title1, obj.title2);
                // console.log(dom);
                $("#oc-portfolio").append(dom);
            }
        }

        var ocPortfolio = $("#oc-portfolio");
        if (ocPortfolio.hasClass("owl-carousel")) {
            ocPortfolio.owlCarousel('destroy'); 
            // ocPortfolio.owlCarousel({touchDrag: false, mouseDrag: false});
        }
        ocPortfolio.owlCarousel({
            margin: 20,
            nav: true,
            navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
            autoplay: false,
            autoplayHoverPause: true,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });
        Template.semicolon.loadjs();
    });

}
Template.product3.rendered = function() {
    Template.semicolon.loadjs();

    var ocImages = $("#oc-images");

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });

    var controller = Router.current();

    var p3_id = controller.params.product3_id;
    var p4_id = Oproduct4.findOne({product3_id:p3_id}, {sort:{order_id:1}})._id;
    // console.log(p4_id);

    if(p4_id){
        Template.product3.loadP4(p4_id);
    }
    Session.set("left_menu3", p3_id);


    Tracker.autorun(function(){
        var p3_id = Session.get("left_menu3");
        if(p3_id && !!Oproduct4.findOne({product3_id: p3_id})){
            // console.log("changed in p3");

            var p4 = Oproduct4.find({
                product3_id: p3_id
            }, {sort:{order_id: 1}}).fetch();

            var dom = "";
            var dom2 = "";

            for(var i in p4){
                data = p4[i];
                if(!!data.value_cht){
                    dom = dom + '<li><a class="p3-tab-href" data-id="'+data._id+'" href="#tabs-'+data._id+'">'+data.value_cht+'</a></li>';

                    dom2 = dom2 + '<div class="tab-content clearfix" id="tabs-'+data._id+'"></div>';
                }
            }
            $(".p3-ul").html(dom);
            $(".p3-tab-container").html(dom2);

            if($(".tabs").hasClass("ui-tabs")){
                $('.tabs').tabs('destroy');
            }
            $('.tabs').tabs();

            if(p4.length > 0){
                Template.product3.loadP4(p4[0]._id);
            }
            
        }
    });

};
Template.product3.helpers({
    // homepics: objHomePic

    breadcrumb: function() {
        var controller = Router.current();
        var p = Oproduct4.findOne({
            product3_id: controller.params.product3_id
        }, {sort:{order_id: 1}}); 
        var str = p.product1_text + " / " + p.product2_text + " / " + p.product3_text;

        return str;
    },
    breadcrumb1: function() {
        var controller = Router.current();
        var p = Oproduct4.findOne({
            product3_id: controller.params.product3_id
        }, {sort:{order_id: 1}}); 
        var str = p.product1_text;

        return str;
    },
    breadcrumb2: function() {
        var controller = Router.current();
        var p = Oproduct4.findOne({
            product3_id: controller.params.product3_id
        }, {sort:{order_id: 1}}); 
        var str = p.product2_text;

        return str;
    },
    breadcrumb3: function() {
        var controller = Router.current();
        var p = Oproduct4.findOne({
            product3_id: controller.params.product3_id
        }, {sort:{order_id: 1}}); 
        var str = p.product3_text;

        return str;
    },
 /*   product4: function() {
        // console.log(this);
        var controller = Router.current();
        return Oproduct4.find({product3_id: controller.params.product3_id}, {sort:{order_id: 1}}); // 房地產
    }*/
});

Template.product3.events({
    'click .btn-showlogin': function() {
        // Session.set('counter', Session.get('counter') + 1);
    },
    'click .p3-tab-href': function(event) {
        // Session.set('counter', Session.get('counter') + 1);
        // console.log("a");
        var p4_id = $(event.target).attr("data-id");
        // console.log(p4_id);
        Template.product3.loadP4(p4_id);
    },
});
