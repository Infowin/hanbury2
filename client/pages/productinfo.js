Template.productinfo.rendered = function(){
    Template.semicolon.loadjs();

  $("#fundconfigure").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    noDataContent: "尚無資料",
    sorting: true,
    paging: true,
    // filtering: true,
    editing: true,
    pageSize: 10,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        var formVar = $(".form-modal1").serializeObject();
        $.extend(filter, formVar);

        // if (!!Session.get('my-initial') && Session.get('my-initial').length == 1) {
        //   filter.initial = Session.get('my-initial');
        // }
        // if (!!Session.get('my-nowphase') && Session.get('my-nowphase').length == 1) {
        //   filter.nowphase = Session.get('my-nowphase');
        // }
        // filter.nowphase = '2'; // 已生效
        // filter.product1_id = Product1.findOne({ value: '投資' })._id;
        // filter.product2_id = Product2.findOne({ name_cht: '投資型保單' })._id;
        // filter.field_type_id = "1";
        filter.isShow = 'true'
        // filter.findData = $("input#search_text").val() || "";
        // console.log(filter);

        return jsgridAjax(filter, "fundconfigure", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fundconfigure", "POST");
      },
      updateItem: function (item) {
        // return jsgridAjax(item, "portfolio", "PUT");

        // Meteor.call("updatePortfolio", item._id, item, function(error, result){
        //     if(error){
        //         console.log("error from updatePortfolio: ", error);
        //     }
        //     else{
        //         // console.log(result);
        //         return result;
        //     }
        //});
      }, 
      deleteItem: function (item) {
        return jsgridAjax(item, "fundconfigure", "DELETE");
      },
    },
    fields: [
      /*
      client_id: id,
      product1: data.product1,
      product2: data.product2,
      account_num: data.account_num,
      fpi_num: data.fpi_num,
      start_date: data.start_date,
      invest_money: data.invest_money,
      - 項目
      - 項目起啟日(如12/31 或 6/30)
      - 投資金額
      - 幣別
      - 匯款日期(3/1)
      - 第一年利率 x%? (用打的 只能4~10，到小數第一位)
      - 做到7年 (至少前5年)
      - 第一次預計配息 (填數字)
       */
      /*  { name: "review_btn", align: "center", title: "編輯", sorting: false,
            itemTemplate: function(value, item) {
                // console.log(item);
                return '<input class="my-open-modal-1" type="button" data-id="'+item._id+'" value="編輯">';
            },
        },*/
      // { type: "control", width:60, editButton: false, width: 60  },
      // { name: "uid", title: "#", width:60 },
      { name: "FundDescription", title: "基金名稱", type: "text"},
      {
        name: "arrRecord", title: `現在價格<br/>${moment().format('YYYY/M/DD')}`, type: "text", width: 70, align: "center",
        itemTemplate: function (value, item) {
          // console.log(item);
          // console.log(value)
          var today = moment().format('YYYY/M/DD');

          if (value.find(function (doc) { return doc.PriceDate == today})) {
            var todayDoc = value.filter(function (doc) { return doc.PriceDate == today })
            var todayPrice = todayDoc[0].BidPrice
            // return '<div>' + todayPrice + "</div><hr>" + '<div> </div>';
            // return todayPrice + "<hr>";
            return todayPrice;
          }
          return '無當日價格';
        }
      },
      {
        name: "arrRecord", title: `一週前價格<br/>${moment().subtract(7, 'days').format('YYYY/M/DD')}`, type: "text", width: 70, align: "center",
        itemTemplate: function (value, item) {
          // console.log(item);
          // console.log(value)
          var lastWeek = moment().subtract(7, 'days').format('YYYY/M/DD');
          var today = moment().format('YYYY/M/DD');

          if (value.find(function (doc) { return doc.PriceDate == lastWeek})) {
            var todayDoc = value.filter(function (doc) { return doc.PriceDate == today })
            var lastWeekDoc = value.filter(function (doc) { return doc.PriceDate == lastWeek })
            var todayPrice = todayDoc[0].BidPrice
            var lastWeekPrice = lastWeekDoc[0].BidPrice
            var percent = Math.round((todayPrice - lastWeekPrice) * 10000 / lastWeekPrice) / 100;

            return '<div>' + lastWeekPrice + "</div><hr>" + `<div style=${ percent > item.fluctuation ? 'background-color:red': '' }>` + percent + '%</div>';
          }
          return '無當日價格';
        }
      },
      {
        name: "arrRecord", title: `一月前價格<br/>${moment().subtract(1, 'months').format('YYYY/M/DD')}`, type: "text", width: 70, align: "center",
        itemTemplate: function (value, item) {
          // console.log(item);
          // console.log(value)
          var lastMonth = moment().subtract(1, 'months').format('YYYY/M/DD');
          var today = moment().format('YYYY/M/DD');

          if (value.find(function (doc) { return doc.PriceDate == lastMonth })) {
            var todayDoc = value.filter(function (doc) { return doc.PriceDate == today })
            var lastMonthDoc = value.filter(function (doc) { return doc.PriceDate == lastMonth })
            var todayPrice = todayDoc.length ? todayDoc[0].BidPrice : null
            var lastMonthPrice = lastMonthDoc[0].BidPrice
            var percent = Math.round((todayPrice - lastMonthPrice) * 10000 / lastMonthPrice) / 100;
            if (!todayDoc) {
              return '無當日價格';
            }
            return '<div>' + lastMonthPrice + "</div><hr>" + `<div style=${percent > item.fluctuation ? 'background-color:red' : ''}>` + percent + '%</div>';
          }
          return '無當日價格';
        }
      },
      {
        name: "arrRecord", title: `一年前價格<br/>${moment().subtract(1, 'years').format('YYYY/M/DD')}`, type: "text", width: 70, align: "center",
        itemTemplate: function (value, item) {
          // console.log(item);
          // console.log(value)
          var lastYear = moment().subtract(1, 'years').format('YYYY/M/DD');
          var today = moment().format('YYYY/M/DD');

          if (value.find(function (doc) { return doc.PriceDate == lastYear })) {
            var todayDoc = value.filter(function (doc) { return doc.PriceDate == today })
            var lastYearDoc = value.filter(function (doc) { return doc.PriceDate == lastYear })
            var todayPrice = todayDoc[0].BidPrice
            var lastYearPrice = lastYearDoc[0].BidPrice
            var percent = Math.round((todayPrice - lastYearPrice) * 10000 / lastYearPrice) / 100;

            return '<div>' + lastYearPrice + "</div><hr>" + `<div style=${percent > item.fluctuation ? 'background-color:red' : ''}>` + percent + '%</div>';
          }
          return '無當日價格';
        }
      },
      // { name: "arrFundConfigure",title: 'test',type: "repeating", width: 100, align: "left" },
      // { name: "contactnum", title: "連絡電話", type: "text", width: 80 },
      // { name: "email", title: "電子信箱", type: "text" },
      // {
      //   name: "product4_text", title: "投資項目", type: "text",
      //   itemTemplate: function (value, item) {
      //     // console.log(item);
      //     if (!!item.product4_text) {
      //       return '<a target="_blank" href="/portfolio/' + item._id + '">' + item.product4_text + '</a>';
      //     }
      //     else {
      //       return "";
      //     }
      //   }
      // },
      // { name: "account_num", title: "Policy No.", type: "text", width: 90 },
      // { name: "agent_text", title: "財務顧問", type: "text", width: 60 },
      // { name: "provider_chttext", title: "供應商", type: "text", width: 60 },
    ],
    rowClick: function (args) {
      // console.log(args);
    },
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#fundconfigure").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#fundconfigure").jsGrid("loadData");
    },
  });
};

Template.productinfo.helpers({
  // homepics: objHomePics
  nowDate: function () {
    return moment().format('YYYY-MM-DD')
  },
  lastWeek: function () {
    return moment().subtract(7, 'days').format('YYYY-MM-DD')
  },
  lastMonth: function () {
    return moment().subtract(1, 'months').format('YYYY-MM-DD')
  },
  lastYear: function () {
    return moment().subtract(1, 'years').format('YYYY-MM-DD')
  }
});

Template.productinfo.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});

Template.productinfo.loadPortfolio = function () {
  $("#fundconfigure").jsGrid("loadData");
  $("#fundconfigure").jsGrid("openPage", 1);
}

