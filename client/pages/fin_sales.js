Template.fin_sales.rendered = function () {
  Template.semicolon.loadjs();
  var d = new Date();
  var auto_sel_year = Fin_sales_year.findOne({ sales_year: (d.getFullYear()).toString() })._id
  $("#sel_sales_year").val(auto_sel_year);
  $("#sel_sales_year_1").val(auto_sel_year);

  var ocImages = $("#oc-images");

  ocImages.owlCarousel({
    margin: 30,
    nav: false,
    autoplayHoverPause: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      480: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      }
    }
  });

  $.fn.editable.defaults.mode = 'inline';
  $("#unedit-form-btn").hide();

  var data = this.data;
  $("#e_target").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        return jsgridAjax(filter, "provider", "GET");
      },
      // insertItem: function (item) {
      //   return jsgridAjax(item, "provider", "POST");
      // },
      // updateItem: function (item) {
      //   return jsgridAjax(item, "provider", "PUT");
      // },
      // deleteItem: function (item) {
      //   return jsgridAjax(item, "provider", "DELETE");
      // },
    },
    fields: [
      { type: "control", width: 60, editButton: false },
      // { name: "uid", title: "#", width: 60},
      // { name: "isopen", type: "checkbox", title: "開啟" },
      // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
      { name: "", title: "收入項目", type: "text" },
      { name: "", title: "1月 目標 保額", type: "text" },
      { name: "", title: "1月 目標 保費", type: "text" },
      { name: "", title: "1月 目標 件數", type: "text" },
      { name: "", title: "1月 達成 保額", type: "text" },
      { name: "", title: "1月 達成 保費", type: "text" },
      { name: "", title: "1月 達成 件數", type: "text" },
      { name: "", title: "2月 目標 保額", type: "text" },
      { name: "", title: "2月 目標 保費", type: "text" },
      { name: "", title: "2月 目標 件數", type: "text" },
      { name: "", title: "2月 達成 保額", type: "text" },
      { name: "", title: "2月 達成 保費", type: "text" },
      { name: "", title: "2月 達成 件數", type: "text" },
      { name: "", title: "達成率", type: "text" },
      // { name: "ps", title: "備註", type: "text", width: 200 },
      // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#e_provider").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_provider").jsGrid("loadData");
    },
  });
  $("#cFiles_level").jsGrid({
    // height: "90%",
    editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        // filter.fin_sales = "1"; // 1:尚未送出 2: 已送出
        // filter.fin_sales_id = data._id;
        return jsgridAjax(filter, "sales_level", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "sales_level", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "sales_level", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "sales_level", "DELETE");
      },
    },
    fields: [
      { type: "control", width: 40 },
      { name: "level_1", title: "Diamond", type: "text", align: "center" },
      { name: "level_2", title: "Gold", type: "text", align: "center" },
      { name: "level_3", title: "Silver", type: "text", align: "center" },
      { name: "level_4", title: "Green", type: "text", align: "center" },
    ],
  });
  $("#cFirst_Half_Year").jsGrid({
    // height: "90%",
    editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year_1").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales1", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 60,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "姓名", align: "center" },
      {
        name: "First_Half_Year_1", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.First_Half_Year_1 = Number(item.First_Half_Year_1) || 0;
          return commaSeparateNumber(item.First_Half_Year_1)
        }
      },
      {
        name: "First_Half_Year_2", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.First_Half_Year_2 = Number(item.First_Half_Year_2) || 0;
          return commaSeparateNumber(item.First_Half_Year_2)
        }
      },
      {
        name: "First_Half_Year_3", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.First_Half_Year_3 = Number(item.First_Half_Year_3) || 0;
          return commaSeparateNumber(item.First_Half_Year_3)
        }
      },
      {
        name: "First_Half_Year_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.First_Half_Year_Total = item.First_Half_Year_1 + item.First_Half_Year_2 + item.First_Half_Year_3
          return commaSeparateNumber(item.First_Half_Year_Total)
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#cFirst_Half_Year").jsGrid("loadData");
      $("#cSecond_Half_Year").jsGrid("loadData");
      $("#cFull_Year").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      $("#cFirst_Half_Year").jsGrid("loadData");
      $("#cSecond_Half_Year").jsGrid("loadData");
      $("#cFull_Year").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "First_Half_Year_1": 0, "First_Half_Year_2": 0, "First_Half_Year_3": 0, IsTotal: true };

      items.forEach(function (item) {
        total.First_Half_Year_1 += (Number(item.First_Half_Year_1) || 0);
        total.First_Half_Year_2 += (Number(item.First_Half_Year_2) || 0);
        total.First_Half_Year_3 += (Number(item.First_Half_Year_3) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#cSecond_Half_Year").jsGrid({
    // height: "90%",
    editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year_1").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales1", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 60, deleteButton: false,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "姓名", align: "center" },
      {
        name: "Second_Half_Year_1", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Second_Half_Year_1 = Number(item.Second_Half_Year_1) || 0;
          return commaSeparateNumber(item.Second_Half_Year_1)
        }
      },
      {
        name: "Second_Half_Year_2", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Second_Half_Year_2 = Number(item.Second_Half_Year_2) || 0;
          return commaSeparateNumber(item.Second_Half_Year_2)
        }
      },
      {
        name: "Second_Half_Year_3", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Second_Half_Year_3 = Number(item.Second_Half_Year_3) || 0;
          return commaSeparateNumber(item.Second_Half_Year_3)
        }
      },
      {
        name: "Second_Half_Year_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Second_Half_Year_Total = item.Second_Half_Year_1 + item.Second_Half_Year_2 + item.Second_Half_Year_3
          return commaSeparateNumber(item.Second_Half_Year_Total)
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#cSecond_Half_Year").jsGrid("loadData");
      $("#cFull_Year").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Second_Half_Year_1": 0, "Second_Half_Year_2": 0, "Second_Half_Year_3": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Second_Half_Year_1 += (Number(item.Second_Half_Year_1) || 0);
        total.Second_Half_Year_2 += (Number(item.Second_Half_Year_2) || 0);
        total.Second_Half_Year_3 += (Number(item.Second_Half_Year_3) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#cFull_Year").jsGrid({
    // height: "90%",
    editing: false,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year_1").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales1", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales1", "DELETE");
      },
    },
    fields: [
      { type: "control", width: 60, deleteButton: false, editButton: false },
      { name: "chtname", title: "姓名", align: "center" },
      {
        name: "Full_year_1", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Full_year_1 = (Number(item.First_Half_Year_1) || 0) + (Number(item.Second_Half_Year_1) || 0);
          return commaSeparateNumber(item.Full_year_1)
        }
      },
      {
        name: "Full_year_2", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Full_year_2 = (Number(item.First_Half_Year_2) || 0) + (Number(item.Second_Half_Year_2) || 0);
          return commaSeparateNumber(item.Full_year_2)
        }
      },
      {
        name: "Full_year_3", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Full_year_3 = (Number(item.First_Half_Year_3) || 0) + (Number(item.Second_Half_Year_3) || 0);
          return commaSeparateNumber(item.Full_year_3)
        }
      },
      {
        name: "Full_year_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Full_year_Total = Number(item.Full_year_1) + Number(item.Full_year_2) + Number(item.Full_year_3)
          return commaSeparateNumber(item.Full_year_Total)
        }
      },
    ],
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "First_Half_Year_1": 0, "First_Half_Year_2": 0, "First_Half_Year_3": 0, "Second_Half_Year_1": 0, "Second_Half_Year_2": 0, "Second_Half_Year_3": 0, "Full_year_1": 0, "Full_year_2": 0, "Full_year_3": 0, IsTotal: true };

      items.forEach(function (item) {

        total.First_Half_Year_1 += (Number(item.First_Half_Year_1) || 0);
        total.First_Half_Year_2 += (Number(item.First_Half_Year_2) || 0);
        total.First_Half_Year_3 += (Number(item.First_Half_Year_3) || 0);
        total.Second_Half_Year_1 += (Number(item.Second_Half_Year_1) || 0);
        total.Second_Half_Year_2 += (Number(item.Second_Half_Year_2) || 0);
        total.Second_Half_Year_3 += (Number(item.Second_Half_Year_3) || 0);
        total.Full_year_1 += (Number(item.Full_year_1) || 0);
        total.Full_year_2 += (Number(item.Full_year_2) || 0);
        total.Full_year_3 += (Number(item.Full_year_3) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#cFiles2").jsGrid({
    // height: "90%",
    editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 60,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "姓名", align: "center" },
      {
        name: "targets1", title: "Insurance<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets1) || 0);
        }
      },
      {
        name: "targets2", title: "Insurance<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets2) || 0);
        }
      },
      {
        name: "targets3", title: "Insurance<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets1 && !!item.targets2) {
            item.targets3 = Number(item.targets1) / Number(item.targets2)
            return (item.targets3 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets4", title: "US Property<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets4) || 0);
        }
      },
      {
        name: "targets5", title: "US Property<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets5) || 0);
        }
      },
      {
        name: "targets6", title: "US Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets4 && !!item.targets5) {
            item.targets6 = Number(item.targets4) / Number(item.targets5)
            return (item.targets6 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets7", title: "UK Property<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets7) || 0);
        }
      },
      {
        name: "targets8", title: "UK Property<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets8) || 0);
        }
      },
      {
        name: "targets9", title: "UK Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets7 && !!item.targets8) {
            item.targets9 = Number(item.targets7) / Number(item.targets8)
            return (item.targets9 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets10", title: "ITA<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets10) || 0);
        }
      },
      {
        name: "targets11", title: "ITA<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets11) || 0);
        }
      },
      {
        name: "targets12", title: "ITA<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets10 && !!item.targets11) {
            item.targets12 = Number(item.targets10) / Number(item.targets11)
            return (item.targets12 * 100).toFixed(2);
          }
        }
      },
      // { name: "targets7" , title: "UK Property<br>Actual", type: "text", align: "center"},
      // { name: "targets8" , title: "UK Property<br>Goal"  , type: "text", align: "center"},
      // { name: "targets9" , title: "UK Property<br>%"     , align: "center"},
      // { name: "targets10", title: "ITA<br>Actual"        , type: "text", align: "center"},
      // { name: "targets11", title: "ITA<br>Goal"          , type: "text", align: "center"},
      // { name: "targets12", title: "ITA<br>%"             , align: "center"},
    ],
    onItemUpdated: function (args) {
      $("#cFiles2").jsGrid("loadData");
      $("#cFiles3").jsGrid("loadData");
      $("#cFiles4").jsGrid("loadData");
      $("#cFirst_Half_Year").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      $("#cFiles2").jsGrid("loadData");
      $("#cFiles3").jsGrid("loadData");
      $("#cFiles4").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "targets1": 0, "targets2": 0, "targets4": 0, "targets5": 0, "targets7": 0, "targets8": 0, "targets10": 0, "targets11": 0, IsTotal: true };

      items.forEach(function (item) {
        total.targets1 += (Number(item.targets1) || 0);
        total.targets2 += (Number(item.targets2) || 0);
        total.targets4 += (Number(item.targets4) || 0);
        total.targets5 += (Number(item.targets5) || 0);
        total.targets7 += (Number(item.targets7) || 0);
        total.targets8 += (Number(item.targets8) || 0);
        total.targets10 += (Number(item.targets10) || 0);
        total.targets11 += (Number(item.targets11) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#cFiles3").jsGrid({
    // height: "90%",
    editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 60, deleteButton: false,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "姓名", align: "center" },
      // { name: "depart_text", title: "事業群", align: "center"},
      {
        name: "targets13", title: "Insurance<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets13) || 0);
        }
      },
      {
        name: "targets14", title: "Insurance<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets14) || 0);
        }
      },
      {
        name: "targets15", title: "Insurance<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets13 && !!item.targets14) {
            item.targets15 = Number(item.targets13) / Number(item.targets14)
            return (item.targets15 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets16", title: "US Property<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets16) || 0);
        }
      },
      {
        name: "targets17", title: "US Property<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets17) || 0);
        }
      },
      {
        name: "targets18", title: "US Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets16 && !!item.targets17) {
            item.targets18 = Number(item.targets16) / Number(item.targets17)
            return (item.targets18 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets19", title: "UK Property<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets19) || 0);
        }
      },
      {
        name: "targets20", title: "UK Property<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets20) || 0);
        }
      },
      {
        name: "targets21", title: "UK Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets19 && !!item.targets20) {
            item.targets21 = Number(item.targets19) / Number(item.targets20)
            return (item.targets21 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets22", title: "ITA<br>Actual", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets22) || 0);
        }
      },
      {
        name: "targets23", title: "ITA<br>Goal", type: "text", align: "center",
        itemTemplate: function (value, item) {
          return commaSeparateNumber(Number(item.targets23) || 0);
        }
      },
      {
        name: "targets24", title: "ITA<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets22 && !!item.targets23) {
            item.targets24 = Number(item.targets22) / Number(item.targets23)
            return (item.targets24 * 100).toFixed(2);
          }
        }
      },
      // { name: "targets19", title: "UK Property<br>Actual", type: "text", align: "center"},
      // { name: "targets20", title: "UK Property<br>Goal"  , type: "text", align: "center"},
      // { name: "targets21", title: "UK Property<br>%"     , align: "center"},
      // { name: "targets22", title: "ITA<br>Actual"        , type: "text", align: "center"},
      // { name: "targets23", title: "ITA<br>Goal"          , type: "text", align: "center"},
      // { name: "targets24", title: "ITA<br>%"             , align: "center"},
    ],
    onItemUpdated: function (args) {
      $("#cFiles2").jsGrid("loadData");
      $("#cFiles3").jsGrid("loadData");
      $("#cFiles4").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "targets13": 0, "targets14": 0, "targets16": 0, "targets17": 0, "targets19": 0, "targets20": 0, "targets22": 0, "targets23": 0, IsTotal: true };

      items.forEach(function (item) {
        total.targets13 += (Number(item.targets13) || 0);
        total.targets14 += (Number(item.targets14) || 0);
        total.targets16 += (Number(item.targets16) || 0);
        total.targets17 += (Number(item.targets17) || 0);
        total.targets19 += (Number(item.targets19) || 0);
        total.targets20 += (Number(item.targets20) || 0);
        total.targets22 += (Number(item.targets22) || 0);
        total.targets23 += (Number(item.targets23) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#cFiles4").jsGrid({
    // height: "90%",
    // editing: true,
    width: "100%",
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    controller: {
      loadData: function (filter) {
        var sy = $("#sel_sales_year").find("option:selected").text();
        filter.year = sy;
        return jsgridAjax(filter, "fin_sales", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales", "DELETE");
      },
    },

    fields: [
      {
        type: "control", width: 60, deleteButton: false, editButton: false,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "姓名", align: "center" },
      // { name: "depart_text", title: "事業群", align: "center"},
      {
        name: "targets25", title: "Insurance<br>Actual", align: "center",
        itemTemplate: function (value, item) {
          item.targets25 = (Number(item.targets1) || 0) + (Number(item.targets13) || 0);
          return commaSeparateNumber(item.targets25);
        }
      },
      {
        name: "targets26", title: "Insurance<br>Goal", align: "center",
        itemTemplate: function (value, item) {
          item.targets26 = (Number(item.targets2) || 0) + (Number(item.targets14) || 0);
          return commaSeparateNumber(item.targets26);
        }
      },
      {
        name: "targets27", title: "Insurance<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets25 && !!item.targets26) {
            item.targets27 = Number(item.targets25) / Number(item.targets26);
            return (item.targets27 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets28", title: "US Property<br>Actual", align: "center",
        itemTemplate: function (value, item) {
          item.targets28 = (Number(item.targets4) || 0) + (Number(item.targets16) || 0);
          return commaSeparateNumber(item.targets28);
        }
      },
      {
        name: "targets29", title: "US Property<br>Goal", align: "center",
        itemTemplate: function (value, item) {
          item.targets29 = (Number(item.targets5) || 0) + (Number(item.targets17) || 0);
          return commaSeparateNumber(item.targets29);
        }
      },
      {
        name: "targets30", title: "US Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets28 && !!item.targets29) {
            item.targets30 = Number(item.targets28) / Number(item.targets29);
            return (item.targets30 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets31", title: "UK Property<br>Actual", align: "center",
        itemTemplate: function (value, item) {
          item.targets31 = (Number(item.targets7) || 0) + (Number(item.targets19) || 0);
          return commaSeparateNumber(item.targets31);
        }
      },
      {
        name: "targets32", title: "UK Property<br>Goal", align: "center",
        itemTemplate: function (value, item) {
          item.targets32 = (Number(item.targets8) || 0) + (Number(item.targets20) || 0);
          return commaSeparateNumber(item.targets32);
        }
      },
      {
        name: "targets33", title: "UK Property<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets31 && !!item.targets32) {
            item.targets33 = Number(item.targets31) / Number(item.targets32);
            return (item.targets33 * 100).toFixed(2);
          }
        }
      },
      {
        name: "targets34", title: "ITA<br>Actual", align: "center",
        itemTemplate: function (value, item) {
          item.targets34 = (Number(item.targets10) || 0) + (Number(item.targets22) || 0);
          return commaSeparateNumber(item.targets34);
        }
      },
      {
        name: "targets35", title: "ITA<br>Goal", align: "center",
        itemTemplate: function (value, item) {
          item.targets35 = (Number(item.targets11) || 0) + (Number(item.targets23) || 0);
          return commaSeparateNumber(item.targets35);
        }
      },
      {
        name: "targets36", title: "ITA<br>%", align: "center",
        itemTemplate: function (value, item) {
          if (!!item.targets34 && !!item.targets35) {
            item.targets36 = Number(item.targets34) / Number(item.targets35);
            return (item.targets36 * 100).toFixed(2);
          }
        }
      },
      // { name: "targets31", title: "UK Property<br>Actual", align: "center"},
      // { name: "targets32", title: "UK Property<br>Goal"  , align: "center"},
      // { name: "targets33", title: "UK Property<br>%"     , align: "center"},
      // { name: "targets34", title: "ITA<br>Actual"        , align: "center"},
      // { name: "targets35", title: "ITA<br>Goal"          , align: "center"},
      // { name: "targets36", title: "ITA<br>%"             , align: "center"},
    ],
    onItemUpdated: function (args) {
      $("#cFiles4").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "targets1": 0, "targets13": 0, "targets25": 0, "targets2": 0, "targets14": 0, "targets26": 0, "targets4": 0, "targets16": 0, "targets28": 0, "targets5": 0, "targets17": 0, "targets29": 0, "targets7": 0, "targets19": 0, "targets31": 0, "targets8": 0, "targets20": 0, "targets32": 0, "targets10": 0, "targets22": 0, "targets34": 0, "targets11": 0, "targets23": 0, "targets35": 0, IsTotal: true };

      items.forEach(function (item) {
        total.targets1 += (Number(item.targets1) || 0);
        total.targets13 += (Number(item.targets13) || 0);
        total.targets25 += item.targets25;

        total.targets2 += (Number(item.targets2) || 0);
        total.targets14 += (Number(item.targets14) || 0);
        total.targets26 += item.targets26;

        total.targets4 += (Number(item.targets4) || 0);
        total.targets16 += (Number(item.targets16) || 0);
        total.targets28 += item.targets28;

        total.targets5 += (Number(item.targets5) || 0);
        total.targets17 += (Number(item.targets17) || 0);
        total.targets29 += item.targets29;

        total.targets7 += (Number(item.targets7) || 0);
        total.targets19 += (Number(item.targets19) || 0);
        total.targets31 += item.targets31;

        total.targets8 += (Number(item.targets8) || 0);
        total.targets20 += (Number(item.targets20) || 0);
        total.targets32 += item.targets32;

        total.targets10 += (Number(item.targets10) || 0);
        total.targets22 += (Number(item.targets22) || 0);
        total.targets34 += item.targets34;

        total.targets11 += (Number(item.targets11) || 0);
        total.targets23 += (Number(item.targets23) || 0);
        total.targets35 += item.targets35;
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  });
  $("#e_sales_year").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    // autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        return jsgridAjax(filter, "fin_sales_year", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "fin_sales_year", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "fin_sales_year", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "fin_sales_year", "DELETE");
      },
    },
    fields: [
      { type: "control", title: "刪除", align: "center" },
      { name: "sales_year", title: "年度", type: "text", align: "center" },
    ],
    onDataLoading: function (args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function (args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function (args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#e_sales_year").jsGrid("loadData");
    },
    onItemDeleted: function (args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#e_sales_year").jsGrid("loadData");
    },
    onRefreshed: function () {
      var $gridData = $("#e_sales_year .jsgrid-grid-body tbody");
      $gridData.sortable({
        update: function (e, ui) {
          var items = $.map($gridData.find("tr"), function (row) {
            return $(row).data("JSGridItem");
          });
          // console.log("Reordered items", items);
          Meteor.call("updateSort_sales_year", items, function (error, result) {
            if (error) {
              console.log("error from Hrform: ", error);
            }
            else {
              $("#e_sales_year").jsGrid("loadData");
            }
          });
        }
      });
    }
  });

  //打開modal讀取jsGrid調整尺寸
  $('#myModal3').on('shown.bs.modal', function (event) {
    $("#e_sales_year").jsGrid("loadData");
  });

  //關閉modal清空資料
  $('#myModal3').on('hide.bs.modal', function (event) {
    $("#sales_year").val("");
  });

  //全選打勾
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });
  $("#checkAll2").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });
  $('#myModal1').on('show.bs.modal', function (event) {
    $(".check").prop('checked', false);
  });
  $('#myModal2').on('show.bs.modal', function (event) {
    $(".check").prop('checked', false);
  });

  var sy = $("#sel_sales_year").find("option:selected").text();
  $('.year1').text(sy)

  var sy_1 = $("#sel_sales_year_1").find("option:selected").text();
  $('.year2').text(sy_1)

  $("#csv_comm_1").on('click', function (event) {
    var csv_year = $("#sel_sales_year_1").find("option:selected").text();
    var args = [$("#cFirst_Half_Year"), '' + csv_year + '上半年實收佣金.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_comm_2").on('click', function (event) {
    var csv_year = $("#sel_sales_year_1").find("option:selected").text();
    var args = [$("#cSecond_Half_Year"), '' + csv_year + '下半年實收佣金.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_comm_3").on('click', function (event) {
    var csv_year = $("#sel_sales_year_1").find("option:selected").text();
    var args = [$("#cFull_Year"), '' + csv_year + '全年度實收佣金.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_sales_1").on('click', function (event) {
    var csv_year = $("#sel_sales_year").find("option:selected").text();
    var args = [$("#cFiles2"), '' + csv_year + '上半年業績目標.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_sales_2").on('click', function (event) {
    var csv_year = $("#sel_sales_year").find("option:selected").text();
    var args = [$("#cFiles3"), '' + csv_year + '下半年業績目標.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_sales_3").on('click', function (event) {
    var csv_year = $("#sel_sales_year").find("option:selected").text();
    var args = [$("#cFiles4"), '' + csv_year + '全年度業績目標.csv'];
    exportTableToCSV.apply(this, args);
  });
};
Template.fin_sales.helpers({
  acc_year: Accountyear.find({}, { sort: { value: 1 } }),
  fin_sales: Meteor.users.find({}),
  get_sales_year: Fin_sales_year.find({}, { sort: { order_id: 1 } }),
  get_user: function () {
    var arr = Meteor.users.find({
      $or: [
        { 'auth.is_auth': '1' },
        {
          'auth.is_auth':
            { $exists: false }
        }
      ]
    }).fetch();
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
      var obj = {};
      obj._id = arr[i]._id;
      obj.engname = arr[i].profile.engname;
      obj.chtname = arr[i].profile.chtname;

      resArr.push(obj);
    }

    return resArr;
  },
});

Template.fin_sales.events({
  'change #sel_sales_year': function (event) {//切換業績年度
    var sy = $("#sel_sales_year").find("option:selected").text();
    $('.year1').text(sy)
    $("#cFiles2").jsGrid("loadData");
    $("#cFiles3").jsGrid("loadData");
    $("#cFiles4").jsGrid("loadData");
  },
  'change #sel_sales_year_1': function (event) {//切換佣金年度
    var sy = $("#sel_sales_year_1").find("option:selected").text();
    $('.year2').text(sy)
    $("#cFirst_Half_Year").jsGrid("loadData");
    $("#cSecond_Half_Year").jsGrid("loadData");
    $("#cFull_Year").jsGrid("loadData");
  },

  'click .btn-new-sales': function () {
    var formVar = {};
    $("#cFiles_level").jsGrid("insertItem", formVar).done(function (ret) {
      if (ret.insert == "success") {
      } else {
      }
    });
  },
  'click .btn-m1-new': function () {
    var formVar = $(".form-modal1").serializeObject();
    var sy = $("#sel_sales_year_1").find("option:selected").text();
    formVar.year = sy;
    Meteor.call("addSalesMember1", formVar, function (error, result) {
      if (error) {
        console.log("error from updateSortUploads: ", error);
      }
      else {
        $("#cFirst_Half_Year").jsGrid("loadData");
        $("#cSecond_Half_Year").jsGrid("loadData");
        $("#cFull_Year").jsGrid("loadData");
      }
    });
  },
  'click .btn-m2-new': function () {
    var formVar = $(".form-modal2").serializeObject();
    var sy = $("#sel_sales_year").find("option:selected").text();
    formVar.year = sy;
    Meteor.call("addSalesMember", formVar, function (error, result) {
      if (error) {
        console.log("error from updateSortUploads: ", error);
      }
      else {
        $("#cFiles2").jsGrid("loadData");
        $("#cFiles3").jsGrid("loadData");
        $("#cFiles4").jsGrid("loadData");
      }
    });
  },
  'click #unedit-form-btn': function () {
    // Session.set('counter', Session.get('counter') + 1);
    $('.my-xedit').editable('destroy');
    $("#unedit-form-btn").hide();
    $("#edit-form-btn").show();
    $(".on-editing").hide();
  },
  'change #sel-fin_sales': function (event, template) {
    // console.log($(event.target).val());
    // var u = Meteor.users.findOne({_id:$(event.target).val()});
    // $("#username").text(u.username);
    // $("#cht-name").text(u.profile.name);
    // Session.set('counter', Session.get('counter') + 1);

    Router.go("/fin_sales/" + $(event.target).val());
    // Template.fin_sales.loadData(this);

  },
  'submit .form-modal1': function (event) {
    event.preventDefault();
    event.stopPropagation();
    var formVar = $(".form-modal1").serializeObject();
    $("#e_sales_year").jsGrid("insertItem", formVar).done(function (ret) {
      if (ret.insert == "success") {
        // $('#modal2').closeModal();
        // $('#myModal1').modal('hide');
        $(".form-modal1").trigger('reset');
        $("#e_sales_year").jsGrid("loadData");
        // Materialize.toast('資料已新增', 3000, 'rounded');
      } else {
        $(".modal1-error").text(ret);
      }
    });
  },
});
