Template.birthreminder.rendered = function(){
    Template.semicolon.loadjs();
    $("#birthreminder").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        // paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.bith = "1"
                filter.periodmail7 = '1'
                var sa = $("#sel_agent").find("option:selected").val();
                if (sa != "-1") { filter.agent_id = sa };
                return jsgridAjax(filter, "clients", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "clients", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "clients", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "clients", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40, deleteButton: false },
            { name: "agent_text", title: "Agent", width:80, align: "center"},
            { name: "name_cht", title: "客戶姓名", width:80, align: "center",
                itemTemplate: function(value, item) {
                    return '<a target="_blank" href="/client/'+item._id+'">'+item.name_cht+'</a>';
                }
            },
            // { name: "email", title: "電子信箱", align: "left"},
            { name: "birthday", title: "客戶生日", align: "center"},
            { name: "psps", title: "備註", type: "text", width:220},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#birthreminder").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#birthreminder").jsGrid("loadData");
        },
        rowClass: function(item, itemIndex) {
            let Bth_Mon = (new Date (item.birthday)).getMonth() + 1;
            let d_Mon = (new Date()).getMonth() + 1;//現在的月份

            if (Bth_Mon !== d_Mon) {
                return "row-danger";
            }
        },
    });
};
Template.birthreminder.helpers({
    get_agent: Agents.find(),
});

Template.birthreminder.events({
    'change #sel_agent': function(event) {
        $("#birthreminder").jsGrid("loadData");
    },
    'click .btn-sendEmails': function() {
        if($("#sel_agent").val() == -1){
            alert("請先選擇要寄送的Agent");
            return;
        }
        if(!confirm("確定寄送生日提醒給Agent?")){
            return;
        }
        var data = $("#birthreminder").jsGrid("option", "data");

        $("#birthreminder2").html($("#birthreminder").html());

        $("#birthreminder2 .jsgrid-grid-body table").prepend("<tr class='jsgrid-header-row'>"+$("#birthreminder2 .jsgrid-grid-header table tr").html()+"</tr>");
        var row_html = $("#birthreminder2 .jsgrid-grid-body").html();

        // var table_html = '<base href="http://system.hanburyifa.com/" />' + $("#birthreminder").html().replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");
        // var table_html = '<base href="http://system.hanburyifa.com/" />' + row_html.replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");

        row_html = row_html.replace(/<th class=\"jsgrid-header-cell jsgrid-control-field jsgrid-align-center\" style=\"width: 40px;\"><\/th>/g, "");
        var table_html = '<base href="http://system.hanburyifa.com/" />' + row_html.replace(/<td class=\"jsgrid-cell jsgrid-control-field jsgrid-align-center\" style=\"width: 40px;\"><input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\"><\/td>/g, "");

        Meteor.call("sendBirthdayReminder", data, table_html, function(error, result){
            if(error){
                console.log("error from updateSortUploads: ", error);
            }
            else {
                alert("已成功送出生日提醒清單\n"+ result);
            }
        });
    },
});
