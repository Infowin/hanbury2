Template.hrform_management.rendered = function(){
    Template.semicolon.loadjs();

    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 20,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                return jsgridAjax(filter, "hrform_management", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "hrform_management", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "hrform_management", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "hrform_management", "DELETE");
            },
        },
        fields: [
            { type: "control", width:50, },
            { name: "name", title: "檔案名稱", type: "text", align: "left"},
            { name: "preview", width:50, align:"left", title: "預覽",
                itemTemplate: function(value, item) {
                    var str = "";
                    if(!!item.url){
                        str = str + '<a href="#" data-num="1" data-toggle="modal" data-target="#myModalFile" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+"[預覽]"+'</a>';
                        str = str + " ";
                    }
                    return str;
                }
            },
            { name: "display", title: "是否顯示於人事規章", width: 60, type: "select", items: [{ id: "2", value: "顯示" },{ id: "3", value: "不顯示" }], valueField: "id", textField: "value"  },
            { name: "remark", title: "備註", width:60, type:"text", align: "left"},
            // { name: "createdByName", width:70, title: "檔案建立者", type: "text" },
            // { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],/*
        onDataLoading: function(args) {
        },
        onItemInserted: function(args) {
        },
        onItemUpdated: function(args) {
            console.log(args);
            $("#aProduct").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#aProduct").jsGrid("loadData");
        },*/
        onRefreshed: function() {
            var $gridData = $("#cFiles .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortHrform", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#cFiles").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });

    $("#input-3").fileinput({
        'language': "zh-TW",
    });
    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var index = button.data("index");
        var data = $("#cFiles").data("JSGrid").data[index];
        var url = button.data("url");
        $("#file_iframe").attr("src", url);
    });
    //預覽文件,關閉modal
    $('#myModalFile').on('hide.bs.modal', function (event) {
    $("#file_iframe").attr("src", "");
    });
};

Template.hrform_management.helpers({
    "files": function(){
        return S3.collection.find();
    }
});

Template.hrform_management.events({
    "click a.upload": function(){
        $(".progress").hide();
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                client: "1", // 1:尚未送出 2: 已送出
                client_id: now_id,
                name: r.file.original_name,
                // size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    }
});

