Template.clientslist.rendered = function() {
    Template.semicolon.loadjs();

    var $container = $('#portfolio'); 

    $container.isotope({
        transitionDuration: '0.65s'
    });

    $('#portfolio-filter a').click(function() {
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });

    $('#portfolio-shuffle').click(function() {
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function() {
        $container.isotope('layout');
    });

    Session.set('my-initial', "");

    $("#eproduct2").attr("disabled", true);
    $("#eproduct3").attr("disabled", true);
    $("#eproduct4").attr("disabled", true);

    $("#portfolio").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        noDataContent: "尚無資料",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                var formVar = $(".form-modal1").serializeObject();
                $.extend(filter, formVar);

                if(!!Session.get('my-initial') && Session.get('my-initial').length == 1){
                    filter.initial = Session.get('my-initial');
                }
                if(!!Session.get('my-nowphase') && Session.get('my-nowphase').length == 1){
                    filter.nowphase = Session.get('my-nowphase');
                }
                // filter.field_type_id = "1";

                filter.findData = $("input#search_text").val() || "";
                console.log(filter);

                return jsgridAjax(filter, "portfolio", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "portfolio", "POST");
            },
            updateItem: function(item) {
                // return jsgridAjax(item, "portfolio", "PUT");

                // Meteor.call("updatePortfolio", item._id, item, function(error, result){
                //     if(error){
                //         console.log("error from updatePortfolio: ", error);
                //     }
                //     else{
                //         // console.log(result);
                //         return result;
                //     }
                // });
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "portfolio", "DELETE");
            },
        },
        fields: [
        /*
        client_id: id,
        product1: data.product1,
        product2: data.product2,
        account_num: data.account_num,
        fpi_num: data.fpi_num,
        start_date: data.start_date,
        invest_money: data.invest_money,
        - 項目
        - 項目起啟日(如12/31 或 6/30)
        - 投資金額
        - 幣別
        - 匯款日期(3/1)
        - 第一年利率 x%? (用打的 只能4~10，到小數第一位)
        - 做到7年 (至少前5年)
        - 第一次預計配息 (填數字)
         */
          /*  { name: "review_btn", align: "center", title: "編輯", sorting: false,
                itemTemplate: function(value, item) {
                    // console.log(item);
                    return '<input class="my-open-modal-1" type="button" data-id="'+item._id+'" value="編輯">';
                },
            },*/
            // { type: "control", width:60, editButton: false, width: 60  },
            // { name: "uid", title: "#", width:60 },
            { name: "client_uid", title: "客戶編號", type: "text", width:60},
            { name: "name_cht", title: "客戶", type: "text", width:70, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    if(!!item.name_eng){
                        return '<a target="_blank" href="/client/'+item.client_id+'">'+item.name_cht+"<BR>"+item.name_eng+'</a>';
                    }
                    else if(!!item.name_cht){
                        return '<a target="_blank" href="/client/'+item.client_id+'">'+item.name_cht+'</a>';
                    }
                    else{
                        return '<a target="_blank" href="/client/'+item.client_id+'">(名稱未填)</a>';
                    }
                }
            },
            { name: "contactnum", title: "連絡電話", type: "text", width:80},
            { name: "email", title: "電子信箱", type: "text"},
            // { name: "review_id", title: "審核狀況", type: "select", items: [{id:"",value:""}].concat(objReviewRes), valueField: "id", textField: "value" },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",name:""}].concat(Products.find().fetch()), valueField: "_id", textField: "name"   },
            // { name: "product1_id", title: "投資大類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"   },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",code:""}].concat(Products.find().fetch()), valueField: "code", textField: "code"   },
            // { name: "product2", title: "投資案名", type: "text"},
            // { name: "product1_text", title: "產品類別1", type: "text"  },
            // { name: "product2_text", title: "產品類別2", type: "text"  },
            // { name: "product3_text", title: "產品類別3", type: "text"  },
            { name: "product4_text", title: "投資項目", type: "text",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    if(!!item.product4_text){
                        return '<a target="_blank" href="/portfolio/'+item._id+'">'+item.product4_text+'</a>';
                    }
                    else{
                        return "";
                    }
                }
            },
            { name: "account_num", title: "Policy No.", type: "text", width:90  },
            { name: "agent_text", title: "財務顧問", type: "text", width:60  },
            { name: "provider_chttext", title: "供應商", type: "text", width:60  },
            // { name: "start_date", title: "項目起啟日", type: "text"  },
            // { name: "prod_money", title: "投資金額", type: "text"  },
            // { name: "prod_money_curtype", title: "幣別", type: "select", items: [{id:"",value:""}].concat(objSalaryCash), valueField: "id", textField: "value"   },

            // { name: "prod_apply_book", title: "申請書", type: "text"  },
            // { name: "prod_healthy_check", title: "體檢狀況", type: "text"  },
            // { name: "prod_tele_invest", title: "電話徵信", type: "text"  },
            // { name: "prod_remit_indicate", title: "匯款指示", type: "text"  },
            // { name: "prod_remit_date", title: "匯款日", type: "text"  },
            // { name: "prod_fund_receipt", title: "Fund Receipt", type: "text"  },
            // { name: "prod_certificate_soft_copy", title: "Certificate Soft Copy", type: "text"  },
            // { name: "prod_customer_recv_sign", title: "客戶簽收合約正本", type: "text"  },
            // { name: "prod_new_case_ps", title: "新件進度附註", type: "text"  },


            // { name: "remit_date", title: "匯款日期", type: "text"  },
            // { name: "invest_money_pay_method", title: "供款方式", type: "select", items: [{id:"",value:""}].concat(objPayMethod), valueField: "id", textField: "value"   },
            // { name: "invest_money_period", title: "供款週期", type: "select", items: [{id:"",value:""}].concat(objPaymentFreq), valueField: "id", textField: "value"   },
            // { name: "first_rate", title: "第一年利率", type: "text"  },
            // { name: "first_prepay", title: "第一次預計配息", type: "text"  },

            // { name: "submit_date", title: "提交日", type: "text"  },
            // { name: "start_date", title: "廣達生效日", type: "text"  },
            // { name: "insertedName", title: "建檔人", type: "text" },
            // { name: "insertedAt", title: "建檔日期",
            //     itemTemplate: function(value, item) {
            //         if(!!item.insertedAt)
            //             return (new Date(item.insertedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "updatedName", title: "最後更新者", type: "text" },
            // { name: "updatedAt", title: "最後更新時間",
            //     itemTemplate: function(value, item) {
            //         if(!!item.updatedAt)
            //             return (new Date(item.updatedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "ps", title: "備註", type: "text" },
        ],
        rowClick: function(args) {
            // console.log(args);
        },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#portfolio").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#portfolio").jsGrid("loadData");
        },
    });


};

Template.clientslist.helpers({
    Product1: Product1.find({},{ sort: { order_id: 1 }}),
    Agents: Agents.find(),
    Provider: Provider.find(),
    PT: objPortfolioTemplate,
});

Template.clientslist.loadPortfolio = function(){
    $("#portfolio").jsGrid("loadData");
    $("#portfolio").jsGrid("openPage", 1);
}
Template.clientslist.events({
    'click #newclient-form-btn': function() {
        if(!confirm("確認要新增客戶嗎?")){
            return;
        }
        $.ajax({
            url: '/api/clients',
            type: 'POST',
            data: {
              user_name: $('#user_name').val()
            },
            success: function(response) {
                Router.go("/client/"+response.data._id);
            }
        });
    },
    'change #search_text, keypress #search_text, click #search_text': function (event) { //#
        Template.clientslist.loadPortfolio();
    },
    'click .my-initial': function(event) {
        var letter = "";
        if($(event.target).text()){
            if($(event.target).text().length == 1){
                letter = $(event.target).text();
            }
        }
        Session.set('my-initial', letter);
        // console.log(letter);
        $('a.my-initial').removeClass("my-active");
        $(event.target).addClass("my-active");

        $("#portfolio").jsGrid("loadData");
        $("#portfolio").jsGrid("openPage", 1);
    },
    'click .nowphase_btn': function(event) {
        var data_id = $(event.target).attr("data-id") || "0";
        console.log(data_id);
        if(data_id != "0"){
            Session.set('my-nowphase', data_id);

            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage", 1);
        }
        else{
            Session.set('my-nowphase', "");

            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage", 1);
        }
    },
    // #myTab a').on('click', function(e) {
    'click #myTab a': function(e) {
        // e.preventDefault()
        $(e.target).tab('show')
    },
    'click .btn-showlogin': function() {
        // Session.set('counter', Session.get('counter') + 1);
        $("#portfolio").jsGrid("loadData");
        $("#portfolio").jsGrid("openPage", 1);
    },
    "change select": function(event, template) {
        // console.log(event.currentTarget);
        // console.log(event.target);
        // console.log(template);
        if ($(event.target).hasClass("product")) {
            // $("#eproduct2").attr("disabled", true);
            // $("#eproduct3").attr("disabled", true);
            // $("#eproduct4").attr("disabled", true);

            var nowid = $(event.target).attr("id");
            if (nowid == "eproduct1") {
                $("#eproduct2").removeAttr("disabled");
                $("#eproduct3").attr("disabled", true);
                $("#eproduct4").attr("disabled", true);

                $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#eproduct2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                var fieldtype = $('#eproduct1 :selected').val();

                var type = Product2.find({
                    product1_id: fieldtype.toString()
                }, { sort: { order_id: 1 } }).fetch();

                $.each(type, function(i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.name_cht
                    }));
                });
            } else if (nowid == "eproduct2") {
                $("#eproduct3").removeAttr("disabled");
                $("#eproduct4").attr("disabled", true);

                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 無 --</option>');

                var fieldtype = $('#eproduct2 :selected').val();

                var type = Product3.find({
                    product2_id: fieldtype.toString()
                }, { sort: { order_id: 1 } }).fetch();

                if(!type.length){
                  $("#eproduct3").attr("disabled", true);
                  var sel = $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else{
                  $("#eproduct3").removeAttr("disabled");
                  var sel = $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                  $.each(type, function(i, item2) {
                      sel.append($('<option>', {
                          value: item2._id,
                          text: item2.name_cht
                      }));
                  });
                }
            } else if (nowid == "eproduct3") {
                var fieldtype = $('#eproduct3 :selected').val();

                var type = Product4.find({
                    product3_id: fieldtype.toString()
                }).fetch();

                if(!type.length){
                  $("#eproduct4").attr("disabled", true);
                  var sel = $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else{
                  $("#eproduct4").removeAttr("disabled");
                  var sel = $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                  $.each(type, function(i, item2) {
                      sel.append($('<option>', {
                          value: item2._id,
                          text: item2.name_cht
                      }));
                  });
                }
            }
            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage",1);
        }
    },
});
