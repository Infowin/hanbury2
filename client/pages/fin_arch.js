Template.fin_arch.rendered = function () {
    Template.semicolon.loadjs();
    var ocImages = $("#oc-images");

    var data = this.data;
    $("#title").text(data.value);

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });
    $.fn.editable.defaults.mode = 'inline';
    $("#unedit-form-btn").hide();

    //關帳日
    var arr_ac = Accountyear.find().fetch();
    for (var i = 0; i < arr_ac.length; i++) {
        var entry = arr_ac[i];
        for (var j = 1; j < 13; j++) {
            var lockbook = entry["lockbook" + j];
            if (typeof lockbook == "undefined" || !lockbook) {
                // $("."+entry.value+"_date_"+j).text("尚未關帳");
            } else if (lockbook == "0") { // 已解鎖
                $("." + entry.value + "_date_" + j).text((currentdate(entry["lockbook" + j + "_un_time"])) + " 已解除鎖定");
            } else if (lockbook == "1") { // 關帳
                $("." + entry.value + "_date_" + j).text((currentdate(entry["lockbook" + j + "_time"])) + " 已關帳");
            }
        }

    }

    this.autorun(function () {
        var arr_ac = Accountyear.find().fetch();

        for (var i = 0; i < arr_ac.length; i++) {
            var entry = arr_ac[i];

            entry.month1_total = (Math.round(commaToNumber(entry.month1_total))) || "0";
            entry.month2_total = (Math.round(commaToNumber(entry.month2_total))) || "0";
            entry.month3_total = (Math.round(commaToNumber(entry.month3_total))) || "0";
            entry.month4_total = (Math.round(commaToNumber(entry.month4_total))) || "0";
            entry.month5_total = (Math.round(commaToNumber(entry.month5_total))) || "0";
            entry.month6_total = (Math.round(commaToNumber(entry.month6_total))) || "0";
            entry.month7_total = (Math.round(commaToNumber(entry.month7_total))) || "0";
            entry.month8_total = (Math.round(commaToNumber(entry.month8_total))) || "0";
            entry.month9_total = (Math.round(commaToNumber(entry.month9_total))) || "0";
            entry.month10_total = (Math.round(commaToNumber(entry.month10_total))) || "0";
            entry.month11_total = (Math.round(commaToNumber(entry.month11_total))) || "0";
            entry.month12_total = (Math.round(commaToNumber(entry.month12_total))) || "0";

            //commaToNumber        remove,
            //commaSeparateNumber  add,

            var season_1 = (Math.round(Number(entry.month1_total) + Number(entry.month2_total) + Number(entry.month3_total)));
            var season_2 = (Math.round(Number(entry.month4_total) + Number(entry.month5_total) + Number(entry.month6_total)));
            var season_3 = (Math.round(Number(entry.month7_total) + Number(entry.month8_total) + Number(entry.month9_total)));
            var season_4 = (Math.round(Number(entry.month10_total) + Number(entry.month11_total) + Number(entry.month12_total)));
            var year_total = Math.round(Number(season_1) + Number(season_2) + Number(season_3) + Number(season_4));

            if (isNaN(season_1) == false) {
                $("#ta1-" + entry._id).text(commaSeparateNumber(season_1));
            }
            if (isNaN(season_2) == false) {
                $("#ta2-" + entry._id).text(commaSeparateNumber(season_2));
            }
            if (isNaN(season_3) == false) {
                $("#ta3-" + entry._id).text(commaSeparateNumber(season_3));
            }
            if (isNaN(season_4) == false) {
                $("#ta4-" + entry._id).text(commaSeparateNumber(season_4));
            }
            $("#year_total" + entry._id).text(commaSeparateNumber(year_total));
        }
    });

    var d = new Date();
    $("#acc_year-" + d.getFullYear()).trigger("click");
};

Template.fin_arch.helpers({
    acc_year: Accountyear.find({}, { sort: { value: -1 } }),
    get_month1_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month1_total))) || "0");
    },
    get_month2_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month2_total))) || "0");
    },
    get_month3_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month3_total))) || "0");
    },
    get_month4_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month4_total))) || "0");
    },
    get_month5_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month5_total))) || "0");
    },
    get_month6_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month6_total))) || "0");
    },
    get_month7_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month7_total))) || "0");
    },
    get_month8_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month8_total))) || "0");
    },
    get_month9_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month9_total))) || "0");
    },
    get_month10_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month10_total))) || "0");
    },
    get_month11_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month11_total))) || "0");
    },
    get_month12_total: function () {
        return commaSeparateNumber((Math.round(commaToNumber(this.month12_total))) || "0");
    },
    bg_id: function () {
        var controller = Router.current();
        return controller.params.f1_id;
    }
});

Template.fin_arch.events({
    'click #edit-form-btn': function () {
        // Session.set('counter', Session.get('counter') + 1);
        var now_id = this._id;
        //
        $(".my-xedit-text").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                // title: 'Enter username'
            });
        });
        $(".my-xedit-date").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                format: 'YYYY/MM/DD',
                viewformat: 'YYYY/MM/DD',
                template: 'YYYY / MMMM / D',
                combodate: {
                    minYear: 1900,
                    maxYear: 2020,
                    minuteStep: 1
                }
            });
        });
        $(".my-xedit-sel").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'select',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                // source: ,
                source: function () {
                    var objname = $(this).attr("data-obj");
                    if (objname == "objBodyCheckType1") return objBodyCheckType1;
                    else if (objname == "objDepartment") return objDepartment;
                },
                value: $(this).text()
            });
        });

        $("#unedit-form-btn").show();
        $("#edit-form-btn").hide();
        $(".on-editing").show();


        $('.editable').on('hidden', function (e, reason) {
            // console.log("aaa " + reason);
            if (reason === 'save' || reason === 'nochange') {
                var $next = "";
                if ($(this).next('.editable').length) { // 同td的下一個
                    $next = $(this).next('.editable');
                }
                else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
                    $next = $(this).parent().nextAll().children(".editable").eq(0);
                }
                else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
                    $next = $(this).closest('tr').next().find('.editable').eq(0);
                }

                if ($next) {
                    setTimeout(function () {
                        $next.editable('show');
                    }, 300);
                }
            }
        });
    },
    'click #unedit-form-btn': function () {
        // Session.set('counter', Session.get('counter') + 1);
        $('.my-xedit').editable('destroy');
        $("#unedit-form-btn").hide();
        $("#edit-form-btn").show();
        $(".on-editing").hide();
    },
    'change #sel-fin_arch': function (event, template) {
        // console.log($(event.target).val());
        // var u = Meteor.users.findOne({_id:$(event.target).val()});
        // $("#username").text(u.username);
        // $("#cht-name").text(u.profile.name);
        // Session.set('counter', Session.get('counter') + 1);

        Router.go("/fin_arch/" + $(event.target).val());
        // Template.fin_arch.loadData(this);

    },
});
