Template.applyservice.rendered = function () {
    Template.semicolon.loadjs();

    var $container = $('#portfolio');

    $container.isotope({
        transitionDuration: '0.65s'
    });

    // $('#portfolio-filter a').click(function() {
    //     $('#portfolio-filter li').removeClass('activeFilter');
    //     $(this).parent('li').addClass('activeFilter');
    //     var selector = $(this).attr('data-filter');
    //     $container.isotope({
    //         filter: selector
    //     });
    //     return false;
    // });

    // $('#portfolio-shuffle').click(function() {
    //     $container.isotope('updateSortData').isotope({
    //         sortBy: 'random'
    //     });
    // });

    $(window).resize(function () {
        $container.isotope('layout');
    });

    Session.set('my-initial', "");

    $("#eproduct2").attr("disabled", true);
    $("#eproduct3").attr("disabled", true);
    $("#eproduct4").attr("disabled", true);

    $("#portfolio").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        noDataContent: "尚無資料",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                var formVar = $(".form-modal1").serializeObject();
                $.extend(filter, formVar);

                if (!!Session.get('my-initial') && Session.get('my-initial').length == 1) {
                    filter.initial = Session.get('my-initial');
                }
                if (!!Session.get('my-nowphase') && Session.get('my-nowphase').length == 1) {
                    filter.nowphase = Session.get('my-nowphase');
                }
                // filter.field_type_id = "1";

                filter.findData = $("input#search_text").val() || "";

                return jsgridAjax(filter, "portfolio", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "portfolio", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "portfolio", "PUT");

                // Meteor.call("updatePortfolio", item._id, item, function(error, result){
                //     if(error){
                //         console.log("error from updatePortfolio: ", error);
                //     }
                //     else{
                //         // console.log(result);
                //         return result;
                //     }
                // });
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "portfolio", "DELETE");
            },
        },
        fields: [
            /*
            client_id: id,
            product1: data.product1,
            product2: data.product2,
            account_num: data.account_num,
            fpi_num: data.fpi_num,
            start_date: data.start_date,
            invest_money: data.invest_money,
            - 項目
            - 項目起啟日(如12/31 或 6/30)
            - 投資金額
            - 幣別
            - 匯款日期(3/1)
            - 第一年利率 x%? (用打的 只能4~10，到小數第一位)
            - 做到7年 (至少前5年)
            - 第一次預計配息 (填數字)
             */
            /*  { name: "review_btn", align: "center", title: "編輯", sorting: false,
                  itemTemplate: function(value, item) {
                      // console.log(item);
                      return '<input class="my-open-modal-1" type="button" data-id="'+item._id+'" value="編輯">';
                  },
              },*/
            // { type: "control", width:60, editButton: false, width: 60  },
            // { name: "uid", title: "#", width:60 },
            {
                name: "apply_btn", title: "售後服務", type: "text", width: 50, align: "center",
                itemTemplate: function (value, item) {
                    // console.log(value);
                    // console.log(item);
                    return '<a href="#" data-toggle="modal" data-target="#myModal" data-index="' + item.myItemIndex + '">新申請</a>';
                }
            },
            { name: "client_uid", title: "客戶編號", type: "text", width: 60 },
            {
                name: "name_cht", title: "客戶", type: "text", width: 70, align: "center",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    if (!!item.name_eng) {
                        return '<a target="_blank" href="/client/' + item.client_id + '">' + item.name_cht + "<BR>" + item.name_eng + '</a>';
                    }
                    else if (!!item.name_cht) {
                        return '<a target="_blank" href="/client/' + item.client_id + '">' + item.name_cht + '</a>';
                    }
                    else {
                        return '<a target="_blank" href="/client/' + item.client_id + '">(名稱未填)</a>';
                    }
                }
            },
            { name: "contactnum", title: "連絡電話", type: "text", width: 80 },
            { name: "email", title: "電子信箱", type: "text" },
            // { name: "review_id", title: "審核狀況", type: "select", items: [{id:"",value:""}].concat(objReviewRes), valueField: "id", textField: "value" },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",name:""}].concat(Products.find().fetch()), valueField: "_id", textField: "name"   },
            // { name: "product1_id", title: "投資大類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"   },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",code:""}].concat(Products.find().fetch()), valueField: "code", textField: "code"   },
            // { name: "product2", title: "投資案名", type: "text"},
            // { name: "product1_text", title: "產品類別1", type: "text"  },
            // { name: "product2_text", title: "產品類別2", type: "text"  },
            // { name: "product3_text", title: "產品類別3", type: "text"  },
            {
                name: "product4_text", title: "投資項目", type: "text",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    if (!!item.product4_text) {
                        return '<a target="_blank" href="/portfolio/' + item._id + '">' + item.product4_text + '</a>';
                    }
                    else {
                        return "";
                    }
                }
            },
            { name: "account_num", title: "Policy No.", type: "text", width: 90 },
            { name: "agent_text", title: "財務顧問", type: "text", width: 60 },
            { name: "provider_chttext", title: "供應商", type: "text", width: 60 },
            // { name: "start_date", title: "項目起啟日", type: "text"  },
            // { name: "prod_money", title: "投資金額", type: "text"  },
            // { name: "prod_money_curtype", title: "幣別", type: "select", items: [{id:"",value:""}].concat(objSalaryCash), valueField: "id", textField: "value"   },

            // { name: "prod_apply_book", title: "申請書", type: "text"  },
            // { name: "prod_healthy_check", title: "體檢狀況", type: "text"  },
            // { name: "prod_tele_invest", title: "電話徵信", type: "text"  },
            // { name: "prod_remit_indicate", title: "匯款指示", type: "text"  },
            // { name: "prod_remit_date", title: "匯款日", type: "text"  },
            // { name: "prod_fund_receipt", title: "Fund Receipt", type: "text"  },
            // { name: "prod_certificate_soft_copy", title: "Certificate Soft Copy", type: "text"  },
            // { name: "prod_customer_recv_sign", title: "客戶簽收合約正本", type: "text"  },
            // { name: "prod_new_case_ps", title: "新件進度附註", type: "text"  },


            // { name: "remit_date", title: "匯款日期", type: "text"  },
            // { name: "invest_money_pay_method", title: "供款方式", type: "select", items: [{id:"",value:""}].concat(objPayMethod), valueField: "id", textField: "value"   },
            // { name: "invest_money_period", title: "供款週期", type: "select", items: [{id:"",value:""}].concat(objPaymentFreq), valueField: "id", textField: "value"   },
            // { name: "first_rate", title: "第一年利率", type: "text"  },
            // { name: "first_prepay", title: "第一次預計配息", type: "text"  },

            // { name: "submit_date", title: "提交日", type: "text"  },
            // { name: "start_date", title: "廣達生效日", type: "text"  },
            // { name: "insertedName", title: "建檔人", type: "text" },
            // { name: "insertedAt", title: "建檔日期",
            //     itemTemplate: function(value, item) {
            //         if(!!item.insertedAt)
            //             return (new Date(item.insertedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "updatedName", title: "最後更新者", type: "text" },
            // { name: "updatedAt", title: "最後更新時間",
            //     itemTemplate: function(value, item) {
            //         if(!!item.updatedAt)
            //             return (new Date(item.updatedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "ps", title: "備註", type: "text" },
        ],
        rowClick: function (args) {
            // console.log(args);
        },
        onDataLoading: function (args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function (args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function (args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#portfolio").jsGrid("loadData");
        },
        onItemDeleted: function (args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#portfolio").jsGrid("loadData");
        },
    });
    $("#e_followservice").jsGrid({
        width: "100%",
        paging: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                return jsgridAjax(filter, "afterservice", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "afterservice", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "afterservice", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "afterservice", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, editButton: false },
            { name: "uid", title: "#", width: 60 },
            { name: "value", title: "會計科目", type: "text" },
        ],
    });
    $("#e_nopdfform").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                return jsgridAjax(filter, "nopdfform", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "nopdfform", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "nopdfform", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "nopdfform", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, editButton: false },
            // { name: "uid", title: "#", width: 60},
            // { name: "isopen", type: "checkbox", title: "開啟" },
            { name: "nopdf", title: "服務項目", type: "text" },
        ],
        onRefreshed: function () {
            var $gridData = $("#e_nopdfform .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function (e, ui) {
                    var items = $.map($gridData.find("tr"), function (row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortNopdfform", items, function (error, result) {
                        if (error) {
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_nopdfform").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var index = button.data("index");
        var data = $("#portfolio").data("JSGrid").data[index];

        console.log(data);

        Session.set("applyservice_portfolio", data);

        Meteor.call("getClientData", data.client_id, function (error, result) {
            if (error) {
                console.log("error from updatePortfolio: ", error);
            }
            else {
                $("#modal-identify").val(result.identify);
                $("#modal-addr1_eng").val(result.addr1_eng);
                $("#modal-policy_currency").val(result.policy_currency);
                $("#modal-policyxx_num").val(result.policyxx_num);
                $("#modal-birthday").val(result.birthday);
                return 1;
            }
        });

        $("#as_pid").val(data._id); // input 欄位 給值
        $("#modal-name_cht").text(data.name_cht);
        $("#modal-product").text(data.product4_text + " (" + data.product4_engtext + ")");//產品名稱中+英
        $("#modal-insertedBy_CE").val(data.product4_text + " (" + data.product4_engtext + ")");//產品名稱中+英
        $("#modal-name_eng").val(data.name_eng);
        // $("#modal-contactnum").val("+886-"+((data.contactnum).slice(1))); //(slice)取得切割字串
        $("#modal-email").val(data.email);
        $("#modal-agent_text").val(data.agent_text);
        $("#modal-account_num").val(data.account_num);//計劃號碼
        $("#modal-account_num2").val(data.account_num2);//保單號碼
        $("#modal-policy_num").val(data.policy_num);
        $("#modal-insertedBy").val(data.product4_engtext);//產品名稱英
        $("#modal-trust_name").val(data.trust_name);
        $("#modal-trust_trustee").val(data.trust_trustee);
        $("#modal-trust_ssnitin").val(data.trust_ssnitin);
        $("#modal-trust_lawyer").val(data.trust_lawyer);
        $("#modal-trust_grantor").val(data.trust_grantor);
        $("#modal-beneficial1_name").val(data.beneficial1_name);
        $("#modal-beneficial1_name2").val(data.beneficial1_name2);
        $("#modal-beneficial1_relationship").val(data.beneficial1_relationship);
        $("#modal-beneficial2_name").val(data.beneficial2_name);
        $("#modal-beneficial3_name").val(data.beneficial3_name);
        $("#modal-beneficial4_name").val(data.beneficial4_name);
        $("#modal-beneficial1_percent").val(data.beneficial1_percent);
        $("#modal-beneficial2_percent").val(data.beneficial2_percent);
        $("#modal-beneficial3_percent").val(data.beneficial3_percent);
        $("#modal-beneficial4_percent").val(data.beneficial4_percent);

        if (!!data.arrWithdrawBankAccount && data.arrWithdrawBankAccount.length >= 1) {
            $("#modal-wdba_bankname").val(data.arrWithdrawBankAccount[0].wdba_bankname || "");//銀行名稱
            $("#modal-wdba_bankaddr").val(data.arrWithdrawBankAccount[0].wdba_bankaddr || "");//銀行地址
            $("#modal-wdba_holdernum").val(data.arrWithdrawBankAccount[0].wdba_holdernum || "");//戶口號碼
            $("#modal-wdba_swift_bic").val(data.arrWithdrawBankAccount[0].wdba_swift_bic || "");//SWIFT / BIC 編碼
        }

        $("#modal-last_name").val(data.name_eng);
        $("#modal-first_name").val(data.name_eng);
        $("#modal-apply_passport").val(data.apply_passport);//護照號碼
        $("#modal-apply_telephone").val(data.apply_telephone);//連絡電話
        $("#modal-apply_cellphone").val(data.apply_cellphone);//手機號碼
        $("#modal-hsbc_account_name").val(data.hsbc_account_name);//HSBC帳戶姓名
        $("#modal-hsbc_account_num").val(data.hsbc_account_num);//HSBC帳戶號碼
        $("#modal-creditcard_num").val(data.creditcard_num);//信用卡號碼
        $("#modal-creditcard_bank").val(data.creditcard_bank);//信用卡銀行
        $("#modal-creditcard_bankname").val(data.creditcard_bankname);//發卡銀行國家
        $("#modal-fpi_now_same_payment").val(data.fpi_now_same_payment);//現時定額供款
        $("#modal-policy_currency_type").val(funcObjFind2(objSalaryCash_eng, data.policy_currency_type));//保單幣別
        $("#modal-paymethod_fpi").val(funcObjFind2(objPayMethodFPI, data.paymethod_fpi));//供款方式
        $("#modal-payperiod_fpi").val(funcObjFind2(objPayPeriodFPI, data.payperiod_fpi));//供款週期
        $("#modal-beneficiary_bank_location").val(data.beneficiary_bank_location);
        $("#modal-beneficiary_bank_name").val(data.beneficiary_bank_name);
        $("#modal-beneficiary_bank_address").val(data.beneficiary_bank_address);
        $("#modal-swift_code").val(data.swift_code);
        $("#modal-beneficiary_account_number").val(data.beneficiary_account_number);
        $("#modal-beneficiary_name").val(data.beneficiary_name);
        $("#modal-policy_firstperson").val(data.policy_firstperson);//保單持有人
        $("#modal-policy_secondperson").val(data.policy_secondperson);//第二保單持有人

        if (!!data.agent_id) {
            $("#modal-agent_id").val(data.agent_id);//agent
        }
        if (!!data.creditcard_name) {
            $("#modal-creditcard_name").val(data.creditcard_name);//agent
        }
        if (!!data.creditcard_addr) {
            $("#modal-creditcard_addr").val(data.creditcard_addr);//agent
        }

        if (!!data.contactnum && (data.contactnum).charAt(0) == 0) {
            $("#modal-contactnum").val("+886-" + ((data.contactnum).slice(1)));
        }
        if (!!data.creditcard_date && typeof data.creditcard_date == "string") {
            $("#modal-creditcard_term_month").val((data.creditcard_date).slice(0, 2));//信用卡效期(月)
            $("#modal-creditcard_term_year").val((data.creditcard_date).slice(3));//信用卡效期(年)
        }
        if (!!data.fpi_now_same_payment) {
            $("#modal-fpi_now_same_payment_words").val(numToWords(data.fpi_now_same_payment));//現時定額供款(字)
        }

        // 把會用到的值 寫入input欄位
        $("#as_template_iframe").attr("src", "");
        $("#as_template option[value='']").attr('selected', true);
    });
};

Template.applyservice.helpers({
    Product1: Product1.find(),
    Agents: Agents.find(),
    Provider: Provider.find(),
    PT: objPortfolioTemplate,
    Nopdfform: Nopdfform.find({}, { sort: { order_id: 1 } }),
});

Template.applyservice.loadPortfolio = function () {
    $("#portfolio").jsGrid("loadData");
    $("#portfolio").jsGrid("openPage", 1);
}
Template.applyservice.events({
    /* 'click #newclient-form-btn': function() {
         if(!confirm("確認要新增客戶嗎?")){
             return;
         }
         $.ajax({
             url: '/api/clients',
             type: 'POST',
             data: {
               user_name: $('#user_name').val()
             },
             success: function(response) {
                 Router.go("/client/"+response.data._id);
             }
         });
     },*/
    'click .btn-new': function () {
        console.log("insertion completed");
        var formVar = {};

        $("#e_nopdfform").jsGrid("insertItem", formVar).done(function (ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                // $('#myModal').modal('hide');
                // $(".form-modal2").trigger('reset');
                // $("#e_interestcondition").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                // $(".modal2-error").text(ret);
            }
        });
    },
    'change #search_text, keypress #search_text, click #search_text': function (event) { //#
        Template.applyservice.loadPortfolio();
    },
    'click .my-initial': function (event) {
        var letter = "";
        if ($(event.target).text()) {
            if ($(event.target).text().length == 1) {
                letter = $(event.target).text();
            }
        }
        Session.set('my-initial', letter);
        // console.log(letter);
        $('a.my-initial').removeClass("my-active");
        $(event.target).addClass("my-active");
        $("#portfolio").jsGrid("loadData");
        $("#portfolio").jsGrid("openPage", 1);
    },
    'click .nowphase_btn': function (event) {
        var data_id = $(event.target).attr("data-id") || "0";
        // console.log(data_id);
        if (data_id != "0") {
            Session.set('my-nowphase', data_id);

            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage", 1);
        }
        else {
            Session.set('my-nowphase', "");

            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage", 1);
        }
    },
    'click .btn-myupdate': function () {
        var template_id = $("#as_template :selected").val();

        var formVar = $(".form-modal2").serialize();

        var src = "api/pdfafterservice" + template_id + "?" + (formVar
        );

        $("#as_template_iframe").attr("src", src);
        $("#as_template_iframe").attr("height", $(".modal-body").height() + "px");

        $("#modal2-link").html('<a target="_blank" href="' + src + '">(開新視窗)</a>');
    },
    'click .btn-showlogin': function () {
        // Session.set('counter', Session.get('counter') + 1);
        $("#portfolio").jsGrid("loadData");
        $("#portfolio").jsGrid("openPage", 1);
    },
    'click .btn-mycancel': function () {
        $("#as_template option[value='']").attr('selected', true);
    },
    'click .btn-saveprint': function () {
        var template_id = $("#as_template :selected").val();
        if (!template_id) { alert("請先選擇 服務項目"); return; }
        if (template_id == "25" || template_id == "28") {
            if (!confirm("確認更新客戶資料?")) {
                return;
            }
        }
        var formVar = $(".form-modal2").serializeObject();

        formVar.status = "1";
        formVar.status_text = "申請中";
        // formVar.url = $(".form-modal2").serialize();
        formVar.as_template_id = $("#as_template option:selected").val();
        formVar.as_template_text = $("#as_template option:selected").text();
        // formVar.portfolio = Session.get("applyservice_portfolio");
        formVar.as_owner_id = Meteor.userId();
        formVar.as_owner_text = Meteor.user().profile.engname;
        formVar.as_action = "2";
        formVar.as_action_owner = "4";
        // formVar.p_agent_id = ;

        $("#e_followservice").jsGrid("insertItem", formVar).done(function (ret) {
            // console.log("insertion completed");
            // console.log(ret);

            if (ret.insert == "success") {
                Router.go("/followservice");
                // $('#modal2').closeModal();
                // $('#myModal').modal('hide')
                $('.modal-backdrop').hide();
                $(".form-modal2").trigger('reset');
                // $("#e_account").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
            // location.reload();
        });
        $("#as_template_iframe").get(0).contentWindow.print();
    },
    'click .btn-print': function () {
        var template_id = $("#as_template :selected").val();
        if (!template_id) { alert("請先選擇 服務項目"); return; }
        if (template_id == "25" || template_id == "28") {
            if (!confirm("確認更新客戶資料?")) {
                return;
            }
        }
        $("#as_template_iframe").get(0).contentWindow.print();
    },
    /*'click .btn-example': function() {
        var src = $("#as_template option:selected").data("exurl");
        $("#as_template_iframe").attr("src", src);
        $("#as_template_iframe").attr("height", $(".modal-body").height()+"px");
        $("#ex-link").html('<a target="_blank" href="'+src+'"></a>');
    },*/
    'click .btn-save': function () {
        var template_id = $("#as_template :selected").val();
        if (!template_id) { alert("請先選擇 服務項目"); return; }

        if (template_id == "25" || template_id == "28") {
            if (!confirm("確認更新客戶資料?")) {
                return;
            }
        }

        var formVar = $(".form-modal2").serializeObject();

        formVar.status = "1";
        formVar.status_text = "申請中";
        // formVar.url = $(".form-modal2").serialize();
        formVar.as_template_id = $("#as_template option:selected").val();
        formVar.as_template_text = $("#as_template option:selected").text();
        // formVar.portfolio = Session.get("applyservice_portfolio");
        formVar.as_owner_id = Meteor.userId();
        formVar.as_owner_text = Meteor.user().profile.engname;
        formVar.as_action = "2";
        formVar.as_action_owner = "4";

        console.log(formVar);
        $("#e_followservice").jsGrid("insertItem", formVar).done(function (ret) {
            // console.log("insertion completed");
            // console.log(ret);

            if (ret.insert == "success") {
                $('#myModal').modal('hide');
                $('.modal-backdrop').hide()
                Router.go("/followservice");
                // $('#modal2').closeModal();
                $(".form-modal2").trigger('reset');
                // $("#e_account").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
            // location.reload();
        });
    },
    "change select": function (event, template) {
        /*console.log(event.currentTarget);
        console.log(event.target);
        console.log(template);*/
        if ($(event.target).hasClass("as_template")) {
            var template_id = $("#as_template :selected").val();
            // if(template_id == "25" || template_id == "28"){
            //     if(!confirm("確認更新客戶資料?")){
            //         return;
            //     }
            // }
            for (var i = 1; i < 16; i++) {
                $("#modal-custom_" + i).val("custom_" + i);
            }

            var formVar = $(".form-modal2").serialize();

            var src = "api/pdfafterservice" + template_id + "?" + (formVar);

            $("#as_template_iframe").attr("src", src);
            $("#as_template_iframe").attr("height", $(".modal-body").height() + "px");

            $("#modal2-link").html('<a target="_blank" href="' + src + '">(開新視窗)</a>');

            var exurl = $("#as_template option:selected").data("exurl");
            if (exurl) {
                $("#ex-link").html('<a target="_blank" href="' + exurl + '"><button type="button" class="btn btn-info" id="btn-ex-link">範例</button></a>');
            }
            else {
                $("#ex-link").html('');
            }

            if (event.target.value.length > 4) {
                $("#as_template_iframe").attr("src", " ");
            }
        }
        else if ($(event.target).hasClass("product")) {
            var nowid = $(event.target).attr("id");
            if (nowid == "eproduct1") {
                $("#eproduct2").removeAttr("disabled");
                $("#eproduct3").attr("disabled", true);
                $("#eproduct4").attr("disabled", true);

                $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#eproduct2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                var fieldtype = $('#eproduct1 :selected').val();

                var type = Product2.find({
                    product1_id: fieldtype.toString()
                }, { sort: { product2_id: 1 } }).fetch();

                $.each(type, function (i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.name_cht
                    }));
                });
            } else if (nowid == "eproduct2") {
                $("#eproduct3").removeAttr("disabled");
                $("#eproduct4").attr("disabled", true);

                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 無 --</option>');

                var fieldtype = $('#eproduct2 :selected').val();

                var type = Product3.find({
                    product2_id: fieldtype.toString()
                }, { sort: { product3_id: 1 } }).fetch();

                if (!type.length) {
                    $("#eproduct3").attr("disabled", true);
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else {
                    $("#eproduct3").removeAttr("disabled");
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht
                        }));
                    });
                }
            } else if (nowid == "eproduct3") {
                var fieldtype = $('#eproduct3 :selected').val();

                var type = Product4.find({
                    product3_id: fieldtype.toString()
                }).fetch();

                if (!type.length) {
                    $("#eproduct4").attr("disabled", true);
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else {
                    $("#eproduct4").removeAttr("disabled");
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht
                        }));
                    });
                }
            }
            $("#portfolio").jsGrid("loadData");
            $("#portfolio").jsGrid("openPage", 1);
        }
    },
});
