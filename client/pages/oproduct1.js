Template.oproduct1.loadP4 = function() {
    var id = Session.get("nowrow_oproduct4")._id;
    $("#cFiles").jsGrid("loadData");
    Meteor.call("getP4obj", id, function(error, result){
        var p4 = result; // Session.get("nowrow_oproduct4");

        // console.log("p4: ");
        // console.log(p4);

        $("#op4-big-pic").attr("src", p4.showpic1 || "");
        $("#op4-big-pic2").attr("src", p4.showpic2 || "");
        $("#title1").val(p4.title1);
        $("#title2").val(p4.title2);
        $("#youtube_url").val(p4.youtube_url);
        $("#video_url").val(p4.video_url);

        $("#intro_title1").val(p4.intro_title1);
        $("#intro_content1").val(p4.intro_content1);
        $("#intro_title2").val(p4.intro_title2);
        $("#intro_content2").val(p4.intro_content2);
        $("#intro_title3").val(p4.intro_title3);
        $("#intro_content3").val(p4.intro_content3);

        $("#youtube_title").val(p4.youtube_title);
        $("#youtube_content").val(p4.youtube_content);
        $("#video_title").val(p4.video_title);
        $("#video_content").val(p4.video_content);

        $('#intro_content1').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('#intro_content2').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('#intro_content3').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('.dropdown-toggle').dropdown();
    /*    if (!$('#intro_content1').data('froala.editor')) {
          $('#intro_content1').froalaEditor({
            language: 'zh_tw'
          });
        }
        if (!$('#intro_content2').data('froala.editor')) {
          $('#intro_content2').froalaEditor({
            language: 'zh_tw'
          });
        }
        if (!$('#intro_content3').data('froala.editor')) {
          $('#intro_content3').froalaEditor({
            language: 'zh_tw'
          });
        }*/
    });
}
Template.oproduct1.rendered = function() {
    Template.semicolon.loadjs();

    // var $container = $('#portfolio');

    // $container.isotope({
    //     transitionDuration: '0.65s'
    // });

   /* $('#portfolio-filter a').click(function() {
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });

    $('#portfolio-shuffle').click(function() {
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });*/

    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,

        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                // var item = Session.get("nowrow");
                var p4_id = Session.get("nowrow_oproduct4")._id;
                filter.carousel = "1";
                filter.oproduct4_id = p4_id;

                // var item = Session.get("nowrow_product");
                // filter.product1_id = item.product1_id;
                // filter.product2 = item.product2;
                // filter.product0 = 1;

                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            { type: "control", width:50,  },
            { name: "download", width:50, align:"left", title: "預覽",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            { name: "name", title: "檔案名稱", type: "text" },
            { name: "title1", title: "圖片大標", type: "text" },
            { name: "title2", title: "圖片小標", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],
        onDataLoading: function(args) {
        },
        onItemInserted: function(args) {
        },/*
        onItemUpdated: function(args) {
            console.log(args);
            $("#aProduct").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#aProduct").jsGrid("loadData");
        },*/
    });

   /* $(window).resize(function() {
        $container.isotope('layout');
    });*/

    var ocPortfolio = $("#oc-portfolio");
    ocPortfolio.owlCarousel({
        margin: 20,
        nav: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        autoplay: false,
        autoplayHoverPause: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    /*    var ocImages = $("#oc-images");

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });*/

    $("#p2").hide();
    $("#p3").hide();
    $("#p4").hide();

    $("#jProduct1").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                // filter.sent_status_id = "2";

                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "oproduct1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "oproduct1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "oproduct1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "oproduct1", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "value", title: "項目", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct1").jsGrid("loadData");
        },
        rowClick: function(args) {
            $("#p3").hide();
            $("#p4").hide();
            Session.set("nowrow_oproduct1", args.item);
            $("#jProduct2").jsGrid("loadData");
            $("#p2").fadeIn("slow");
            $("#p4-management").hide();
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortOproduct1", items, function(error, result){
                        if(error){
                            console.log("error from updateSortOproduct1: ", error);
                        }
                        else {
                            $("#jProduct1").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct2").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_oproduct1");
                if(!!item && !!item._id){
                    filter.product1_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "oproduct2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "oproduct2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "oproduct2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "oproduct2", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "value", title: "項目", type: "text" },
            // { name: "name_cht", title: "項目(中)", type: "text" },
            // { name: "name_eng", title: "項目(英)", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct2").jsGrid("loadData");
        },
        rowClick: function(args) {
            $("#p4").hide();
            Session.set("nowrow_oproduct2", args.item);
            $("#jProduct3").jsGrid("loadData");
            $("#p3").fadeIn("slow");
            $("#p4-management").hide();
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortOproduct2", items, function(error, result){
                        if(error){
                            console.log("error from updateSortOproduct2: ", error);
                        }
                        else {
                            $("#jProduct2").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct3").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_oproduct2");
                if(!!item && !!item._id){
                    filter.product2_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "oproduct3", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "oproduct3", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "oproduct3", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "oproduct3", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "value", title: "項目", type: "text" },
            // { name: "name_eng", title: "項目(英)", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct3").jsGrid("loadData");
        },
        rowClick: function(args) {
            Session.set("nowrow_oproduct3", args.item);
            $("#jProduct4").jsGrid("loadData");
            $("#p4").fadeIn("slow");
            $("#p4-management").hide();
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct3 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortOproduct3", items, function(error, result){
                        if(error){
                            console.log("error from updateSortOproduct3: ", error);
                        }
                        else {
                            $("#jProduct3").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct4").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_oproduct3");
                if(!!item && !!item._id){
                    filter.product3_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "oproduct4", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "oproduct4", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "oproduct4", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "oproduct4", "DELETE");
            },
        },
        fields: [
          /*  { name: "edit_select", align:"center", selecting: false, editing: false, sorting: false, title: "", width:40,
                itemTemplate: function(value, item) {
                    // console.log(item._id);
                    return $("<button>").attr("type", "button").attr("data-id", item._id).addClass("my-open-editing").text("編輯");
                },
            },*/
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "value_cht", title: "項目(中)", type: "text" },
            { name: "value_eng", title: "項目(英)", type: "text" },
            // { name: "template_id", title: "顯示樣板", width: 70 , type: "select", items: objPortfolioTemplate, valueField: "id", textField: "value", width: 100 },
        ],
        rowClick: function(args) {
            Session.set("nowrow_oproduct4", args.item);
            $("#p4-management").fadeIn();
            Template.oproduct1.loadP4();
            return;
        },
        onItemUpdated: function(args) {
            $("#jProduct4").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct4").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct4 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortOproduct4", items, function(error, result){
                        if(error){
                            console.log("error from updateSortOproduct4: ", error);
                        }
                        else {
                            $("#jProduct4").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });

    // if (!$('#intro_content1').data('froala.editor')) {
    //   $('#intro_content1').froalaEditor();
    // }
};

Template.oproduct1.helpers({
    // homepics: objHomePics
});

Template.oproduct1.events({
    'click .my-open-editing': function() {
        var p4 = Session.get("nowrow_oproduct4");
        console.log(p4);
        $("#p4-title").html(p4.product4_text);
    },
    'click .btn-showlogin': function() {
    },
    'click #save-p4-title': function() {
        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.title1 = $("#title1").val() || "";
        obj.title2 = $("#title2").val() || "";
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    'click #add-p4-intro': function() {
        var str = $('<div class="col_one_fifth nobottommargin"> \
            <label for="intro_title">標題</label>\
        </div>\
        <div class="col_three_fifth nobottommargin">\
            <input name="intro_title[]" type="text" class="intro_title text sm-form-control">\
        </div>\
        <div class="col_one_fifth col_last nobottommargin">\
        </div>\
        <div class="clear"></div>\
        <div class="col_one_fifth nobottommargin">\
            <label for="intro_content[]">內文</label>\
        </div>\
        <div class="col_three_fifth col_last nobottommargin">\
            <textarea class="sm-form-control" name="intro_content[]" style="width:100%;height: 100px;"></textarea>\
        </div>\
        <div class="clear"></div>');
        $("#intro_others").append(str);
    },
    'click #save-p4-intro': function() {

      /*  var arr1 = $('input[name="intro_title[]"]').map(function () {
            // console.log(this.value);
            return this.value; // $(this).val()
        }).get();
        var arr2 = $('textarea[name="intro_content[]"]').map(function () {
            return this.value; // $(this).val()
        }).get();

        var arr = [];
        for(var i in arr1){
            if(arr1[i] || arr2[i]){
                var intro = {};
                intro.title = arr1[i];
                intro.content = arr2[i];
                arr.push(intro);
            }
        }
        var item = Session.get("nowrow_oproduct4");
        var obj = {};
        obj.intro = arr;
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });*/

        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.intro_title1 = $("#intro_title1").val() || "";
        obj.intro_content1 = $("#intro_content1").val() || "";
        obj.intro_title2 = $("#intro_title2").val() || "";
        obj.intro_content2 = $("#intro_content2").val() || "";
        obj.intro_title3 = $("#intro_title3").val() || "";
        obj.intro_content3 = $("#intro_content3").val() || "";
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    'click #save-p4-video': function() {
        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.youtube_url = $("#youtube_url").val() || "";
        obj.video_url   = $("#video_url").val() || "";

        obj.youtube_title = $("#youtube_title").val() || "";
        obj.youtube_content = $("#youtube_content").val() || "";
        obj.video_title = $("#video_title").val() || "";
        obj.video_content = $("#video_content").val() || "";
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    'click .btn-new1': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        $("#jProduct1").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#jProduct1").jsGrid("loadData");
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .btn-new2': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal2").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;

        $("#jProduct2").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#jProduct2").jsGrid("loadData");
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .btn-new3': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal3").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_oproduct2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.value;

        $("#jProduct3").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal3').modal('hide');
                $(".form-modal3").trigger('reset');
                $("#jProduct3").jsGrid("loadData");
            } else {
                $(".modal3-error").text(ret);
            }
        });
    },
    'click .btn-new4': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal4").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_oproduct2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.value;
        var p3 = Session.get("nowrow_oproduct3");
        formVar.product3_id = p3._id;
        formVar.product3_text = p3.value;

        $("#jProduct4").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal4').modal('hide');
                $(".form-modal4").trigger('reset');
                $("#jProduct4").jsGrid("loadData");
            } else {
                $(".modal4-error").text(ret);
            }
        });
    },
    /*'submit .form-modal1': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        $("#jProduct1").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#jProduct1").jsGrid("loadData");
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'submit .form-modal2': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal2").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;

        $("#jProduct2").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#jProduct2").jsGrid("loadData");
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'submit .form-modal3': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal3").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_oproduct2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.value;

        $("#jProduct3").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal3').modal('hide');
                $(".form-modal3").trigger('reset');
                $("#jProduct3").jsGrid("loadData");
            } else {
                $(".modal3-error").text(ret);
            }
        });
    },
    'submit .form-modal4': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal4").serializeObject();
        var p1 = Session.get("nowrow_oproduct1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_oproduct2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.value;
        var p3 = Session.get("nowrow_oproduct3");
        formVar.product3_id = p3._id;
        formVar.product3_text = p3.value;

        $("#jProduct4").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal4').modal('hide');
                $(".form-modal4").trigger('reset');
                $("#jProduct4").jsGrid("loadData");
            } else {
                $(".modal4-error").text(ret);
            }
        });
    },*/
    "click a.upload1": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item._id,
                carousel: "0",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    "click a.upload2": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item._id,
                carousel: "0",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    "click a.upload3": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item._id,
                carousel: "1",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },

    // "change .myFileInput": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item._id,
    //                     carousel: "0",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     // $("#cFiles").jsGrid("loadData");
    //                     // console.log(pic_id);
    //                 });
    //
    //
    //                 // var url = "/cfs/files/images/"+fileObj._id;
    //                 // $("#op4-big-pic").attr("src", url);
    //                 Meteor.call("updateP4Pic1", item._id, url, pic_id, function(error, result){
    //                 });
    //
    //                 var tmp = Session.get("nowrow_oproduct4");
    //                 tmp.showpic1 = url;
    //                 tmp.filepic1 = pic_id;
    //                 Session.set("nowrow_oproduct4", tmp);
    //                 // console.log(Session.get("nowrow_oproduct4"));
    //                 Meteor.setTimeout(Template.oproduct.loadP4, 500);
    //             }
    //         });
    //     });
    // },
    // "change .myFileInput2": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item._id,
    //                     carousel: "0",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     // $("#cFiles").jsGrid("loadData");
    //                     // console.log(pic_id);
    //                 });
    //
    //
    //                 // $("#op4-big-pic").attr("src", url);
    //                 Meteor.call("updateP4Pic2", item._id, url, pic_id, function(error, result){
    //                 });
    //
    //                 var tmp = Session.get("nowrow_oproduct4");
    //                 tmp.showpic2 = url;
    //                 tmp.filepic2 = pic_id;
    //                 Session.set("nowrow_oproduct4", tmp);
    //                 // console.log(Session.get("nowrow_oproduct4"));
    //                 Meteor.setTimeout(Template.oproduct.loadP4, 500);
    //             }
    //         });
    //     });
    // },
    // "change .myFileInput3": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item._id,
    //                     carousel: "1",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     $("#cFiles").jsGrid("loadData");
    //                     // console.log(pic_id);
    //                 });
    //
    //             }
    //         });
    //     });
    // },
});
