Template.fundtransfer.rendered = function(){
    Template.semicolon.loadjs();
};

Template.fundtransfer.helpers({
});

Template.fundtransfer.events({
	'click #loadfund-btn': function () {
        var arrLineData = $("#loadfund-text").val();
        $("#import-error").html("");
        $("#import-success").html("");
        Meteor.call("saveAllFundConfigure", arrLineData, function(error, result){
            if(error){
                console.log("error from updateSortProduct2: ", error);
            }
            else {
                $("#loadfund-text").val("");
                alert("匯入成功");
                $("#import-error").html(result.error);
                $("#import-success").html(result.success);

                // Template.portfolio.loadForm();
            }
        });
    },
    'change #csv': function () {
        var file = document.querySelector('#csv').files[0];

        var reader = new FileReader();
        reader.readAsText(file, 'big5');

        reader.onload = function (event) {
            console.log(event)
            $("#loadfund-text").val(event.target.result)
        }
    }  
});

