Template.authority.rendered = function(){
    Template.semicolon.loadjs();
    $("#authority").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }

                var sel_auth = $("#sel_filter").find("option:selected").val();
                if (sel_auth == "1" || !sel_auth) {
                    filter.is_auth = "1"
                } else if (sel_auth == "0") {
                    filter.is_auth = "0"
                } else if (sel_auth == "3") {}
                return jsgridAjax(filter, "authority", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "authority", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "authority", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "authority", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, deleteButton: false },
            { name: "worknumber", title: "工號", type: "text", editing:false, align: "center"},
            { name: "engname", title: "英文姓名", type: "text", editing:false, align: "center"},
            { name: "is_auth",  title: "登入功能", type: "select", items: objAuth0, valueField: "id", textField: "value"},
            // { name: "auth_fin", title: "財務管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            // { name: "auth_hr",  title: "人資管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            // { name: "auth_cus", title: "客戶管理", type: "select", items: objAuth4, valueField: "id", textField: "value"},
            // { name: "auth_as",  title: "售後服務", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            // { name: "auth_prod", title: "產品中心", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            // { name: "auth_web", title: "網站管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#authority").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#authority").jsGrid("loadData");
        },
        rowClick: function (args) {
            console.log(args)
            Session.set("nowrow_authority", args.item);
            $("#jAuthority1").jsGrid("loadData");
        }
    });

    $("#jAuthority1").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                var item = Session.get("nowrow_authority");
                // console.log('#jAuthority1', item)
                if (!!item && !!item._id) {
                    filter._id = item._id;
                }
                // if (!filter.sortField) {
                //     filter.sortField = "order_id";
                //     filter.sortOrder = "asc";
                // }
                return jsgridAjax(filter, "authority2", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "authority2", "POST");
            },
            updateItem: function (item) {
                console.log(item)
                var user = Session.get("nowrow_authority");
                item._id = user._id
                return jsgridAjax(item, "authority2", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "authority2", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 40, deleteButton: false },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "name", title: "項目", type: "select", items: objAuthority, valueField: "id", textField: "value", readOnly: true},
            {
                name: "add", title: "新增", type: "select", items: objAuth0, valueField: "id", textField: "value", itemTemplate: function (value, item) {
                    if (value) {
                        return '允許'
                    }
                    return '關閉'
                } },
            {
                name: "read", title: "閱讀", type: "select", items: objAuth0, valueField: "id", textField: "value", itemTemplate: function (value, item) {
                    if (value) {
                        return '允許'
                    }
                    return '關閉'
                } },
            {
                name: "edit", title: "編輯", type: "select", items: objAuth0, valueField: "id", textField: "value", itemTemplate: function (value, item) {
                    if (value) {
                        return '允許'
                    }
                    return '關閉'
                } },
            { name: "del", title: "刪除", type: "select", items: objAuth0, valueField: "id", textField: "value", itemTemplate: function (value, item) {
                if (value) {
                    return '允許'
                }
                    return '關閉'
            }},
            // { name: "value", title: "項目", type: "text" },
            // { name: "value", title: "項目", type: "text" },
            // { name: "value", title: "項目", type: "text" },
            // { name: "name_cht", title: "項目(中)", type: "text" },
            // { name: "name_eng", title: "項目(英)", type: "text" },
        ],
        onItemUpdated: function (args) {
            $("#jAuthority1").jsGrid("loadData");
        },
        onItemDeleted: function (args) {
            $("#jAuthority1").jsGrid("loadData");
        },
        rowClick: function (args) {
            console.log('jAuthority1' ,args)
            // $("#p4").hide();
            Session.set("nowrow_oproduct2", args.item);
            // $("#jProduct3").jsGrid("loadData");
            // $("#p3").fadeIn("slow");
            // $("#p4-management").hide();
        },
        onItemEditing: function (args) {
            // console.log(args)
            // cancel editing of the row of item with field 'ID' = 0
            // if (args.item.name === `${args.item.name}`) {
            //     args.cancel = true;
            // }
        },
        onRefreshed: function () {
            // var $gridData = $("#jAuthority1 .jsgrid-grid-body tbody");
            // $gridData.sortable({
            //     update: function (e, ui) {
            //         var items = $.map($gridData.find("tr"), function (row) {
            //             return $(row).data("JSGridItem");
            //         });
            //         // console.log("Reordered items", items);
            //         Meteor.call("updateSortOproduct2", items, function (error, result) {
            //             if (error) {
            //                 console.log("error from updateSortOproduct2: ", error);
            //             }
            //             else {
            //                 $("#jAuthority1").jsGrid("loadData");
            //             }
            //         });
            //     }
            // });
        }
    });
};
Template.authority.helpers({
    get_countries: function() {
        return objCountries;
    },
    getSomeone: function () {
        var item = Session.get("nowrow_authority");
        return item ? item : null
    }
});
Template.authority.events({
    "change #sel_filter": function(event, template) {
        $("#authority").jsGrid("loadData");
    },
});

