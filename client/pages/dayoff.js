Template.dayoff.rendered = function() {
    Template.semicolon.loadjs();

    var MyDateTimeField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateTimeField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            // console.log(date1);
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function(value) {
            // console.log(value);
            return value;
            // return new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datetimepicker({ defaultDate: new Date() });
        },

        editTemplate: function(value) {
            // console.log(value);
            // return this._editPicker = $("<input>").datetimepicker().datetimepicker("setDate", new Date(value));

            var date = new Date();

            if (!!value) {
                date = new Date(
                    Date.parse(
                        value,
                        "mm/dd/yyyy hh:MM tt"
                    )
                );
            }
            // console.log(date);
            return this._editPicker = $("<input>").datetimepicker({
                widgetParent: ".datetimepicker_z",
                defaultDate: date
            });
            // ampm: true
            // }).datetimepicker("setDate", date);
            // }).datetimepicker({defaultDate: date});
            // return this._editPicker = $("<input>").val(value);
        },
        insertValue: function() {
            // return this._insertPicker.datetimepicker("getDate").toISOString();
            return this._insertPicker.val();
        },

        editValue: function() {
            // return this._editPicker.datetimepicker("getDate").toISOString();
            return this._editPicker.val();
        }
    });
    jsGrid.fields.myDateTimeField = MyDateTimeField;

    $("#e_dayoff").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.dayoff_status = "1";
                filter.apply_id = Meteor.userId();

                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 50, editButton: true },
            { name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status_first, valueField: "id", textField: "value", align: "center" },
            { name: "day_off_class", width: 70, title: "請假類別", type: "select", items: objDay_off, valueField: "id", textField: "value", align: "center" },
            { name: "start_time", title: "開始時間", type: "myDateTimeField", width: 140, align: "center" },
            { name: "end_time", title: "結束時間", type: "myDateTimeField", width: 140, align: "center" }, {
                name: "total_hours",
                title: "總時數",
                width: 80,
                align: "center",
                itemTemplate: function(value, item) {
                    // console.log(value);
                    if (!value) {
                        return "";
                    }
                    var day = parseInt(value / 8);
                    var hour = value % 8;

                    return day + "天 " + hour + "時";
                    // if(!!item && !!item.start_time && !!item.end_time){
                    //     var date1 = new Date(
                    //         Date.parse(
                    //             item.start_time,
                    //             "mm/dd/yyyy hh:MM tt"
                    //         )
                    //     );
                    //     var date2 = new Date(
                    //         Date.parse(
                    //             item.end_time,
                    //             "mm/dd/yyyy hh:MM tt"
                    //         )
                    //     );
                    //     var diff = date2-date1;
                    //     return diff/1000/60/60;
                    // }
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var day = parseInt(value / 8);
                    var hour = value % 8;

                    var sel = $('<input style="width: 50%"> 天 <select>\
                        <option value="0">0 時</option>\
                        <option value="1">1 時</option>\
                        <option value="2">2 時</option>\
                        <option value="3">3 時</option>\
                        <option value="4">4 時</option>\
                        <option value="5">5 時</option>\
                        <option value="6">6 時</option>\
                        <option value="7">7 時</option>\</select>');
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;

                    if (!!value) {
                        $(sel[0]).val(day);
                        $(sel[2]).val(hour);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    // return this._editPicker.val();
                    return Number($(this._editPicker[0]).val()) * 8 + Number($(this._editPicker[2]).val());
                }
            },
            { name: "reason", title: "事由", type: "text", width: 70, align: "center" }, {
                name: "substitute",
                title: "職務<br>代理人",
                width: 70,
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                    return "";
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-substitute"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    var type = Meteor.users.find({
                        $and: [{
                                $or: [{ 'auth.is_auth': '1' }, {
                                    'auth.is_auth': { $exists: false }
                                }]
                            },
                            { auth_substitutego: "1" },
                            { 'profile.department_id': Router.current().params.f1_id }
                        ]
                    }, { sort: { username: 1 } }).fetch();

                    $.each(type, function(i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    if (!!value) {
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            }, {
                name: "supervisor",
                title: "主管",
                width: 70,
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.supervisor)
                        return Meteor.users.findOne({ _id: item.supervisor }).chtname;
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-supervisor"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    var type = Meteor.users.find({
                        $and: [{
                                $or: [{ 'auth.is_auth': '1' }, {
                                    'auth.is_auth': { $exists: false }
                                }]
                            },
                            { auth_supervisorgo: "1" },
                            { 'profile.department_id': Router.current().params.f1_id }
                        ]
                    }, { sort: { username: 1 } }).fetch();

                    $.each(type, function(i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    if (!!value) {
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
            // { name: "hr", title: "人資", type: "select", items: [{ id: "", value: "" }].concat(), valueField: "id", textField: "value"},
            {
                name: "hr",
                title: "人資",
                width: 70,
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.hr)
                        return Meteor.users.findOne({ _id: item.hr }).chtname;
                    // return Meteor.users.findOne({_id: item.account2_id }).value;
                    return "";
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-hr"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    var type = Meteor.users.find({
                        $and: [{
                                $or: [{ 'auth.is_auth': '1' }, {
                                    'auth.is_auth': { $exists: false }
                                }]
                            },
                            { auth_hrgo: "1" },
                            { 'profile.department_id': Router.current().params.f1_id }
                        ]
                    }, { sort: { username: 1 } }).fetch();

                    $.each(type, function(i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    if (!!value) {
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdating: function(args) {
            if (args.item.day_off_class == "10") {
                // let rest = $("#arrSpecialDayoff td:last").data("rest");
                let rest = $("#arrSpecialDayoff td:nth-last-child(2)").data("rest");
                if (Number(args.item.total_hours) > Number(rest)) {
                    // alert("天數不足");
                    swal(
                        '特休時數不足!',
                        '',
                        'warning'
                    )
                    args.cancel = true;
                    return;
                }
            }
            if (args.item.dayoff_status == "2") {
                if (!confirm("確定送給代理人嗎？")) {
                    args.cancel = true;
                }
            }
            // if (args.item.dayoff_status == "2") {
            //     swal({
            //         title: '確定送給代理人嗎?',
            //         text: "",
            //         type: 'warning',
            //         showCancelButton: true,
            //         confirmButtonColor: '#3085d6',
            //         cancelButtonColor: '#d33',
            //         confirmButtonText: '確定',
            //         cancelButtonText: '取消',
            //         // confirmButtonClass: 'btn btn-success',
            //         // cancelButtonClass: 'btn btn-danger',
            //         // buttonsStyling: false
            //     }).then(function(isConfirm) {
            //         if (isConfirm) {
            //             swal(
            //             '成功送出',
            //             '',
            //             'success'
            //             )
            //         }
            //     }, function(dismiss) {
            //         console.log("dismiss")
            //         // dismiss can be 'cancel', 'overlay',
            //         // 'close', and 'timer'
            //         if (dismiss === 'cancel') {
            //             args.cancel = true;
            //             return;
            //         }
            //     })
            // }
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            if (args.item.no_dayoff == "1") {
                args.item.no_dayoff = "0"
                    // alert("天數不足");
                swal(
                    '補休時數不足!',
                    '',
                    'warning'
                )
            }
            $("#e_dayoff").jsGrid("loadData");
            $("#e_dayoff_records").jsGrid("loadData");
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff").jsGrid("loadData");
        },
        onRefreshed: function(args) {
            // var worknumber = Meteor.user().profile.worknumber;
            // var chtname = Meteor.user().profile.chtname;
            // var department_id = Meteor.user().profile.department_id;

            // $("#department_id").text(Businessgroup.findOne({_id:data.profile.department_id}).value || "");
        }
    });
    $("#e_dayoff_records").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: false,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        pageSize: 10,
        pageButtonCount: 5,
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.isrecord = "1";
                filter.apply_id = Meteor.userId();


                var type_id = $("#type_id :selected").val();
                if (type_id != "-1") {
                    filter.findyear = type_id;
                }

                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            { name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status, valueField: "id", textField: "value" },
            { name: "day_off_class", title: "請假類別", type: "select", items: objDay_off, valueField: "id", textField: "value" },
            { name: "start_time", title: "開始時間", width: 180, align: "center" },
            { name: "end_time", title: "結束時間", width: 180, align: "center" }, {
                name: "total_hours",
                title: "總時數",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!item.total_hours) {
                        return ""
                    }
                    var day = parseInt(item.total_hours / 8);
                    var hour = item.total_hours % 8;
                    return day + "天 " + hour + "時";
                },
            },
            { name: "reason", title: "事由", align: "center" }, {
                name: "ps",
                title: "審核回覆",
                align: "center",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    var str = "";
                    if (!!item && !!item.ps2) {
                        str = "代理人: " + item.ps2;
                    }
                    if (!!item && !!item.ps3) {
                        if (!!str) str += "<BR>";
                        str += "主管: " + item.ps3;
                    }
                    if (!!item && !!item.ps4) {
                        if (!!str) str += "<BR>";
                        str += "人資: " + item.ps4;
                    }
                    return str;
                },
            }, {
                name: "substitute",
                title: "職務代理人",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                },
            }, {
                name: "supervisor",
                title: "主管",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.supervisor)
                        return Meteor.users.findOne({ _id: item.supervisor }).chtname;
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-supervisor"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    var type = Meteor.users.find({}, { sort: { username: 1 } }).fetch();

                    $.each(type, function(i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    if (!!value) {
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
            // { name: "hr", title: "人資", type: "select", items: [{ id: "", value: "" }].concat(), valueField: "id", textField: "value"},
            {
                name: "hr",
                title: "人資",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.hr)
                        return Meteor.users.findOne({ _id: item.hr }).chtname;
                    // return Meteor.users.findOne({_id: item.account2_id }).value;
                    return "";
                },
                editTemplate: function(value, item) {
                    // console.log(item);
                    var sel = $('<select class="my-hr"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    var type = Meteor.users.find({}, { sort: { username: 1 } }).fetch();

                    $.each(type, function(i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    if (!!value) {
                        sel.val(value);
                    }
                    return this._editPicker = sel;
                },
                editValue: function() {
                    return this._editPicker.val();
                }
            },
            /*{ name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status, valueField: "id", textField: "value",
                itemTemplate: function(value, item) {
                    // item.dayoff_status = 5;
                    // return value = "已生效"
                }
            },*/
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_records").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_records").jsGrid("loadData");
        },
    });
    $("#e_dayoff_set_4").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        // paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter._id = Meteor.userId();
                // filter.is_auth = "1"
                // var bg = Session.get("bankacc_bg");
                // filter.bg_id = bg._id;
                return jsgridAjax(filter, "employeelist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "employeelist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "employeelist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "employeelist", "DELETE");
            },
        },
        fields: [
            { name: "worknumber", title: "工號", width: 60, align: "center" },
            { name: "chtname", title: "員工姓名", width: 60, align: "center" },
            { name: "onbroad_date", title: "就職日", width: 80, align: "center" }, {
                name: "jobyear",
                title: "年資",
                width: 60,
                align: "center",
                itemTemplate: function(value, item) {
                    // return Compute_Jobyear(item);

                    if (!item.onbroad_date) {
                        return "";
                    }
                    var JobDays = (new Date - new Date(item.onbroad_date)) / 1000 / 3600 / 24; //365.2422
                    // console.log(JobDays);
                    var year = parseInt(JobDays / 365);
                    var day = parseInt(JobDays % 365); // 半年用182.5天去算
                    if (year >= 1) {
                        return year.toString() + "年" + day + "天"; // 幾個365天 就幾年
                    }
                    if (day > 182) {
                        return day + "天<br>(未滿一年)";
                    } else {
                        return day + "天<br>(未滿半年)";
                    }
                },
            }, /*{
                name: "day_off_class13",
                title: "本年度補休",
                align: "center",
                width: 60,
                itemTemplate: function(value, item) {

                    var getDayoffSelYear = Number(new Date().getFullYear());
                    var userData = Meteor.users.findOne({ "_id": item._id });
                    if (!!userData.arr_dayoff && userData.arr_dayoff.length > 0) { // users有arr_dayoff[]
                        var arr = userData.arr_dayoff;
                        var data = {};
                        var isData = 0;

                        for (let i = 0; i < arr.length; i++) {
                            if (arr[i].year == getDayoffSelYear) {
                                isData = 1;
                                data = arr[i];
                                break;
                            }
                        }
                        if (isData == 1) {
                            item.day_off_class12 = funcHourToDayText(data.dayoff12);
                            item.day_off_class13 = funcHourToDayText(data.dayoff13);
                            item.day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
                        }
                    } else {
                        return '<b>' + funcHourToDayText(0) + '</b>'; }
                    return '<b>' + item.day_off_class13_surplus + '</b>';
                }
            },*/
            /*{
                           name: "this_year_available",
                           title: "特休有效日期",
                           align: "center",
                           width: 100,
                           itemTemplate: function(value, item) {
                               if (!item.onbroad_date) return ""
                               var d = new Date();
                               var n = new Date(item.onbroad_date);//就職日期
                               var start_date = new Date((d.getFullYear()) + "/" + (n.getMonth() + 1) + "/" + n.getDate());//有效期限開始   2017/9/1
                               var end_date = (start_date.getFullYear() + 1) + "/" + (start_date.getMonth() + 1) + "/" + start_date.getDate();//有效期限結束 2018/9/1
                               var last_start_date = new Date((d.getFullYear() - 1) + "/" + (n.getMonth() + 1) + "/" + n.getDate());//上年度有效期限開始 2016/9/1
                               var last_end_date = (last_start_date.getFullYear() + 1) + "/" + (last_start_date.getMonth() + 1) + "/" + last_start_date.getDate();//上年度有效期限結束 2017/9/1
                               if (start_date < d) {
                                   return currentdate_eff(start_date) + " - " + end_date;//本年度有效期限
                               }   else {
                                   return currentdate_eff(last_start_date) + " - " + last_end_date;//上年度有效期
                               }
                           },
                       },*/
            /*{
                           name: "this_year_dayoff",
                           title: "特休",
                           align: "center",
                           width: 60,
                           itemTemplate: function(value, item) {
                               // 可用item.special_dayoff_ava
                               // 已用item.special_dayoff_used
                               // 剩餘item.special_dayoff_surplus

                               if (!item.onbroad_date) {
                                   return "";
                               }
                               var JobDays = (new Date() - new Date(item.onbroad_date)) / 1000 / 3600 / 24; //365.2422
                               var year = parseInt(JobDays / 365);
                               var day = parseInt(JobDays % 365); // 半年用182.5天去算
                               // return year.toString() + "年"; // 幾個365天 就幾年
                               if (year < 1) {
                                   if (day < 183) {
                                       item.special_dayoff_ava = 0;
                                   }
                                   item.special_dayoff_ava = 3;
                               } else if (year >= 1 && year < 2) {
                                   item.special_dayoff_ava = 7;
                               } else if (year >= 2 && year < 3) {
                                   item.special_dayoff_ava = 10;
                               } else if (year >= 3 && year < 5) {
                                   item.special_dayoff_ava = 14;
                               } else if (year >= 5 && year < 10) {
                                   item.special_dayoff_ava = 15;
                               } else if (year >= 10) {
                                   item.special_dayoff_ava = (year - 10) + 16;
                                   if (item.special_dayoff_ava > 30) {
                                       item.special_dayoff_ava = 30;
                                   }
                               }

                               var DC_10 = Dayoff.find({ $and: [{ "dayoff_status": "5" }, { "day_off_class": "10" }, { "apply_id": item._id }] }).fetch();
                               var d = new Date();
                               var n = new Date(item.onbroad_date);
                               var start_date = new Date((d.getFullYear()) + "/" + (n.getMonth() + 1) + "/" + n.getDate());
                               var start_date_add1year_d = new Date((d.getFullYear()) + "/" + (n.getMonth() + 1) + "/" + n.getDate());
                               var end_date_d = start_date_add1year_d;
                               // var end_date_d = start_date_add1year_d.addDays(183);
                               var end_date = new Date((end_date_d.getFullYear() + 1) + "/" + (end_date_d.getMonth() + 1) + "/" + end_date_d.getDate());
                               var This_year_day_executed = 0;

                               for (var i = 0; i < DC_10.length; i++) {
                                   var D = new Date(DC_10[i].start_time)
                                   var DC_10_day = 0;
                                   if (D >= start_date && D <= end_date) {
                                       DC_10_day = Number(DC_10[i].total_hours)
                                   }
                                   This_year_day_executed += DC_10_day;
                               }
                               item.special_dayoff_used = This_year_day_executed;

                               item.special_dayoff_surplus = funcHourToDayText((Number(item.special_dayoff_ava * 8)) - (Number(item.special_dayoff_used))); //剩餘
                               item.special_dayoff_ava = funcHourToDayText(item.special_dayoff_ava * 8); //可用
                               item.special_dayoff_used = funcHourToDayText(This_year_day_executed); //已用
                               return "可用:" + item.special_dayoff_ava + '<br>' + "已用:" + item.special_dayoff_used + '<br>' + '<b>' + "剩餘:" + item.special_dayoff_surplus + '</b>';
                           }
                       },*/
        ],
        // rowRenderer:function(item, itemIndex){
        //     // console.log(item);
        //     console.log(itemIndex);
        // },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_set_4").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_set_4").jsGrid("loadData");
        },
    });
    $("#e_leave_notice1").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                // filter.isnotice = "1";
                filter.dayoff_status = "2";
                filter.substitute = Meteor.userId();
                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, deleteButton: false },
            /*  { name: "e_checkbox", title: "勾選", type: "checkbox",
                  itemTemplate: function(value, item) {
                  return $("<input>").attr("type", "checkbox")
                      .attr("checked", value || item.Checked)
                      .on("change", function() {
                          item.Checked = $(this).is(":checked");
                      });
                }
                  },*/
            { name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status_second, valueField: "id", textField: "value" },
            { name: "ps2", title: "審核回覆", type: "text", align: "center" }, {
                name: "substitute",
                title: "申請人姓名",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                    // return Meteor.users.findOne({_id: item.substitute }).chtname;
                        return Meteor.users.findOne({ _id: item.apply_id }).chtname;
                },
            },
            { name: "day_off_class", title: "請假類別", type: "select", items: objDay_off, valueField: "id", textField: "value", editing: false },
            { name: "start_time", title: "開始時間", width: 180, align: "center" },
            { name: "end_time", title: "結束時間", width: 180, align: "center" }, {
                name: "total_hours",
                title: "總時數",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!item.total_hours) {
                        return ""
                    }
                    var day = parseInt(item.total_hours / 8);
                    var hour = item.total_hours % 8;
                    return day + "天 " + hour + "時";
                },
            },
            { name: "reason", title: "事由", align: "center" }, {
                name: "substitute",
                title: "職務代理人",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                },
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
        },
    });
    $("#e_leave_notice2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                // filter.isnotice = "1";
                filter.dayoff_status = "3";
                filter.supervisor = Meteor.userId();
                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, deleteButton: false },
            /*  { name: "e_checkbox", title: "勾選", type: "checkbox",
                  itemTemplate: function(value, item) {
                  return $("<input>").attr("type", "checkbox")
                      .attr("checked", value || item.Checked)
                      .on("change", function() {
                          item.Checked = $(this).is(":checked");
                      });
                }
                  },*/
            { name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status_third, valueField: "id", textField: "value" },
            { name: "ps3", title: "審核回覆", type: "text", align: "center" }, {
                name: "substitute",
                title: "申請人姓名",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                    // return Meteor.users.findOne({_id: item.substitute }).chtname;
                        return Meteor.users.findOne({ _id: item.apply_id }).chtname;
                },
            },
            { name: "day_off_class", title: "請假類別", type: "select", items: objDay_off, valueField: "id", textField: "value", editing: false },
            { name: "start_time", title: "開始時間", width: 180, align: "center" },
            { name: "end_time", title: "結束時間", width: 180, align: "center" }, {
                name: "total_hours",
                title: "總時數",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!item.total_hours) {
                        return ""
                    }
                    // var day = parseInt(item.total_hours/8);
                    // var hour = item.total_hours%8;
                    // return day + "天 " + hour + "時";
                    return funcHourToDayText(item.total_hours);
                },
            },
            { name: "reason", title: "事由", align: "center" }, {
                name: "substitute",
                title: "職務代理人",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                },
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
        },
    });
    $("#e_leave_notice3").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                // filter.isnotice = "1";
                filter.dayoff_status = "4";
                filter.hr = Meteor.userId();
                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, deleteButton: false },
            /*  { name: "e_checkbox", title: "勾選", type: "checkbox",
                  itemTemplate: function(value, item) {
                  return $("<input>").attr("type", "checkbox")
                      .attr("checked", value || item.Checked)
                      .on("change", function() {
                          item.Checked = $(this).is(":checked");
                      });
                }
                  },*/
            { name: "dayoff_status", title: "狀態", type: "select", items: objDay_off_status_fourth, valueField: "id", textField: "value" },
            { name: "ps4", title: "審核回覆", type: "text", align: "center" }, {
                name: "substitute",
                title: "申請人姓名",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                    // return Meteor.users.findOne({_id: item.substitute }).chtname;
                        return Meteor.users.findOne({ _id: item.apply_id }).chtname;
                },
            },
            { name: "day_off_class", title: "請假類別", type: "select", items: objDay_off, valueField: "id", textField: "value", editing: false },
            { name: "start_time", title: "開始時間", width: 180, align: "center" },
            { name: "end_time", title: "結束時間", width: 180, align: "center" }, {
                name: "total_hours",
                title: "總時數",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!item.total_hours) {
                        return ""
                    }
                    // var day = parseInt(item.total_hours/8);
                    // var hour = item.total_hours%8;
                    // return day + "天 " + hour + "時";
                    return funcHourToDayText(item.total_hours);
                },
            },
            { name: "reason", title: "事由", align: "center" }, {
                name: "substitute",
                title: "職務代理人",
                align: "center",
                itemTemplate: function(value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                },
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onDataLoaded: function(args) {
            //badge通知
            var badgeNum = $("#e_leave_notice1").jsGrid("_itemsCount") + $("#e_leave_notice2").jsGrid("_itemsCount") + $("#e_leave_notice3").jsGrid("_itemsCount");
            if (badgeNum > 0) {
                $(".badge").text(badgeNum);
            } else $(".badge").text("");
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_leave_notice1").jsGrid("loadData");
            $("#e_leave_notice2").jsGrid("loadData");
            $("#e_leave_notice3").jsGrid("loadData");
            $("#e_dayoff_set_4").jsGrid("loadData");
            $("#e_dayoff_records").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#e_leave_notice1").jsGrid("loadData");
            // $("#e_leave_notice2").jsGrid("loadData");
            // $("#e_leave_notice3").jsGrid("loadData");
        },
    });

    var d = new Date();
    $("#dayoff_sel").val(d.getFullYear());
    $(".sel_filter").trigger("change");

    //取得假別計算公式的"可用天數"帶入可用假別一覽表單的"可用"欄位
    for (var i = 1; i < 14; i++) {
        var AvailableDays = "AvailableDays_" + i;
        AvailableDays = (Dayoff_set.findOne({ "all_day_off_class": "" + i })) || "未設定1";
        if (!AvailableDays.available_days) {
            $("#dayoff_" + i + "_ok").text("未設定");
        } else if (!!AvailableDays.available_days) {
            $("#dayoff_" + i + "_ok").text((AvailableDays.available_days) + "天")
        }
    }

    var arrDayoffSet = Dayoff_set.find({}).fetch()
    for (let i = 0; i < arrDayoffSet.length; i++) {
        arrDayoffSet[i].all_day_off_class
    }

    var special_dayoff_used = Dayoff.find({
        'day_off_class': '10',
        'dayoff_status': '5',
        'apply_id': Meteor.userId()
    }).fetch(); //已生效的特休
    var onbroad = Meteor.user().profile.onbroad_date; // 使用者的就職日
    if (!!onbroad && onbroad !== undefined) { //有就職日

        $("#arrSpecialDayoff").empty();
        var now_date = new Date();
        var start_date = new Date(onbroad);
        var end_date = "";
        var JobDays = (new Date() - new Date(onbroad)) / 1000 / 3600 / 24; //365.2422
        var year = parseInt(JobDays / 365);
        var day = parseInt(JobDays % 365); // 半年用182.5天去算

        do {
            if (day < 183) { //如過就未滿半年
                var getDayoffSelYear = Number(new Date().getFullYear());
                var userData = Meteor.user();
                if (!!userData.arr_dayoff && userData.arr_dayoff.length > 0) { // users有arr_dayoff[]
                    var arr = userData.arr_dayoff;
                    var data = {};
                    var isData = 0;

                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].year == getDayoffSelYear) {
                            isData = 1;
                            data = arr[i];
                            break;
                        }
                    }
                    if (isData == 1) {
                        var day_off_class12 = funcHourToDayText(data.dayoff12);
                        var day_off_class13 = funcHourToDayText(data.dayoff13);
                        var day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
                    }
                }
                var do_hour = 0;
                var do_str = "";
                var special_dayoff_ava;
                var rest_hour = 0 - do_hour; //剩餘 = 24小時 - 已用
                start_h_date = (new Date(onbroad)).addDays(183); //半年
                end_date = (new Date(start_h_date)).addDays(365); //一年半


                for (var i = 0; i < special_dayoff_used.length; i++) {
                    var element = special_dayoff_used[i];
                    if (element.start_time_d >= start_h_date && element.start_time_d <= end_date) {
                        do_hour += Number(element.total_hours);
                    }
                }
                var dom = $('\
                <tr id="wdba-tr" data-id="">\
                    <td><span data-mode="popup" id="half">' + (now_date.getFullYear()) + '</span></td>\
                    <td><span data-mode="popup" id="start_h_date">' + "未滿半年" + '</span></td>\
                    <td><span data-mode="popup" id="end_date"></span></td>\
                    <td><span data-mode="popup" id="special_dayoff_ava"></span>' + funcHourToDayText(0) + '</td>\
                    <td><span data-mode="popup" id=""></span>' + funcHourToDayText(do_hour) + '</td>\
                    <td data-hour="' + do_hour + '" data-rest="' + rest_hour + '"><span data-mode="popup" id="" ></span>' + '<b>' + funcHourToDayText(rest_hour) + '</b>' + '</td>\
                    <td><span data-mode="popup" id="class13"></span>' + '<b>' + day_off_class13_surplus + '</b>' +'</td>\
                </tr>');
                // $("#arrSpecialDayoff").append(dom);
            }

            if (day > 183) { //如過就職滿半年
                var getDayoffSelYear = Number(new Date().getFullYear());
                var userData = Meteor.user();
                if (!!userData.arr_dayoff && userData.arr_dayoff.length > 0) { // users有arr_dayoff[]
                    var arr = userData.arr_dayoff;
                    var data = {};
                    var isData = 0;

                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].year == getDayoffSelYear) {
                            isData = 1;
                            data = arr[i];
                            break;
                        }
                    }
                    if (isData == 1) {
                        var day_off_class12 = funcHourToDayText(data.dayoff12);
                        var day_off_class13 = funcHourToDayText(data.dayoff13);
                        var day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
                    }
                }
                var do_hour = 0;
                var do_str = "";
                var special_dayoff_ava;
                var rest_hour = 24 - do_hour; //剩餘 = 24小時 - 已用
                start_h_date = (new Date(onbroad)).addDays(183); //半年
                end_date = (new Date(start_h_date)).addDays(548); //一年半


                for (var i = 0; i < special_dayoff_used.length; i++) {
                    var element = special_dayoff_used[i];
                    if (element.start_time_d >= start_h_date && element.start_time_d <= end_date) {
                        do_hour += Number(element.total_hours);
                        rest_hour = 24 - do_hour;
                    }
                }
                var dom = $('\
                <tr id="wdba-tr" data-id="">\
                    <td><span data-mode="popup" id="half">' + (now_date.getFullYear()) + '</span></td>\
                    <td><span data-mode="popup" id="start_h_date">' + (currentdate_eff(start_h_date)) + '</span></td>\
                    <td><span data-mode="popup" id="end_date">' + (currentdate_eff(end_date)) + '</span></td>\
                    <td><span data-mode="popup" id="special_dayoff_ava"></span>' + funcHourToDayText(24) + '</td>\
                    <td><span data-mode="popup" id=""></span>' + funcHourToDayText(do_hour) + '</td>\
                    <td data-hour="' + do_hour + '" data-rest="' + rest_hour + '"><span data-mode="popup" id="" ></span>' + '<b>' + funcHourToDayText(rest_hour) + '</b>' + '</td>\
                    <td><span data-mode="popup" id="class13"></span>' + '<b>' + day_off_class13_surplus + '</b>' +'</td>\
                </tr>');
                // $("#arrSpecialDayoff").append(dom);
            }

            if (year >= 1) { //如過就職滿一年
                var getDayoffSelYear = Number(new Date().getFullYear());
                // var userData = Meteor.users.findOne({ "_id": item._id });
                var userData = Meteor.user();
                if (!!userData.arr_dayoff && userData.arr_dayoff.length > 0) { // users有arr_dayoff[]
                    var arr = userData.arr_dayoff;
                    var data = {};
                    var isData = 0;

                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].year == getDayoffSelYear) {
                            isData = 1;
                            data = arr[i];
                            break;
                        }
                    }
                    if (isData == 1) {
                        var day_off_class12 = funcHourToDayText(data.dayoff12);
                        var day_off_class13 = funcHourToDayText(data.dayoff13);
                        var day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
                    }
                }
                // } else {
                //     return '<b>' + funcHourToDayText(0) + '</b>'; }
                // return '<b>' + item.day_off_class13_surplus + '</b>';

                var special_dayoff_ava_hour = special_dayoff_ava * 8; //可用的特休時數
                var rest_hour = special_dayoff_ava_hour - do_hour; //剩餘 = 可用 - 已用
                var do_hour = 0;

                for (var i = 1; i <= year; i++) {
                    if (i >= 1 && i < 2) {
                        special_dayoff_ava = 7;
                    } else if (i >= 2 && i < 3) {
                        special_dayoff_ava = 10;
                    } else if (i >= 3 && i < 5) {
                        special_dayoff_ava = 14;
                    } else if (i >= 5 && i < 10) {
                        special_dayoff_ava = 15;
                    } else if (i >= 10) {
                        special_dayoff_ava = (i - 10) + 16;
                        if (special_dayoff_ava > 30) {
                            special_dayoff_ava = 30;
                        }
                    }
                    // console.log(special_dayoff_ava);
                    var special_dayoff_ava_hour = special_dayoff_ava * 8; //可用的特休時數
                    var rest_hour = special_dayoff_ava_hour - do_hour; //剩餘 = 可用 - 已用
                    var do_hour = 0;

                    start_date = new Date((start_date)).addDays(365); //有效開始日期 = 就職日 + 一年
                    end_date = (new Date(start_date)).addDays(365); //有效結束日期 = 有效開始日期 + 一年半

                    for (var j = 0; j < special_dayoff_used.length; j++) {
                        var element = special_dayoff_used[j];
                        if (element.start_time_d >= start_date && element.start_time_d <= end_date && rest_hour > 0) {
                            do_hour += Number(element.total_hours);
                            rest_hour = special_dayoff_ava_hour - do_hour; //剩餘 = 可用 - 已用
                        }
                    }
                    console.log(do_hour);
                    console.log(rest_hour);

                    var dom = $('\
                    <tr id="wdba-tr data-id="arrGiveInterestRecord">\
                        <td><span data-mode="popup" id="year">' + (now_date.getFullYear()) + '</span></td>\
                        <td><span data-mode="popup" id="start_date">' + (currentdate_eff(start_date)) + '</span></td>\
                        <td><span data-mode="popup" id="end_date">' + (currentdate_eff(end_date)) + '</span></td>\
                        <td><span data-mode="popup" id="special_dayoff_ava"></span>' + funcHourToDayText(special_dayoff_ava_hour) + '</td>\
                        <td><span data-mode="popup" id=""></span>' + funcHourToDayText(do_hour) + '</td>\
                        <td data-hour="' + do_hour + '" data-rest="' + rest_hour + '"><span data-mode="popup" id=""></span>' + '<b>' + funcHourToDayText(rest_hour) + '</b>' + '</td>\
                        <td><span data-mode="popup" id="class13"></span>' + '<b>' + day_off_class13_surplus + '</b>' +'</td>\
                    </tr>');
                    // $("#arrSpecialDayoff").append(dom);
                }
            }
            $("#arrSpecialDayoff").append(dom);
            break;
        } while (now_date.getTime() > end_date.getTime());
    }

    this.autorun(function() {
        var getDayoffSelYear = Number($("#dayoff_sel").val());

        var userData = Meteor.user();
        if (!!Meteor.user().arr_dayoff && Meteor.user().arr_dayoff.length > 0 && !!getDayoffSelYear) { // 有休假的欄位可以顯示
            var arr = Meteor.user().arr_dayoff;
            var data = {};
            var isData = 0;

            for (var i = 0; i < arr.length; i++) {
                if (arr[i].year == getDayoffSelYear) {
                    isData = 1;
                    data = arr[i];
                    break;
                }
            }

            if (isData == 1) {
                $("#dayoff_1").text(funcHourToDayText(data.dayoff1));
                $("#dayoff_2").text(funcHourToDayText(data.dayoff2));
                $("#dayoff_3").text(funcHourToDayText(data.dayoff3));
                $("#dayoff_4").text(funcHourToDayText(data.dayoff4));
                $("#dayoff_5").text(funcHourToDayText(data.dayoff5));
                $("#dayoff_6").text(funcHourToDayText(data.dayoff6));
                $("#dayoff_7").text(funcHourToDayText(data.dayoff7));
                $("#dayoff_8").text(funcHourToDayText(data.dayoff8));
                $("#dayoff_9").text(funcHourToDayText(data.dayoff9));
                $("#dayoff_10").text(funcHourToDayText(data.dayoff10));
                $("#dayoff_11").text(funcHourToDayText(data.dayoff11));
                $("#dayoff_12").text(funcHourToDayText(data.dayoff12));
                $("#dayoff_13").text(funcHourToDayText(data.dayoff13));

                var arr_dayoffset = Dayoff_set.find({ all_day_off_class: { $exists: true } }).fetch();
                for (var i = 0; i < arr_dayoffset.length; i++) {
                    var do_class = arr_dayoffset[i].all_day_off_class;
                    var left_hours = arr_dayoffset[i].available_days * 8 - data['dayoff' + do_class];
                    $("#dayoff_" + do_class + "_sur").text(funcHourToDayText(left_hours));
                    var left_hours_2 = Number(data.dayoff13) - Number(data.dayoff12);
                    $("#dayoff_12_sur").text(funcHourToDayText(left_hours_2));
                }
            } else {
                $("#dayoff_1").text("0時");
                $("#dayoff_2").text("0時");
                $("#dayoff_3").text("0時");
                $("#dayoff_4").text("0時");
                $("#dayoff_5").text("0時");
                $("#dayoff_6").text("0時");
                $("#dayoff_7").text("0時");
                $("#dayoff_8").text("0時");
                $("#dayoff_9").text("0時");
                $("#dayoff_10").text("0時");
                $("#dayoff_11").text("0時");
                $("#dayoff_12").text("0時");
                $("#dayoff_13").text("0時");
            }
        }
    });
};
Template.dayoff.helpers({
    acc_year: Accountyear.find({}, { sort: { value: 1 } }),
    get_objDay_off: function() {
        return objDay_off;
    },
    get_bjDay_off_status: function() {
        return objDay_off_status;
    },
});

Template.dayoff.events({
    'click .btn-new_1': function() {
        var formVar = {};
        formVar.dayoff_status = "1";
        formVar.apply_chtname = Meteor.user().profile.chtname;
        formVar.apply_id = Meteor.userId();

        $("#e_dayoff").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {} else {}
        });
    },
    'click .btn-dayoff_1': function() {
        $("#Day_off_3").hide();
        $("#Day_off_1").fadeIn();

        $(".btn-dayoff_1").hide();
        $(".btn-dayoff_3").fadeIn();


        $("#e_dayoff").jsGrid("loadData");
        $("#e_dayoff_records").jsGrid("loadData");
        $("#e_leave_notice3").jsGrid("loadData");
    },
    'click .btn-dayoff_3': function() {
        $("#Day_off_1").hide();
        $("#Day_off_3").fadeIn();

        $(".btn-dayoff_3").hide();
        $(".btn-dayoff_1").fadeIn();

        $("#e_leave_notice1").jsGrid("loadData");
        $("#e_leave_notice2").jsGrid("loadData");
        $("#e_leave_notice3").jsGrid("loadData");
    },
    "change #type_id": function(event, template) {
        if ($(event.target).hasClass("type_id")) {
            $("#e_dayoff_records").jsGrid("loadData");
        }
    },
    "change #dayoff_sel": function(event, template) { //切換會計年度
        var getDayoffSelYear = Number($("#dayoff_sel").val());

        var userData = Meteor.user();
        if (Meteor.user().arr_dayoff && Meteor.user().arr_dayoff.length > 0 && !!getDayoffSelYear) { // 有休假的欄位可以顯示
            var arr = Meteor.user().arr_dayoff;
            var data = {};
            var isData = 0;

            for (var i = 0; i < arr.length; i++) {
                if (arr[i].year == getDayoffSelYear) {
                    isData = 1;
                    data = arr[i];
                    break;
                }
            }

            if (isData == 1) {
                $("#dayoff_1").text(funcHourToDayText(data.dayoff1));
                $("#dayoff_2").text(funcHourToDayText(data.dayoff2));
                $("#dayoff_3").text(funcHourToDayText(data.dayoff3));
                $("#dayoff_4").text(funcHourToDayText(data.dayoff4));
                $("#dayoff_5").text(funcHourToDayText(data.dayoff5));
                $("#dayoff_6").text(funcHourToDayText(data.dayoff6));
                $("#dayoff_7").text(funcHourToDayText(data.dayoff7));
                $("#dayoff_8").text(funcHourToDayText(data.dayoff8));
                $("#dayoff_9").text(funcHourToDayText(data.dayoff9));
                $("#dayoff_10").text(funcHourToDayText(data.dayoff10));
                $("#dayoff_11").text(funcHourToDayText(data.dayoff11));
                $("#dayoff_12").text(funcHourToDayText(data.dayoff12));
                $("#dayoff_13").text(funcHourToDayText(data.dayoff13));

                var arr_dayoffset = Dayoff_set.find({ all_day_off_class: { $exists: true } }).fetch();
                for (var i = 0; i < arr_dayoffset.length; i++) {
                    var do_class = arr_dayoffset[i].all_day_off_class;
                    var left_hours = arr_dayoffset[i].available_days * 8 - data['dayoff' + do_class];
                    $("#dayoff_" + do_class + "_sur").text(funcHourToDayText(left_hours));
                    var left_hours_2 = Number(data.dayoff13) - Number(data.dayoff12)
                    $("#dayoff_12_sur").text(funcHourToDayText(left_hours_2));
                }
            } else {
                $("#dayoff_1").text("0時");
                $("#dayoff_2").text("0時");
                $("#dayoff_3").text("0時");
                $("#dayoff_4").text("0時");
                $("#dayoff_5").text("0時");
                $("#dayoff_6").text("0時");
                $("#dayoff_7").text("0時");
                $("#dayoff_8").text("0時");
                $("#dayoff_9").text("0時");
                $("#dayoff_10").text("0時");
                $("#dayoff_11").text("0時");
                $("#dayoff_12").text("0時");
                $("#dayoff_13").text("0時");
            }
        }
    },
});
