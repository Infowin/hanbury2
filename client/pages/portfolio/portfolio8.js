Template.portfolio8.rendered = function(){
    var data = Blaze.getData();

    $("#account_num").text(data.account_num);
    $("#account_num2").text(data.account_num);
    $("#provider_id").text(Provider.findOne({_id:data.provider_id}).name_cht || "");
    $("#provider_engtext").text(data.provider_engtext);

    $("#product4_text").text(data.product4_text);
    $("#product4_engtext").text(data.product4_engtext);
    $("#apply_birthday").text(data.apply_birthday);
    $("#apply_email").text(data.apply_email);
    $("#apply_telephone").text(data.apply_telephone);
    $("#apply_addr").text(data.apply_addr);
    $("#policy_firstperson").text(data.name_eng || data.policy_firstperson);
    $("#apply_passport").text(data.apply_passport);
    $("#policy_secondperson").text(data.name_eng || data.policy_secondperson);
    $("#apply_cellphone").text(data.apply_cellphone);
    $("#policy_effective_date").text(data.policy_effective_date);
    $("#hsbc_account_name").text(data.hsbc_account_name);
    $("#policy_end_date").text(data.policy_end_date);
    $("#hsbc_account_num").text(data.hsbc_account_num);
    // $("#interest_condition").text(data.interest_condition);
    // $("#policy_period_year").text(funcObjFind2(objYearNum, data.policy_period_year));
    $("#policy_period_year").text(data.policy_period_year);
    $("#policy_period_month").text(funcObjFind2(objPayMonthNum, data.policy_period_month) || "0月");
    $("#creditcard_num").text(data.creditcard_num);
    $("#insurance_status").text(funcObjFind2(objInsuranceStatus, data.insurance_status));
    $("#creditcard_date").text(data.creditcard_date);
    $("#insurancepay_status").text(funcObjFind2(objInsurancePayStatus, data.insurancepay_status));
    $("#creditcard_bank").text(data.creditcard_bank);
    $("#policy_endpay_date").text(data.policy_endpay_date);
    $("#creditcard_bankname").text(data.creditcard_bankname);
    $("#policy_nextpay_date").text(data.policy_nextpay_date);
    $("#creditcard_name").text(data.creditcard_name);
    $("#paymethod_fpi").text(funcObjFind2(objPayMethodFPI, data.paymethod_fpi));
    $("#creditcard_addr").text(data.creditcard_addr);
    $("#payperiod_fpi").text(funcObjFind2(objPayPeriodFPI, data.payperiod_fpi));
    $("#policy_currency_type").text(funcObjFind2(objSalaryCash, data.policy_currency_type));
    $("#nowphase").text(funcObjFind2(objNowPhase, data.nowphase));

    $("#fpi_now_same_payment").text( commaSeparateNumber(data.fpi_now_same_payment));
    $("#fpi_all_withdraw_money").text(commaSeparateNumber(data.fpi_all_withdraw_money));
    $("#fpi_done_withdraw_money").text(commaSeparateNumber(data.fpi_done_withdraw_money));
    $("#fpi_able_withdraw_money").text(commaSeparateNumber(data.fpi_able_withdraw_money));
    $("#fpi_data_updatedate").text(data.fpi_data_updatedate);
    $("#account_invest_per").text(data.account_invest_per);

    $("#terminal_cal").text(data.terminal_cal);
    $("#terminal_price").text(data.terminal_price);
    $("#terminal_restvalue").text(data.terminal_restvalue);

    $("#beneficial1_name")        .text(data.beneficial1_name);
    $("#beneficial1_name2")        .text(data.beneficial1_name2);
    $("#beneficial1_percent")     .text(data.beneficial1_percent);
    $("#beneficial1_relationship").text(data.beneficial1_relationship);
    $("#beneficial2_name")        .text(data.beneficial2_name);
    $("#beneficial2_name2")        .text(data.beneficial2_name2);
    $("#beneficial2_percent")     .text(data.beneficial2_percent);
    $("#beneficial2_relationship").text(data.beneficial2_relationship);
    $("#beneficial3_name")        .text(data.beneficial3_name);
    $("#beneficial3_name2")        .text(data.beneficial3_name2);
    $("#beneficial3_percent")     .text(data.beneficial3_percent);
    $("#beneficial3_relationship").text(data.beneficial3_relationship);

    $("#lockmonth").text(funcObjFind2(objLockMonth, data.lockmonth));
    $("#lockmonth2").text(data.lockmonth2);

    Template.portfolio.loadForm();
    $("#e_followservice").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                filter.portfolio_id = data._id;
                filter.pf6_status = "1"

                return jsgridAjax(filter, "afterservice", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "afterservice", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "afterservice", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "afterservice", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:60, editButton: false },
            { name: "apply_btn", title: "#", type: "text", width:60, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(value);
                    // console.log(item);
                    return '<a href="#" data-toggle="modal" data-target="#myModal" data-index="'+item.myItemIndex+'">'+item.uid+'</a>';
                }
            },
            { name: "p_uid", title: "Portfolio", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.portfolio && !!item.portfolio.uid)
                        return '<a href="/portfolio/'+item.portfolio._id+'" target="_blank">'+item.portfolio.uid+'</a>';
                    else
                        return "";
                },
            },
            // { name: "provider_text", title: "Provider", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.provider_engtext)
            //             return item.portfolio.provider_engtext;
            //         else
            //             return "";
            //     },
            // },
            { name: "insertedAt", title: "Date", width:100,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
            // { name: "agent_text", title: "Agent Name", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.agent_text)
            //             return item.portfolio.agent_text;
            //         else
            //             return "";
            //     },
            // },
            // { name: "name_eng", title: "Client Name", type: "text",
            //     // itemTemplate: function(value, item) {
            //     //     if(!!item.insertedAt)
            //     //         return item.provider_engtext;
            //     //     else
            //     //         return "";
            //     // },
            // },
            { name: "policy_num", title: "Service Item", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.product4_engtext)
                        return item.product4_engtext;
                    else
                        return "";
                },
            },
            { name: "status", title: "Status", type: "text",
              itemTemplate: function(value, item) {
                return funcObjFind(objASPhase, value)
              }
            },
            { name: "description", title: "Description", type: "text"},
            { name: "as_template_text", title: "Action", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            { name: "as_owner_text", title: "Action Owner", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            // { name: "contactnum", title: "", type: "text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_account").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account").jsGrid("loadData");
        },
    });
};

Template.portfolio8.helpers({
    nowUpdatedAt: function(){
        return (this.updatedAt || this.insertedAt).yyyymmddhm();
    },
});

Template.portfolio8.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});
