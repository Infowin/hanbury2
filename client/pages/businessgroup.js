Template.businessgroup.rendered = function () {
    Template.semicolon.loadjs();
    $("#e_businessgroup").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                return jsgridAjax(filter, "businessgroup", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "businessgroup", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "businessgroup", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "businessgroup", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 10 },
            { name: "value", title: "隸屬事業群", width: 60, type: "text", align: "center" },
        ],
        onDataLoading: function (args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function (args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function (args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_businessgroup").jsGrid("loadData");
        },
        onItemDeleted: function (args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_businessgroup").jsGrid("loadData");
        },
        onRefreshed: function () {
            var $gridData = $("#e_businessgroup .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function (e, ui) {
                    var items = $.map($gridData.find("tr"), function (row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortBusinessgroup", items, function (error, result) {
                        if (error) {
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_businessgroup").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};

Template.businessgroup.helpers({
    get_countries: function () {
        return objCountries;
    },
});

Template.businessgroup.events({
    'click .btn-new': function () {
        $("#e_businessgroup").jsGrid("insertItem", {}).done(function (ret) {
            $("#e_businessgroup").jsGrid("loadData");
        });
    },
});

