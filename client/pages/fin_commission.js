Template.fin_commission.rendered = function () {
  Template.semicolon.loadjs();
  var data = this.data;
  $("#title").text(data.value);

  var year_form = $("#year_id").val();
  $(".year_form").text(year_form + "年");

  Session.set("bankacc_bg", data);


  $("#e_commission_0").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      // loadData: function (filter) {
      //   filter.year = $("#year_id").val();
      //   filter.month = "1"
      //   var bg = Session.get("bankacc_bg");
      //   filter.bg_id = bg._id;
      //   return jsgridAjax(filter, "commission", "GET");
      // },
      // insertItem: function (item) {
      //   return jsgridAjax(item, "commission", "POST");
      // },
      // updateItem: function (item) {
      //   return jsgridAjax(item, "commission", "PUT");
      // },
      // deleteItem: function (item) {
      //   return jsgridAjax(item, "commission", "DELETE");
      // },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "", title: "年", align: "center", width: 60 },
      { name: "", title: "月", align: "center", width: 60 },
      { name: "", title: "應收金額", align: "center", width: 60 },
      { name: "", title: "實收金額", align: "center", width: 60 },
      { name: "", title: "公司應收佣金", align: "center", width: 60 },
      { name: "", title: "公司實收傭金", align: "center", width: 60 },
      { name: "", title: "Agent佣金", align: "center", width: 60 },
      { name: "", title: "客戶利息", align: "center", width: 60 },
      { name: "", title: "淨利", align: "center", width: 60 },
      { name: "", title: "件數", align: "center", width: 60 },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_1").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onItemDeleted: function (args) {
      $("#e_commission_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })

  $("#e_commission_0a").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      // loadData: function (filter) {
      //   filter.year = $("#year_id").val();
      //   filter.month = "1"
      //   var bg = Session.get("bankacc_bg");
      //   filter.bg_id = bg._id;
      //   return jsgridAjax(filter, "commission", "GET");
      // },
      // insertItem: function (item) {
      //   return jsgridAjax(item, "commission", "POST");
      // },
      // updateItem: function (item) {
      //   return jsgridAjax(item, "commission", "PUT");
      // },
      // deleteItem: function (item) {
      //   return jsgridAjax(item, "commission", "DELETE");
      // },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "", title: "年", align: "center", width: 60 },
      { name: "", title: "應收金額", align: "center", width: 60 },
      { name: "", title: "實收金額", align: "center", width: 60 },
      { name: "", title: "公司應收佣金", align: "center", width: 60 },
      { name: "", title: "公司實收傭金", align: "center", width: 60 },
      { name: "", title: "Agent佣金", align: "center", width: 60 },
      { name: "", title: "客戶利息", align: "center", width: 60 },
      { name: "", title: "淨利", align: "center", width: 60 },
      { name: "", title: "件數", align: "center", width: 60 },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_1").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onItemDeleted: function (args) {
      $("#e_commission_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_0b").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "1"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      { name: "", title: "一月保額", align: "center", width: 60 },
      { name: "", title: "一月實收保費", align: "center", width: 60 },
      { name: "", title: "一月佣金", align: "center", width: 60 },
      { name: "", title: "二月保額", align: "center", width: 60 },
      { name: "", title: "二月實收保費", align: "center", width: 60 },
      { name: "", title: "二月佣金", align: "center", width: 60 },
      { name: "", title: "三月保額", align: "center", width: 60 },
      { name: "", title: "三月實收保費", align: "center", width: 60 },
      { name: "", title: "三月佣金", align: "center", width: 60 },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment;
          return commaSeparateNumber(item.Commission_Total);
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_1").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onItemDeleted: function (args) {
      $("#e_commission_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_1").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "1"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance);
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate);
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment);
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment;
          return commaSeparateNumber(item.Commission_Total);
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_1").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onItemDeleted: function (args) {
      $("#e_commission_1").jsGrid("loadData");
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_2").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "2"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_2").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_3").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "3"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_3").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_4").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "4"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_4").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_5").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "5"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_5").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_6").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "6"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_6").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_7").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "7"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_7").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_8").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "8"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_8").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_9").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "9"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_9").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_10").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "10"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_10").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_11").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "11"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_11").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_12").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "12"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_12").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_13").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "13"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment;
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_13").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_14").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "14"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_14").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_15").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "15"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_15").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_16").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "16"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_16").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_17").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "17"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_17").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_18").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "18"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_18").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_19").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "19"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_19").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_20").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "20"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_20").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_21").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "21"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_21").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_22").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "22"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_22").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_23").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "23"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_23").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })
  $("#e_commission_24").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    // paging: true,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function (filter) {
        filter.year = $("#year_id").val();
        filter.month = "24"
        var bg = Session.get("bankacc_bg");
        filter.bg_id = bg._id;
        return jsgridAjax(filter, "commission", "GET");
      },
      insertItem: function (item) {
        return jsgridAjax(item, "commission", "POST");
      },
      updateItem: function (item) {
        return jsgridAjax(item, "commission", "PUT");
      },
      deleteItem: function (item) {
        return jsgridAjax(item, "commission", "DELETE");
      },
    },
    fields: [
      {
        type: "control", width: 50,
        itemTemplate: function (_, item) {
          if (item.IsTotal)
            return "";
          return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
        }
      },
      { name: "chtname", title: "員工姓名", align: "center", width: 60 },
      {
        name: "Commission_Insurance", title: "保險", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Insurance = Number(item.Commission_Insurance) || 0;
          return commaSeparateNumber(item.Commission_Insurance)
        }
      },
      {
        name: "Commission_Estate", title: "房產", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Estate = Number(item.Commission_Estate) || 0;
          return commaSeparateNumber(item.Commission_Estate)
        }
      },
      {
        name: "Commission_Investment", title: "投資", type: "text", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Investment = Number(item.Commission_Investment) || 0;
          return commaSeparateNumber(item.Commission_Investment)
        }
      },
      {
        name: "Commission_Total", title: "總和", align: "center",
        itemTemplate: function (value, item) {
          item.Commission_Total = item.Commission_Insurance + item.Commission_Estate + item.Commission_Investment
          return commaSeparateNumber(item.Commission_Total)
        }
      },
      {
        name: "blank_doc", title: "上傳", align: "center", editing: false, width: 100,
        itemTemplate: function (value, item) {
          if (item.IsTotal)
            return "";

          var str = "";
          if (!!item.upload_url) {
            str = str + '<a target="_blank" href="' + item.upload_url + '">' + "[預覽]" + '</a> ';
          }
          str = str + '<a href="#" data-id="' + item._id + '" data-toggle="modal" data-target="#myModal2" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + "[上傳]" + '</a>';
          return str;
        }
      },
    ],
    onItemUpdated: function (args) {
      $("#e_commission_24").jsGrid("loadData");
      Session.set("commission_update_time", new Date().getTime());
    },
    onRefreshed: function (args) {
      var items = args.grid.option("data");
      var total = { chtname: "Total", "Commission_Insurance": 0, "Commission_Estate": 0, "Commission_Investment": 0, IsTotal: true };

      items.forEach(function (item) {
        total.Commission_Insurance += (Number(item.Commission_Insurance) || 0);
        total.Commission_Estate += (Number(item.Commission_Estate) || 0);
        total.Commission_Investment += (Number(item.Commission_Investment) || 0);
      });

      var $totalRow = $("<tr>").addClass("total-row");

      args.grid._renderCells($totalRow, total);

      args.grid._content.append($totalRow);
    },
  })

  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });

  $('#myModal1').on('show.bs.modal', function (e) {
    $(".check").prop('checked', false);
    var id = $(e.relatedTarget).data("id");
    $("#month_id").val(id);
  })

  $('#myModal2').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).data("id");
    $("#data_id").val(id);
    console.log(id)
    $(".progress").hide();
  })

  $("#input-3").fileinput({
    'language': "zh-TW",
  });

  $('#collapseOne').on('show.bs.collapse', function () {
    $("#e_commission_1").jsGrid("loadData");
    $("#e_commission_2").jsGrid("loadData");
    $("#e_commission_3").jsGrid("loadData");
    $("#e_commission_4").jsGrid("loadData");
    $("#e_commission_5").jsGrid("loadData");
    $("#e_commission_6").jsGrid("loadData");
  })
  $('#collapseTwo').on('show.bs.collapse', function () {
    $("#e_commission_7").jsGrid("loadData");
    $("#e_commission_8").jsGrid("loadData");
    $("#e_commission_9").jsGrid("loadData");
    $("#e_commission_10").jsGrid("loadData");
    $("#e_commission_11").jsGrid("loadData");
    $("#e_commission_12").jsGrid("loadData");
  })
  $('#collapseThree').on('show.bs.collapse', function () {
    $("#e_commission_13").jsGrid("loadData");
    $("#e_commission_14").jsGrid("loadData");
    $("#e_commission_15").jsGrid("loadData");
    $("#e_commission_16").jsGrid("loadData");
    $("#e_commission_17").jsGrid("loadData");
    $("#e_commission_18").jsGrid("loadData");
  })
  $('#collapseFour').on('show.bs.collapse', function () {
    $("#e_commission_19").jsGrid("loadData");
    $("#e_commission_20").jsGrid("loadData");
    $("#e_commission_21").jsGrid("loadData");
    $("#e_commission_22").jsGrid("loadData");
    $("#e_commission_23").jsGrid("loadData");
    $("#e_commission_24").jsGrid("loadData");
  })


  // var obj_season_1_salary = Commission.find({$and: [
  //     {$or: [{"month": "1"}, {"month": "2"}, {"month": "3"}, {"month": "4"}, {"month": "5"}, {"month": "6"}]},
  //     {"year": $("#year_id").val()}
  // ],}).fetch();

  // var obj_season_2_salary = Commission.find({$and: [
  //     {$or: [{"month": "7"}, {"month": "8"}, {"month": "9"}, {"month": "10"}, {"month": "11"}, {"month": "12"}]},
  //     {"year": $("#year_id").val()}
  // ],}).fetch();

  // var obj_season_3_salary = Commission.find({$and: [
  //     {$or: [{"month": "13"}, {"month": "14"}, {"month": "15"}, {"month": "16"}, {"month": "17"}, {"month": "18"}]},
  //     {"year": $("#year_id").val()}
  // ],}).fetch();

  // var obj_season_4_salary = Commission.find({$and: [
  //     {$or: [{"month": "19"}, {"month": "20"}, {"month": "21"}, {"month": "22"}, {"month": "23"}, {"month": "24"}]},
  //     {"year": $("#year_id").val()}
  // ],}).fetch();

  // var num_season_1_total = 0;
  // var num_season_2_total = 0;
  // var num_season_3_total = 0;
  // var num_season_4_total = 0;

  // for (var i = 0; i < obj_season_1_salary.length; i++) {
  //     var entry = obj_season_1_salary[i];
  //     entry.Commission_Insurance = entry.Commission_Insurance || 0;
  //     entry.Commission_Estate = entry.Commission_Estate || 0;
  //     entry.Commission_Investment = entry.Commission_Investment || 0;
  //     num_season_1_total += (Number(entry.Commission_Insurance) || 0 ) + (Number(entry.Commission_Estate) || 0 ) + (Number(entry.Commission_Investment) || 0 ) ;
  // }
  // for (var i = 0; i < obj_season_2_salary.length; i++) {
  //     var entry = obj_season_2_salary[i];
  //     entry.Commission_Insurance = entry.Commission_Insurance || 0;
  //     entry.Commission_Estate = entry.Commission_Estate || 0;
  //     entry.Commission_Investment = entry.Commission_Investment || 0;
  //     num_season_2_total += (Number(entry.Commission_Insurance) || 0 ) + (Number(entry.Commission_Estate) || 0 ) + (Number(entry.Commission_Investment) || 0 ) ;
  // }
  // for (var i = 0; i < obj_season_3_salary.length; i++) {
  //     var entry = obj_season_3_salary[i];
  //     entry.Commission_Insurance = entry.Commission_Insurance || 0;
  //     entry.Commission_Estate = entry.Commission_Estate || 0;
  //     entry.Commission_Investment = entry.Commission_Investment || 0;
  //     num_season_3_total += (Number(entry.Commission_Insurance) || 0 ) + (Number(entry.Commission_Estate) || 0 ) + (Number(entry.Commission_Investment) || 0 ) ;
  // }
  // for (var i = 0; i < obj_season_4_salary.length; i++) {
  //     var entry = obj_season_4_salary[i];
  //     entry.Commission_Insurance = entry.Commission_Insurance || 0;
  //     entry.Commission_Estate = entry.Commission_Estate || 0;
  //     entry.Commission_Investment = entry.Commission_Investment || 0;
  //     num_season_4_total += (Number(entry.Commission_Insurance) || 0 ) + (Number(entry.Commission_Estate) || 0 ) + (Number(entry.Commission_Investment) || 0 ) ;
  // }


  // $("#num_season_1_total").text(commaSeparateNumber(num_season_1_total));
  // $("#num_season_2_total").text(commaSeparateNumber(num_season_2_total));
  // $("#num_season_3_total").text(commaSeparateNumber(num_season_3_total));
  // $("#num_season_4_total").text(commaSeparateNumber(num_season_4_total));
  // $("#num_full_year_total").text(commaSeparateNumber(num_season_1_total + num_season_2_total + num_season_3_total + num_season_4_total));

  $("#year_id").change(function () {
    console.log("change")
    let obj_season_1_salary = Commission.find({
      $and: [
        { $or: [{ "month": "1" }, { "month": "2" }, { "month": "3" }, { "month": "4" }, { "month": "5" }, { "month": "6" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_2_salary = Commission.find({
      $and: [
        { $or: [{ "month": "7" }, { "month": "8" }, { "month": "9" }, { "month": "10" }, { "month": "11" }, { "month": "12" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_3_salary = Commission.find({
      $and: [
        { $or: [{ "month": "13" }, { "month": "14" }, { "month": "15" }, { "month": "16" }, { "month": "17" }, { "month": "18" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let obj_season_4_salary = Commission.find({
      $and: [
        { $or: [{ "month": "19" }, { "month": "20" }, { "month": "21" }, { "month": "22" }, { "month": "23" }, { "month": "24" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    let num_season_1_total = 0;
    let num_season_2_total = 0;
    let num_season_3_total = 0;
    let num_season_4_total = 0;

    for (var i = 0; i < obj_season_1_salary.length; i++) {
      var entry = obj_season_1_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_1_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_2_salary.length; i++) {
      var entry = obj_season_2_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_2_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_3_salary.length; i++) {
      var entry = obj_season_3_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_3_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_4_salary.length; i++) {
      var entry = obj_season_4_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_4_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }

    $("#num_season_1_total").text(commaSeparateNumber(num_season_1_total));
    $("#num_season_2_total").text(commaSeparateNumber(num_season_2_total));
    $("#num_season_3_total").text(commaSeparateNumber(num_season_3_total));
    $("#num_season_4_total").text(commaSeparateNumber(num_season_4_total));
    $("#num_full_year_total").text(commaSeparateNumber(num_season_1_total + num_season_2_total + num_season_3_total + num_season_4_total));
  });

  this.autorun(function () {
    // console.log("autorun: "+Session.get("commission_update_time"));
    Session.get("commission_update_time");
    // $("#year_id").val();

    var obj_season_1_salary = Commission.find({
      $and: [
        { $or: [{ "month": "1" }, { "month": "2" }, { "month": "3" }, { "month": "4" }, { "month": "5" }, { "month": "6" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    var obj_season_2_salary = Commission.find({
      $and: [
        { $or: [{ "month": "7" }, { "month": "8" }, { "month": "9" }, { "month": "10" }, { "month": "11" }, { "month": "12" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    var obj_season_3_salary = Commission.find({
      $and: [
        { $or: [{ "month": "13" }, { "month": "14" }, { "month": "15" }, { "month": "16" }, { "month": "17" }, { "month": "18" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    var obj_season_4_salary = Commission.find({
      $and: [
        { $or: [{ "month": "19" }, { "month": "20" }, { "month": "21" }, { "month": "22" }, { "month": "23" }, { "month": "24" }] },
        { "year": $("#year_id").val() }
      ],
    }).fetch();

    var num_season_1_total = 0;
    var num_season_2_total = 0;
    var num_season_3_total = 0;
    var num_season_4_total = 0;

    for (var i = 0; i < obj_season_1_salary.length; i++) {
      var entry = obj_season_1_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_1_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_2_salary.length; i++) {
      var entry = obj_season_2_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_2_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_3_salary.length; i++) {
      var entry = obj_season_3_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_3_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }
    for (var i = 0; i < obj_season_4_salary.length; i++) {
      var entry = obj_season_4_salary[i];
      entry.Commission_Insurance = entry.Commission_Insurance || 0;
      entry.Commission_Estate = entry.Commission_Estate || 0;
      entry.Commission_Investment = entry.Commission_Investment || 0;
      num_season_4_total += (Number(entry.Commission_Insurance) || 0) + (Number(entry.Commission_Estate) || 0) + (Number(entry.Commission_Investment) || 0);
    }


    $("#num_season_1_total").text(commaSeparateNumber(num_season_1_total));
    $("#num_season_2_total").text(commaSeparateNumber(num_season_2_total));
    $("#num_season_3_total").text(commaSeparateNumber(num_season_3_total));
    $("#num_season_4_total").text(commaSeparateNumber(num_season_4_total));
    $("#num_full_year_total").text(commaSeparateNumber(num_season_1_total + num_season_2_total + num_season_3_total + num_season_4_total));
  });

  for (let i = 0; i < 25; i++) {
    $("#csv_commission_" + i).on('click', function (event) {
      var csv_year = $("#year_id").val() + "年"
      var args = [$("#e_commission_" + i), '' + data.value + csv_year + '發放佣金總額_' + i + '.csv'];
      exportTableToCSV.apply(this, args);
    });
  }

  var d = new Date();
  $("#year_id").val(d.getFullYear());
  $(".sel_filter").trigger("change");
};

Template.fin_commission.helpers({
  acc_year: Accountyear.find({}, { sort: { value: 1 } }),
  get_bg: function () {
    var r = Router.current().params;
    return r.f1_id;
  },
  get_user: function () {
    var arr = Meteor.users.find(
      {
        $and: [
          { $or: [{ 'auth.is_auth': '1' }, { 'auth.is_auth': { $exists: false } }] },
          { 'profile.department_id': Router.current().params.f1_id }
        ]
      }).fetch();
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
      var obj = {};
      obj._id = arr[i]._id;
      obj.engname = arr[i].profile.engname;
      obj.chtname = arr[i].profile.chtname;
      resArr.push(obj);
    }

    return resArr;
  },
  "files": function () {
    return S3.collection.find();
  }
});

Template.fin_commission.events({
  "change .sel_filter": function (event, template) {
    var year_form = $("#year_id").val();
    $(".year_form").text(year_form + "年");
    $("#e_commission_1").jsGrid("loadData");
    $("#e_commission_2").jsGrid("loadData");
    $("#e_commission_3").jsGrid("loadData");
    $("#e_commission_4").jsGrid("loadData");
    $("#e_commission_5").jsGrid("loadData");
    $("#e_commission_6").jsGrid("loadData");
    $("#e_commission_7").jsGrid("loadData");
    $("#e_commission_8").jsGrid("loadData");
    $("#e_commission_9").jsGrid("loadData");
    $("#e_commission_10").jsGrid("loadData");
    $("#e_commission_11").jsGrid("loadData");
    $("#e_commission_12").jsGrid("loadData");
    $("#e_commission_13").jsGrid("loadData");
    $("#e_commission_14").jsGrid("loadData");
    $("#e_commission_15").jsGrid("loadData");
    $("#e_commission_16").jsGrid("loadData");
    $("#e_commission_17").jsGrid("loadData");
    $("#e_commission_18").jsGrid("loadData");
    $("#e_commission_19").jsGrid("loadData");
    $("#e_commission_20").jsGrid("loadData");
    $("#e_commission_21").jsGrid("loadData");
    $("#e_commission_22").jsGrid("loadData");
    $("#e_commission_23").jsGrid("loadData");
    $("#e_commission_24").jsGrid("loadData");
  },
  'click .btn-m1-new': function (e) {
    var formVar = $(".form-modal1").serializeObject();
    var year = $("#year_id").find("option:selected").text();
    formVar.year = year;
    var bg = Session.get("bankacc_bg");
    formVar.bg_id = bg._id;
    // var month = $(e.relatedTarget).data("id");
    formVar.month = $("#month_id").val();
    Meteor.call("addCommissionMember", formVar, function (error, result) {
      if (error) {
        console.log("error from updateSortUploads: ", error);
      }
      else {
        $("#e_commission_1").jsGrid("loadData");
        $("#e_commission_2").jsGrid("loadData");
        $("#e_commission_3").jsGrid("loadData");
        $("#e_commission_4").jsGrid("loadData");
        $("#e_commission_5").jsGrid("loadData");
        $("#e_commission_6").jsGrid("loadData");
        $("#e_commission_7").jsGrid("loadData");
        $("#e_commission_8").jsGrid("loadData");
        $("#e_commission_9").jsGrid("loadData");
        $("#e_commission_10").jsGrid("loadData");
        $("#e_commission_11").jsGrid("loadData");
        $("#e_commission_12").jsGrid("loadData");
        $("#e_commission_13").jsGrid("loadData");
        $("#e_commission_14").jsGrid("loadData");
        $("#e_commission_15").jsGrid("loadData");
        $("#e_commission_16").jsGrid("loadData");
        $("#e_commission_17").jsGrid("loadData");
        $("#e_commission_18").jsGrid("loadData");
        $("#e_commission_19").jsGrid("loadData");
        $("#e_commission_20").jsGrid("loadData");
        $("#e_commission_21").jsGrid("loadData");
        $("#e_commission_22").jsGrid("loadData");
        $("#e_commission_23").jsGrid("loadData");
        $("#e_commission_24").jsGrid("loadData");
      }
    });
  },
  'submit .form-modal1': function (event) {
    event.preventDefault();
    event.stopPropagation();
    var formVar = $(".form-modal1").serializeObject();
    $("#e_commission_1").jsGrid("insertItem", formVar).done(function (ret) {
      if (ret.insert == "success") {
        $(".form-modal1").trigger('reset');
        $("#e_commission_1").jsGrid("loadData");
        // Materialize.toast('資料已新增', 3000, 'rounded');
      } else {
        $(".modal1-error").text(ret);
      }
    });
  },
  "click a.upload1": function () {
    var files = $("input.file_bag")[0].files;
    var now_id = this._id;

    S3.upload({
      files: files,
      uploader: "1",
      path: "subfolder"
    }, function (e, r) {
      // console.log(r);
      $('#input-3').fileinput('clear');

      var formVar = {};

      var file = {
        fin_upload: "1",
        fin_id: $("#data_id").val(),
        name: r.file.original_name,
        size: r.file.size,
        file: r,
        image_id: r._id,
        url: r.secure_url,
        insertedById: Meteor.userId(),
        insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
        insertedAt: new Date()
      };

      formVar.upload_url = r.secure_url;
      formVar.upload = file;
      // console.log("formVar");
      // console.log(formVar);
      // return;/

      Meteor.call("updateCommissionUpload", $("#data_id").val(), formVar, function (error, result) {
        if (error) {
          console.log("error from updateSortUploads: ", error);
        }
        else {
          $("#e_commission_1").jsGrid("loadData");
          $("#e_commission_2").jsGrid("loadData");
          $("#e_commission_3").jsGrid("loadData");
          $("#e_commission_4").jsGrid("loadData");
          $("#e_commission_5").jsGrid("loadData");
          $("#e_commission_6").jsGrid("loadData");
          $("#e_commission_7").jsGrid("loadData");
          $("#e_commission_8").jsGrid("loadData");
          $("#e_commission_9").jsGrid("loadData");
          $("#e_commission_10").jsGrid("loadData");
          $("#e_commission_11").jsGrid("loadData");
          $("#e_commission_12").jsGrid("loadData");
          $("#e_commission_13").jsGrid("loadData");
          $("#e_commission_14").jsGrid("loadData");
          $("#e_commission_15").jsGrid("loadData");
          $("#e_commission_16").jsGrid("loadData");
          $("#e_commission_17").jsGrid("loadData");
          $("#e_commission_18").jsGrid("loadData");
          $("#e_commission_19").jsGrid("loadData");
          $("#e_commission_20").jsGrid("loadData");
          $("#e_commission_21").jsGrid("loadData");
          $("#e_commission_22").jsGrid("loadData");
          $("#e_commission_23").jsGrid("loadData");
          $("#e_commission_24").jsGrid("loadData");
        }
      });
      // $("#m2-file1_url").val(r.secure_url);
    });
  },
});
// Template.fin_commission_month.rendered = function() {
//     //全選checkbox
//     $("#checkAll").click(function () {
//         $(".check").prop('checked', $(this).prop('checked'));
//     });
// }

// Template.fin_commission_month.helpers({
//     get_month_member: function(){
// //         // console.log(this);
//         var data = this;
//         return Commission.find({year: data.year, month: data.month});
//     }

// });
// Template.fin_commission_month.events({
//     "click .myFileInput": function(event, template) {
//         $("#progress_bar").hide();
//     },
//     "change .myFileInput": function(event, template) {
//         // console.log("change .myFileInput");

//         var files = $(event.currentTarget).find("input")[0].files;
//         var year = $(event.currentTarget).data("year");
//         var month = $(event.currentTarget).data("month");
//         var employee = $(event.currentTarget).data("fid");
//         var employee_id = $(event.currentTarget).data("eid");
//         var commission_id = $(event.currentTarget).data("sid");

//         S3.upload({
//                 files:files,
//                 uploader: "1",
//                 path:"subfolder"
//         },function(e,r){
//             // console.log(r);

//             var selVar = {};
//             selVar.year        = year;
//             selVar.month       = month;
//             selVar.employee    = employee;
//             selVar.employee_id = employee_id;
//             selVar.commission_id   = commission_id;
//             selVar.file_rurl   = r.relative_url;

//             var formVar = {
//                 employee: "3", // 1:尚未送出 2: 已送出
//                 employee_id: employee_id,
//                 year: year,
//                 month: month,

//                 commission: "1",
//                 name: r.file.original_name,
//                 size: r.file.size,
//                 file: r,
//                 image_id: r._id,
//                 url: r.secure_url,
//                 insertedById: Meteor.userId(),
//                 insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
//                 insertedAt: new Date()
//             };
//             $("#cFiles").jsGrid("insertItem", formVar).done(function(ret) {
//                 // console.log("insertion completed");
//                 // console.log(ret);

//                 if (ret.insert == "success") {
//                     var data = ret.data;

//                     selVar.upload_id = data._id;
//                     Meteor.call("updateCommissionUrl", commission_id, selVar, formVar, function(error, result){
//                         if(error){
//                             console.log("error from updateSortUploads: ", error);
//                         }
//                         else {
//                             // $("#cFiles2").jsGrid("loadData");
//                         }
//                     });

//                 } else {
//                     $(".modal2-error").text(ret);
//                 }

//             });

//         });
//     },
//     'click a.del-commission': function(event) {
//         if(!confirm("確認要刪除嗎?")){
//             return;
//         }

//         var id = $(event.currentTarget).data("sid");
//         var upload_id = $(event.currentTarget).data("uid");
//         var file_rurl = $(event.currentTarget).data("rurl");

//         Meteor.call("delCommission", id, upload_id, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");

//             }
//         });
//     },
//     // 'click .my-xedit': function() {
//     //     // Session.set('counter', Session.get('counter') + 1);
//     //     /*$("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", true);*/
//     //     var now_id = this._id;
//     //     //

//     //     $(".my-xedit-text").each(function() {
//     //         var domId = $(this).attr("id");
//     //         $(this).editable({
//     //             type: 'text',
//     //             name: domId,
//     //             pk: now_id,
//     //             url: '/api/xedituser',
//     //             // title: 'Enter username'
//     //         });
//     //     });
//     // },
// });

// Template.fin_commission.rendered = function() {
//     Template.semicolon.loadjs();
//     var ocImages = $("#oc-images");

//     var data = this.data;
//     $("#title").text(data.value);

//     ocImages.owlCarousel({
//         margin: 30,
//         nav: false,
//         autoplayHoverPause: true,
//         dots: true,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             480: {
//                 items: 2
//             },
//             768: {
//                 items: 3
//             },
//             992: {
//                 items: 4
//             }
//         }
//     });


//     $("#cFiles").jsGrid({
//         // height: "90%",
//         width: "100%",
//         loadIndication: true,
//         autoload: false,
//         pageLoading: true,
//         loadMessage: "讀取中...",
//         deleteConfirm: "確定要刪除此筆資料?",
//         controller:{
//             loadData: function(filter) {
//                 return jsgridAjax(filter, "uploads", "GET");
//             },
//             insertItem: function(item) {
//                 return jsgridAjax(item, "uploads", "POST");
//             },
//             updateItem: function(item) {
//                 return jsgridAjax(item, "uploads", "PUT");
//             },
//             deleteItem: function(item) {
//                 return jsgridAjax(item, "uploads", "DELETE");
//             },
//         },
//         fields: [
//             { name: "ctrl_field", type: "control", width:40, editButton:false},
//             // { type: "control", width:40, editButton:false},
//             { name: "download", width:100, align:"center", title: "檔案",
//                 itemTemplate: function(value, item) {
//                     // console.log(item);
//                     // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
//                     // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
//                     return '<a href="#" data-num="2" data-toggle="modal" data-target="#myModalFile" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+item.name+'</a>';
//                 },
//             },
//             // { name: "uid", title: "#", width: 50 },
//             // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
//             // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
//             // { name: "product2", title: "產品代碼", type: "text" },
//             // { name: "name", title: "檔案名稱", type: "text" },
//             // { name: "receiver_name", title: "收件人", type: "text" },
//             // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
//             // { name: "url", title: "圖片連結", type: "text" },
//             // { name: "size", title: "檔案大小", type: "text" },
//             { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
//         ],
//     });

//     $.fn.editable.defaults.mode = 'inline';
//     $("#unedit-form-btn").hide();

//     // $("#modaluser").serialize()

//     $('#myModal2').on('show.bs.modal', function (event) {
//         var button = $(event.relatedTarget) // Button that triggered the modal
//         // console.log(button);
//         // console.log(button.data("id"));

//         var bg = button.data("bg");
//         var year = button.data("year");
//         var month = button.data("month");

//         $("#m2-bg_id").val(bg);
//         $("#m2-year").val(year);
//         $("#m2-month").val(month);
//     });


// /*    $("#input").fileinput({
//         'language': "zh-TW",
//         'dropZoneEnabled': true
//     });*/

//     $('#myModalFile').on('show.bs.modal', function (event) {
//         var button = $(event.relatedTarget) // Button that triggered the modal
//         // console.log(button);
//         // console.log(button.data("id"));
//         /*var now_data = $(event.target).attr("data-url");*/
//         var index = button.data("index");
//         var num = button.data("num");
//         var url = button.data("url");
//         $("#modal2-link").html( '<a target="_blank" href="'+url+'"> (開新視窗)</a>');

//         $("#file_iframe").attr("src", url);
//     });
//     $('#myModalFile').on('hide.bs.modal', function (event) {
//         $("#file_iframe").attr("src", "");
//     });


//     this.autorun(function(){
//         var arr_ac = Accountyear.find().fetch();

//         for(var i=0; i<arr_ac.length; i++){
//             var entry = arr_ac[i];

//             entry.commission1  = entry.commission1 || "0";
//             entry.commission2  = entry.commission2 || "0";
//             entry.commission3  = entry.commission3 || "0";
//             entry.commission4  = entry.commission4 || "0";
//             entry.commission5  = entry.commission5 || "0";
//             entry.commission6  = entry.commission6 || "0";
//             entry.commission7  = entry.commission7 || "0";
//             entry.commission8  = entry.commission8 || "0";
//             entry.commission9  = entry.commission9 || "0";
//             entry.commission10 = entry.commission10 || "0";
//             entry.commission11 = entry.commission11 || "0";
//             entry.commission12 = entry.commission12 || "0";
//             entry.commission13 = entry.commission13 || "0";
//             entry.commission14 = entry.commission14 || "0";
//             entry.commission15 = entry.commission15 || "0";
//             entry.commission16 = entry.commission16 || "0";
//             entry.commission17 = entry.commission17 || "0";
//             entry.commission18 = entry.commission18 || "0";
//             entry.commission19 = entry.commission19 || "0";
//             entry.commission20 = entry.commission20 || "0";
//             entry.commission21 = entry.commission21 || "0";
//             entry.commission22 = entry.commission22 || "0";
//             entry.commission23 = entry.commission23 || "0";
//             entry.commission24 = entry.commission24 || "0";

//             $("[data-ayid='"+entry._id+"'][data-field='commission1']").text(entry.commission1 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission2']").text(entry.commission2 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission3']").text(entry.commission3 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission4']").text(entry.commission4 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission5']").text(entry.commission5 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission6']").text(entry.commission6 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission7']").text(entry.commission7 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission8']").text(entry.commission8 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission9']").text(entry.commission9 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission10']").text(entry.commission10 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission11']").text(entry.commission11 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission12']").text(entry.commission12 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission13']").text(entry.commission13 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission14']").text(entry.commission14 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission15']").text(entry.commission15 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission16']").text(entry.commission16 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission17']").text(entry.commission17 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission18']").text(entry.commission18 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission19']").text(entry.commission19 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission20']").text(entry.commission20 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission21']").text(entry.commission21 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission22']").text(entry.commission22 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission23']").text(entry.commission23 || "0");
//             $("[data-ayid='"+entry._id+"'][data-field='commission24']").text(entry.commission24 || "0");

//             var season_1 = Number(entry.commission1) + Number(entry.commission2) + Number(entry.commission3) + Number(entry.commission4) + Number(entry.commission5) + Number(entry.commission6);
//             var season_2 = Number(entry.commission7) + Number(entry.commission8) + Number(entry.commission9) + Number(entry.commission10) + Number(entry.commission11) + Number(entry.commission12);
//             var season_3 = Number(entry.commission13) + Number(entry.commission14) + Number(entry.commission15) + Number(entry.commission16) + Number(entry.commission17) + Number(entry.commission18);
//             var season_4 = Number(entry.commission19) + Number(entry.commission20) + Number(entry.commission21) + Number(entry.commission22) + Number(entry.commission23) + Number(entry.commission24);
//             var ta_year = season_1 + season_2 + season_3 + season_4 ;

//             if (isNaN(season_1) == false ) {
//                 $("#cta1-"+entry._id).text(season_1);
//             }
//             if (isNaN(season_2) == false ) {
//                 $("#cta2-"+entry._id).text(season_2);
//             }
//             if (isNaN(season_3) == false ) {
//                 $("#cta3-"+entry._id).text(season_3);
//             }
//             if (isNaN(season_4) == false ) {
//                 $("#cta4-"+entry._id).text(season_4);
//             }

//             $("#ctayear"+entry._id).text(ta_year)
//         }
//     });

//     $(".my-xedit-text").each(function() {
//         var domId = $(this).data("field");
//         var now_id = $(this).data("ayid");
//         $(this).editable({
//             type: 'text',
//             name: domId, // 欄位名稱
//             pk: now_id,  // account year's id
//             url: '/api/xeditaccountyear',
//             // title: 'Enter username'
//         });
//     });
// };

// Template.fin_commission.helpers({
//     acc_year: Accountyear.find({}, {sort:{value:1}}),
//     get_bg: function(){
//         var r = Router.current().params;
//         return r.f1_id;
//     },
//     get_user: function(){
//       var arr = Meteor.users.find(
//         {$and: [
//             {$or: [{'auth.is_auth':'1'},{'auth.is_auth':{ $exists : false }}]},
//             {'profile.department_id':Router.current().params.f1_id}
//         ]}).fetch();
//       var resArr = [];
//       for (var i = 0; i < arr.length; i++) {
//         var obj = {};
//         obj._id = arr[i]._id;
//         obj.engname = arr[i].profile.engname;
//         obj.chtname = arr[i].profile.chtname;
//         resArr.push(obj);
//       }

//       return resArr;
//     },
//     "files": function(){
//         return S3.collection.find();
//     }
// });
// Template.fin_commission.events({
//     'click .btn-m2-new': function() {

//         var formVar = $(".form-modal2").serializeObject();

//         console.log(formVar);
//         // return;
//         Meteor.call("addCommissionMember", formVar, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");
//             }
//         });
//     },
//     'click #edit-form-btn': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         var now_id = this._id;
//         //
//         $(".my-xedit-text").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 // title: 'Enter username'
//             });
//         });
//         $(".my-xedit-date").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 format: 'YYYY/MM/DD',
//                 viewformat: 'YYYY/MM/DD',
//                 template: 'YYYY / MMMM / D',
//                 combodate: {
//                     minYear: 1900,
//                     maxYear: 2020,
//                     minuteStep: 1
//                 }
//             });
//         });
//         $(".my-xedit-sel").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'select',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xedituser',
//                 // source: ,
//                 source: function(){
//                     var objname = $(this).attr("data-obj");
//                     if(objname == "objBodyCheckType1") return objBodyCheckType1;
//                     else if(objname == "objDepartment") return objDepartment;
//                 },
//                 value: $(this).text()
//             });
//         });

//         $("#unedit-form-btn").show();
//         $("#edit-form-btn").hide();
//         $(".on-editing").show();
//     },
//     'click #unedit-form-btn': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         $('.my-xedit').editable('destroy');
//         $("#unedit-form-btn").hide();
//         $("#edit-form-btn").show();
//         $(".on-editing").hide();
//     },
//     'click a.del-file': function(event) {
//         if(!confirm("確認要刪除嗎?")){
//             return;
//         }

//         var id = $(event.currentTarget).data("sid");
//         var upload_id = $(event.currentTarget).data("uid");
//         var file_rurl = $(event.currentTarget).data("rurl");

//         S3.delete(file_rurl, function(e, r){
//             if(!!e) console.log(e);
//             else{
//                 // Materialize.toast('資料已刪除 '+ data.name, 3000, 'rounded');
//                 console.log("資料已刪除"+ data.name);
//             }
//         });
//         Meteor.call("delCommissionUrl", id, upload_id, function(error, result){
//             if(error){
//                 console.log("error from updateSortUploads: ", error);
//             }
//             else {
//                 // $("#cFiles").jsGrid("loadData");

//             }
//         });
//     },
//     'change #sel-fin_commission': function(event, template) {
//         // console.log($(event.target).val());
//         // var u = Meteor.users.findOne({_id:$(event.target).val()});
//         // $("#username").text(u.username);
//         // $("#cht-name").text(u.profile.name);
//         // Session.set('counter', Session.get('counter') + 1);

//         Router.go("/fin_commission/"+$(event.target).val());
//         // Template.fin_commission.loadData(this);

//     },
//   /*  'click .my-xedit': function() {
//         // Session.set('counter', Session.get('counter') + 1);
//         var now_id = this._id;

//         $(".my-xedit-text").each(function() {
//             var domId = $(this).attr("id");
//             $(this).editable({
//                 type: 'text',
//                 name: domId,
//                 pk: now_id,
//                 url: '/api/xeditaccountyear',
//                 // title: 'Enter username'
//             });
//         });
//     },*/
// });