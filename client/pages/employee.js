Template.employee.loadData = function (data) {
    /*console.log("loadData");*/
    // $("#chtname").text("");

    $("#chtname").text(data.profile.chtname || "");
    $("#engname").text(data.profile.engname || "");
    $("#lastname").text(data.profile.lastname || "");
    $("#firstname").text(data.profile.firstname || "");
    $("#tw_id").text(data.profile.tw_id || "");
    $("#passport_name").text(data.profile.passport_name || "");
    $("#birthday_date").text(data.profile.birthday_date || "");
    $("#onbroad_date").text(data.profile.onbroad_date || "");
    $("#leave_date").text(data.profile.leave_date || "");
    $("#jobyear").text((jobyear($("#onbroad_date").text())) || "");
    $("#depart_text").text(data.profile.depart_text || "");
    $("#jobtitle").text(data.profile.jobtitle || "");
    $("#jobline").text(data.profile.jobline || "");
    $("#cellphone").text(data.profile.cellphone || "");
    $("#email").text(data.emails[0].address || "");
    $("#email2").text(data.profile.email2 || "");
    $("#aan").text(data.profile.aan || "");
    $("#worknumber").text(data.profile.worknumber || "");
    $("#employee_ps").text(data.profile.employee_ps || "");
    // $("#department_id").text(funcObjFind2(objDepartment,data.profile.department_id));

    if (Businessgroup.findOne({ _id: data.profile.department_id })) {
        $("#department_id").text(Businessgroup.findOne({ _id: data.profile.department_id }).value || "");
    }
    else {
        $("#department_id").text("");
    }

    if (!!data.profile.aan) {
        $(".isann0").hide();
        $(".isann1").fadeIn();
    }
    else {
        $(".isann1").hide();
        $(".isann0").fadeIn();
    }

    // Agents.insert({
    //     name:data.profile.chtname
    // })
}

Template.employee.rendered = function () {

    Template.semicolon.loadjs();
    var ocImages = $("#oc-images");

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });

    $.fn.editable.defaults.mode = 'inline';
    $("#unedit-form-btn").hide();

    var data = this.data;
    Session.set("employee", data);
    $("#cFiles1").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                var data = Session.get("employee");

                filter.employee = "1"; // 1:尚未送出 2: 已送出
                filter.employee_id = data._id;

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
            onRefreshed: function () {
                var $gridData = $("#cFiles1 .jsgrid-grid-body tbody");
                $gridData.sortable({
                    update: function (e, ui) {
                        // arrays of items
                        var items = $.map($gridData.find("tr"), function (row) {
                            return $(row).data("JSGridItem");
                        });
                        // console.log("Reordered items", items);
                        Meteor.call("updateSortUploads", items, function (error, result) {
                            if (error) {
                                console.log("error from updateSortUploads: ", error);
                            }
                            else {
                                $("#cFiles1").jsGrid("loadData");
                            }
                        });
                    }
                });
            },
        },
        fields: [
            { name: "ctrl_field", type: "control", width: 40, editButton: false },
            // { type: "control", width:40, editButton:false},
            {
                name: "download", width: 100, align: "center", title: "檔案",
                itemTemplate: function (value, item) {
                    return '<a href="#" data-num="1" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            // { name: "receiver_name", title: "收件人", type: "text" },
            // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width: 70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width: 70, title: "上傳者", type: "text", editing: false },
        ],
    });
    $("#cFiles2").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                var data = Session.get("employee");
                filter.employee = "2"; // 1:尚未送出 2: 已送出
                filter.employee_id = data._id;

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
            onRefreshed: function () {
                var $gridData = $("#cFiles2 .jsgrid-grid-body tbody");
                $gridData.sortable({
                    update: function (e, ui) {
                        // arrays of items
                        var items = $.map($gridData.find("tr"), function (row) {
                            return $(row).data("JSGridItem");
                        });
                        // console.log("Reordered items", items);
                        Meteor.call("updateSortUploads", items, function (error, result) {
                            if (error) {
                                console.log("error from updateSortUploads: ", error);
                            }
                            else {
                                $("#cFiles2").jsGrid("loadData");
                            }
                        });
                    }
                });
            },
        },
        fields: [
            { name: "ctrl_field", type: "control", width: 40, editButton: false },
            // { type: "control", width:40, editButton:false},
            {
                name: "download", width: 100, align: "center", title: "檔案",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a href="#" data-num="2" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            // { name: "receiver_name", title: "收件人", type: "text" },
            // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width: 70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width: 70, title: "上傳者", type: "text", editing: false },
        ],
    });
    $("#cFiles3").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                var data = Session.get("employee");
                filter.employee = "3"; // 1:尚未送出 2: 已送出
                filter.employee_id = data._id;

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
            onRefreshed: function () {
                var $gridData = $("#cFiles3 .jsgrid-grid-body tbody");
                $gridData.sortable({
                    update: function (e, ui) {
                        // arrays of items
                        var items = $.map($gridData.find("tr"), function (row) {
                            return $(row).data("JSGridItem");
                        });
                        // console.log("Reordered items", items);
                        Meteor.call("updateSortUploads", items, function (error, result) {
                            if (error) {
                                console.log("error from updateSortUploads: ", error);
                            }
                            else {
                                $("#cFiles3").jsGrid("loadData");
                            }
                        });
                    }
                });
            },

        },
        fields: [
            { name: "ctrl_field", type: "control", width: 40, editButton: false },
            // { type: "control", width:40, editButton:false},
            {
                name: "download", width: 100, align: "center", title: "檔案",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a href="#" data-num="3" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            // { name: "receiver_name", title: "收件人", type: "text" },
            // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width: 70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width: 70, title: "上傳者", type: "text", editing: false },
        ],
    });
    $("#cFiles4").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                var data = Session.get("employee");
                filter.employee = "4"; // 1:尚未送出 2: 已送出
                filter.employee_id = data._id;

                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
            onRefreshed: function () {
                var $gridData = $("#cFiles4 .jsgrid-grid-body tbody");
                $gridData.sortable({
                    update: function (e, ui) {
                        // arrays of items
                        var items = $.map($gridData.find("tr"), function (row) {
                            return $(row).data("JSGridItem");
                        });
                        // console.log("Reordered items", items);
                        Meteor.call("updateSortUploads", items, function (error, result) {
                            if (error) {
                                console.log("error from updateSortUploads: ", error);
                            }
                            else {
                                $("#cFiles4").jsGrid("loadData");
                            }
                        });
                    }
                });
            },

        },
        fields: [
            { name: "ctrl_field", type: "control", width: 40, editButton: false },
            // { type: "control", width:40, editButton:false},
            {
                name: "download", width: 100, align: "center", title: "檔案",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a href="#" data-num="4" data-toggle="modal" data-target="#myModalFile" data-url="' + item.url + '" data-index="' + item.myItemIndex + '">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            // { name: "receiver_name", title: "收件人", type: "text" },
            // { name: "receiver_email", title: "E-Mail(自動)", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width: 70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width: 70, title: "上傳者", type: "text", editing: false },
        ],
    });
    // Template.employee.loadData(this.data);
    // console.log(this.data);

    $("#input1").fileinput({
        'language': "zh-TW",
    });
    $("#input2").fileinput({
        'language': "zh-TW",
    });
    $("#input3").fileinput({
        'language': "zh-TW",
    });
    $("#input4").fileinput({
        'language': "zh-TW",
    });

    $(".upload_show").hide()

    $("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", false);
    $("#cFiles2").jsGrid("fieldOption", "ctrl_field", "visible", false);
    $("#cFiles3").jsGrid("fieldOption", "ctrl_field", "visible", false);
    $("#cFiles4").jsGrid("fieldOption", "ctrl_field", "visible", false);

    this.autorun(function () {
        var data = Blaze.getData();

        Session.set("employee", data);
        // 非編輯狀態
        if ($("#unedit-form-btn").css("display") == "none") {
            Template.employee.loadData(data);
        }
        if (typeof $("#unedit-form-btn").html() == "undefined") {
            Template.employee.loadData(data);
        }

        $("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles2").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles3").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles4").jsGrid("fieldOption", "ctrl_field", "visible", false);

        $("#cFiles1").jsGrid("loadData");
        $("#cFiles2").jsGrid("loadData");
        $("#cFiles3").jsGrid("loadData");
        $("#cFiles4").jsGrid("loadData");
    });

    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        /*var now_data = $(event.target).attr("data-url");*/
        var index = button.data("index");
        var num = button.data("num");
        var data = $("#cFiles" + num).data("JSGrid").data[index];

        var url = button.data("url");
        $("#modal2-link").html('<a target="_blank" href="' + data.url + '">' + data.name + ' (開新視窗)</a>');

        $("#file_iframe").attr("src", url);
    });
    $('#myModalFile').on('hide.bs.modal', function (event) {
        $("#file_iframe").attr("src", "");
    });
};

Template.employee.helpers({
    // homepics: objHomePics
    employee: Meteor.users.find({
        $or: [
            { 'auth.is_auth': '1' },
            {
                'auth.is_auth':
                    { $exists: false }
            }
        ]
    }, { sort: { order_id: 1 } }),
    portfolios: Portfolios.find(),
    Product1: Product1.find(),
    "files1": function () {
        return S3.collection.find({ uploader: "1" });
    },
    "files2": function () {
        return S3.collection.find({ uploader: "2" });
    },
    "files3": function () {
        return S3.collection.find({ uploader: "3" });
    },
    // "files4": function(){
    //     return S3.collection.find({uploader:"4"});
    // },
});

Template.employee.events({
    'click #del-form-btn': function () {
        if (!confirm("確定要刪除此使用者?")) {
            return;
        }

        var now_id = $("#sel-employee").val();

        Meteor.call("removeUser", now_id, function (error, result) {
            $("#sel-employee").val(Meteor.userId());
            Router.go("/employee/" + Meteor.userId());
        });
    },
    'click #edit-form-btn': function () {
        // Session.set('counter', Session.get('counter') + 1);
        // $("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", true);

        var now_id = this._id;
        $(".upload_show").show();


        $(".my-xedit-text").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                // title: 'Enter username'
            });
        });
        $(".my-xedit-date").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'text',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                format: 'YYYY/MM/DD',
                viewformat: 'YYYY/MM/DD',
                template: 'YYYY / MMMM / D',
                combodate: {
                    minYear: 1900,
                    maxYear: 2020,
                    minuteStep: 1
                }
            });
        });
        $(".my-xedit-sel").each(function () {
            var domId = $(this).attr("id");
            $(this).editable({
                type: 'select',
                name: domId,
                pk: now_id,
                url: '/api/xedituser',
                // source: ,
                /*source: function(){
                    var objname = $(this).attr("data-obj");
                    if(objname == "objBodyCheckType1") return objBodyCheckType1;
                    else if(objname == "objDepartment") return objDepartment;
                },*/
                source: function () {
                    var objname = $(this).attr("data-obj");
                    if (objname == "objBodyCheckType1") return objBodyCheckType1;
                    else if (objname == "objAgents") return $.map(Agents.find({}).fetch(), function (item) { return { value: item._id, text: item.name }; });
                    else if (objname == "Businessgroup") return $.map(Businessgroup.find({}).fetch(), function (item) { return { value: item._id, text: item.value }; });
                },
                value: $(this).text()
            });
        });

        $("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", true);
        $("#cFiles2").jsGrid("fieldOption", "ctrl_field", "visible", true);
        $("#cFiles3").jsGrid("fieldOption", "ctrl_field", "visible", true);
        $("#cFiles4").jsGrid("fieldOption", "ctrl_field", "visible", true);


        $(".on-editing").show();
        $("#unedit-form-btn").show();
        $("#edit-form-btn").hide();


        $('.editable').on('hidden', function (e, reason) {
            // console.log("aaa " + reason);
            if (reason === 'save' || reason === 'nochange') {
                var $next = "";
                if ($(this).next('.editable').length) { // 同td的下一個
                    $next = $(this).next('.editable');
                }
                else if ($(this).parent().nextAll().children(".editable").length) { // 下面還有
                    $next = $(this).parent().nextAll().children(".editable").eq(0);
                }
                else if ($(this).closest('tr').next().find('.editable').length) { // 下一行的
                    $next = $(this).closest('tr').next().find('.editable').eq(0);
                }

                if ($next) {
                    setTimeout(function () {
                        $next.editable('show');
                    }, 300);
                }
            }
        });
    },
    'click #unedit-form-btn': function () {
        // Session.set('counter', Session.get('counter') + 1);
        $(".upload_show").hide()
        $("#cFiles1").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles2").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles3").jsGrid("fieldOption", "ctrl_field", "visible", false);
        $("#cFiles4").jsGrid("fieldOption", "ctrl_field", "visible", false);

        $('.my-xedit').editable('destroy');
        $("#unedit-form-btn").hide();
        $("#edit-form-btn").show();
        $(".on-editing").hide();
    },
    'change #sel-employee': function (event, template) {
        // console.log($(event.target).val());
        // var u = Meteor.users.findOne({_id:$(event.target).val()});
        // $("#username").text(u.username);
        // $("#cht-name").text(u.profile.name);
        // Session.set('counter', Session.get('counter') + 1);
        $(".upload_show").hide()
        $('.my-xedit').editable('destroy');
        $("#unedit-form-btn").hide();
        $("#edit-form-btn").show();
        $(".on-editing").hide();

        Router.go("/employee/" + $(event.target).val());
        // $(event.target).val()
        // Template.employee.loadData(this);

    },
    "click a.upload": function (event, template) {
        // var files = $("input.file_bag")[0].files;
        $('.progress').hide()
        var now_id = this._id;
        // var now_data = $(event.target).removeAttr("data-id");
        var now_data = $(event.target).attr("data-id");

        console.log("now_id");
        console.log(now_data);

        var files = $("#input" + now_data + ".file_bag")[0].files;
        files[0].now_data = now_data;
        console.log(now_data);
        S3.upload({
            uploader: now_data,
            files: files,
            path: "subfolder"
        }, function (e, r) {
            console.log(r);
            $("#input" + now_data).fileinput('clear');

            var formVar = {
                employee: now_data, // 1:尚未送出 2: 已送出
                employee_id: now_id,
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles" + now_data).jsGrid("insertItem", formVar);
        });
    },
    // "click a.upload": function(event, template){
    //     // var files = $("input.file_bag")[0].files;

    //     var now_id = this._id;
    //     // var now_data = $(event.target).removeAttr("data-id");
    //     var now_data = 1

    //     var files = $("#input"+now_data+".file_bag")[0].files;
    //     files[0].now_data = now_data;
    //     // console.log(now_data);
    //     S3.upload({
    //             uploader:now_data,
    //             files:files,
    //             path:"subfolder"
    //     },function(e,r){
    //         // console.log(r);
    //         $("#input"+now_data).fileinput('clear');

    //         var formVar = {
    //             employee: now_data, // 1:尚未送出 2: 已送出
    //             employee_id: now_id,
    //             name: r.file.original_name,
    //             size: r.file.size,
    //             file: r,
    //             image_id: r._id,
    //             url: r.secure_url,
    //             insertedById: Meteor.userId(),
    //             insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //             insertedAt: new Date()
    //         };
    //         $("#cFiles" + now_data).jsGrid("insertItem", formVar);
    //     });
    // },
});