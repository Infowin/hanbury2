Template.interestcondition.rendered = function(){
    Template.semicolon.loadjs();
    $("#e_interestcondition").jsGrid({
        // height: "90%",
        height: "600px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "interestcondition", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "interestcondition", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "interestcondition", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "interestcondition", "DELETE");
            },
        },
        fields: [
            { type: "control", width:70, editButton: false },
            // { name: "uid", title: "#", width: 60},
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
            { name: "class", title: "類別", type: "text"},
            { name: "interestcondition", title: "配息條件", type: "text"},
            { name: "item", title: "項目", type: "text"},
            { name: "policyroomno", title: "Policy No/Room No.", type: "text"},
            { name: "checkcondition", title: "檢查機制(總投資額)", type: "text"},
            { name: "interest_period", title: "配息週期", type: "select", items: objICPeriod, valueField: "id", textField: "value" },
            { name: "y1", title: "第1期 (%)", type: "text"},
            { name: "y2", title: "第2期 (%)", type: "text"},
            { name: "y3", title: "第3期 (%)", type: "text"},
            { name: "y4", title: "第4期 (%)", type: "text"},
            { name: "y5", title: "第5期 (%)", type: "text" },
            { name: "y6", title: "第6期 (%)", type: "text" },
            { name: "y7", title: "第7期 (%)", type: "text" },
            { name: "y8", title: "第8期 (%)", type: "text" },
            { name: "y9", title: "第9期 (%)", type: "text" },
            { name: "y10", title: "第10期 (%)", type: "text" },
            { name: "y11", title: "第11期 (%)", type: "text" },
            { name: "y12", title: "第12期 (%)", type: "text" },
            { name: "y13", title: "第13期 (%)", type: "text" },
            { name: "y14", title: "第14期 (%)", type: "text" },
            { name: "y15", title: "第15期 (%)", type: "text" },
            { name: "interestgive_date", title: "利息配發日期 (月初)", type: "select", items: objICPayDate, valueField: "id", textField: "value" },
            { name: "leavecondition", title: "離場機制(參考備註)", type: "select", items: objICLeave, valueField: "id", textField: "value"},
            // { name: "intereststart_date", title: "利息起算日", type: "select", items: objICStartDay, valueField: "id", textField: "value"},
            // { name: "cutoff_date", title: "配息cut off date", type: "select", items: objICCutOff, valueField: "id", textField: "value"},
            // { name: "collectmoney_date", title: "集資完成日", type: "text"},
            // { name: "inv_year", title: "投資年期", type: "text"},
            // { name: "pre_leave_time", title: "預計離場時間", type: "text"},
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onRefreshed: function() {
            var $gridData = $("#e_interestcondition .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    // arrays of items
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortInterestcondition", items, function(error, result){
                        if(error){
                            console.log("error from updateSortUploads: ", error);
                        }
                        else {
                            $("#e_interestcondition").jsGrid("loadData");
                        }
                    });
                }
            });
        },
        // onDataLoading: function(args) {
        //     // $('.jsgrid select').material_select();
        // },
        // onItemInserted: function(args) {
        //     // Materialize.toast('資料已新增!', 3000, 'rounded')
        // },
        // onItemUpdated: function(args) {
        //     // Materialize.toast('資料已更新', 3000, 'rounded')
        //     // console.log(args);
        //     $("#e_interestcondition").jsGrid("loadData");
        // },
        // onItemDeleted: function(args) {
        //     // Materialize.toast('資料已刪除', 3000, 'rounded')
        //     $("#e_interestcondition").jsGrid("loadData");
        // },
    });
};

Template.interestcondition.helpers({

});

Template.interestcondition.events({
    'click .btn-new': function() {
        var formVar = {};
        formVar.leavecondition = 2;
        formVar.intereststart_date = 1;
        formVar.interest_period = 1;
        formVar.cutoff_date = 1;

        $("#e_interestcondition").jsGrid("insertItem", formVar).done(function(ret) {
            console.log("insertion completed");
            console.log(ret);

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                // $('#myModal').modal('hide');
                // $(".form-modal2").trigger('reset');
                // $("#e_interestcondition").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                // $(".modal2-error").text(ret);
            }
        });
    },
    //  'click .label-provider': function () {
    // $('#modal2').closeModal();
    // Meteor.setTimeout(function(){ Router.go('e_provider'); }, 10);
    //  },
    // 'click .my-btn-cancel': function() {
    //     $('#modal2').closeModal();
    // }
});

