Template.homepics.rendered = function(){
    Template.semicolon.loadjs();

    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                // var item = Session.get("nowrow");
                filter.homepic = "1";

                // var item = Session.get("nowrow_product");
                // filter.product1_id = item.product1_id;
                // filter.product2 = item.product2;
                // filter.product0 = 1;

                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            { type: "control", width:50,  },
            { name: "download", width:50, align:"left", title: "預覽",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            { name: "name", title: "檔案名稱", type: "text", editing:false },
            { name: "title1", title: "圖片大標", type: "text" },
            { name: "title2", title: "圖片小標", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width:70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],/*
        onDataLoading: function(args) {
        },
        onItemInserted: function(args) {
        },
        onItemUpdated: function(args) {
            console.log(args);
            $("#aProduct").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#aProduct").jsGrid("loadData");
        },*/
    });

    $("#input-3").fileinput({
        'language': "zh-TW",
        'dropZoneEnabled': true
    });
};

Template.homepics.helpers({
  // homepics: objHomePics
});

Template.homepics.events({
    'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
    },
    // "change .myFileInput3": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     homepic: "1",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     $("#cFiles").jsGrid("loadData");
    //                     // console.log(pic_id);
    //                 });
    //
    //             }
    //         });
    //     });
    // },
    "click a.upload": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                homepic: "1",
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
});
