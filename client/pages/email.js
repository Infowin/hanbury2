Template.email.rendered = function () {

    Template.semicolon.loadjs();

    var p_id = Router.current().params.p_id;
    var p_data = [];
    var sel2_data = [];

    if (p_id) {
        Meteor.call('getArrPortfolios', p_id, function (err, res) {

            p_data = res;
            var obj = p_data[0];
            var agentObj = Agents.findOne({
                _id: obj.agent_id
            });
            sel2_data.push({
                id: p_data[0].client_id,
                name: p_data[0].name_cht + "(" + p_data[0].name_eng + ") [" + p_data[0].email + "]",
                name2: p_data[0].name_cht + "(" + p_data[0].name_eng + ")",
                value: p_data[0].client_id
            })
            // console.log(p_data, agentObj);

            Meteor.call('getEmailByUserId', agentObj.user_id, function (err, agent) {
                // console.log(res);
                // $("#modal2-mail3").val(obj.email);
                $("#modal2-mail4").val(obj.uid);
                $("#bcc_mail").val(agent.email);
                // 繳費提醒
                $("#template-tc").val('J2hgMeRCndeH9SQTH').trigger('change');
                $("#cFiles").jsGrid("loadData");
                $(".js-data-example-ajax2").select2({
                    data: sel2_data,
                    templateResult: formatState, // omitted for brevity, see the source of this page
                    templateSelection: formatState2 // omitted for brevity, see the source of this page
                })
                $(".js-data-example-ajax2").prop('disabled', true);
                $(".js-data-example-ajax2").val(obj.client_id).trigger('change');

            });
        })
    }

    $(".div-use_auto_mail").hide();

    $("#eproduct2").attr("disabled", true);
    $("#eproduct3").attr("disabled", true);
    $("#eproduct4").attr("disabled", true);
    $("#issendtime").attr("checked", 1);
    $('#issendtime').change()

    // $('#email-send-context').summernote({
    //     lang: 'zh-TW', dialogsInBody: true
    // });

    CKEDITOR.config.removePlugins = 'forms';
    CKEDITOR.config.removeButtons = 'Save,Print,Preview,Find,About,Maximize,ShowBlocks';

    CKEDITOR.replace('email-send-context');

    $('.dropdown-toggle').dropdown();

    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",

        // sorting: true,
        // paging: true,
        // filtering: true,
        // editing: true,
        // pageSize: 10,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                // var item = Session.get("nowrow");
                // var p4_id = Session.get("nowrow_oproduct4")._id;
                // filter.oproduct4_id = p4_id;
                if (p_id && p_data.length) {
                    filter.client = "1"; // 1:尚未送出 2: 已送出
                    filter.client_id = p_data[0].client_id;
                } else {
                    filter.email = "1"; // 1:尚未送出 2: 已送出
                    filter.insertedById = Meteor.userId();
                }
                // filter.client_id = data._id;
                // var item = Session.get("nowrow_product");
                // filter.product1_id = item.product1_id;
                // filter.product2 = item.product2;
                // filter.product0 = 1;
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [{
                type: "control",
                width: 40,
                editButton: false
            },
            {
                name: "download",
                width: 100,
                align: "left",
                title: "檔案",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a href="' + item.url + '" target="_blank">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            {
                name: "receiver_name",
                title: "收件人",
                type: "text"
            },
            {
                name: "receiver_email",
                title: "E-Mail(自動)",
                type: "text"
            },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            // { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],
        onDataLoaded: function (args) {
            // console.log("onDataLoading");
            // console.log(args.grid);
            var str = "";
            if (!!args.grid.data) {
                var arr = args.grid.data;
                for (var i in arr) {
                    var data = arr[i];
                    if (!!data.receiver_name && !!data.receiver_email) {
                        // console.log(data.receiver_name + " " + data.receiver_email);
                        str = str + data.receiver_name + " " + data.receiver_email + "\n";
                    }
                }
                if (str.length) { //上傳的檔案偵測到對應的客戶姓名和電子郵件，
                    // if(confirm("是否要用偵測到的位址寄出?\n"+str)){
                    $(".user-input-email").hide();
                    $(".div-use_auto_mail").show();
                    $("#use_auto_mail").prop("checked", true);
                    $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", true);
                    $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", true);
                    // }
                } else {
                    $(".user-input-email").show();
                    $(".div-use_auto_mail").hide();
                    $("#use_auto_mail").prop("checked", false);
                    $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", false);
                    $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", false);
                }
            }
        },
        onItemInserted: function (args) {},
        /*
                onItemUpdated: function(args) {
                    console.log(args);
                    $("#aProduct").jsGrid("loadData");
                },
                onItemDeleted: function(args) {
                    // Materialize.toast('資料已刪除', 3000, 'rounded')
                    // $("#cFiles").jsGrid("loadData");
                },*/
    });
    $("#cFiles2").jsGrid({
        // height: "90%",
        width: "100%",

        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,
        pageSize: 10,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                // var item = Session.get("nowrow");
                var userId = Session.get('userId');
                // var p4_id = Session.get("nowrow_oproduct4")._id;
                // filter.oproduct4_id = p4_id;
                // if (p_id && p_data.length) {
                //     filter.client = "1"; // 1:尚未送出 2: 已送出
                //     filter.client_id = p_data[0].client_id;
                // }else {
                //     filter.email = "1"; // 1:尚未送出 2: 已送出
                //     filter.insertedById = Meteor.userId();
                // }
                filter.client = "1"; // 1:尚未送出 2: 已送出
                filter.client_id = userId;
                // filter.client_id = data._id;
                // var item = Session.get("nowrow_product");
                // filter.product1_id = item.product1_id;
                // filter.product2 = item.product2;
                // filter.product0 = 1;
                filter.findData = $("input#search_text2").val() || ""
                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:40, editButton:false},
            // { type: "control", width:40,
            //     itemTemplate: function (value, item) {
            //         // console.log(item);
            //         // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
            //         return '<button type="button" class="btn btn-primary">帶入</button>';
            //     },
            // },
            {
                name: "download",
                width: 100,
                align: "left",
                title: "檔案",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a href="' + item.url + '" target="_blank">' + item.name + '</a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            // { name: "name", title: "檔案名稱", type: "text" },
            {
                name: "receiver_name",
                title: "收件人",
                type: "text"
            },
            {
                name: "receiver_email",
                title: "E-Mail(自動)",
                type: "text"
            },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            // { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],
        onDataLoaded: function (args) {
            // console.log("onDataLoading");
            // console.log(args.grid);
            var str = "";
            // if(!!args.grid.data){
            //     var arr = args.grid.data;
            //     for(var i in arr){
            //         var data = arr[i];
            //         if(!!data.receiver_name && !!data.receiver_email){
            //             // console.log(data.receiver_name + " " + data.receiver_email);
            //             str = str + data.receiver_name + " " + data.receiver_email + "\n";
            //         }
            //     }
            //     if(str.length){//上傳的檔案偵測到對應的客戶姓名和電子郵件，
            //         // if(confirm("是否要用偵測到的位址寄出?\n"+str)){
            //         $(".user-input-email").hide();
            //         $(".div-use_auto_mail").show();
            //         $("#use_auto_mail").prop("checked", true);
            //         $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", true);
            //         $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", true);
            //         // }
            //     }
            //     else{
            //         $(".user-input-email").show();
            //         $(".div-use_auto_mail").hide();
            //         $("#use_auto_mail").prop("checked", false);
            //         $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", false);
            //         $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", false);
            //     }
            // }
        },
        onItemInserted: function (args) {},
        /*
                onItemUpdated: function(args) {
                    console.log(args);
                    $("#aProduct").jsGrid("loadData");
                },
                onItemDeleted: function(args) {
                    // Materialize.toast('資料已刪除', 3000, 'rounded')
                    // $("#cFiles").jsGrid("loadData");
                },*/
    });

    $("#input-3").fileinput({
        'language': "zh-TW",
        'dropZoneEnabled': true
    });

    $("#aEmail1").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        pageSize: 6,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                filter.sent_status_id = "2";

                if (!filter.sortField) {
                    filter.sortField = "sent_time";
                    filter.sortOrder = "asc";
                }

                return jsgridAjax(filter, "email", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "email", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "email", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "email", "DELETE");
            },
        },
        fields: [{
                type: "control",
                // editButton: false,
                width: 40
            },
            {
                name: "uid",
                title: "#",
                width: 70
            },
            // { name: "preview", title: "預覽", align: "center",
            //     itemTemplate: function(value, item) {
            //         // console.log(item);
            //         return '<input class="review-btn" type="button" data-id="'+item._id+'" value="檢視">';
            //     },
            // },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "sented_status_id", title: "寄送結果", editing:false, type: "number", type: "select", items: objMailsendresult, valueField: "id", textField: "value", width: 100 },
            //
            {
                name: "subject",
                title: "主旨",
                type: "text",
                width: 200,
                itemTemplate: function (value, item) {
                    // console.log(item);
                    if (!!item.subject)
                        return '<a href="#" class="mail-view" data-id="' + item._id + '">' + item.subject + '</a>';
                    else
                        return "";
                },
            },
            // { name: "context", title: "內文", type: "text", width: 200 },
            {
                name: "receiver",
                title: "收件人",
                type: "text"
            },
            {
                name: "cc",
                title: "副本",
                type: "text"
            },
            {
                name: "bcc",
                title: "密件副本",
                type: "text"
            },
            // { name: "sender", title: "寄件人", type: "text" },
            {
                name: "sent_status_id",
                title: "狀態",
                width: 70,
                type: "select",
                items: objMailsended,
                valueField: "id",
                textField: "value",
                width: 100
            },
            {
                name: "sent_time",
                title: "排程時間",
                type: "text",
                itemTemplate: function (value, item) {
                    if (!!item.sent_time)
                        return (new Date(item.sent_time)).yyyymmddhm();
                    else
                        return "";
                },
            },
            {
                name: "attachments",
                title: "附件",
                type: "text",
                itemTemplate: function (value, item) {
                    if (!!item.filename)
                        return '<a target="_blank" href="' + item.filepath + '">' + item.filename + '</a>';
                    else if (!!item.attachments && typeof item.attachments == "object") {
                        var str = "";
                        for (var i = 0; i < item.attachments.length; i++) {
                            var entry = item.attachments[i];
                            if (!!entry.fileName && typeof entry.fileName != "undefined")
                                str = str + '<a target="_blank" href="' + entry.filePath + '">' + entry.fileName + '</a><BR>';
                            else if (!!entry.filename && typeof entry.filename != "undefined" && !!entry.path) {
                                str = str + '<a target="_blank" href="' + entry.path + '">' + entry.filename + '</a><BR>';
                            } else if (!!entry.filename && typeof entry.filename != "undefined")
                                str = str + '<a target="_blank" href="' + entry.href + '">' + entry.filename + '</a><BR>';
                        }
                        return str;
                    } else
                        return "";
                },
            },
            // { name: "sender_name", title: "操作者", type: "text" },
            // { name: "ps", title: "備註", type: "text" },
            // { type: "control" }
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        rowClick: function (args) {
            console.log(args);
            const item =  args;
        }
        /* onDataLoading: function(args) {
             // $('.jsgrid select').material_select();
         },
         onItemInserted: function(args) {
             // Materialize.toast('資料已新增!', 3000, 'rounded')
         },*/
        /*  onItemUpdated: function(args) {
              // Materialize.toast('資料已更新', 3000, 'rounded')
              // console.log(args);
              $("#aEmail1").jsGrid("loadData");
          },*/
        /*   onItemDeleted: function(args) {
               // Materialize.toast('資料已刪除', 3000, 'rounded')
               $("#aEmail1").jsGrid("loadData");
           },*/
    });
    $("#aEmail2").jsGrid({
        // height: "400px",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,
        pageSize: 6,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function (filter) {
                filter.sent_status_id = "1";

                filter.searchtext = $("#email-search-context").val();

                if (!filter.sortField) {
                    filter.sortField = "sent_time";
                    filter.sortOrder = "desc";
                }
                return jsgridAjax(filter, "email", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "email", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "email", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "email", "DELETE");
            },
        },
        fields: [{
                name: "uid",
                title: "#",
                width: 70
            },
            // { name: "preview", title: "預覽", align: "center",
            //     itemTemplate: function(value, item) {
            //         // console.log(item);
            //         return '<input class="review-btn" type="button" data-id="'+item._id+'" value="檢視">';
            //     },
            // },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "sented_status_id", title: "寄送結果", editing:false, type: "number", type: "select", items: objMailsendresult, valueField: "id", textField: "value", width: 100 },
            {
                name: "subject",
                title: "主旨",
                type: "text",
                width: 100,
                itemTemplate: function (value, item) {
                    // console.log(item);
                    if (!!item.subject)
                        return '<a href="#" class="mail-view" data-id="' + item._id + '">' + item.subject + '</a>';
                    else
                        return "";
                },
            },
            // { name: "message", title: "內文", type: "text", width: 200 },
            {
                name: "receiver",
                title: "收件人",
                type: "text"
            },
            {
                name: "cc",
                title: "副本",
                type: "text"
            },
            {
                name: "bcc",
                title: "密件副本",
                type: "text"
            },
            // { name: "sender", title: "寄件人", type: "text" },
            {
                name: "sent_status_id",
                title: "狀態",
                width: 50,
                type: "select",
                items: objMailsended,
                valueField: "id",
                textField: "value",
                width: 100
            },
            {
                name: "sent_time",
                title: "寄送時間",
                type: "text",
                itemTemplate: function (value, item) {
                    if (!!item.sent_time)
                        return (new Date(item.sent_time)).yyyymmddhm();
                    else
                        return "";
                },
            },
            {
                name: "attachments",
                title: "附件",
                type: "text",
                width: 150,
                itemTemplate: function (value, item) {
                    // console.log(item);
                    if (!!item.filename)
                        return '<a target="_blank" href="' + item.filepath + '">' + item.filename + '</a>';
                    else if (!!item.attachments && typeof item.attachments == "object") {
                        var str = "";
                        for (var i in item.attachments) {
                            var entry = item.attachments[i];
                            if (!!entry.fileName && typeof entry.fileName != "undefined")
                                str = str + '<a target="_blank" href="' + entry.filePath + '">' + entry.fileName + '</a><BR>';
                            else if (!!entry.filename && typeof entry.filename != "undefined" && !!entry.path) {
                                str = str + '<a target="_blank" href="' + entry.path + '">' + entry.filename + '</a><BR>';
                            } else if (!!entry.filename && typeof entry.filename != "undefined")
                                str = str + '<a target="_blank" href="' + entry.href + '">' + entry.filename + '</a><BR>';
                        }
                        return str;
                    } else
                        return "";
                },
            },
            // { name: "sender_name", title: "操作者", type: "text" },
            // { name: "ps", title: "備註", type: "text" },
            // { type: "control" }
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
    });
    $("#portfolio").jsGrid({
        // height: "90%",
        // height: "400px",
        width: "100%",
        noDataContent: "尚無資料",
        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                var formVar = $(".form-modal1").serializeObject();
                $.extend(filter, formVar);

                // filter.field_type_id = "1";
                filter.findData = $("input#search_text").val() || "";
                return jsgridAjax(filter, "portfolio", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "portfolio", "POST");
            },
            updateItem: function (item) {
                // return jsgridAjax(item, "portfolio", "PUT");

                // Meteor.call("updatePortfolio", item._id, item, function(error, result){
                //     if(error){
                //         console.log("error from updatePortfolio: ", error);
                //     }
                //     else{
                //         // console.log(result);
                //         return result;
                //     }
                // });
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "portfolio", "DELETE");
            },
        },
        fields: [
            /*
            client_id: id,
            product1: data.product1,
            product2: data.product2,
            account_num: data.account_num,
            fpi_num: data.fpi_num,
            start_date: data.start_date,
            invest_money: data.invest_money,
            - 項目
            - 項目起啟日(如12/31 或 6/30)
            - 投資金額
            - 幣別
            - 匯款日期(3/1)
            - 第一年利率 x%? (用打的 只能4~10，到小數第一位)
            - 做到7年 (至少前5年)
            - 第一次預計配息 (填數字)
             */
            /*  { name: "review_btn", align: "center", title: "編輯", sorting: false,
                  itemTemplate: function(value, item) {
                      // console.log(item);
                      return '<input class="my-open-modal-1" type="button" data-id="'+item._id+'" value="編輯">';
                  },
              },*/
            // { type: "control", width:60, editButton: false, width: 60  },
            {
                name: "uid",
                title: "系統序號",
                width: 60
            },
            {
                name: "name_cht",
                title: "姓名",
                type: "text",
                width: 110,
                itemTemplate: function (value, item) {
                    // console.log(item);
                    return item.name_cht + " <BR>" + item.email;
                },
            },
            // { name: "email", title: "電子信箱", type: "text"},
            // { name: "review_id", title: "審核狀況", type: "select", items: [{id:"",value:""}].concat(objReviewRes), valueField: "id", textField: "value" },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",name:""}].concat(Products.find().fetch()), valueField: "_id", textField: "name"   },
            // { name: "product1_id", title: "投資大類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"   },
            // { name: "product2", title: "投資案名", type: "select", items: [{_id:"",code:""}].concat(Products.find().fetch()), valueField: "code", textField: "code"   },
            // { name: "product2", title: "投資案名", type: "text"},
            {
                name: "product1_text",
                title: "產品類別1/2/3",
                type: "text",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    return item.product1_text + "/" + item.product2_text + "/" + item.product3_text + " (" + item.product3_engtext + ")";
                },
            },
            // { name: "product2_text", title: "產品類別2", type: "text"  },
            // { name: "product3_text", title: "產品類別3", type: "text"  },
            {
                name: "product4_text",
                title: "產品類別4",
                type: "text",
                width: 130,
                itemTemplate: function (value, item) {
                    // console.log(item);
                    return item.product4_text + " (" + item.product4_engtext + ")";
                },
            },
            // { name: "account_num", title: "廣達 帳戶/表單號", type: "text"  },
            {
                name: "agent_text",
                title: "Agent",
                type: "text",
                width: 60
            },
            // { name: "start_date", title: "項目起啟日", type: "text"  },
            // { name: "prod_money", title: "投資金額", type: "text"  },
            // { name: "prod_money_curtype", title: "幣別", type: "select", items: [{id:"",value:""}].concat(objSalaryCash), valueField: "id", textField: "value"   },

            // { name: "prod_apply_book", title: "申請書", type: "text"  },
            // { name: "prod_healthy_check", title: "體檢狀況", type: "text"  },
            // { name: "prod_tele_invest", title: "電話徵信", type: "text"  },
            // { name: "prod_remit_indicate", title: "匯款指示", type: "text"  },
            // { name: "prod_remit_date", title: "匯款日", type: "text"  },
            // { name: "prod_fund_receipt", title: "Fund Receipt", type: "text"  },
            // { name: "prod_certificate_soft_copy", title: "Certificate Soft Copy", type: "text"  },
            // { name: "prod_customer_recv_sign", title: "客戶簽收合約正本", type: "text"  },
            // { name: "prod_new_case_ps", title: "新件進度附註", type: "text"  },


            // { name: "remit_date", title: "匯款日期", type: "text"  },
            // { name: "invest_money_pay_method", title: "供款方式", type: "select", items: [{id:"",value:""}].concat(objPayMethod), valueField: "id", textField: "value"   },
            // { name: "invest_money_period", title: "供款週期", type: "select", items: [{id:"",value:""}].concat(objPaymentFreq), valueField: "id", textField: "value"   },
            // { name: "first_rate", title: "第一年利率", type: "text"  },
            // { name: "first_prepay", title: "第一次預計配息", type: "text"  },

            // { name: "submit_date", title: "提交日", type: "text"  },
            // { name: "start_date", title: "廣達生效日", type: "text"  },
            // { name: "insertedName", title: "建檔人", type: "text" },
            // { name: "insertedAt", title: "建檔日期",
            //     itemTemplate: function(value, item) {
            //         if(!!item.insertedAt)
            //             return (new Date(item.insertedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "updatedName", title: "最後更新者", type: "text" },
            // { name: "updatedAt", title: "最後更新時間",
            //     itemTemplate: function(value, item) {
            //         if(!!item.updatedAt)
            //             return (new Date(item.updatedAt)).yyyymmddhm();
            //         else
            //             return "";
            //     },
            // },
            // { name: "ps", title: "備註", type: "text" },
        ],
        rowClick: function (args) {
            // console.log(args);
        },
        onDataLoading: function (args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function (args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function (args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#portfolio").jsGrid("loadData");
        },
        onItemDeleted: function (args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#portfolio").jsGrid("loadData");
        },
    });

    function formatState(state) {
        if (state.id === '') { // adjust for custom placeholder values
            return '請輸入至少一個字';
        }
        var $state = $(
            '<span>' + state.name + '</span>'
        );
        return $state;
    };

    function formatState2(state) {
        return state.name2;
    };

    function formatState3(state) {
        return state.name;
    };
    $(".js-data-example-ajax").select2({
        ajax: {
            url: "/api/contactlist",
            dataType: 'json',
            delay: 250,
            data: function (params) { // 送要查的資料
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                var results = [];

                $.each(data.items, function (i, v) {
                    var o = {};
                    o.id = v._id;
                    o.name = v.name;
                    o.value = v._id;
                    results.push(o);
                })

                return {
                    results: results
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        formatInputTooShort: function () {
            return "請輸入至少一個字";
        },
        placeholder: '請輸入中英文姓名或e-mail',
        multiple: true,
        templateResult: formatState, // omitted for brevity, see the source of this page
        templateSelection: formatState3 // omitted for brevity, see the source of this page
    });
    $(".js-data-example-ajax2").select2({
        ajax: {
            url: "/api/emaillist",
            dataType: 'json',
            delay: 250,
            allowClear: true,
            data: function (params) { // 送要查的資料
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                var results = [];
                // console.log(data.items);

                $.each(data.items, function (i, v) {
                    var o = {};
                    o.id = v._id;
                    o.name = v.name_cht + "(" + v.name_eng + ") [" + v.email + "]";
                    o.name2 = v.name_cht + "(" + v.name_eng + ")";
                    o.value = v._id;
                    results.push(o);
                    Session.set('userId', v._id);
                })

                return {
                    results: results
                };
            },
            cache: true,
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        formatInputTooShort: function () {
            return "請輸入至少一個字";
        },
        placeholder: '請輸入中英文姓名或e-mail',
        multiple: true,
        templateResult: formatState, // omitted for brevity, see the source of this page
        templateSelection: formatState2 // omitted for brevity, see the source of this page
    });

    /*   $('.datetimepicker').datetimepicker({
           minDate: moment(),
           showClose: true,
           format: "YYYY-MM-DD HH:mm"
           // dateFormat: "dd-mm-yy",
           // timeFormat: "HH:mm"
           // collapse: false
           // language: 'zh-TW'
       });*/
    $('#myModal').on('shown.bs.modal', function (e) {
        // alert('modal show');
        $("#portfolio").jsGrid("loadData");
    });
    $('#myModal3').on('shown.bs.modal', function (e) {
        // alert('modal show');
        $("#cFiles2").jsGrid("loadData");
    });
};

Template.email.helpers({
    // homepics: objHomePics
    Product1: Product1.find(),
    Agents: Agents.find(),
    Provider: Provider.find(),
    get_mailTitle: function () {
        return MailTitle.find({}, {
            sort: {
                inserted: -1
            }
        });
    },
    get_mailContext: function () {
        return MailContext.find({}, {
            sort: {
                inserted: -1
            }
        });
    },
});
Template.email.loadcFiles2 = function () {
    $("#cFiles2").jsGrid("loadData");
    $("#cFiles2").jsGrid("openPage", 1);
}

Template.email.events({
    'change #search_text, keypress #search_text, click #search_text': function (event) { //#
        Template.clientslist.loadPortfolio();
    },
    'change #search_text2, keypress #search_text2, click #search_text2': function (event) { //#
        Template.email.loadcFiles2();
    },
    'change #template-periodmail': function (event) { //#
        Meteor.call("getPeriodMailList", $("#template-periodmail").val(), function (error, result) {
            if (error) {
                console.log("error from updateSortUploads: ", error);
            } else {
                // alert("已成功送出案件追蹤清單\n" + result);

                $("#modal2-mail3").val(result);
            }
        });
    },
    "change #use_auto_mail": function (event, template) {
        if ($("#use_auto_mail").prop("checked")) {
            $(".user-input-email").hide();

            $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", true);
            $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", true);
            // $(".div-use_auto_mail").show();
        } else {
            $(".user-input-email").show();

            $("#cFiles").jsGrid("fieldOption", "receiver_name", "visible", false);
            $("#cFiles").jsGrid("fieldOption", "receiver_email", "visible", false);
            // $(".div-use_auto_mail").hide();
        }
    },
    "change #issendtime": function (event, template) {
        // console.log("change");
        if ($(event.target).prop('checked')) {
            //Do stuff
            $("#email-send-datetime").fadeIn();
            $('.datetimepicker').datetimepicker({
                minDate: moment().add({
                    hours: 1
                }),
                showClose: true,
                format: "YYYY-MM-DD HH:mm"
                // dateFormat: "dd-mm-yy",
                // timeFormat: "HH:mm"
                // collapse: false
                // language: 'zh-TW'
            });
        } else {
            $('.datetimepicker').data('DateTimePicker').destroy();
            $("#email-send-datetime").hide();
        }
    },
    // "change .myFileInput": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         // console.log(file);
    //         // var ori_filename = file.name;
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 var formVar = {
    //                     email: "1",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 };
    //                 var pic_id = Uploads.insert(formVar);
    //
    //                 Meteor.setTimeout(function(){
    //                     $("#cFiles").jsGrid("loadData");
    //
    //                     var input = $(".myFileInput");
    //                     input = input.val('').clone(true);
    //                 }, 500);
    //             }
    //         });
    //     });
    // },
    "click a.upload": function () {
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
            files: files,
            path: "subfolder"
        }, function (e, r) {
            console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                email: "1",
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    "click .btn-get-cFiles": function () {
        var files = [];
        var now_id = this._id;
        var userId = Session.get('userId')

        $.ajax({
            url: "/api/uploads",
            type: 'GET',
            data: {
                findData: $('#search_text2').val(),
                client_id: userId,
                client: '1'
            },
            success: function (res) {
                var files = res.data;
                if (files.length){
                    files.forEach(item => {
                        var formVar = {
                            email: "1",
                            // name: fileObj.name(),
                            // size: fileObj.size(),
                            // file: fileObj,
                            // image_id: fileObj._id,
                            // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                            name: item.name,
                            size: item.size,
                            file: item.file,
                            image_id: item.image_id,
                            url: item.url,

                            insertedById: Meteor.userId(),
                            insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                            insertedAt: new Date()
                        };
                        $("#cFiles").jsGrid("insertItem", formVar);
                    });
                }
                $("#myModal3").modal('hide');
                $("#search_text2").val('');
            }
        });
        // alert('已匯入用戶文件');
    },
    // 'click .btn-saveTitle': function() {
    'click .btn-addTitle': function () {
        if (!confirm("確定新增本「主旨」、「內文」做樣版?")) {
            return;
        }
        MailTitle.insert({
            data: $("#email-send-title").val(),
            view_title: $("#email-send-title").val(),
            // context: $("#email-send-context").val(),
            // context: $('#email-send-context').summernote('code'),
            periodmail: $("#template-periodmail").val(),
            context: CKEDITOR.instances["email-send-context"].getData(),
            insertedAt: new Date()
        });
        // alert("已新增");
    },
    'click .btn-saveTitle': function () {
        if (!confirm("確定儲存本「主旨」、「內文」做樣版?")) {
            return;
        }
        var nowid = $("#template-tc").val();
        MailTitle.update(nowid, {
            $set: {
                data: $("#email-send-title").val(),
                view_title: $("#email-send-title").val(),
                periodmail: $("#template-periodmail").val(),
                context: CKEDITOR.instances["email-send-context"].getData(),
                updateedAt: new Date()
            }
        });
        // alert("已新增");
    },
    'click .btn-saveContext': function () {
        if (!confirm("確定新增本內文樣版?")) {
            return;
        }
        MailContext.insert({
            // data: $("#email-send-context").val(),
            data: CKEDITOR.instances["email-send-context"].getData(),
            view_title: $("#email-send-context").val().substr(0, 10),
            periodmail: $("#template-periodmail").val(),
            insertedAt: new Date()
        });
        alert("已新增");
    },
    'click .btn-delTitle': function () {
        if (!confirm("確定刪除本內文主旨?")) {
            return;
        }
        // template-tc
        $("#template-tc").val()
        // MailTitle.remove({_id: $(".title").val() });
        MailTitle.remove({
            _id: $("#template-tc").val()
        });
        // alert("已刪除");
    },
    'click .btn-delContext': function () {
        if (!confirm("確定刪除本內文樣版?")) {
            return;
        }
        MailContext.remove({
            _id: $(".context").val()
        });
        // alert("已刪除");
    },
    'click .test1': function () {
        // Session.set('counter', Session.get('counter') + 1);
        console.log("aaa");
    },
    'click .btn-searchtext': function () {
        // Session.set('counter', Session.get('counter') + 1);
        // console.log("aaa");
        $("#aEmail2").jsGrid("loadData");
    },
    'click .mail-view': function (event) {
        // Session.set('counter', Session.get('counter') + 1);
        // console.log(e);
        // console.log("e");
        // data-toggle="modal" data-target="#myModal2"
        var id = $(event.currentTarget).attr("data-id");

        var arr1 = $("#aEmail1").data("JSGrid").data;

        for (var i = 0; i < arr1.length; i++) {
            if (arr1[i]._id == id) {
                console.log(arr1[i]);
                var item = arr1[i];
                $("#mail-uid").text(item.uid);
                $("#mail-receiver").text(item.receiver);
                $("#mail-subject").text(item.subject);
                $("#mail-context").html(item.context);

                if (!!item.attachments && typeof item.attachments == "object") {
                    var str = "";
                    for (var i = 0; i < item.attachments.length; i++) {
                        var entry = item.attachments[i];
                        if (!!entry.fileName && typeof entry.fileName != "undefined")
                            str = str + '<a target="_blank" href="' + entry.filePath + '">' + entry.fileName + '</a><BR>';
                        else if (!!entry.filename && typeof entry.filename != "undefined" && !!entry.path) {
                            str = str + '<a target="_blank" href="' + entry.path + '">' + entry.filename + '</a><BR>';
                        } else if (!!entry.filename && typeof entry.filename != "undefined")
                            str = str + '<a target="_blank" href="' + entry.href + '">' + entry.filename + '</a><BR>';
                    }
                    $("#mail-attachments").html(str);
                }
                $('#myModal2').modal('show');
                break;
            }
        }

        var arr2 = $("#aEmail2").data("JSGrid").data;

        for (var i = 0; i < arr2.length; i++) {
            if (arr2[i]._id == id) {
                console.log(arr2[i]);
                var item = arr2[i];
                $("#mail-uid").text(item.uid);
                $("#mail-receiver").text(item.receiver);
                $("#mail-subject").text(item.subject);
                $("#mail-context").html(item.context);

                if (!!item.attachments && typeof item.attachments == "object") {
                    var str = "";
                    for (var i = 0; i < item.attachments.length; i++) {
                        var entry = item.attachments[i];
                        if (!!entry.fileName && typeof entry.fileName != "undefined")
                            str = str + '<a target="_blank" href="' + entry.filePath + '">' + entry.fileName + '</a><BR>';
                        else if (!!entry.filename && typeof entry.filename != "undefined" && !!entry.path) {
                            str = str + '<a target="_blank" href="' + entry.path + '">' + entry.filename + '</a><BR>';
                        } else if (!!entry.filename && typeof entry.filename != "undefined")
                            str = str + '<a target="_blank" href="' + entry.href + '">' + entry.filename + '</a><BR>';
                    }
                    $("#mail-attachments").html(str);
                }
                $('#myModal2').modal('show');
                break;
            }
        }
    },
    'click #clear-mail-select': function () {
        $(".js-data-example-ajax2").val('').trigger('change');
    },
    'click #clear-mail3': function () {
        $("#modal2-mail3").val("");
        $("#email3-status").html("");
    },
    'click #clear-mail4': function () {
        $("#modal2-mail4").val("");
        $("#email4-status").html("");
    },
    'click .btn-clearTC': function () {
        $("#email-send-title").val("");
        // $("#email-send-context").val("");
        // CKEDITOR.instances["email-send-context"].getData()
        CKEDITOR.instances["email-send-context"].setData("");
    },
    'click .btn-get-mail': function () {
        var formVar = $(".form-modal1").serializeObject();

        Meteor.call("getMailAddrs", formVar, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                // console.log(result);
                var ret = "";
                // $(".form-modal1").trigger('reset');

                var array2 = $("#modal2-mail3").val().split(",");
                array2 = array2.clean();
                if (!!array2.length) {
                    ret = "原有mail數：" + array2.length + "<BR>";
                }

                var array3 = result.concat(array2).unique();
                array3 = array3.clean();

                if (!!array3.length) {
                    ret = ret + "目前mail數：" + array3.length;
                }

                $("#modal2-mail3").val(array3.join(","));
                $("#email3-status").html(ret);
                $('#myModal').modal('hide');
            }
        });


        Meteor.call("getMailAddrs2", formVar, function (error, result) {
            if (error) {
                console.log(error);
            } else if (result) {
                alert(result);
            }
        });

        /* var str = "";
         if(!!$("#eproduct3").val()){
             str = str + 'Product 3: <a href="#" class="dynamic_text">' + $("#eproduct3 :selected").text() + '</a>';
         }
         if(!!$("#eproduct4").val()){
             str = str + '<BR>Product 4: <a href="#" class="dynamic_text">' + $("#eproduct4 :selected").text() + '</a>';
         }
         $("#sel-product").html(str);*/

    },
    'click .btn-get-portfolioid': function () {
        var formVar = $(".form-modal1").serializeObject();


        Meteor.call("getMailAddrsUid", formVar, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                // console.log(result);
                var ret = "";
                // $(".form-modal1").trigger('reset');

                /*var array2 = $("#modal2-mail4").val().split(",");
                array2 = array2.clean();
                if(!!array2.length){
                    ret = "原有mail數："+array2.length +"<BR>";
                }

                var array3 = result.concat(array2).unique();
                array3 = array3.clean();
                */
                var array3 = result.unique();
                array3 = array3.clean();

                if (!!array3.length) {
                    ret = ret + "目前投資單號：" + array3.length;
                }

                $("#modal2-mail4").val(array3.join(","));
                $("#email4-status").html(ret);
                $('#myModal').modal('hide');
            }
        });

        Meteor.call("getMailAddrs2", formVar, function (error, result) {
            if (error) {
                console.log(error);
            } else if (result) {
                alert(result);
            }
        });

        var str = "";
        // if(!!$("#eproduct3").val()){
        // str = str + 'Product 3: <a href="#" class="dynamic_text">' + $("#eproduct3 :selected").text() + '</a>';
        str = str + 'Product 3: <a href="#" class="dynamic_text">[product3]</a>';
        // }
        // if(!!$("#eproduct4").val()){
        // str = str + '<BR>Product 4: <a href="#" class="dynamic_text">' + $("#eproduct4 :selected").text() + '</a>';
        str = str + '<BR>Product 4: <a href="#" class="dynamic_text">[product4]</a>';
        // }
        $("#sel-product").html(str);

    },
    'click .dynamic_text': function (event) {
        var now_text = $(event.target).text();
        // console.log(now_text);
        // var old = $('#email-send-context').summernote('code');
        // $('#email-send-context').summernote('code', );

        var old = CKEDITOR.instances["email-send-context"].getData();
        CKEDITOR.instances["email-send-context"].setData(old + now_text);

    },
    'click .my-btn-sendmail': function () {
        event.preventDefault();
        event.stopPropagation();
        if (!$("#email-send-title").val()) {
            alert("請輸入主旨");
            return;
        } else if (!!$("#bcc_mail").val() && !$("#modal2-mail3").val() && !$("#modal2-mail2").val()) {
            alert("有密件副本時，請輸入收件信箱");
            return;
        }

        // if( !$("#email-send-context").val() ){
        if (!CKEDITOR.instances["email-send-context"].getData()) {
            alert("請輸入內文");
            return;
        }
        if (!confirm("確定要送出本郵件嗎?")) {
            return;
        }
        // console.log(formVar);

        var formVar = $("#form-email").serializeObject();
        if (!!formVar.templatetc) delete formVar.templatetc;
        if (!!formVar.send_datetime) {
            formVar.send_datetime_utc = new Date(formVar.send_datetime).toISOString();
            formVar.send_datetime = formVar.send_datetime;
        };
        // console.log(formVar);
        // return;
        //

        formVar.context = CKEDITOR.instances["email-send-context"].getData();

        Meteor.call("sendEmails", formVar, function (err, response) {
            // Meteor.call("sendMailHtml", formVar, function(err, response) {
            if (!!err) {
                return;
            }
            // console.log(response);

            // $("#form-email").trigger('reset');
            // $(".js-data-example-ajax2").val('').trigger('change');
            $("#aEmail1").jsGrid("loadData");
            $("#aEmail2").jsGrid("loadData");
            $("#cFiles").jsGrid("loadData");
        });

        /*  Meteor.call("sendGroupEmail", formVar, function(err, response) {
            if(!!err){
              return;
            }
            $('#modal2').closeModal();
            $(".form-modal2").trigger('reset');
            $("#aEmail1").jsGrid("loadData");
          });*/
    },
    "change select": function (event, template) {
        // console.log(event.currentTarget);
        // console.log(event.target);
        // console.log(template);
        if ($(event.target).hasClass("product")) {
            // $("#eproduct2").attr("disabled", true);
            // $("#eproduct3").attr("disabled", true);
            // $("#eproduct4").attr("disabled", true);

            var nowid = $(event.target).attr("id");
            if (nowid == "eproduct1") {
                $("#eproduct2").removeAttr("disabled");
                $("#eproduct3").attr("disabled", true);
                $("#eproduct4").attr("disabled", true);

                $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#eproduct2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 請選擇 --</option>');

                var fieldtype = $('#eproduct1 :selected').val();

                var type = Product2.find({
                    product1_id: fieldtype.toString()
                }, {
                    sort: {
                        product2_id: 1
                    }
                }).fetch();

                $.each(type, function (i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.name_cht
                    }));
                });
            } else if (nowid == "eproduct2") {
                $("#eproduct3").removeAttr("disabled");
                $("#eproduct4").attr("disabled", true);

                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var fieldtype = $('#eproduct2 :selected').val();

                var type = Product3.find({
                    product2_id: fieldtype.toString()
                }, {
                    sort: {
                        product3_id: 1
                    }
                }).fetch();

                if (!type.length) {
                    $("#eproduct3").attr("disabled", true);
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                } else {
                    $("#eproduct3").removeAttr("disabled");
                    var sel = $('#eproduct3')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht + " (" + item2.name_eng + ")"
                        }));
                    });
                }
            } else if (nowid == "eproduct3") {
                var fieldtype = $('#eproduct3 :selected').val();

                var type = Product4.find({
                    product3_id: fieldtype.toString()
                }).fetch();

                if (!type.length) {
                    $("#eproduct4").attr("disabled", true);
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 無 --</option>');
                } else {
                    $("#eproduct4").removeAttr("disabled");
                    var sel = $('#eproduct4')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" disabled selected>-- 請選擇 --</option>');

                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.name_cht + " (" + item2.name_eng + ")"
                        }));
                    });
                }
            }
            $("#portfolio").jsGrid("loadData");
        } else if ($(event.target).hasClass("emailtemplate")) {
            if ($(event.target).hasClass("title")) {
                var val = MailTitle.findOne({
                    _id: $(event.target).val()
                });
                if (!!val) {
                    $("#email-send-title").val(val.data);
                    $("#template-periodmail").val(val.periodmail);
                    // $("#email-send-context").val(val.context);
                    // $('#email-send-context').summernote('code', );
                    CKEDITOR.instances["email-send-context"].setData(val.context);
                } else {
                    $("#email-send-title").val("");
                    $("#template-periodmail").val("1");
                    // $("#email-send-context").val("");
                    // $('#email-send-context').summernote('code', "");
                    CKEDITOR.instances["email-send-context"].setData("");
                }
            }
            /*  else if($(event.target).hasClass("context")){
                  var val = MailContext.findOne({_id: $(event.target).val() });
                  if(!!val){
                      $("#email-send-context").val(val.data);
                  }
                  else{
                      $("#email-send-context").val("");
                  }
              }*/

        }
    },
});