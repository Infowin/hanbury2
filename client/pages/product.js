Template.product.rendered = function() {
    Template.semicolon.loadjs();
    var ocPortfolio = $("#oc-portfolio");

    var $container = $('#portfolio');

    $container.isotope({
        transitionDuration: '0.65s'
    });

    $('#portfolio-filter a').click(function() {
        $('#portfolio-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });

    $('#portfolio-shuffle').click(function() {
        $container.isotope('updateSortData').isotope({
            sortBy: 'random'
        });
    });

    $(window).resize(function() {
        $container.isotope('layout');
    });
    ocPortfolio.owlCarousel({
        margin: 20,
        nav: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        autoplay: false,
        autoplayHoverPause: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    var ocImages = $("#oc-images");

    ocImages.owlCarousel({
        margin: 30,
        nav: false,
        autoplayHoverPause: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });

    $("#p2").hide();
    $("#p3").hide();
    $("#p4").hide();

    $("#jProduct1").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                // filter.sent_status_id = "2";

                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "product1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "product1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "product1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "product1", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "value", title: "項目", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct1").jsGrid("loadData");
        },
        rowClick: function(args) {
            $("#p3").hide();
            $("#p4").hide();
            Session.set("nowrow_product1", args.item);
            $("#jProduct2").jsGrid("loadData");
            $("#p2").fadeIn("slow");
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortProduct1", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct1: ", error);
                        }
                        else {
                            $("#jProduct1").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct2").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_product1");
                if(!!item && !!item._id){
                    filter.product1_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "product2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "product2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "product2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "product2", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "name_cht", title: "項目(中)", type: "text" },
            { name: "name_eng", title: "項目(英)", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct2").jsGrid("loadData");
        },
        rowClick: function(args) {
            $("#p4").hide();
            Session.set("nowrow_product2", args.item);
            $("#jProduct3").jsGrid("loadData");
            $("#p3").fadeIn("slow");
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    console.log("Reordered items", items);
                    Meteor.call("updateSortProduct2", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct2: ", error);
                        }
                        else {
                            $("#jProduct2").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct3").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_product2");
                if(!!item && !!item._id){
                    filter.product2_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "product3", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "product3", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "product3", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "product3", "DELETE");
            },
        },
        fields: [
            { type: "control", width:50 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "name_cht", title: "項目(中)", type: "text" },
            { name: "name_eng", title: "項目(英)", type: "text" },
        ],
        onItemUpdated: function(args) {
            $("#jProduct3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct3").jsGrid("loadData");
        },
        rowClick: function(args) {
            Session.set("nowrow_product3", args.item);
            $("#jProduct4").jsGrid("loadData");
            $("#p4").fadeIn("slow");
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct3 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortProduct3", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct3: ", error);
                        }
                        else {
                            $("#jProduct3").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#jProduct4").jsGrid({
        // height: "400px",
        // height: "90%",
        width: "100%",
        sorting: true,
        paging: true,
        pageSize: 5,
        // filtering: true,
        editing: true,
        headercss: "table table-hover",
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_product3");
                if(!!item && !!item._id){
                    filter.product3_id = item._id;
                }
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "product4", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "product4", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "product4", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "product4", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "name_cht", title: "項目(中)", type: "text" },
            { name: "name_eng", title: "項目(英)", type: "text" },
            { name: "template_id", title: "顯示樣板", width: 70 , type: "select", items: objPortfolioTemplate, valueField: "id", textField: "value", width: 100 },
            { name: "provider_id", title: "供應商", type: "select", items: [{_id:"",name:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "name_cht"   },
        ],
        onItemUpdated: function(args) {
            console.log(args.item);
            Meteor.call("updateProduct4", args.item, function (error, result) {
                if (error) {
                    console.log("error from updateProduct4: ", error);
                }
                else {
                    $("#jProduct4").jsGrid("loadData");
                }
            });
            // $("#jProduct4").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            $("#jProduct4").jsGrid("loadData");
        },
        rowClick: function(args) {
            return;
        },
        onRefreshed: function() {
            var $gridData = $("#jProduct4 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortProduct4", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct4: ", error);
                        }
                        else {
                            $("#jProduct4").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });

};

Template.product.helpers({
    // homepics: objHomePics
});

Template.product.events({
    'click .btn-showlogin': function() {
        // Session.set('counter', Session.get('counter') + 1);
    },
    /*'submit .form-modal1': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        $("#jProduct1").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#jProduct1").jsGrid("loadData");
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },*/
    /*'submit .form-modal2': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal2").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;

        $("#jProduct2").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#jProduct2").jsGrid("loadData");
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'submit .form-modal3': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal3").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_product2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.name_cht;

        $("#jProduct3").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal3').modal('hide');
                $(".form-modal3").trigger('reset');
                $("#jProduct3").jsGrid("loadData");
            } else {
                $(".modal3-error").text(ret);
            }
        });
    },
    'submit .form-modal4': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal4").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_product2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.name_cht;
        var p3 = Session.get("nowrow_product3");
        formVar.product3_id = p3._id;
        formVar.product3_text = p3.name_cht;

        $("#jProduct4").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal4').modal('hide');
                $(".form-modal4").trigger('reset');
                $("#jProduct4").jsGrid("loadData");
            } else {
                $(".modal4-error").text(ret);
            }
        });
    },*/
    'click .btn-new1': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal1").serializeObject();
        $("#jProduct1").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#jProduct1").jsGrid("loadData");
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    'click .btn-new2': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal2").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;

        $("#jProduct2").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#jProduct2").jsGrid("loadData");
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
    'click .btn-new3': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal3").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_product2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.name_cht;

        $("#jProduct3").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal3').modal('hide');
                $(".form-modal3").trigger('reset');
                $("#jProduct3").jsGrid("loadData");
            } else {
                $(".modal3-error").text(ret);
            }
        });
    },
    'click .btn-new4': function () {
        event.preventDefault();
        event.stopPropagation();

        var formVar = $(".form-modal4").serializeObject();
        var p1 = Session.get("nowrow_product1");
        formVar.product1_id = p1._id;
        formVar.product1_text = p1.value;
        var p2 = Session.get("nowrow_product2");
        formVar.product2_id = p2._id;
        formVar.product2_text = p2.name_cht;
        var p3 = Session.get("nowrow_product3");
        formVar.product3_id = p3._id;
        formVar.product3_text = p3.name_cht;

        $("#jProduct4").jsGrid("insertItem", formVar).done(function(ret) {
            // console.log("insertion completed");
            // console.log(ret);
            if (ret.insert == "success") {
                $('#myModal4').modal('hide');
                $(".form-modal4").trigger('reset');
                $("#jProduct4").jsGrid("loadData");
            } else {
                $(".modal4-error").text(ret);
            }
        });
    },
});