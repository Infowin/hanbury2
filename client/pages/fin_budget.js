Template.fin_budget.rendered = function(){
    Template.semicolon.loadjs();

};
Template.fin_budget.helpers({

  acc_year: Accountyear.find({}, { sort: { value: 1 } }),
  get_bg: function () {
    var r = Router.current().params;
    return r.f1_id;
  },
  get_user: function () {
    var arr = Meteor.users.find(
      {
        $and: [
          { $or: [{ 'auth.is_auth': '1' }, { 'auth.is_auth': { $exists: false } }] },
          { 'profile.department_id': Router.current().params.f1_id }
        ]
      }).fetch();
    var resArr = [];
    for (var i = 0; i < arr.length; i++) {
      var obj = {};
      obj._id = arr[i]._id;
      obj.engname = arr[i].profile.engname;
      obj.chtname = arr[i].profile.chtname;
      resArr.push(obj);
    }

    return resArr;
  },
});

Template.fin_budget.events({

});
