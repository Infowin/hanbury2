Template.dayoff_management.rendered = function () {
    Template.semicolon.loadjs();
    var data = this.data;
    $("#title").text(data && data.value || '');
    // Session.set("bankacc_bg", data);

    $("#e_dayoff_management").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        filtering: true,
        // editing: false,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function (filter) {
                // filter.isnotice = "1";
                // filter.isrecord = "1";
                // filter.is_auth = "1"
                // var bg = Session.get("bankacc_bg");
                // filter.bg_id = bg._id;
                return jsgridAjax(filter, "dayoff", "GET");
            },
            insertItem: function (item) {
                return jsgridAjax(item, "dayoff", "POST");
            },
            updateItem: function (item) {
                return jsgridAjax(item, "dayoff", "PUT");
            },
            deleteItem: function (item) {
                return jsgridAjax(item, "dayoff", "DELETE");
            },
        },
        fields: [
            {
                type: "control", width: 60, deleteButton: true, editButton: false,
                itemTemplate: function (_, item) {
                    if (item.IsTotal)
                        return "";
                    return jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                }
            },
            // { type: "control", width:50, deleteButton: true, editButton: false },
            { name: "dayoff_status", title: "狀態", type: "select", items: [{ id: "", value: "所有狀態" }].concat(objDay_off_status), valueField: "id", textField: "value" },
            {
                name: "apply_id", title: "申請人姓名", type: "select", items: [{ id: "", value: "所有姓名" }], valueField: "id", textField: "value",
                itemTemplate: function (value, item) {
                    if (!!item && !!item.apply_id)
                        return Meteor.users.findOne({ _id: item.apply_id }).chtname;
                },
                filterTemplate: function () {
                    // console.log(item);
                    var sel = $('<select class="my-hr"></select>')
                        .find('option')
                        .remove()
                        .end();
                    // .append('<option value="" disabled selected>請選擇</option>')
                    ;
                    // var fieldtype = item.account1_id; //$(".account1_id-col select option:selected").val();
                    // var type = Meteor.users.find({$or: [
                    //       {'auth.is_auth':'1'},
                    //       {'auth.is_auth':
                    //         { $exists : false }
                    //       }
                    //     ]}).fetch();
                    var type = Meteor.users.find(
                        {
                            $and: [
                                { $or: [{ 'auth.is_auth': '1' }, { 'auth.is_auth': { $exists: false } }] },
                                { 'profile.department_id': Router.current().params.f1_id }
                            ]
                        }).fetch();
                    sel.append($('<option value="">所有人員</option>'));
                    // console.log(type);
                    $.each(type, function (i, item2) {
                        sel.append($('<option>', {
                            value: item2._id,
                            text: item2.chtname //+ " ($" + item2.price + ")",
                        }));
                    });

                    // if(!!value){
                    //     sel.val(value);
                    // }
                    return this._filterPicker = sel;
                },
                filterValue: function () {
                    // console.log(this._filterPicker);
                    return this._filterPicker.val();
                    // return {
                    //     apply_id: this._filterPicker.val()
                    // };
                },
            },
            { name: "day_off_class", title: "請假類別", type: "select", items: [{ id: "", value: "所有類別" }].concat(objDay_off), valueField: "id", textField: "value" },
            { name: "start_time", title: "開始時間", width: 160, align: "center" },
            { name: "end_time", title: "結束時間", width: 160, align: "center" },
            {
                name: "total_hours", title: "總時數", width: 80, align: "center",
                itemTemplate: function (value, item) {
                    if (!item.total_hours) {
                        return ""
                    }
                    var day = parseInt(item.total_hours / 8);
                    var hour = item.total_hours % 8;
                    return day + "天 " + hour + "時";
                },
            },
            { name: "reason", title: "事由", align: "center" },
            {
                name: "ps", title: "審核回覆", align: "center",
                itemTemplate: function (value, item) {
                    // console.log(item);
                    var str = "";
                    if (!!item && !!item.ps2) {
                        str = "代理人: " + item.ps2;
                    }
                    if (!!item && !!item.ps3) {
                        if (!!str) str += "<BR>";
                        str += "主管: " + item.ps3;
                    }
                    if (!!item && !!item.ps4) {
                        if (!!str) str += "<BR>";
                        str += "人資: " + item.ps4;
                    }
                    return str;
                },
            },
            {
                name: "substitute", title: "職務代理人", align: "center",
                itemTemplate: function (value, item) {
                    if (!!item && !!item.substitute)
                        return Meteor.users.findOne({ _id: item.substitute }).chtname;
                },
            },
            {
                name: "supervisor", title: "主管", align: "center",
                itemTemplate: function (value, item) {
                    if (!!item && !!item.supervisor)
                        return Meteor.users.findOne({ _id: item.supervisor }).chtname;
                },
            },
            {
                name: "hr", title: "人資", align: "center",
                itemTemplate: function (value, item) {
                    if (!!item && !!item.hr)
                        return Meteor.users.findOne({ _id: item.hr }).chtname;
                },
            },
        ],
        onDataLoading: function (args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function (args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function (args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_management").jsGrid("loadData");
        },
        onItemDeleted: function (args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_management").jsGrid("loadData");
        },
        onRefreshed: function (args) {
            var items = args.grid.option("data");
            var total = { chtname: "Total", "total_hours": 0, IsTotal: true };

            items.forEach(function (item) {
                total.total_hours += (Number(item.total_hours) || 0);
            });

            var $totalRow = $("<tr>").addClass("total-row");

            args.grid._renderCells($totalRow, total);

            args.grid._content.append($totalRow);
        },
    });

    $("#csv_dayoff_management").on('click', function (event) {
        var args = [$("#e_dayoff_management"), '' + data.value + '差勤總覽.csv'];
        exportTableToCSV.apply(this, args);
    });

};
Template.dayoff_management.helpers({
    acc_year: Accountyear.find({}, { sort: { value: 1 } }),
    get_objDay_off: function () {
        return objDay_off;
    },
    get_bjDay_off_status: function () {
        return objDay_off_status;
    },
});

Template.dayoff_management.events({
    'change .my-hr': function () {
        $("#e_dayoff_management").jsGrid("loadData");
    },
});
