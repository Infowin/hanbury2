Template.dayoff_set.rendered = function(){
    Template.semicolon.loadjs();
    var data = this.data;
    $("#title").text(data.value);
    Session.set("bankacc_bg", data);
    $("#e_dayoff_set_2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.is_auth = "1"
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "employeelist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "employeelist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "employeelist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "employeelist", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, deleteButton: false},
            { name: "worknumber", title: "工號", align: "center"},
            { name: "chtname", title: "員工姓名", align: "center"},
            { name: "auth_substitutego", title: "代理人權限", type: "select", items: objAuth3, valueField: "id", textField: "value"},
            { name: "auth_supervisorgo", title: "放行主管權限", type: "select", items: objAuth3, valueField: "id", textField: "value"},
            { name: "auth_hrgo", title: "放行人資權限", type: "select", items: objAuth3, valueField: "id", textField: "value"},
        ],
       /* onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },*/
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_set_2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_set_2").jsGrid("loadData");
        },
    });

    $("#e_dayoff_set_3").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                return jsgridAjax(filter, "dayoff_set", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "dayoff_set", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "dayoff_set", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "dayoff_set", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60},
            { name: "all_day_off_class", title: "假別", type: "select", items: objDay_off_set, valueField: "id", textField: "value"},
            { name: "equation", title: "每月工作天數", type:"text", align: "center"},
            { name: "deductions", title: "扣薪", type: "select", items: objDeductions, valueField: "id", textField: "value"},
            { name: "available_days", title: "可用天數", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_set_3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_set_3").jsGrid("loadData");
        },
    });

    $("#e_dayoff_set_4").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.is_auth = "1"
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;

                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "employeelist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "employeelist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "employeelist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "employeelist", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:30, deleteButton: false},
            { name: "worknumber", title: "工號", width:60, align: "center"},
            { name: "chtname", title: "員工姓名", width:60, align: "center"},
            { name: "onbroad_date", title: "就職日", width:80, align: "center"},
            { name: "jobyear", title: "年資", width:60, align: "center",
                itemTemplate: function(value, item) {
                    // return Compute_Jobyear(item);

                    if(!item.onbroad_date){
                        return "";
                    }
                    var JobDays = (new Date - new Date(item.onbroad_date))/1000/3600/24; //365.2422
                    // console.log(JobDays);
                    var year = parseInt( JobDays / 365 );
                    // console.log(year);
                    var day = parseInt(JobDays % 365 )
                    if(year >= 1){
                        return year.toString() + "年" + day + "天"; // 幾個365天 就幾年
                    }
                    // var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
                    if( day > 182){
                        return day+"天<br>(未滿一年)";
                    }
                    else{
                        return day+"天<br>(未滿半年)";
                    }
                    // if(year >= 1){
                    //     return year.toString() + "年"; // 幾個365天 就幾年
                    // }
                    // var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
                    // if( day > 182){
                    //     return day+"天<br>(未滿一年)";
                    // }
                    // else{
                    //     return day+"天<br>(未滿半年)";
                    // }
                },
            },
            // { name: "last_year_available", title: "上年度<br>有效日期", align: "center", width: 120,
            //     itemTemplate: function(value, item) {
            //         if (!item.onbroad_date) return ""
            //         var d = new Date(); //.getFullYear();
            //         var n = new Date(item.onbroad_date);
            //         // return new Date().getFullYear()+"/"+(n.getMonth()+1) +"/"+n.getDate() +" - "+ (new Date().getFullYear()+1)+"/"+(n.getMonth() + 1) +"/"+n.getDate();
            //         var start_date = (d.getFullYear() - 1)+"/"+ (n.getMonth()+1) +"/"+ n.getDate();
            //         var start_date_add1year_d = new Date( (d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate());
            //         var end_date_d = start_date_add1year_d.addDays(183);
            //         var end_date = (end_date_d.getFullYear() )+"/"+ (end_date_d.getMonth()+1) +"/"+ end_date_d.getDate();
            //         return start_date + " - " + end_date;
            //     },
            // },
            // { name: "last_year_dayoff", title: "上年度<br>天數", type: "text", align: "center",width: 40,
            //     itemTemplate: function(value, item) {
            //         // console.log(value);
            //         if(!value){
            //             return "";
            //         }
            //         var day = parseInt(value/8);
            //         var hour = value%8;

            //         return day + "天 " + hour + "時";
            //     },
            //     editTemplate: function(value, item) {
            //         // console.log(item);
            //         var day = parseInt(value/8);
            //         var hour = value%8;

            //         var sel = $('<input style="width: 50%"> 天 <select>\
            //             <option value="0">0 時</option>\
            //             <option value="1">1 時</option>\
            //             <option value="2">2 時</option>\
            //             <option value="3">3 時</option>\
            //             <option value="4">4 時</option>\
            //             <option value="5">5 時</option>\
            //             <option value="6">6 時</option>\
            //             <option value="7">7 時</option>\</select>');
            //             // .append('<option value="" disabled selected>請選擇</option>')
            //         ;

            //         if(!!value){
            //             $(sel[0]).val(day);
            //             $(sel[2]).val(hour);
            //         }
            //         return this._editPicker = sel;
            //     },
            //     editValue: function() {
            //         // return this._editPicker.val();
            //         return Number($(this._editPicker[0]).val())*8 + Number($(this._editPicker[2]).val());
            //     }
            // },
            // { name: "last_year_dayoff", title: "上年度<br>天數", align: "center",width: 40,
            //     itemTemplate: function(value, item) {
            //         if(!item.onbroad_date){
            //             return "";
            //         }
            //         var JobDays = (new Date().addDays(-365) - new Date(item.onbroad_date))/1000/3600/24; //365.2422
            //         var year = parseInt( JobDays / 365 );
            //         var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
            //             // return year.toString() + "年"; // 幾個365天 就幾年
            //         if(year < 1 ){
            //             if(day < 183){
            //                 return 0;
            //             }
            //             return 3;
            //         }
            //         else if(year >= 1 && year < 2){
            //             return 7;
            //         }
            //         else if(year >= 2 && year < 3){
            //             return 10;
            //         }
            //         else if(year >= 3 && year < 5){
            //             return 14;
            //         }
            //         else if(year >= 5 && year < 10){
            //             return 15;
            //         }
            //         else if(year >= 10){
            //             var dayoff = (year - 10) + 16;
            //             if(dayoff > 30){
            //                 dayoff = 30;
            //             }
            //             return dayoff;
            //         }
            //     }
            // },
            // { name: "day_off_class13", title: "本年度補休", align: "center", width: 60,
            //     itemTemplate: function(value, item) {

            //         var getDayoffSelYear = Number(new Date().getFullYear());
            //         var userData = Meteor.users.findOne({"_id": item._id});
            //         if(!!userData.arr_dayoff && userData.arr_dayoff.length > 0){ // users有arr_dayoff[]
            //             var arr = userData.arr_dayoff;
            //             var data = {};
            //             var isData = 0;

            //             for (let i = 0; i < arr.length; i++) {
            //                 if(arr[i].year == getDayoffSelYear){
            //                     isData = 1;
            //                     data = arr[i];
            //                     break;
            //                 }
            //             }
            //             if(isData == 1){
            //                 item.day_off_class12 = funcHourToDayText(data.dayoff12);
            //                 item.day_off_class13 = funcHourToDayText(data.dayoff13);
            //                 item.day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
            //             }
            //         } else {item.day_off_class13 = funcHourToDayText(0)}
            //         return "可用:" + item.day_off_class13 + '<br>'+ "已用:" + item.day_off_class12 + '<br>'+'<b>'+ "剩餘:" + item.day_off_class13_surplus +'</b>';
            //     }
            // },
            { name: "day_off_class13", title: "本年度補休", align: "center", width: 60,
                itemTemplate: function(value, item) {

                    var getDayoffSelYear = Number(new Date().getFullYear());
                    var userData = Meteor.users.findOne({"_id": item._id});
                    if(!!userData.arr_dayoff && userData.arr_dayoff.length > 0){ // users有arr_dayoff[]
                        var arr = userData.arr_dayoff;
                        var data = {};
                        var isData = 0;

                        for (let i = 0; i < arr.length; i++) {
                            if(arr[i].year == getDayoffSelYear){
                                isData = 1;
                                data = arr[i];
                                break;
                            } else { return '<b>'+ funcHourToDayText(0) +'</b>'; }
                        }
                        if(isData == 1){
                            item.day_off_class12 = funcHourToDayText(data.dayoff12);
                            item.day_off_class13 = funcHourToDayText(data.dayoff13);
                            item.day_off_class13_surplus = funcHourToDayText((Number(data.dayoff13) - (Number(data.dayoff12))));
                        }
                    } else { return '<b>'+ funcHourToDayText(0) +'</b>'; }
                    // } else { return "可用:" + funcHourToDayText(0) + '<br>'+ "已用:" + funcHourToDayText(0) + '<br>'+'<b>'+ "剩餘:" + funcHourToDayText(0) +'</b>'; }
                    // return "可用:" + item.day_off_class13 + '<br>'+ "已用:" + item.day_off_class12 + '<br>'+'<b>'+ item.day_off_class13_surplus +'</b>';
                    return '<b>'+ item.day_off_class13_surplus +'</b>';
                }
            },
            // { name: "this_year_available", title: "特休有效日期", align: "center", width: 100,
            //     itemTemplate: function(value, item) {
            //         if (!item.onbroad_date) return ""
            //         var d = new Date(); //.getFullYear();
            //         var n = new Date(item.onbroad_date);
            //         // return new Date().getFullYear()+"/"+(n.getMonth()+1) +"/"+n.getDate() +" - "+ (new Date().getFullYear()+1)+"/"+(n.getMonth() + 1) +"/"+n.getDate();
            //         var start_date = (d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate();
            //         var start_date_add1year_d = new Date( (d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate());
            //         var end_date_d = start_date_add1year_d.addDays(183);
            //         var end_date = (end_date_d.getFullYear() + 1)+"/"+ (end_date_d.getMonth()+1) +"/"+ end_date_d.getDate();
            //         return start_date + " - " + end_date;
            //     },
            // },
            // { name: "this_year_dayoff", title: "特休", align: "center", width: 60,
            //     itemTemplate: function(value, item) {
            //         // 可用item.special_dayoff_ava
            //         // 已用item.special_dayoff_used
            //         // 剩餘item.special_dayoff_surplus

            //         if(!item.onbroad_date){
            //             return "";
            //         }
            //         var JobDays = (new Date() - new Date(item.onbroad_date))/1000/3600/24; //365.2422
            //         var year = parseInt( JobDays / 365 );
            //         var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
            //             // return year.toString() + "年"; // 幾個365天 就幾年
            //         if(year < 1 ){
            //             if(day < 183){
            //                 item.special_dayoff_ava = 0;
            //             }
            //             item.special_dayoff_ava = 3;
            //         }
            //         else if(year >= 1 && year < 2){
            //             item.special_dayoff_ava = 7;
            //         }
            //         else if(year >= 2 && year < 3){
            //             item.special_dayoff_ava = 10;
            //         }
            //         else if(year >= 3 && year < 5){
            //             item.special_dayoff_ava = 14;
            //         }
            //         else if(year >= 5 && year < 10){
            //             item.special_dayoff_ava = 15;
            //         }
            //         else if(year >= 10){
            //             item.special_dayoff_ava =(year - 10) + 16;
            //             if(item.special_dayoff_ava > 30){
            //                 item.special_dayoff_ava = 30;
            //             }
            //         }

            //         var DC_10 = Dayoff.find({ $and: [{"dayoff_status": "5"}, {"day_off_class": "10"}, {"apply_id": item._id}]}).fetch();
            //         var d = new Date(); //.getFullYear();
            //         var n = new Date(item.onbroad_date);
            //         var start_date = new Date((d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate());
            //         var start_date_add1year_d = new Date( (d.getFullYear())+"/"+ (n.getMonth()+1) +"/"+ n.getDate());
            //         var end_date_d = start_date_add1year_d.addDays(183);
            //         var end_date = new Date((end_date_d.getFullYear() + 1)+"/"+ (end_date_d.getMonth()+1) +"/"+ end_date_d.getDate());
            //         var This_year_day_executed = 0;

            //         for (var i = 0; i < DC_10.length; i++) {
            //             var D = new Date(DC_10[i].start_time)
            //             var DC_10_day = 0;
            //             if (D >= start_date && D <= end_date ) {
            //                 DC_10_day = Number(DC_10[i].total_hours)
            //             }
            //             This_year_day_executed += DC_10_day;
            //         }
            //         item.special_dayoff_used = This_year_day_executed;

            //         item.special_dayoff_surplus = funcHourToDayText((Number(item.special_dayoff_ava * 8)) - (Number(item.special_dayoff_used)));//剩餘
            //         item.special_dayoff_ava = funcHourToDayText(item.special_dayoff_ava * 8);//可用
            //         item.special_dayoff_used = funcHourToDayText(This_year_day_executed);//已用
            //         return "可用:" + item.special_dayoff_ava + '<br>'+ "已用:" + item.special_dayoff_used + '<br>'+'<b>'+ "剩餘:" + item.special_dayoff_surplus +'</b>';
            //     }
            // },
        ],
        // rowRenderer:function(item, itemIndex){
        //     // console.log(item);
        //     console.log(itemIndex);
        // },
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_dayoff_set_4").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_dayoff_set_4").jsGrid("loadData");
        },
        // onRefreshed: function() {
        //     var $gridData = $("#e_dayoff_set_4 .jsgrid-grid-body tbody");
        //     $gridData.sortable({
        //         update: function(e, ui) {
        //             var items = $.map($gridData.find("tr"), function(row) {
        //                 return $(row).data("JSGridItem");
        //             });
        //             // console.log("Reordered items", items);
        //             Meteor.call("updateSortEmployee", items, function(error, result){
        //                 if(error){
        //                     console.log("error from Hrform: ", error);
        //                 }
        //                 else {
        //                     $("#e_dayoff_set_4").jsGrid("loadData");
        //                 }
        //             });
        //         }
        //     });
        // }
    });

    $("#csv_special_dayoff").on('click', function (event) {
        var args = [$("#e_dayoff_set_4"), ''+data.value+'特休天數計算.csv'];
        exportTableToCSV.apply(this, args);
    });
};
Template.dayoff_set.helpers({
    acc_year: Accountyear.find({}, {sort:{value:1}}),

    get_objDay_off: function() {
        return objDay_off;
    },

    get_bjDay_off_status: function() {
        return objDay_off_status;
    },
});

Template.dayoff_set.events({
    'click .btn-new_1': function() {
        var formVar = {};
        var bg = Session.get("bankacc_bg");
        formVar.bg_id = bg._id;
        formVar.bg_text = bg.value;

        $("#e_dayoff_set_3").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
            } else {
              }
        });
    },
});

