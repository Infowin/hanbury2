Template.menu_inhouse.rendered = function() {
};
Template.menu_inhouse.helpers({
  counter: function() {
    // return Session.get('counter');
  },
  userid: Meteor.userId(),
  department_id: function() {
    if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.department_id) {
      return Meteor.user().profile.department_id;
    } else {
      return "";
    }
  }
});

Template.menu_inhouse.events({
  'click button': function() {
    // Session.set('counter', Session.get('counter') + 1);
  },
  'click a': function(e) {
    let pathname = window.location.pathname;
    // Session.set('counter', Session.get('counter') + 1);
    if (pathname == e.currentTarget.pathname) {
      location.reload();
    }
  }
});
