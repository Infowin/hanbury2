var objHomePics = [{
    title: "外商投資歐洲 首選英國",
    meta: "ABP皇家阿爾伯特碼頭開發案在台記者會暨合作協議簽訂儀式",
    pic_ori: "1.jpg",
    pic_thumb: "1.jpg",
}, {
    title: "基金保險規劃",
    meta: "尊榮完整的客戶服務",
    pic_ori: "2.jpg",
    pic_thumb: "2.jpg",
}, {
    title: "不動產投資",
    meta: "致力於為客戶和商業合作夥伴提供獨具一格的金融產品及投資策劃",
    pic_ori: "3.jpg",
    pic_thumb: "3.jpg",
}, {
    title: "個人化金融服務",
    meta: "精心挑選業內優質產品，悉心為客戶打造獨具一格的投資組合",
    pic_ori: "4.jpg",
    pic_thumb: "4.jpg",
}, {
    title: "投資策劃",
    meta: "於香港和台北設有營運公司",
    pic_ori: "5.jpg",
    pic_thumb: "5.jpg",
}, {
    title: "外商投資",
    meta: "並於中國北京、上海、杭州等地不斷開拓業務網絡",
    pic_ori: "4.jpg",
    pic_thumb: "4.jpg",
}, ];


Template.home_announce.rendered = function(){
    // Template.home_slides.loadjs();
};

Template.home_slides.helpers({
  // homepics: objHomePics
  homepics: Uploads.find({homepic: "1"}, {sort:{order_id: 1}})
});

Template.home_slides.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  }

});

